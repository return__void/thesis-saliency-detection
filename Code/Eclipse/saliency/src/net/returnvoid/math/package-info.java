
/**
 * This package includes solutions for recurring mathematical tasks.
 * @author Diana Lange
 *
 */
package net.returnvoid.math;