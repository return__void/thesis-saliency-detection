package net.returnvoid.math;

import net.returnvoid.functions.DistanceFct;

/**
 * A class for the storage of spatial locations or two-dimensional vectors.
 * @author Diana Lange
 *
 */
public class Coord implements XY {
	
	/**
	 * The x coordinate.
	 */
	private float x;
	
	/**
	 * The y coordinate.
	 */
	private float y;
	
	/**
	 * A distance function for XY elements.
	 */
	private static DistanceFct<XY> euclideanDistanceFct = null; 
	
	/**
	 * A distance function for XY elements.
	 */
	private static DistanceFct<XY> manhattenDistanceFct = null;
	
	/**
	 * A distance function for XY elements using only the angle between the vectors.
	 */
	private static DistanceFct<XY> angleDistanceFct = null;

	/**
	 * Creates an instance of Coord from any kind of XY elements.
	 * @param xy Any element that has the x, y properties.
	 */
	public Coord(XY xy) {
		this.x = xy.fx();
		this.y = xy.fy();
	}
	
	/**
	 * Creates a new instance of Coord with the given coordinates.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 */
	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Creates a new instance of Coord with the given coordinates.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 */
	public Coord(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Adds the other Coord element to this instance (vector addition).
	 * @param other The other element for the addition.
	 */
	public void add(Coord other) {
		this.x += other.x;
		this.y += other.y;
	}
	
	/**
	 * Subtracts the other Coord element from this instance (vector subtraction).
	 * @param other The other element for the subtraction.
	 */
	public void sub(Coord other) {
		x -= other.x;
		y -= other.y;
	}
	
	/**
	 * Estimates the dot product between this vector and the input vector.
	 * @param other The other vector.
	 * @return The value of the dot product.
	 */
	public float dot(Coord other) {
		return x * other.x + y * other.y;
	}
	
	/**
	 * Multiplies the vector with the input scalar.
	 * @param scalar The scalar for the multiplication.
	 */
	public void mult(float scalar) {
		x *= scalar;
		y *= scalar;
	}
	
	/**
	 * Multiplies the vector with the input scalar.
	 * @param scalar The scalar for the multiplication.
	 */
	public void mult(double scalar) {
		x = (float) (x * scalar);
		y = (float) (y * scalar);
	}
	
	/**
	 * Creates a hard copy of this instance and returns a new instance with
	 * same values.
	 * @return The new instance of this Coord.
	 */
	public Coord copy() {
		return new Coord(x, y);
	}
	
	/**
	 * Calculates the magnitude of the vector / the length of the vector.
	 * @return The magnitude of this element.
	 */
	public float mag() {
		return (float) Math.sqrt(x * x + y * y);
	}
	
	/**
	 * Calculates the angle of this vector.
	 * @return The angle of the vector.
	 */
	public float angle() {
		return (float) Math.atan2(y, x);
	}
	
	/**
	 * Rotates the vector.
	 * @param angle The rotation value.
	 */
	public void rotate(float angle) {
		float a = angle() + angle;
		float m = mag();
		
		x = (float) (Math.cos(a) * m);
		y = (float) (Math.sin(a) * m);
	}
	
	/**
	 * Sets the magnitude of the vector to the input value.
	 * @param mag The new magnitude of the vector.
	 */
	public void setMag(float mag) {
		unit();
		mult(mag);
	}
	
	/**
	 * Limits the magnitude of the vector to the input value.
	 * @param limit The maximum magnitude vector.
	 */
	public void limit(float limit) {
		if (mag() > limit) {
			unit();
			mult(limit);
		}
	}
	
	/**
	 * Transforms the vector to a unit vector with a magnitude of 1.
	 */
	public void unit() {
		float a = angle();
		x = (float) (Math.cos(a));
		y = (float) (Math.sin(a));
	}
	
	/**
	 * Calculates the angle between two vectors.
	 * @param other The other vector.
	 * @return The value of the angle between the vectors.
	 */
	public float angle(Coord other) {
		float d = dot(other);
		float m1 = mag();
		float m2 = other.mag();
		float m = m1 * m2;
		
		if (m == 0) {
			
			if (m1 == 0 && m2 == 0) {
				return 0;
			} else {
			
				return (float) Math.PI;
			}
		} else {
			return (float) Math.acos(d / m);
		}
		
	}
	
	/**
	 * Sets the x coordinate.
	 * @param x The new x coordiante of the vector.
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Sets the y coordinate.
	 * @param y The new y coordiante of the vector.
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Estimates if two elements are neighbors. Two Coord elements are 
	 * neighbors if their spatial locations in discrete domain evaluated using x() 
	 * and y() function are next to each other. The neighborhood estimation is
	 * based on a 8-neighborhood grid.
	 * @param other The other location.
	 * @return True, if the location are next to each other. 
	 */
	public boolean neighbor(Coord other) {
		return Coord.neighbor((int) x, (int) y, (int) other.x, (int) other.y);
	}
	
	/**
	 * Estimates the distance between two elements using the Manhatten distance.
	 * @param other The other Coord.
	 * @return The distance between the two elements.
	 */
	public float distance(Coord other) {
		return distance(other, Coord.manhattenDistanceFct());
	}
	
	/**
	 * Estimates the distance between two elements using the provided distance 
	 * function.
	 * @param other The other Coord.
	 * @param fct A distance function for XY elements.
	 * @return The distance between the two elements.
	 */
	public float distance(Coord other, DistanceFct<XY> fct) {
		return fct.distance(this, other);
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#fx()
	 */
	public float fx() {
		return x;
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#fy()
	 */
	public float fy() {
		return y;
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#x()
	 */
	public int x() {
		return (int) x;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#y()
	 */
	public int y() {
		return (int) y;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return Coord.key((int) x, (int) y);
	}
	
	/**
	 * Creates a String including the values of the input.
	 * @param x The x value.
	 * @param y The y value.
	 * @return A String version of the input.
	 */
	public static String key(int x, int y) {
		return x + ", " + y;
	}
	
	/**
	 * Estimates if two elements are neighbors. Two XY elements are 
	 * neighbors if their spatial locations in discrete domain evaluated using x() 
	 * and y() function are next to each other. The neighborhood estimation is
	 * based on a 8-neighborhood grid.
	 * @param o The first element.
	 * @param p The second element.
	 * @return True, if the p and o are next to each other. 
	 */
	public static boolean neighbor(XY o, XY p) {
		return Coord.neighbor(o.x(), o.y(), p.x(), p.y());
	}
	
	/**
	 * Estimates if two elements are neighbors. Two elements are 
	 * neighbors if their spatial locations are next to each other. 
	 * The neighborhood estimation is based on a 8-neighborhood grid.
	 * @param o The first element with o[0] = x and o[1] = y.
	 * @param p The second element with p[0] = x and p[1] = y.
	 * @return True, if the p and o are next to each other. 
	 */
	public static boolean neighbor(int[] o, int[] p) {
		return Coord.neighbor(p[0], p[1], o[0], o[1]);
	}
	
	/**
	 * Estimates if two elements are neighbors. Two elements are 
	 * neighbors if their spatial locations are next to each other. 
	 * The neighborhood estimation is based on a 8-neighborhood grid.
	 * @param x1 The x coordinate of the first element.
	 * @param y1 The y coordinate of the first element.
	 * @param x2 The x coordinate of the second element.
	 * @param y2 The y coordinate of the second element.
	 * @return True, if the two elements are next to each other. 
	 */
	public static boolean neighbor(int x1, int y1, int x2, int y2) {
		if (x1 == x2 - 1 && y1 == y2 - 1) {
			return true;
		} else if (x1 == x2 && y1 == y2 - 1) {
			return true;
		} else if (x1 == x2 + 1 && y1 == y2 - 1) {
			return true;
		}  else if (x1 == x2 - 1 && y1 == y2) {
			return true;
		} else if (x1 == x2 + 1 && y1 == y2) {
			return true;
		} else if (x1 == x2 - 1 && y1 == y2 + 1) {
			return true;
		} else if (x1 == x2 && y1 == y2 + 1) {
			return true;
		} else if (x1 == x2 + 1 && y1 == y2 + 1) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns a distance function for XY elements. The distance is equal to 
	 * the angle between two vectors. 
	 * @return The distance function.
	 */
	public static DistanceFct<XY> angleDistanceFct() {
		if (Coord.angleDistanceFct == null) {
			Coord.angleDistanceFct = (a, b) -> {

				return (new Coord(a)).angle(new Coord(b));
			};
		}

		return Coord.angleDistanceFct;
	}
	
	/**
	 * Return a distance function for XY elements.
	 * @return The Euclidean distance for spatial locations. 
	 */
	public static DistanceFct<XY> euclideanDistanceFct() {
		if (Coord.euclideanDistanceFct == null) {
			Coord.euclideanDistanceFct = (a, b) -> {
				float deltaX = a.fx() - b.fx();
				float deltaY = a.fy() - b.fy();

				return (float) Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			};
		}

		return Coord.euclideanDistanceFct;
	}
	
	/**
	 * Return a distance function for XY elements.
	 * @return The Manhatten distance for spatial locations. 
	 */
	public static DistanceFct<XY> manhattenDistanceFct() {
		if (Coord.manhattenDistanceFct == null) {
			Coord.manhattenDistanceFct = (a, b) -> {
				float deltaX = a.fx() - b.fx();
				float deltaY = a.fy() - b.fy();

				if (deltaX < 0) {
					deltaX *= -1;
				}

				if (deltaY < 0) {
					deltaY *= -1;
				}


				return deltaX + deltaY;
			};
		}

		return Coord.manhattenDistanceFct;
	}
}
