package net.returnvoid.math;

/**
 * A basic interface for the storage of two-dimensional vectors. The vector
 * data may be spatial locations (x, y) but is not limited to that. The interface
 * is design for classes that store either floating point numbers or integer numbers. 
 * 
 * @author Diana Lange
 *
 */
public interface XY {
	
	/**
	 * The x coordinate as a floating point number.
	 * @return The x coordinate.
	 */
	public float fx();
	
	/**
	 * The y coordinate as a floating point number.
	 * @return The y coordinate.
	 */
	public float fy();
	
	/**
	 * The x coordinate.
	 * @return The int casted version of fx().
	 */
	default public int x() {
		return (int) fx();
	}
	
	/**
	 * The y coordinate.
	 * @return The int casted version of fy().
	 */
	default public int y() {
		return (int) fy();
	}
}
