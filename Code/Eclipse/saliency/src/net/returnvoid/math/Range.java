package net.returnvoid.math;

/**
 * A class representation of intervals. Elements of this class can evaluate
 * if an value is an element of the range or can limit values to the range or
 * can estimate the range of values.
 * 
 * @author Diana Lange
 *
 */
public class Range {

	/**
	 * Modes is needed to evaluate the is() function:
	 * 0 : min, max excluded
	 * 1 : min, max included
	 * 2 : min included, max excluded
	 * 3 : min excluded, max included
	 * 4 : left open, max excluded
	 * 5 : left open, max included
	 * 6 : min excluded, right open
	 * 7 : min included, right open
	 * 8 : left and right open
	 */
	protected int mode = 0;

	/**
	 * The minimum of the interval. Always true: min < max.
	 */
	protected float min;
	
	/**
	 * The maximum of the interval
	 */
	protected float max;

	/**
	 * Creates a new range instance where the boundaries of the interval is
	 * equal to the range of floating point numbers in Java. The range can be
	 * iteratively updated by using the record() function. The default mode is 1.
	 */
	public Range() {
		this(Float.MIN_VALUE, Float.MAX_VALUE, 1);
	}
	
	/**
	 * Creates a new range instance with mode 0 and the given min and max values.
	 * Make sure that min &lt; max.
	 * @param min The minimum of the interval.
	 * @param max The maximum of the interval.
	 */
	public Range(float min, float max) {
		this(min, max, 0);
	}
	
	/**
	 * Creates a new range instance with the given min and max values. Make sure 
	 * that min &lt; max. The mode value sets whether or not the min / max are 
	 * included in the range. The options for mode are:
	 * <br>
	 * 0 : min, max excluded <br>
	 * 1 : min, max included <br>
	 * 2 : min included, max excluded <br>
	 * 3 : min excluded, max included <br>
	 * 4 : left open, max excluded <br>
	 * 5 : left open, max included <br>
	 * 6 : min excluded, right open <br>
	 * 7 : min included, right open <br>
	 * 8 : left and right open <br>
	 * <br>
	 * The open options refer to min / max values of minus or plus infinite.
	 * @param min The minimum of the interval.
	 * @param max The maximum of the interval.
	 * @param mode The interval mode.
	 */
	public Range(float min, float max, int mode) {
		mode(mode);
		this.min = min;
		this.max = max;
	}

	/**
	 * The minimum of the range.
	 * @return The minimum of the range.
	 */
	public float min() {
		return min;
	}
	
	/**
	 * The maximum of the range.
	 * @return The maximum of the range.
	 */
	public float max() {
		return max;
	}
	
	/**
	 * Updates the boundaries of the range. Make sure that min &lt; max.
	 * @param min The new minimum of the range.
	 * @param max The new maximum of the range.
	 */
	public void update(float min, float max) {
		this.min = min;
		this.max = max;
	}


	/**
	 * Returns the mode of the range. The mode value sets whether or not the 
	 * min / max are included in the range. The values for mode are:
	 * <br>
	 * 0 : min, max excluded <br>
	 * 1 : min, max included <br>
	 * 2 : min included, max excluded <br>
	 * 3 : min excluded, max included <br>
	 * 4 : left open, max excluded <br>
	 * 5 : left open, max included <br>
	 * 6 : min excluded, right open <br>
	 * 7 : min included, right open <br>
	 * 8 : left and right open <br>
	 * <br>
	 * The open options refer to min / max values of minus or plus infinite.
	 * @return The current mode.
	 */
	public int mode() {
		return mode;
	}
	
	/**
	 * Sets the modee of the range. The mode value sets whether or not the 
	 * min / max are included in the range. The values for mode are:
	 * <br>
	 * 0 : min, max excluded <br>
	 * 1 : min, max included <br>
	 * 2 : min included, max excluded <br>
	 * 3 : min excluded, max included <br>
	 * 4 : left open, max excluded <br>
	 * 5 : left open, max included <br>
	 * 6 : min excluded, right open <br>
	 * 7 : min included, right open <br>
	 * 8 : left and right open <br>
	 * <br>
	 * The open options refer to min / max values of minus or plus infinite.
	 * @param mode The new mode of the range [0, 8].
	 */
	public void mode(int mode) {
		if (mode < 0) {
			mode = 0;
		} else if (mode > 8) {
			mode = 8;
		}
		
		this.mode = mode;
	}
	
	/**
	 * Checks if two ranges have the same boundaries.
	 * @param other The other range.
	 * @return True, when both min and max of both ranges are equal.
	 */
	public boolean equals(Range other) {
		return min() == other.min() && max() == other.max();
	}
	
	/**
	 * Creates a hard copy of the range and returns a new instance.
	 * @return The copied range.
	 */
	public Range copy() {
		Range copyRange = new Range();
		copyRange.min = min;
		copyRange.max = max;
		copyRange.mode = mode;
		
		return copyRange;
	}

	/**
	 * Returns a String which includes information about the mode value.
	 * @return The info text for the mode option.
	 */
	public String modeInfo() {

		String txt = "";
		txt += "0 : min, max excluded \n";
		txt += "1 : min, max included \n";
		txt += "2 : min included, max excluded \n";
		txt += "3 : min excluded, max included \n";
		txt += "4 : left open, max excluded \n";
		txt += "5 : left open, max included \n";
		txt += "6 : min excluded, right open \n";
		txt += "7 : min included, right open \n";
		txt += "8 : left and right open \n";
		return txt;
	}
	
	/**
	 * Updates the range if needed. The range is only updated when the input
	 * value is smaller than the current min value or bigger than the current
	 * max value.
	 * @param value A value for the range update.
	 */
	public void record(double value) {
		record((float) value);
	}
	
	/**
	 * Updates the range if needed. The range is only updated when the input
	 * value is smaller than the current min value or bigger than the current
	 * max value.
	 * @param value A value for the range update.
	 */
	public void record(float value) {
		if (min == Float.MIN_VALUE || value < min) {
			min = value;
		}
		
		if (max == Float.MAX_VALUE || value > max) {
			max = value;
		}
	}
	
	/**
	 * Limits the input value to the range and returns the limited value which
	 * will be an element of the range.
	 * @param value The value which should be limited.
	 * @return A value that is element of the range or the input value if the 
	 * input was already an element of the range.
	 */
	public double limit(double value) {
	
		switch(mode) {
		case 0:
		case 1:
		case 2:
		case 3:
			value = value < min ? min : value > max ? max : value;
			break;
		case 4:
		case 5:
			value = value > max ? max : value;
			break;
		case 6:
		case 7:
			value = value < min ? min : value;
			break;
		}
		
		return value;
	}
	
	
	/**
	 * Limits the input value to the range and returns the limited value which
	 * will be an element of the range.
	 * @param value The value which should be limited.
	 * @return A value that is element of the range or the input value if the 
	 * input was already an element of the range.
	 */
	public float limit(float value) {
	
		switch(mode) {
		case 0:
		case 1:
		case 2:
		case 3:
			value = value < min ? min : value > max ? max : value;
			break;
		case 4:
		case 5:
			value = value > max ? max : value;
			break;
		case 6:
		case 7:
			value = value < min ? min : value;
			break;
		}
		
		return value;
	}
	
	/**
	 * Removes the min and max values, i.e. sets the range to an unknown
	 * interval. Implementation: Sets the min and max values to the bounds
	 * of floating point numbers.
	 */
	public void reset() {
		min = Float.MIN_VALUE;
		max = Float.MAX_VALUE;
	}
	
	

	/**
	 * Tests whether or not the input value is an element of the range.
	 * @param val The test value.
	 * @return True if val is an element of the range.
	 */
	public boolean is(float val) {
		boolean answer = true;

		switch(mode) {
		case 0:
			answer = min < val && max > val;
			break;
		case 1: 
			answer = min <= val && max >= val;
			break;
		case 2:
			answer = min <= val && max > val;
			break;
		case 3:
			answer = min < val && max >= val;
			break;
		case 4:
			answer = max > val;
			break;
		case 5:
			answer = max >= val;
			break;
		case 6:
			answer = min < val;
			break;
		case 7:
			answer = min <= val;
			break;
		}

		return answer;
	}
}
