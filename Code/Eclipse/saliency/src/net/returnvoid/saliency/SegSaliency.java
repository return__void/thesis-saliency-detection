package net.returnvoid.saliency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.returnvoid.color.LabColor;
import net.returnvoid.functions.DistanceFct;
import net.returnvoid.functions.FeatureExtractorFct;
import net.returnvoid.image.GaborImg;
import net.returnvoid.image.IntensityImg;
import net.returnvoid.image.LabImg;
import net.returnvoid.image.RGBYColor;
import net.returnvoid.image.RGBYImg;
import net.returnvoid.imagefeatures.Feature;
import net.returnvoid.imagefeatures.FilterFactory;
import net.returnvoid.imagefeatures.Gradient;
import net.returnvoid.imagefeatures.Histogram;
import net.returnvoid.imagefeatures.Histogram1D;
import net.returnvoid.imagefeatures.Histogram2D;
import net.returnvoid.imagefeatures.SegmentationFeatures;
import net.returnvoid.math.Coord;
import net.returnvoid.math.Range;
import net.returnvoid.math.XY;
import net.returnvoid.segmentation.LabDistanceFactory;
import net.returnvoid.segmentation.LabXY;
import net.returnvoid.segmentation.SLIC;
import net.returnvoid.segmentation.Segment;
import net.returnvoid.segmentation.Superpixel;
import processing.core.PImage;

/**
 * A class for saliency estimation. The saliency estimation is based on image 
 * segmentation using SLIC algorithm. Various features are estimated which and
 * various 'rarity' estimation methods are implemented. The saliency maps can
 * be obtained by the getMap() method. All maps are named. See options() method
 * for all available map names.<br>
 * The implementation is based on: <a href="https://ieeexplore.ieee.org/abstract/document/6247743/" target="_blank">https://ieeexplore.ieee.org/abstract/document/6247743/</a>
 * 
 * @author Diana Lange
 *
 */
public class SegSaliency {

	/**
	 * Set to true to print the progress in the terminal.
	 */
	public static boolean debug = false;

	/**
	 * Storage for Segmentation.
	 */
	private SLIC slic = null;

	// Extractor functions - functions that extract features and map them to a segment

	/**
	 * Extractor for edge histograms (two one dimensional histograms).
	 */
	private static FeatureExtractorFct<Gradient, Segment, Histogram1D[]> edgeSimpleFeatureFct = Feature.simpleEdgeFeatureFct();
	
	/**
	 * Extractor for edge histograms (one two dimensional histogram).
	 */
	private static FeatureExtractorFct<Gradient, Segment, Histogram2D> edgeFeatureFct = Feature.edgeFeatureFct();
	
	
	/**
	 * Extractor for edge orientation histograms.
	 */
	private static FeatureExtractorFct<Gradient, Segment, Histogram1D> edgeOrientationFeatureFct = Feature.HOGFeatureFct();

	/**
	 * Extractor for edge magnitude histograms.
	 */
	private static FeatureExtractorFct<Gradient, Segment, Histogram1D> edgeMagFeatureFct = Feature.edgeMagnitudeFeatureFct();

	/**
	 * Extractor for color.
	 */
	private static FeatureExtractorFct<LabImg, Superpixel, LabColor> labFeatureFct = Feature.colorFeatureFct();

	/**
	 * Extractor for texture (one two dimensional histogram).
	 */
	private static FeatureExtractorFct<GaborImg, Segment, Histogram2D> textureFeatureFct = Feature.textureFeatureFct();
	
	/**
	 * Extractor for texture (one two dimensional histogram).
	 */
	private static FeatureExtractorFct<GaborImg, Segment, Histogram1D[]> textureSimpleFeatureFct = Feature.simpleTextureFeatureFct();


	/**
	 * Extractor for RGBY/Intensity feature.
	 */
	private static FeatureExtractorFct<RGBYImg, Segment, RGBYColor> rgbyColorFeatureFct = Feature.rgbyColorFeatureFct();

	// Distance measures for all features

	/**
	 * Distance measure for RGBY colors.
	 */
	private DistanceFct<RGBYColor> rgbyDis = RGBYColor.euclideanDistanceFct();

	/**
	 * Distance measure for histograms.
	 */
	private DistanceFct<Histogram> hisDis = Histogram.chiSquaredDistanceFct();

	/**
	 * Distance measure for Lab colors.
	 */
	private DistanceFct<LabColor> colDis = LabDistanceFactory.RGBPlusLabEuclideanDistanceFct();

	/**
	 * Distance measure for a set of histograms.
	 */
	private DistanceFct<Histogram[]> multiHisDis = Histogram.arrayDistanceFct("mult", hisDis);

	/**
	 * Storage for all extracted features.
	 */
	private SegmentationFeatures sf;
	
	/**
	 * A set of all active and available features.
	 */
	private HashMap<FeatureName, FeatureName> activeFeatures;

	/**
	 * Set of map names - combination of all active features with all active methods.
	 */
	private ArrayList<String> mapNames;

	/**
	 * The input image.
	 */
	private PImage input = null;

	/**
	 * An image representation of the segmentation result.
	 */
	private PImage segmentationImg = null;

	/**
	 * Storage for all created saliency maps
	 */
	private HashMap<String, IntensityImg> maps = new HashMap<String, IntensityImg>();

	/**
	 * Number of segments. 
	 */
	private int K;

	/**
	 * The gradient of the input image.
	 */
	private Gradient gradient;

	/**
	 * RGBY/Intensity representation of input image.
	 */
	private RGBYImg rgbyImg;
	
	/**
	 * Gabor responses of input image.
	 */
	private GaborImg gaborResponse;

	/**
	 * Creates a new instance of this saliency estimator with 4000 segments and
	 * input images will be scaled down to 200000 pixels (only if they have
	 * more pixels). Saliency estimation not start until setImage() is called.
	 * Will use CieLab colors, edge and texture information as featurs.
	 */
	public SegSaliency() {
		this(3000, new FeatureName[] {FeatureName.COLOR_LAB, FeatureName.EDGE, FeatureName.TEXTURE}, SumMethod.values());
	}

	/**
	 * Creates a new instance of this saliency estimator with K segments and
	 * input images will be scaled down to 200000 pixels (only if they have
	 * more pixels). Saliency estimation not start until setImage() is called.
	 * Will use CieLab colors, edge and texture information as featurs.
	 * @param K Number of segments (positive integer)
	 */
	public SegSaliency(int K) {
		this(K, new FeatureName[] {FeatureName.COLOR_LAB, FeatureName.EDGE, FeatureName.TEXTURE}, SumMethod.values());
	}
	
	/**
	 * Creates a new instance of this saliency estimator with K segments and
	 * input images will be scaled down to the given pixel size(only if they have
	 * more pixels). Saliency estimation not start until setImage() is called.
	 * @param K Number of segments (positive integer)
	 * @param features A set of features which should be used for saliency detection.
	 */
	public SegSaliency(int K, FeatureName[] features) {
		this(K, features, SumMethod.values());

	}

	/**
	 * Creates a new instance of this saliency estimator with K segments and
	 * input images will be scaled down to the given pixel size(only if they have
	 * more pixels). Saliency estimation not start until setImage() is called.
	 * @param K Number of segments (positive integer)
	 * @param features A set of features which should be used for saliency detection.
	 * @param sumMethods A set of methods for computing a final saliency map from intermediate maps. If null or empty,
	 * no such 'summed' saliency map will be computed.
	 */
	public SegSaliency(int K, FeatureName[] features, SumMethod[] sumMethods) {
		this.K = K;
		this.activeFeatures = new HashMap<FeatureName, FeatureName>();
		
		for (FeatureName f : features) {
			this.activeFeatures.put(f, f);
		}
		
		this.mapNames = SegSaliency.createMapNames(features, MethodName.values(), sumMethods);
	}
	
	/**
	 * Returns all active methods for this detector.
	 * @return The active methods.
	 */
	public MethodName[] activeMethods() {
		return MethodName.values();
	}
	
	/**
	 * Returns all active features for this detector.
	 * @return The active features (array entries are all elements of "FeatureName").
	 */
	public Object[] activeFeatures() {
		return activeFeatures.values().toArray();
	}

	/**
	 * Clears all maps.
	 */
	private void reset() {
		this.gradient = null;
		this.rgbyImg = null;
		this.gaborResponse = null;
		sf = null;
		maps.clear();
		segmentationImg = null;
	}

	/**
	 * Sets a new distance measure for histograms. Some estimated saliency maps
	 * might be re-calculated after calling this method.
	 * @param hisDis The new histogram distance measure.
	 */
	public void setHistogramDistance(DistanceFct<Histogram> hisDis) {
		this.hisDis = hisDis;
		this.multiHisDis = Histogram.arrayDistanceFct("mult", hisDis);

		// find maps that use histogram distance and remove them
		ArrayList<String> removers = new ArrayList<String>();
		for (Map.Entry<String, IntensityImg> entry : maps.entrySet()) {
			boolean n1 = entry.getKey().indexOf(FeatureName.EDGE.name()) > -1;
			boolean n2 = entry.getKey().indexOf(FeatureName.EDGE_ALT.name()) > -1;
			boolean n3 = entry.getKey().indexOf(FeatureName.EDGE_MAG.name()) > -1;
			boolean n4 = entry.getKey().indexOf(FeatureName.EDGE_ORIENTATION.name()) > -1;
			boolean n5 = entry.getKey().indexOf(FeatureName.TEXTURE.name()) > -1;
			boolean n6 = entry.getKey().indexOf(FeatureName.TEXTURE_ALT.name()) > -1;

			if (n1 || n2 || n3 || n4 || n5 || n6) {
				removers.add(entry.getKey());
			}
		}

		for (SumMethod m : SumMethod.values()) {
			removers.add(m.name());
		}
		
		for (String name : removers) {
			maps.remove(name);
		}

		sf.updateDistanceFct(FeatureName.EDGE.name(), this.multiHisDis);
		sf.updateDistanceFct(FeatureName.EDGE_ALT.name(), this.hisDis);
		sf.updateDistanceFct(FeatureName.EDGE_MAG.name(), this.hisDis);
		sf.updateDistanceFct(FeatureName.EDGE_ORIENTATION.name(), this.hisDis);
		sf.updateDistanceFct(FeatureName.TEXTURE.name(), this.multiHisDis);
		sf.updateDistanceFct(FeatureName.TEXTURE_ALT.name(), this.hisDis);
	}
	
	/**
	 * Sets a new distance measure for features of computed as multiple histograms. 
	 * Some estimated saliency maps might be re-calculated after calling this method.
	 * @param String The name of the combination method ("sum", "min", "max" or "mult").
	 */
	public void setHistogramDistance(String name) {
		if (!(name.equals("sum") || name.equals("min") || name.equals("max") || name.equals("mult"))) {
			name = "sum";
		}

		
		this.multiHisDis = Histogram.arrayDistanceFct(name, hisDis);

		// find maps that use histogram distance and remove them
		ArrayList<String> removers = new ArrayList<String>();
		for (Map.Entry<String, IntensityImg> entry : maps.entrySet()) {
			boolean n1 = entry.getKey().indexOf(FeatureName.EDGE.name()) > -1;
			boolean n2 = entry.getKey().indexOf(FeatureName.TEXTURE.name()) > -1;

			if (n1 || n2 ) {
				removers.add(entry.getKey());
			}
		}

		for (SumMethod m : SumMethod.values()) {
			removers.add(m.name());
		}
		
		for (String map : removers) {
			maps.remove(map);
		}

		sf.updateDistanceFct(FeatureName.EDGE.name(), this.multiHisDis);
		sf.updateDistanceFct(FeatureName.TEXTURE.name(), this.multiHisDis);
	}

	/**
	 * Sets a new distance measure for LabColor elements. Some estimated saliency 
	 * maps might be re-calculated after calling this method.
	 * @param colDis The new LabColor distance measure.
	 */
	public void setColorDistance(DistanceFct<LabColor> colDis) {
		this.colDis = colDis;

		// find maps that use LabColor distance and remove them
		ArrayList<String> removers = new ArrayList<String>();
		for (Map.Entry<String, IntensityImg> entry : maps.entrySet()) {
			if (entry.getKey().indexOf(FeatureName.COLOR_LAB.name()) > -1) {
				removers.add(entry.getKey());
			}
		}

		for (SumMethod m : SumMethod.values()) {
			removers.add(m.name());
		}

		for (String name : removers) {
			maps.remove(name);
		}

		sf.updateDistanceFct(FeatureName.COLOR_LAB.name(), colDis);
	}

	/**
	 * Sets a new distance measure for RGBYColor elements. Some estimated saliency 
	 * maps might be re-calculated after calling this method.
	 * @param rgbyDis The new RGBYColor distance measure.
	 */
	public void setRGBYDistance(DistanceFct<RGBYColor> rgbyDis) {
		this.rgbyDis = rgbyDis;

		// find maps that use RGBYColor distance and remove them
		ArrayList<String> removers = new ArrayList<String>();
		for (Map.Entry<String, IntensityImg> entry : maps.entrySet()) {
			if (entry.getKey().indexOf(FeatureName.COLOR_RGBY.name()) > -1) {
				removers.add(entry.getKey());
			}
		}

		for (SumMethod m : SumMethod.values()) {
			removers.add(m.name());
		}

		for (String name : removers) {
			maps.remove(name);
		}

		sf.updateDistanceFct(FeatureName.COLOR_RGBY.name(), rgbyDis);
	}

	/**
	 * Sets the input image for saliency detection. Segmentation and feature
	 * extraction will start after calling this method. The saliency maps will
	 * be estimated by calling the method getMap().
	 * @param img The input image for the saliency detection.
	 */
	public void setImage(PImage img) {
		this.reset();
		this.input = img;
		this.performSegmentation();
		this.performFeatureExtraction();
	}

	/**
	 * The gradient of the input image using LabImg from input. Will be null
	 * if setImage() has not been called yet.
	 * @return The gradient.
	 */
	public Gradient gradient() {
		if (this.gradient == null) {
			this.gradient = new Gradient(new IntensityImg(slic.labimg()));
		}
		return gradient;
	}
	
	/**
	 * The GaborImg representation of the gabor responses (used for texture feature).
	 * Will be null if setImage() has not been called yet.
	 * @return The gabor image.
	 */
	public GaborImg gabor() {
		if (this.gaborResponse == null) {
			this.gaborResponse = new GaborImg(new IntensityImg(slic.labimg()), new double[] {0, Math.PI / 4, Math.PI / 2, Math.PI / 4 + Math.PI / 2}, 17, 5, 0.5);
		}
		
		return this.gaborResponse;
	}

	/**
	 * The RGBYImg of the input image. Will be null if setImage() has not been called yet.
	 * @return The RGBYImg.
	 */
	public RGBYImg rgby() {
		if (this.rgbyImg == null) {
			this.rgbyImg = new RGBYImg(slic.labimg());
		}
		return rgbyImg;
	}

	/**
	 * Merge similar segments to reduce the number of segments. All created saliency 
	 * maps will be cleared and the features are re-estimated.
	 */
	public void merge() {

		if (SegSaliency.debug) {
			System.out.println("start merging with " + slic.K() + " segments");
		}
		float d = slic.meanNeighborSegmentDistance(slic.clusteringDistanceFct());
		slic = slic.mergeSegments(slic.clusteringDistanceFct(), d * 0.66f);
		if (SegSaliency.debug) {
			System.out.println("number of segments after merging: " + slic.K());

		}

		reset();
		performFeatureExtraction();
	}

	/**
	 * Creates all combinations of the featureNames with all methodNames. These
	 * resulting Strings will be the saliency map names. The names a designed as 
	 * follows "feaure: method".
	 * @param featureNames A set of feature names.
	 * @param methodNames  A set of method names.
	 * @return A set of all combination of featureNames and methodNames.
	 */
	public static ArrayList<String> createMapNames(FeatureName[] featureNames, MethodName[] methodNames, SumMethod[] sumNames) {
		ArrayList<String> mapNames = new ArrayList<String>(featureNames.length * methodNames.length);

		for (FeatureName feature : featureNames) {
			for (MethodName method : methodNames) {
				String mapName = mapName(feature.name(), method.name());
				mapNames.add(mapName);
			}
		}
		
		if (sumNames != null) {
			for (SumMethod name : sumNames) {
				mapNames.add(name.name());
			}
		}

		return mapNames;
	}
	
	
	/**
	 * Creates a map name for a featureName and methodName.
	 * @param featureName The feature name.
	 * @param methodName The method name.
	 * @return The map name (for keys in maps).
	 */
	private static String mapName(String featureName, String methodName) {
		return featureName + ": " + methodName;
	}

	/**
	 * Creates segmentation from currently set image.
	 */
	private void performSegmentation() {
		if (SegSaliency.debug) {
			System.out.println("Starting segmentation");
		}
		long start = System.currentTimeMillis();
		this.slic = new SLIC(input, K);
		long end = System.currentTimeMillis();
		if (SegSaliency.debug) {
			System.out.println("Segmentation execution time: " + (end - start) / 1000 + "s");
		}	
	}

	/**
	 * Extracts features and maps them to their segments. 
	 */
	private void performFeatureExtraction() {

		if (SegSaliency.debug) {
			System.out.println("Starting feature extraction");
		}

		LabImg labImg = slic.labimg();

		sf = new SegmentationFeatures();

		for (int i = 0; i < slic.superpixels().size(); i++) {
			if (activeFeatures.containsKey(FeatureName.COLOR_LAB)) {
				LabColor col = labFeatureFct.extract(labImg, slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.COLOR_LAB.name(), Feature.create(col, colDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.COLOR_RGBY)) {
				RGBYColor rgby = rgbyColorFeatureFct.extract(rgby(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.COLOR_RGBY.name(), Feature.create(rgby, rgbyDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.EDGE_ALT)) {
				Histogram2D edge = edgeFeatureFct.extract(gradient(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.EDGE_ALT.name(), Feature.create(edge, hisDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.EDGE)) {
				Histogram1D[] edge = edgeSimpleFeatureFct.extract(gradient(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.EDGE.name(), Feature.create(edge, multiHisDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.EDGE_ORIENTATION)) {
				Histogram1D edge = edgeOrientationFeatureFct.extract(gradient(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.EDGE_ORIENTATION.name(), Feature.create(edge, hisDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.EDGE_MAG)) {
				Histogram1D edge = edgeMagFeatureFct.extract(gradient(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.EDGE_MAG.name(), Feature.create(edge, hisDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.TEXTURE_ALT)) {
				Histogram2D tex = textureFeatureFct.extract(gabor(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.TEXTURE_ALT.name(), Feature.create(tex, hisDis));
			}
			
			if (activeFeatures.containsKey(FeatureName.TEXTURE)) {
				Histogram1D[] tex = textureSimpleFeatureFct.extract(gabor(), slic.superpixels().get(i));
				sf.add(slic.superpixels().get(i), FeatureName.TEXTURE.name(), Feature.create(tex, multiHisDis));
			}
			
			
		}

		if (SegSaliency.debug) {
			System.out.println("Done with feature extraction");
		}
	}
	
	/**
	 * Saliency optimization as proposed by "Saliency Filters: Contrast Based Filtering for Salient Region Detection"
	 * (objective: similar colors in the input image should be assigned similar saliency scores).
	 * This optimization operates on pixels not on segments and is therefore extremely
	 * slow.
	 * @param slic A segmentation of the input image.
	 * @param saliencyMap A raw saliency map.
	 * @return A new instance of the saliency map with applied optimization.
	 */
	public static IntensityImg optimize(SLIC slic, IntensityImg saliencyMap) {
		IntensityImg img = saliencyMap.copy();
		IntensityImg container = new IntensityImg(img.width(), img.height());
		ArrayList<Superpixel> superpixels = slic.superpixels();
		
		if (slic.labimg().width() != img.width() || slic.labimg().height() != img.height()) {
			img.resize(slic.labimg().width(), slic.labimg().height());
		}
		
		DistanceFct<XY> spatialDis = Coord.euclideanDistanceFct();
		DistanceFct<LabColor> colorDis = LabDistanceFactory.RGBEuclideanDistanceFct();
		
		float maxSpatialDistance = spatialDis.distance(new Coord(0, 0), new Coord(img.width(), img.height()));
		float maxColorDistance = (float) Math.sqrt(3 * 255 * 255);
		ArrayList<Double> weights = new ArrayList<Double>();
		double weightsSum = 0;
		
		for (int i = 0; i < superpixels.size(); i++) {
			
			if (SegSaliency.debug && i % 20 == 0) {
				System.out.println("Optimization in progress. " + i + " of " + superpixels.size() +  " done");
			}

			LabXY centeri = superpixels.get(i).center();
			LabColor ci = centeri.lab();
			
			for (int[] p : superpixels.get(i).points()) {
				Coord coord = new Coord(p[0], p[1]);
				
				weights.clear();
				weightsSum = 0;
				for (int j = 0; j < superpixels.size(); j++) {
					LabXY centerj = superpixels.get(j).center();
					LabColor cj = centerj.lab();
					
					float d1 = colorDis.distance(ci, cj) / maxColorDistance;
					float d2 = spatialDis.distance(coord, centerj) / maxSpatialDistance;
					
					float d =  d1 * d1 + d2 * d2;
					double w = FilterFactory.gaussFunction1D(d / 30, 1, 1);
					weights.add(w);
					weightsSum += w;
				}
				
				double newSalVal = 0;
				
				for (int j = 0; j < superpixels.size(); j++) {
					LabXY centerj = superpixels.get(j).center();
					double salVal = img.get(centerj.x(), centerj.y());
					
					newSalVal += salVal * weights.get(j) / weightsSum;
				}	
				container.set(p[0], p[1], newSalVal);
			}
		}
		
		container.toRange(new Range(0, 255));
		
		return container;
	}
	
	/**
	 * Optimizes a saliency map as proposed by "Saliency by image manipulation".
	 * Under the assumption that segments in a neighborhood probably are member
	 * of the same object and therefore should have a similar saliency score,
	 * the input saliency map is refined.
	 * @param slic A segmentation of the input image.
	 * @param input A raw saliency map.
	 * @param dropOff A score >= 1 which sets the size of the neighborhood used for optimization.
	 * @return A new instance of the saliency map with applied optimization.
	 */
	public static IntensityImg optimize(SLIC slic, IntensityImg input, float dropOff) {
		
		if (dropOff < 1) {
			dropOff = 1;
		}
		
		IntensityImg img = input.copy();
		
		if (slic.labimg().width() != img.width() || slic.labimg().height() != img.height()) {
			img.resize(slic.labimg().width(), slic.labimg().height());
		}		
		
		ArrayList<Superpixel> superpixels = slic.superpixels();

		// find HDP (high distinct pixel) = segments where the sal value is above mean sal
		ArrayList<Superpixel> HDP = new ArrayList<Superpixel>();
		float meanVal = 0;
		for (int i = 0; i < superpixels.size(); i++) {
			LabXY center = superpixels.get(i).center();
			meanVal += (float) input.get(center.x(), center.y());
		}
		meanVal /= superpixels.size();
		
		for (int i = 0; i < superpixels.size(); i++) {
			LabXY center = superpixels.get(i).center();
			float s = (float) input.get(center.x(), center.y());
			
			if (s >= meanVal) {
				HDP.add(superpixels.get(i));
			}
		}
		
		if (HDP.size() == 0) {
			return img;
		}

		return optimizeImpl(img, superpixels, dropOff, HDP);
	}
	
	/**
	 * Optimizes a saliency map as proposed by "Saliency by image manipulation".
	 * Under the assumption that segments in a neighborhood probably are member
	 * of the same object and therefore should have a similar saliency score,
	 * the input saliency map is refined.
	 * @param slic A segmentation of the input image.
	 * @param input A raw saliency map.
	 * @param dropOff A score >= 1 which sets the size of the neighborhood used for optimization.
	 * @param p In range of [0, 1]. Sets the percentage of pixels which are considered high saliency pixels.
	 * @return A new instance of the saliency map with applied optimization.
	 */
	public static IntensityImg optimize(SLIC slic, IntensityImg input, float dropOff, double p) {
		
		if (dropOff < 1) {
			dropOff = 1;
		}
		
		if (p < 0) {
			p = 0;
		} else if (p > 1) {
			p = 1;
		}
		
		IntensityImg img = input.copy();
		
		if (slic.labimg().width() != img.width() || slic.labimg().height() != img.height()) {
			img.resize(slic.labimg().width(), slic.labimg().height());
		}
		
		
		ArrayList<Superpixel> superpixels = new ArrayList<Superpixel>(slic.superpixels().size());
		superpixels.addAll(slic.superpixels());
		
		Collections.sort(superpixels, (a, b) -> {
			LabXY centerA = a.center();
			LabXY centerB = b.center();
			float salValA = (float) img.get(centerA.x(), centerA.y());
			float salValB = (float) img.get(centerB.x(), centerB.y());
			
			if (salValA < salValB) {
				return 1;
			} else if (salValA > salValB) {
				return -1;
			} else {
				return 0;
			}
		});
		
		

		// find HDP (high distinct pixel) = segments where the sal value is above mean sal
		ArrayList<Superpixel> HDP = new ArrayList<Superpixel>();
		for (int i = 0; i < superpixels.size() * p; i++) {

			HDP.add(superpixels.get(i));
		}
		
		if (HDP.size() == 0) {
			return img;
		}

		return optimizeImpl(img, superpixels, dropOff, HDP);
	}
	
	/**
	 * Implementation of the algorithm proposed by "Saliency by image manipulation". 
	 * A set of high saliency pixels / segments has to be estimated prior to this
	 * function, i.e. there can be various functions that can define this set differently
	 * that can call this function for the rest of the computation.
	 * @param container A container image (saliency scores are written to that image).
	 * @param superpixels A set of superpixels.
	 * @param dropOff A score >= 1 which sets the size of the neighborhood used for optimization.
	 * @param HDP A set of pixels with high saliency values (not empty, not null).
	 * @return
	 */
	private static IntensityImg optimizeImpl(IntensityImg container, ArrayList<Superpixel> superpixels, float dropOff, ArrayList<Superpixel> HDP) {
		// estimate distance to nearest HDP
		ArrayList<Double> mues = new ArrayList<Double>(superpixels.size());
		Range mueRange = new Range();
		float maxSpatialDistance = Coord.euclideanDistanceFct().distance(new Coord(0, 0), new Coord(container.width(), container.height()));

		for (int i = 0; i < superpixels.size(); i++) {

			float minDistance = Coord.euclideanDistanceFct().distance(superpixels.get(i).center(), HDP.get(0).center());

			for (int j = 1; j < HDP.size(); j++) {

				float d = Coord.euclideanDistanceFct().distance(superpixels.get(i).center(), HDP.get(j).center());

				if (d < minDistance) {
					minDistance = d;
				}
			}

			double mue = Math.log(minDistance + dropOff);			

			mues.add(mue);
			mueRange.record(mue);
		}

		float maxDim = container.width() >= container.height() ? container.width() : container.height();
		float ratio = maxDim / maxSpatialDistance;

		ArrayList<Double> deltas = new ArrayList<Double>(superpixels.size());
		Range deltaRange = new Range();

		for (int i = 0; i < superpixels.size(); i++) {
			double delta = ratio - mues.get(i) / mueRange.max();

			deltas.add(delta);
			deltaRange.record(delta);
		}

		for (int i = 0; i < superpixels.size(); i++) {
			LabXY center = superpixels.get(i).center();
			float salVal = (float) container.get(center.x(), center.y());
			double r = deltas.get(i) / deltaRange.max();

			salVal *= (float) r;
			writeToMap(superpixels.get(i), container, salVal);
		}

		container.toRange(new Range(0, 255));

		return container;
	}
	
	/**
	 * Combines the input maps to one map. The input will not be altered. Each
	 * input is weighted equally.
	 * @param maps A set of saliency maps.
	 * @return A max combination of the input maps nomalized to range [0, 255].
	 */
	public static IntensityImg max(IntensityImg...maps) {

		IntensityImg summed = new IntensityImg(maps[0].width(), maps[0].height());

		for (int i = 0; i < summed.size(); i++) {
			double sal = maps[0].get(i);
			for (int j = 1; j < maps.length; j++) {
				double val = maps[j].get(i);
				
				if (val > sal) {
					sal = val;
				}
			}
			
			summed.set(i, sal);
		}

		summed.toRange(new Range(0, 255));

		return summed;
	}

	/**
	 * Combines the input maps to one map. The input will not be altered. Each
	 * input is weighted equally.
	 * @param maps A set of saliency maps.
	 * @return A linear combination of the input maps nomalized to range [0, 255].
	 */
	public static IntensityImg sum(IntensityImg...maps) {

		IntensityImg summed = maps[0].copy();

		for (int i = 1; i < maps.length; i++) {
			summed.add(maps[i]);
		}

		summed.toRange(new Range(0, 255));

		return summed;
	}

	/**
	 * Gets an available saliency map name. 
	 * @param i The index of the map name (in range of options().size()).
	 * @return The name of an available saliency map.
	 */
	public String option(int i) {
		return mapNames.get(i);
	}

	/**
	 * Gets the names of all available saliency maps.
	 * @return The names of all available saliency maps.
	 */
	public ArrayList<String> options() {
		return mapNames;
	}

	/**
	 * Gets all currently created saliency maps. The saliency maps will be created
	 * by calling the getMap() method.
	 * @return A set of all created saliency maps.
	 */
	public HashMap<String, IntensityImg> saliencyMaps() {
		return maps;
	}

	/**
	 * The input image (same instance and not altered).
	 * @return The input image.
	 */
	public PImage input() {
		return this.input;
	}

	/**
	 * The LabImg variation of the input image which might have been scaled.
	 * @return The LabImg from the input.
	 */
	public LabImg labInput() {
		return slic.labimg();
	}

	/**
	 * Returns the segmentation which is the foundation for this saliency
	 * detection.
	 * @return The segmentation of the input image.
	 */
	public SLIC segmentation() {
		return slic;
	}

	/**
	 * An image representation of the segmentation result.
	 * @return The segmentation image.
	 */
	public PImage segmentationImg() {
		if (segmentationImg == null) {
			LabImg labImg = new LabImg(labInput().width(), labInput().height());
			SLIC.writeToImage(slic.superpixels(), labImg);
			segmentationImg = labImg.img();
			segmentationImg.parent = input.parent;
		}

		return segmentationImg;
	}
	
	/**
	 * Gets the saliency map with the given input name.
	 * @param featureName of the feature - an element of FEATURE_NAMES.
	 * @param methodName of the method - an element of METHOD_NAMES.
	 * @return The saliency map.
	 */
	public IntensityImg getMap(String featureName, String methodName) {

		return getMap(mapName(featureName, methodName));
	}

	/**
	 * Gets the saliency map with the given input name.
	 * @param name The name of the map - an element of options().
	 * @return The saliency map.
	 */
	public IntensityImg getMap(String name) {

		if (!maps.containsKey(name)) {

			if (SegSaliency.debug) {
				System.out.println("calculating saliency map for " + name + " method. Please wait");
			}
			IntensityImg container = computeSaliency(name);

			maps.put(name, container);
		}

		return maps.get(name);
	}
	
	/**
	 * Gets the mean of all active color maps for the input method name.
	 * @param methodName One element of activeMethods().
	 * @return The color saliency map for the input method.
	 */
	public IntensityImg getColorMap(String methodName) {
		IntensityImg map = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		
		for (FeatureName f : FeatureName.COLOR_FEATURES) {
			if (this.activeFeatures.containsKey(f)) {
				map.add(getMap(f.name(), methodName));
			}
		}
		
		map.toRange(new Range(0, 255));
		
		return map;
	}
	
	/**
	 * Gets the mean of all active edge maps for the input method name.
	 * @param methodName One element of activeMethods().
	 * @return The edge saliency map for the input method.
	 */
	public IntensityImg getEdgeMap(String methodName) {
		IntensityImg map = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		
		for (FeatureName f : FeatureName.EDGE_FEATURES) {
			if (this.activeFeatures.containsKey(f)) {
				map.add(getMap(f.name(), methodName));
			}
		}
		
		map.toRange(new Range(0, 255));
		
		return map;
	}
	
	/**
	 * Gets the mean of all active texture maps for the input method name.
	 * @param methodName One element of activeMethods().
	 * @return The texture saliency map for the input method.
	 */
	public IntensityImg getTextureMap(String methodName) {
		IntensityImg map = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		
		for (FeatureName f : FeatureName.TEXTURE_FEATURES) {
			if (this.activeFeatures.containsKey(f)) {
				map.add(getMap(f.name(), methodName));
			}
		}
		
		map.toRange(new Range(0, 255));
		
		return map;
	}

	/* -----------------------------------------------------
	 * Saliency functions
	 * -----------------------------------------------------
	 */

	/**
	 * Writes a saliency value to a container image to the area of the segment.
	 * @param pix A segments - sets which area in the container image will be painted.
	 * @param container An image - the saliency value will be put there.
	 * @param distance A saliency score for the input segment.
	 */
	private static void writeToMap(Superpixel pix, IntensityImg container, float distance) {
		for (int[] coord : pix.points()) {    
			container.set(coord[0], coord[1], distance);
		}
	}

	/**
	 * Maps a map name to a method implementation and creates / returns the 
	 * saliency map with the given name. 
	 * @param mapName The name of the saliency map - one element of options(). 
	 * @return The saliency map with the name.
	 */
	private IntensityImg computeSaliency(String mapName) {

		// the names consist of feature name and method name 
		String[] splittedMapName = mapName.split(":");
		IntensityImg container = null;

		if (splittedMapName.length == 2) {

			final String featureName = splittedMapName[0].trim();
			final String methodName = splittedMapName[1].trim();

			if (MethodName.GLOBAL_CONTRAST.is(methodName)) {
				container = globalContrast(featureName);
			} else if (MethodName.REGIONAL_CONTRAST.is(methodName)) {
				container = regionalContrast(featureName);
			} else if (MethodName.LOCAL_CONTRAST.is(methodName)) {
				container = localContrast(featureName);
			} else if (MethodName.GAUSS_WEIGHTED_CONTRAST.is(methodName)) {
				container = gaussWeightedContrast(featureName);
			} else if (MethodName.LINEAR_WEIGHTED_CONTRAST.is(methodName)) {
				container = linearWeightedContrast(featureName);
			} else if (MethodName.DISTRIBUTION_CONTRAST.is(methodName)) {
				container = distributionContrast(featureName);
			} else {
				container = distributionWeightedContrast(featureName);
			}
		} else {

			// if no feature name is in the map name then one of the sum
			// methods is created

			if (SumMethod.SUM_OF_DISTRIBUTION_WEIGHTED_CONTRAST.is(mapName)) {
				container = sumOfDistributionWeightedMaps();
			} else if (SumMethod.SUM_OF_LINEAR_WEIGHTED_CONTRAST.is(mapName)) { 
				container = sumOfLinearWeightedMaps();
			} else {
				container = sumOfGlobalMaps();
			}
		}

		return container;
	}

	/**
	 * Sums up all global distance maps.
	 * @return The saliency map.
	 */
	private IntensityImg sumOfGlobalMaps() {

		IntensityImg colorMap = getColorMap(MethodName.GLOBAL_CONTRAST.name());
		IntensityImg edgeMap = getEdgeMap(MethodName.GLOBAL_CONTRAST.name());
		IntensityImg textureMap = getTextureMap(MethodName.GLOBAL_CONTRAST.name());

		return SegSaliency.sum(colorMap, edgeMap, textureMap);

	}

	/**
	 * Sums up all distribution weighted maps.
	 * @return The saliency map.
	 */
	private IntensityImg sumOfDistributionWeightedMaps() {

		IntensityImg colorMap = getColorMap(MethodName.DISTRIBUTION_WEIGHTED_CONTRAST.name());
		IntensityImg edgeMap = getEdgeMap(MethodName.DISTRIBUTION_WEIGHTED_CONTRAST.name());
		IntensityImg textureMap = getTextureMap(MethodName.DISTRIBUTION_WEIGHTED_CONTRAST.name());
		
		return SegSaliency.sum(colorMap, edgeMap, textureMap);

	}

	/**
	 * Sums up all linear weighted maps.
	 * @return The saliency map.
	 */
	private IntensityImg sumOfLinearWeightedMaps() {

		IntensityImg colorMap = getColorMap(MethodName.LINEAR_WEIGHTED_CONTRAST.name());
		IntensityImg edgeMap = getEdgeMap(MethodName.LINEAR_WEIGHTED_CONTRAST.name());
		IntensityImg textureMap = getTextureMap(MethodName.LINEAR_WEIGHTED_CONTRAST.name());

		return SegSaliency.sum(colorMap, edgeMap, textureMap);

	}

	/**
	 * A method for saliency estimation. Gaussian weighted contrast weighted
	 * with distribution contrast.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg distributionWeightedContrast(String featureName) {

		IntensityImg global = getMap(mapName(featureName, MethodName.GAUSS_WEIGHTED_CONTRAST.name())).copy();
		IntensityImg distribution = getMap(mapName(featureName, MethodName.DISTRIBUTION_CONTRAST.name())).copy();

		global.toRange(new Range(0, 1));
		distribution.toRange(new Range(0, 1));

		distribution.mult(6);
		global.mult(distribution);
		global.toRange(new Range(0, 255));

		return global;

	}

	/**
	 * A method for saliency estimation. Sum of linear, local and global map.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg linearWeightedContrast(String featureName) {

		IntensityImg global = getMap(featureName, MethodName.GLOBAL_CONTRAST.name()).copy();
		IntensityImg regional = getMap(featureName, MethodName.REGIONAL_CONTRAST.name());
		IntensityImg local = getMap(featureName, MethodName.LOCAL_CONTRAST.name());
		global.add(regional);
		global.add(local);
		global.toRange(new Range(0, 255));

		return global;

	}

	/**
	 * A method for saliency estimation. Distribution: The distribution of 
	 * a feature in the segmentation.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg distributionContrast(String featureName) {

		ArrayList<double[]> distanceStorage = new ArrayList<double[]>(slic.superpixels().size());
		Range distanceRange = new Range();

		// calculate the weights, which are the distances between two segments
		for (int i = 0; i < slic.superpixels().size(); i++) {
			double[] distances = new double[slic.superpixels().size()];

			for (int j = 0; j < slic.superpixels().size(); j++) {

				distances[j] = sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName);
				distanceRange.record(distances[j]);
			}
			distanceStorage.add(distances);
		}

		double[] similaritySums = new double[slic.superpixels().size()];
		IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());

		for (int i = 0; i < slic.superpixels().size(); i++) {

			// transform weights - which are currently a distance based with
			// arbitrary range - to similarity measure range from [0, 1]
			// record the sum of all (similarity) weights so that in
			// the next step it will be possible that the sum of weights
			// is always 1
			double[] similarities = distanceStorage.get(i);
			float similaritySum = 0;
			for (int j = 0; j < slic.superpixels().size(); j++) {

				similarities[j] = FilterFactory.gaussFunction1D((similarities[j] - distanceRange.min()) / (distanceRange.max() - distanceRange.min()), 0.25, 1);
				//weight[j] = (weightRange.max() - weight[j]) / weightRange.max();
				similaritySum += similarities[j];
			}

			similaritySums[i] = similaritySum;

			// find weighted mean coordinate for the current lookup
			Coord mue = new Coord(0, 0);

			for (int j = 0; j < slic.superpixels().size(); j++) {
				
				// make the sum of all weights 1
				similarities[j] = similarities[j] / similaritySums[i];

				// update mean coordinate with the center coordinate of
				// this superpixel based on the detected feature similarity
				LabXY center = slic.superpixels().get(j).center();
				Coord loc = new Coord(center.fx(), center.fy());
				loc.mult(similarities[j]);
				mue.add(loc);
			}

			// finally calculate the distance for the current superpixel
			double distance = 0;

			for (int j = 0; j < slic.superpixels().size(); j++) {

				LabXY center = slic.superpixels().get(j).center();
				Coord locOther = new Coord(center.fx(), center.fy());
				locOther.sub(mue);

				float spatialDistanceMean = locOther.dot(locOther); // variance of similar features
				distance += spatialDistanceMean * spatialDistanceMean * similarities[j];
			}


			SegSaliency.writeToMap(slic.superpixels().get(i), saliencyMap, (float) distance);
		}

		saliencyMap.toRange(new Range(0, 255));
		// highly distributed elements have high distance values
		// to find compact regions the values must be inverted
		saliencyMap.invert(); 
		return saliencyMap;
	}

	/**
	 * A method for saliency estimation. Global contrast: Distance of a segment
	 * to all other segments.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg globalContrast(String featureName) {
		IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());

		for (int i = 0; i < slic.superpixels().size(); i++) {

			// sum the distance to all other segments weighted with their 
			// size (big distances to small segments are less important because
			// they are not that visible)
			float distance = 0;
			for (int j = 0; j < slic.superpixels().size(); j++) {

				if (i == j) {
					continue;
				}

				float weight = slic.superpixels().get(j).size();
				distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight;
			}

			SegSaliency.writeToMap(slic.superpixels().get(i), saliencyMap, distance);
		}

		saliencyMap.toRange(new Range(0, 255));
		return saliencyMap;
	}

	/**
	 * A method for saliency estimation. Gaussian Weighted Contrast: Distance
	 * of a segment to all other segments weighted by their spatial distance
	 * weighted with a gaussian.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg gaussWeightedContrast(String featureName) {
		IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());

		Coord middle = new Coord(slic.labimg().width(), slic.labimg().height());
		Coord corner = new Coord(0, 0);
		
		float maxSpatialDistance = Coord.euclideanDistanceFct().distance(middle, corner);

		// same as global contrast, but distances are weighted by the spatial
		// distances of the segments (aka far away segments are less important)
		for (int i = 0; i < slic.superpixels().size(); i++) {
			
			float distance = 0;
			for (int j = 0; j < slic.superpixels().size(); j++) {
				
				float sizeWeight = slic.superpixels().get(j).size();
				float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());		
				float spatialWeight = (float) FilterFactory.gaussFunction1D(spatialDistance/maxSpatialDistance, 0.25, 1);		
				
				distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * sizeWeight * spatialWeight;
			}
			
			

			SegSaliency.writeToMap(slic.superpixels().get(i), saliencyMap, distance);
		}

		saliencyMap.toRange(new Range(0, 255));
		return saliencyMap;
	}

	/**
	 * A method for saliency estimation. Local contrast: Distance of a segment 
	 * to its neighbor segments.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg localContrast(String featureName) {
		IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		Coord middle = new Coord(slic.labimg().width() / 2, slic.labimg().height() / 2);
		Coord corner = new Coord(0, 0);

		float maxSpatialDistance = Coord.euclideanDistanceFct().distance(middle, corner);
		
		// local contrast: distance estimation only between neighboring segments
		for (int i = 0; i < slic.superpixels().size(); i++) {


			float distance = 0;
			int localCount = 0;

			for (int j = 0; j < slic.superpixels().size(); j++) {

				if (i == j || !slic.superpixels().get(i).neighbor(slic.superpixels().get(j))) {
					continue;
				}
				
				float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());

				float weight = slic.superpixels().get(j).size() * (1 - spatialDistance / maxSpatialDistance);

				distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight;
				localCount++;
			}

			if (localCount > 0) {
				distance /= localCount;
			}


			SegSaliency.writeToMap(slic.superpixels().get(i), saliencyMap, distance);
		}

		saliencyMap.toRange(new Range(0, 255));
		return saliencyMap;
	}


	/**
	 * A method for saliency estimation. Regional contrast: Distance of a segment 
	 * to all segments in a fixed neighborhood.
	 * @param featureName The name of the feature for which the saliency score
	 * should be estimated. The featureName is an element of FEATURE_NAMES.
	 * @return The saliency map.
	 */
	private IntensityImg regionalContrast(String featureName) {
		IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		Coord middle = new Coord(slic.labimg().width() / 2, slic.labimg().height() / 2);
		Coord corner = new Coord(0, 0);

		float maxSpatialDistance = Coord.euclideanDistanceFct().distance(middle, corner);
		float maxRegionalDistance = maxSpatialDistance / 4;

		// regional contrast: distance to all segments that are spatially near 
		// weight distances by spatial distance
		for (int i = 0; i < slic.superpixels().size(); i++) {

			float distance = 0;

			float regionalSum = 0;

			for (int j = 0; j < slic.superpixels().size(); j++) {

				float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());

				if (i == j || !(spatialDistance < maxRegionalDistance)) {
					continue;
				}

				float spatialWeight = (float) FilterFactory.gaussFunction1D(spatialDistance / maxRegionalDistance, 0.25, 1);
				float sizeWeight = slic.superpixels().get(j).size();
				distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * spatialWeight * sizeWeight;
				regionalSum += spatialWeight;
			}

			if (regionalSum != 0) {
				distance  /= regionalSum;
			}

			SegSaliency.writeToMap(slic.superpixels().get(i), saliencyMap, distance);
		}

		saliencyMap.toRange(new Range(0, 255));
		return saliencyMap;
	}

	/**
	 * Names for all saliency score estimation methods.
	 * @author Diana Lange
	 */
	public static enum MethodName {
		
		/**
		 * Saliency detection using rarity in respect to the whole image.
		 */
		GLOBAL_CONTRAST("global-contrast"),
		/**
		 * Saliency detection using rarity in respect to a neighborhood.
		 */
		REGIONAL_CONTRAST("regional-contrast"),
		/**
		 * Saliency detection using rarity in respect to a small neighborhood.
		 */
		LOCAL_CONTRAST("local-contrast"),
		
		/**
		 * Saliency detection using rarity weighted Gaussian of spatial distance.
		 */
		GAUSS_WEIGHTED_CONTRAST("gauss-weighted-contrast"),
		
		/**
		 * Saliency detection using distribution of a feature.
		 */
		DISTRIBUTION_CONTRAST("distribution-contrast"),
		
		/**
		 * Saliency detection using linear combination of global, regional and local method.
		 */
		LINEAR_WEIGHTED_CONTRAST("linear-weighted-contrast"),
		
		/**
		 * Saliency detection using linear combination of Gaussian weighted and distribution method.
		 */
		DISTRIBUTION_WEIGHTED_CONTRAST("distribution-weighted-contrast");
		
		/**
		 * The name of the method.
		 */
		private final String name;
		
		/**
		 * Sets the name of the method.
		 * @param name The name of the method.
		 */
		MethodName(String name) {
			this.name = name;
		}
		
		/**
		 * Checks weather or not the input is equal to this method's name.
		 * @param input The String which will be checked for name match.
		 * @return True if input was equal to this MethodName's name.
		 */
		public boolean is(String input) {
			return input.equals(name());
		}
		
		/**
		 * Checks if input MethodName is equal to this one.
		 * @param other Any MethodName.
		 * @return True, if input is the same instance as this one.
		 */
		public boolean equals(MethodName other) {
			return this == other;
		}
		
		/**
		 * Returns the name of the method (differs from name()).
		 * @return The name of the method.
		 */
		public String method() {
			return name;
		}
	}
	
	/**
	 * Names for all extracted features.
	 * EDGE and TEXTURE will compute the feature using two one-dimensional histograms
	 * (one for orientation and one for response strength). EDGE_ALT and TEXTURE_ALT
	 * will compute the same features but using one two-dimensional histogram.
	 * @author Diana Lange
	 */
	public static enum FeatureName {
		/**
		 * Saliency detection using differences of CieLab colors.
		 */
		COLOR_LAB("Lab"), 
		/**
		 * Saliency detection using differences of RGBY colors.
		 */
		COLOR_RGBY("RGBY"), 
		/**
		 * Saliency detection using differences in edge information. Computation
		 * via two one-dimensional histograms.
		 */
		EDGE("edge"), 
		/**
		 * Saliency detection using differences in edge information. Computation
		 * via one two-dimensional histogram.
		 */
		EDGE_ALT("edge-alt"), 
		/**
		 * Saliency detection using differences in edge orientations.
		 */
		EDGE_ORIENTATION("edge-orientation"), 
		/**
		 * Saliency detection using differences in edge magnitudes.
		 */
		EDGE_MAG("edge-magnitude"), 
		/**
		 * Saliency detection using differences in texture information. Computation
		 * via two one-dimensional histograms.
		 */
		TEXTURE("texture"), 
		/**
		 * Saliency detection using differences in texture information. Computation
		 * via one two-dimensional histogram
		 */
		TEXTURE_ALT("texture-alt");
		
		/**
		 * A subset of FeatureNames that correspond to color differences.
		 */
		final public static FeatureName[] COLOR_FEATURES = {FeatureName.COLOR_LAB, FeatureName.COLOR_RGBY};
		/**
		 * A subset of FeatureNames that correspond to edge differences.
		 */
		final public static FeatureName[] EDGE_FEATURES = {FeatureName.EDGE, FeatureName.EDGE_ALT, FeatureName.EDGE_MAG, FeatureName.EDGE_ORIENTATION};
		/**
		 * A subset of FeatureNames that correspond to texture differences.
		 */
		final public static FeatureName[] TEXTURE_FEATURES = {FeatureName.TEXTURE, FeatureName.TEXTURE_ALT};
		
		/**
		 * The name of the feature.
		 */
		private final String name;
		
		/**
		 * Sets the name of the feature.
		 * @param name The name of the feature.
		 */
		private FeatureName(String name) {
			this.name = name;
		}
		
		/**
		 * Checks weather or not the input is equal to this feature's name.
		 * @param input The String which will be checked for name match.
		 * @return True if input was equal to this FeautureName's name.
		 */
		public boolean is(String input) {
			return input.equals(name());
		}
		
		/**
		 * Checks if input FeatureName is equal to this one.
		 * @param other Any FeatureName.
		 * @return True, if input is the same instance as this one.
		 */
		public boolean equals(FeatureName other) {
			return this == other;
		}
		
		/**
		 * Returns the name of the feature (differs from name()).
		 * @return The name of the feature.
		 */
		public String feature() {
			return name;
		}
		
	}
	
	/**
	 * Names for methods that combine intermediate saliency maps.
	 * @author Diana Lange
	 */
	public static enum SumMethod {
		/**
		 * Saliency detection using linear combination of color, texture and edge using
		 * global method for each feature. Only active features are used, of course.
		 */
		SUM_OF_GLOBAL_CONTRAST("Sum-of-" + MethodName.GLOBAL_CONTRAST.method()),
		/**
		 * Saliency detection using linear combination of color, texture and edge using
		 * linear weighted method (linear combination of global, regional and local method) 
		 * for each feature. Only active features are used, of course.
		 */
		SUM_OF_LINEAR_WEIGHTED_CONTRAST("Sum-of-" + MethodName.LINEAR_WEIGHTED_CONTRAST.method()),
		/**
		 * Saliency detection using linear combination of color, texture and edge using
		 * distribution weighted method for each feature. Only active features are used, of course.
		 */
		SUM_OF_DISTRIBUTION_WEIGHTED_CONTRAST("Sum-of-" + MethodName.DISTRIBUTION_WEIGHTED_CONTRAST.method());;
		
		/**
		 * The name of the method.
		 */
		private final String name;
		
		/**
		 * Sets the name of the method.
		 * @param name The name of the method.
		 */
		SumMethod(String name) {
			this.name = name;
		}
		
		/**
		 * Checks weather or not the input is equal to this method's name.
		 * @param input The String which will be checked for name match.
		 * @return True if input was equal to this MethodName's name.
		 */
		public boolean is(String input) {
			return input.equals(name());
		}
		
		/**
		 * Checks if input SumMethod is equal to this one.
		 * @param other Any SumMethod.
		 * @return True, if input is the same instance as this one.
		 */
		public boolean equals(SumMethod other) {
			return this == other;
		}
		
		/**
		 * Returns the name of the method (differs from name()).
		 * @return The name of the method.
		 */
		public String method() {
			return name;
		}
	}

}
