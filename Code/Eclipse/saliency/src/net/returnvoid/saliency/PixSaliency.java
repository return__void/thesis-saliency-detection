package net.returnvoid.saliency;

import java.util.ArrayList;

import net.returnvoid.image.IntensityImg;
import net.returnvoid.image.LabImg;
import net.returnvoid.image.RGBYImg;
import net.returnvoid.imagefeatures.Pyramid;
import net.returnvoid.math.Range;
import net.returnvoid.segmentation.SLIC;
import net.returnvoid.segmentation.Superpixel;
import processing.core.PImage;

/**
 * A class for saliency estimation. The saliency is based on three features: 
 * color, intensity and orientation. The saliency scores are calculated via
 * across scale differences in gaussian pyramids.
 * <br><br>
 * Implementation based on: <a href="https://ieeexplore.ieee.org/abstract/document/730558/" target="_blank">https://ieeexplore.ieee.org/abstract/document/730558/</a>
 * 
 * @author Diana Lange
 *
 */
public class PixSaliency {

	/**
	 * A new instance of the input image (maybe resized when original input was greater than maxPixels).
	 */
	private PImage img = null;

	/**
	 * The map of intensity feature.
	 */
	private IntensityImg intensityMap = null;
	private IntensityImg intensityMapSeg = null;

	/**
	 * The map of color feature.
	 */
	private IntensityImg colorMap = null;
	private IntensityImg colorMapSeg = null;

	/**
	 * The map of orientation feature.
	 */
	private IntensityImg orientationMap = null;
	private IntensityImg orientationMapSeg = null;

	/**
	 * The saliency map (combination of the 3 feature maps).
	 */
	private IntensityImg saliencyMap = null;
	private IntensityImg saliencyMapSeg = null;
	
	/**
	 * Segmentation of the input.
	 */
	private SLIC slic = null;

	/**
	 * Maximum number of pixels for finest scale in pyramids.
	 */
	public static int maxPixels = 500000;

	/**
	 * Set to true to print the progress in the terminal.
	 */
	public static boolean debug = true;

	/**
	 * Creates a new pixel based saliency detector. The estimation will not start
	 * until setImage() is called.
	 */
	public PixSaliency() {
	}

	/**
	 * Creates a new pixel based saliency detector using the input image.
	 * @param img The input image (will not be altered).
	 */
	public PixSaliency(PImage img) {
		this.setImage(img);
	}

	/**
	 * Gets all map names of this detector.
	 * @return The map names.
	 */
	public static String[] maps() {
		return new String[] {"color", "intensity", "orientation", "saliency"};
	}

	/**
	 * Gets a saliency map of this detector.
	 * @param i The index of the saliency map - i is in range of maps().
	 * @return The saliency map with the input index.
	 */
	public IntensityImg getMap(int i) {
		if (i == 0) {
			return colorMap;
		} else if (i == 1) {
			return intensityMap;
		} else if (i == 2) {
			return orientationMap;
		} else {
			return saliencyMap;
		} 
	}

	/**
	 * Gets a saliency map of this detector. This is equal to getMap(), but the
	 * saliency scores are written to superpixel segmented version of the input
	 * image as the mean saliency value of the segments. This results in 
	 * edge in a saliency map that adheres the image edges better than the raw
	 * saliency map.
	 * @param i The index of the saliency map - i is in range of maps().
	 * @return The saliency map with the input index.
	 */
	public IntensityImg getSegMap(int i) {
		return getSegMap(i, null);
	}
	
	/**
	 * Gets a saliency map of this detector. This is equal to getMap(), but the
	 * saliency scores are written to superpixel segmented version of the input
	 * image as the mean saliency value of the segments. This results in 
	 * edge in a saliency map that adheres the image edges better than the raw
	 * saliency map.
	 * @param i The index of the saliency map - i is in range of maps().
	 * @param slic A superpixel segmentation of the input. Can be null. If slic==null,
	 * the segmentation will be performed here.
	 * @return The saliency map with the input index.
	 */
	public IntensityImg getSegMap(int i, SLIC slic) {
		
		if (this.slic == null || this.slic != null && slic != null && this.slic != slic) {
			
			if (slic != null && this.slic != slic) {
				
				colorMapSeg = null;
				intensityMapSeg = null;
				orientationMapSeg = null;
				saliencyMapSeg = null;
			}
			
			this.slic = slic;
		}
		
		if (this.slic == null) {
			  this.slic = new SLIC(img, 4000);
		}

		if (i == 0) {
			if (colorMapSeg == null) {
				colorMapSeg = computeSegmentMap(colorMap);
			}
			return colorMapSeg;
		} else if (i == 1) {
			if (intensityMapSeg == null) {
				intensityMapSeg = computeSegmentMap(intensityMap);
			}
			return intensityMapSeg;
		} else if (i == 2) {
			if (orientationMapSeg == null) {
				orientationMapSeg = computeSegmentMap(orientationMap);
			}
			return orientationMapSeg;
		} else {
			if (saliencyMapSeg == null) {				
				saliencyMapSeg = SegSaliency.sum(getSegMap(0), getSegMap(1), getSegMap(2));
			}
			return saliencyMapSeg;
		}
	}

	/**
	 * Performs a SLIC segmentation on the input image.
	 * @param map The input image.
	 * @return The segmented version of the input.
	 */
	private IntensityImg computeSegmentMap(IntensityImg map) {
	
		IntensityImg copyMap = map.copy();
		copyMap.resize(slic.labimg().width(), slic.labimg().height());
		IntensityImg seg = new IntensityImg(slic.labimg().width(), slic.labimg().height());
		
		for (int i = 0; i < slic.superpixels().size(); i++) {
			Superpixel p = slic.superpixels().get(i);
			float mean = 0;
			for (int[] point : p.points()) {
				mean += copyMap.get(point[0], point[1]);
			}
			
			mean /= p.points().length;
			
			for (int[] point : p.points()) {
				seg.set(point[0], point[1], mean * mean);
			}
			
			
			
		}
		
		seg.toRange(new Range(0, 255));
		
		return seg;
	}

	/**
	 * Resizes the input image if the number of pixels is bigger than maxPixels.
	 * Returns a copy of the input image or the resized version of it.
	 * @param input The input image. This instance will not be altered.
	 * @return The new instance of the image.
	 */
	private PImage resizeImage(PImage input) {
		PImage copyImg = input.copy();

		if (copyImg.pixels.length > PixSaliency.maxPixels) {

			float aspectRatio = (float) copyImg.height / copyImg.width;
			float targetW = (float) Math.sqrt((float) PixSaliency.maxPixels / aspectRatio);

			copyImg.resize((int) Math.floor(targetW), 0);
		}

		return copyImg;
	}

	/**
	 * Creates feature maps from input pyramids. Feature maps are created from
	 * across scale pixel by pixel differences. 
	 * @param input The input pyramid.
	 * @param sub If true, the across scale difference is estimated via subtraction.
	 * Otherwise addition will be used.
	 * @return The extracted feature maps. All maps will have the same dimensions
	 * as the finest scale in the pyramid
	 */
	private ArrayList<IntensityImg> featureMaps(Pyramid<IntensityImg> input, boolean sub) {
		ArrayList<IntensityImg> featureMaps = new ArrayList<IntensityImg>();
		int w = input.get(0).width();
		int h = input.get(0).height();


		for (int i = 2; i <= 4; i++) {
			// create new feature map
			IntensityImg featureMap = new IntensityImg(w, h);
			IntensityImg fine = input.get(i);
			fine.resize(w, h);

			for (int j = 3; j <= 4; j++) {
				IntensityImg coarse = input.get(i + j);
				coarse.resize(w, h);

				for (int y = 0; y < h; y++) {
					for (int x = 0; x < w; x++) {

						// map coordinates to the finest scale
						// just needed when not resizing is off
						/*
						int x1 = input.mappedX(x, 0, i);
						int y1 = input.mappedY(y, 0, i);

						int x2 = input.mappedX(x, 0, i + j);
						int y2 = input.mappedY(y, 0, i + j);
						*/

						// get intensity values
						double valFine = fine.get(x, y);
						double valCoarse = coarse.get(x, y);

						// get saliency score
						double val = 0;

						if (sub) {
							val = valFine - valCoarse;
						} else {
							val = valFine + valCoarse;
						}

						if (val < 0) {
							val *= -1;
						}

						featureMap.set(x, y, val);
					}
				}
			}

			featureMaps.add(featureMap);
		}



		return featureMaps;
	}

	/**
	 * The input image (same instance and not altered).
	 * @return The input image.
	 */
	public PImage input() {
		return this.img;
	}

	/**
	 * Perform normalization by converting measuring the distance of the global
	 * maximum to the mean of the local maxima.
	 * @param img The input img.
	 * @return The normalized image.
	 */
	private IntensityImg normalize(IntensityImg img) {

		img.toRange(new Range(0, 255));
		float mean = (float) img.meanOfMax();
		img.mult((255 - mean) * (255 - mean));
		return img;
	}

	/**
	 * Sets the image and starts the process of saliency detection.
	 * @param img The input image for the saliency detection.
	 */
	public void setImage(PImage img) {

		this.slic = null;
		this.intensityMapSeg = null;
		this.colorMapSeg = null;
		this.orientationMapSeg = null;
		this.saliencyMapSeg = null;

		this.img = resizeImage(img);

		long start = System.currentTimeMillis();

		// Feature extraction

		RGBYImg rgby = new RGBYImg(this.img);

		IntensityImg inte = rgby.intensity();
		IntensityImg R = rgby.R();
		IntensityImg G = rgby.G();
		IntensityImg B = rgby.B();
		IntensityImg Y = rgby.Y();

		if (PixSaliency.debug) {
			System.out.println("start creating gaussian pyramid for intensity");
		}
		int levels = 9;

		Pyramid<IntensityImg> pyramidIntensity = Pyramid.createGauss(inte, levels, 3);

		if (PixSaliency.debug) {
			System.out.println("start creating gaussian pyramids for RGBY");
		}
		Pyramid<IntensityImg> pyramidR = Pyramid.createGauss(R, levels, 3);
		Pyramid<IntensityImg> pyramidG = Pyramid.createGauss(G, levels, 3);
		Pyramid<IntensityImg> pyramidB = Pyramid.createGauss(B, levels, 3);
		Pyramid<IntensityImg> pyramidY = Pyramid.createGauss(Y, levels, 3);

		if (PixSaliency.debug) {
			System.out.println("start creating gaussian pyramids for RG and BY");
		}
		Pyramid<IntensityImg> pyramidRG = Pyramid.sub(pyramidR, pyramidG);
		Pyramid<IntensityImg> pyramidBY = Pyramid.sub(pyramidB, pyramidY);

		if (PixSaliency.debug) {
			System.out.println("start creating gabor pyramids");
		}
		double[] angles = {0, Math.PI / 4, Math.PI / 2, Math.PI / 4 + Math.PI / 2};
		ArrayList<Pyramid<IntensityImg>> gaborRaw = Pyramid.splitGaborPyramid(Pyramid.createGabor(new LabImg(this.img.copy()), levels, 3, angles));
		Pyramid<IntensityImg> g0 = gaborRaw.get(0);
		Pyramid<IntensityImg> g45 = gaborRaw.get(1);
		Pyramid<IntensityImg> g90 = gaborRaw.get(2);
		Pyramid<IntensityImg> g135 = gaborRaw.get(3);

		// Feature Maps creation

		if (PixSaliency.debug) {
			System.out.println("start creating feature maps");
		}
		ArrayList<IntensityImg> featureMapsIntensity = featureMaps(pyramidIntensity, true);
		ArrayList<IntensityImg> featureMapsRG = featureMaps(pyramidRG, false);
		ArrayList<IntensityImg> featureMapsBY = featureMaps(pyramidBY, false);
		ArrayList<IntensityImg> featureMapsG0 = featureMaps(g0, true);
		ArrayList<IntensityImg> featureMapsG45 = featureMaps(g45, true);
		ArrayList<IntensityImg> featureMapsG90 = featureMaps(g90, true);
		ArrayList<IntensityImg> featureMapsG135 = featureMaps(g135, true);

		if (PixSaliency.debug) {
			System.out.println("start creating conspicuity maps");
		}

		// Feature Saliency Maps 

		intensityMap = new IntensityImg(this.img.width, this.img.height);
		colorMap = new IntensityImg(this.img.width, this.img.height);
		orientationMap = new IntensityImg(this.img.width, this.img.height);
		IntensityImg[] tempOrientationMap = {new IntensityImg(this.img.width, this.img.height), new IntensityImg(this.img.width, this.img.height), new IntensityImg(this.img.width, this.img.height), new IntensityImg(this.img.width, this.img.height)};

		for (int i = 0; i < featureMapsIntensity.size(); i++) {
			IntensityImg iMap = featureMapsIntensity.get(i);
			IntensityImg rgMap = featureMapsRG.get(i);
			IntensityImg byMap = featureMapsBY.get(i);
			IntensityImg g0Map = featureMapsG0.get(i);
			IntensityImg g45Map = featureMapsG45.get(i);
			IntensityImg g90Map = featureMapsG90.get(i);
			IntensityImg g135Map = featureMapsG135.get(i);

			intensityMap.add(normalize(iMap));
			colorMap.add(normalize(rgMap));
			colorMap.add(normalize(byMap));
			tempOrientationMap[0].add(normalize(g0Map));
			tempOrientationMap[1].add(normalize(g45Map));
			tempOrientationMap[2].add(normalize(g90Map));
			tempOrientationMap[3].add(normalize(g135Map));
		}

		for (int i = 0; i < tempOrientationMap.length; i++) {
			orientationMap.add(normalize(tempOrientationMap[i]));
		}

		if (PixSaliency.debug) {
			System.out.println("start creating saliency maps");
		}

		// to RGB range
		intensityMap.toRange(new Range(0, 255));
		colorMap.toRange(new Range(0, 255));
		orientationMap.toRange(new Range(0, 255));


		// create final saliency map
		saliencyMap = SegSaliency.sum(intensityMap, orientationMap, colorMap);

		long end = System.currentTimeMillis();

		if (PixSaliency.debug) {
			System.out.println("execution time: " + (end - start) / 1000 + "s");
		}
	}
}
