
/**
 * This package includes two saliency detection algorithms, a pixel based and a segmentation based one.
 * <b>PixSaliency</b> : Estimates saliency scores pixel by pixel in gaussian and gabor
 * pyramids. The implementation is based on: <a href="https://ieeexplore.ieee.org/abstract/document/730558/" target="_blank">https://ieeexplore.ieee.org/abstract/document/730558/</a><br>
 * <b>SegSaliency</b> : Estimates saliency scores based on (superpixel) segments.
 * The implementation is based on: <a href="https://ieeexplore.ieee.org/abstract/document/6247743/" target="_blank">https://ieeexplore.ieee.org/abstract/document/6247743/</a>
 * @author Diana Lange
 *
 */
package net.returnvoid.saliency;