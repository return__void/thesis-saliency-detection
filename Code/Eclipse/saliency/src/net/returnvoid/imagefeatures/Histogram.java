package net.returnvoid.imagefeatures;

import net.returnvoid.functions.DistanceFct;

/**
 * A parent class for creating Histograms. Includes the definitions for various
 * distance measures for all histograms.
 * 
 * @author Diana Lange
 *
 */
public abstract class Histogram {
	/**
	 * The minimum of the range of the histogram. Always true: min < max.
	 */
	protected float min;
	
	/**
	 * The maximum of the range of the histogram.
	 */
	protected float max;
	
	/**
	 * The number of elements that have been added to the Historgram (equal to
	 * the sum of 'binned'.
	 */
	protected int elementsAdded = 0;
	
	/**
	 * The index of the bin with the most elements added. 
	 */
	protected Integer maxBin = null;
	
	/**
	 * A distance function for Histograms.
	 */
	private static DistanceFct<Histogram> chiSquaredDistanceFct = null;

	/**
	 * A distance function for Histograms.
	 */
	private static DistanceFct<Histogram> euclideanDistanceFct = null;

	/**
	 * A distance function for Histograms.
	 */
	private static DistanceFct<Histogram> manhattenDistanceFct = null;

	/**
	 * A distance function for Histograms.
	 */
	private static DistanceFct<Histogram> maxBinDistanceFct = null;
	
	/**
	 * Creates a new histogram instance with the given min and max values. Make sure 
	 * that min &lt; max. 
	 * @param min The minimum of the histogram.
	 * @param max The maximum of the histogram.
	 */
	protected Histogram(float min, float max) {
		this.min = min;
		this.max = max;
	}
	
	/**
	 * The minimum of the range.
	 * @return The minimum of the range.
	 */
	public float min() {
		return min;
	}
	
	/**
	 * The maximum of the range.
	 * @return The maximum of the range.
	 */
	public float max() {
		return max;
	}
	
	/**
	 * Returns the number of values that have been added to the Histogram.
	 * @return The number of values that have been added to the Histogram.
	 */
	public int size() {
		return elementsAdded;
	}
	
	/**
	 * Sets the value of a bin.
	 * @param index The index of the bin. Should be in range of [0, bins()).
	 * @param count The new value of the bin with index i.
	 */
	public abstract void set(int index, int count);
	
	/**
	 * Gets the number of elements added to the histogram that where in range of
	 * the bin with the input index.
	 * @param index The index of the bin.
	 * @return The number of added elements within the range of the bin i.
	 */
	public abstract int get(int index);
	
	/**
	 * Gets the number of elements added to the histogram that where in range of
	 * the bin i normalized by the size() of this Histogram.
	 * @param i The index of the bin.
	 * @return The normalized number of added elements within the range of the bin i.
	 */
	public float normGet(int i) {

		if (i < 0) {
			i = 0;
		} else if (i >= bins()) {
			i = bins() - 1;
		}
		
		return get(i) / (float) get(maxBin());
	}
	
	/**
	 * Returns the number of bins of this Histogram.
	 * @return The number of bins.
	 */
	public abstract int bins();
	
	/**
	 * Returns the index of the bin with the most elements.
	 * @return The index of the bin with the most elements
	 */
	public int maxBin() {
		if (maxBin == null) {
			float maxVal = get(0);
			maxBin = 0;

			for (int i = 1; i < bins(); i++) {
				float val = get(i);
				if (maxVal < val) {
					maxVal = val;
					maxBin = i;
				}
			}
		}

		return maxBin;
	}
	
	/**
	 * Resets mean, variance and maxBin. Should be called whenever the content
	 * of at least one bin has been changed.
	 */
	protected void resetTmpValues() {
		maxBin = null;
	}
	
	/**
	 * Estimates the distance between two Histograms using the input distance
	 * function. The distance can only be estimated when the two histograms have 
	 * same number of bins. The range of the two histograms may vary. 
	 * @param other The other histogram.
	 * @param fct A distance function for histograms.
	 * @return The distance between the histograms or -1 if the two histograms
	 * have different number of bins.
	 */
	public float distance(Histogram other, DistanceFct<Histogram> fct) {

		if (!comparable(other)) {
			return -1;
		}

		return fct.distance(this, other);
	}

	/**
	 * Estimates the distance between two Histograms using Euclidean distance. 
	 * The distance can only be estimated when the two histograms have 
	 * same number of bins. The range of the two histograms may vary. 
	 * @param other The other histogram.
	 * @return The distance between the histograms or -1 if the two histograms
	 * have different number of bins.
	 */
	public float distance(Histogram other) {

		return distance(other, Histogram.euclideanDistanceFct());
	}

	/**
	 * Checks if the distance between the histograms can be calculated. Two
	 * histograms are comparable when they have the same number of bins. They
	 * ranges may vary.
	 * @param other The other histogram.
	 * @return True if this histogram is comparable with the other one.
	 */
	public boolean comparable(Histogram other) {
		return bins() == other.bins();
	}
	
	/**
	 * Returns the Manhatten distance function for Histograms. The histograms are
	 * assumed to have the same number of bins but can have differing sizes (size =
	 * number of elements added to the Histogram). The distance is measured by 
	 * the normalized bin values.
	 * @return The distance function.
	 */
	public static DistanceFct<Histogram> manhattenDistanceFct() {
		if (Histogram.manhattenDistanceFct == null) {
			Histogram.manhattenDistanceFct = new DistanceFct<Histogram>() {
				public float distance(Histogram a, Histogram b) {
					float sum = 0;

					for (int i = 0; i < a.bins(); i++) {
						float binDiff = a.normGet(i) - b.normGet(i);

						if (binDiff < -1) {
							binDiff *= -1;
						}

						sum += binDiff;
					}

					return sum / a.bins();
				}
			};
		}
		return Histogram.manhattenDistanceFct;
	}

	/**
	 * Returns the Euclidean distance function for Histograms. The histograms are
	 * assumed to have the same number of bins but can have differing sizes (size =
	 * number of elements added to the Histogram). The distance is measured by 
	 * the normalized bin values.
	 * @return The distance function.
	 */
	public static DistanceFct<Histogram> euclideanDistanceFct() {
		if (Histogram.euclideanDistanceFct == null) {
			Histogram.euclideanDistanceFct = new DistanceFct<Histogram>() {
				public float distance(Histogram a, Histogram b) {
					float sum = 0;

					for (int i = 0; i < a.bins(); i++) {
						float binDiff = a.normGet(i) - b.normGet(i);
						sum += (binDiff * binDiff);
					}

					return (float) Math.sqrt(sum);
				}
			};
		}
		return Histogram.euclideanDistanceFct;
	}

	/**
	 * Returns the (simple) ChiSquared distance function for Histograms. The histograms are
	 * assumed to have the same number of bins but can have differing sizes (size =
	 * number of elements added to the Histogram). The distance is measured by 
	 * the normalized bin values.
	 * @return The distance function. distance = (a - b) / (a + b).
	 */
	public static DistanceFct<Histogram> chiSquaredDistanceFct() {
		if (Histogram.chiSquaredDistanceFct == null) {
			Histogram.chiSquaredDistanceFct = new DistanceFct<Histogram>() {
				public float distance(Histogram a, Histogram b) {
					float sum = 0;

					for (int i = 0; i < a.bins(); i++) {
						float binDiff = a.normGet(i) - b.normGet(i);
						float binSum = a.normGet(i) + b.normGet(i);
						sum += binSum == 0 ? 0 : ((binDiff * binDiff) / binSum);
					}

					return 0.5f * sum;
				}
			};
		}

		return Histogram.chiSquaredDistanceFct;
	}

	/**
	 * Returns a distance function for Histograms. The distance is estimated by
	 * the distance of the bins with the maximum of elements.
	 * @return The distance function.
	 */
	public static DistanceFct<Histogram> maxBinDistanceFct() {

		if (Histogram.maxBinDistanceFct == null) {

			Histogram.maxBinDistanceFct = (a, b) -> {
				float delta = a.maxBin() - b.maxBin();

				return (float) Math.sqrt(delta * delta);

			};

		}

		return Histogram.maxBinDistanceFct ;
	}
	
	/**
	 * Creates a distance function for a set of Histograms. The distance will
	 * be estimated pairwise i.e. distance(histogramA[0], histogramB[0]). 
	 * @param comb Sets how the pairwise distance values are combined for the final
	 * distance value. The options are: "max" for the maximum distance found along
	 * the histogram pairs. "min"  for the minimum distance found along
	 * the histogram pairs. "mult" for multiplication. "sum" for a sum of all distances.
	 * @param fct A distance function for Histograms.
	 * @return The distance function for Histogram arrays.
	 */
	public static DistanceFct<Histogram[]> arrayDistanceFct(final String comb, final DistanceFct<Histogram> fct) {


		DistanceFct<Histogram[]> arrFct = (arr1, arr2) -> {
			int n = arr1.length <= arr2.length ? arr1.length : arr2.length;

			float sum = 0;

			for (int i = 0; i < n; i++) {


				if (comb.equals("max")) {
					if (i == 0) {
						sum = fct.distance(arr1[i], arr2[i]);
					} else {
						sum = Math.max(sum, fct.distance(arr1[i], arr2[i]));
					}
				} else if (comb.equals("min")) {
					if (i == 0) {
						sum = fct.distance(arr1[i], arr2[i]);
					} else {
						sum = Math.min(sum, fct.distance(arr1[i], arr2[i]));
					}

				} else if (comb.equals("mult")) {
					if (i == 0) {
						sum = fct.distance(arr1[i], arr2[i]);
					} else {
						sum *= Math.min(sum, fct.distance(arr1[i], arr2[i]));
					}

				} else {
					sum += fct.distance(arr1[i], arr2[i]);
				}

			}

			return sum;
		};

		return arrFct;
	}
}
