package net.returnvoid.imagefeatures;

import java.util.ArrayList;
import java.util.Arrays;

import net.returnvoid.image.GaborImg;
import net.returnvoid.image.IntensityImg;
import net.returnvoid.image.LabImg;
import net.returnvoid.image.RVImage;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * A class for the creation and storage of the pyramid representation an image.
 * @author Diana Lange
 *
 * @param <Type> The type of the input image. Must implement the RVImage interface.
 */
public class Pyramid<Type extends RVImage> {

	/**
	 * The images / layers of the pyramid.
	 */
	private ArrayList<Type> images;

	/**
	 * Creates an image pyramid. Use either Pyramid.createGabor() or Pyramid.createGauss() 
	 * functions to create a new instance. Pyramids are meant for
	 * storing one image which has been filtered and resized multiple times.
	 * The layers of the pyramid are then equal to these resized versions of the 
	 * original image. But the Pyramid can also be used for the storage of 
	 * multiple, various images. 
	 * @param images The layers of the pyramid.
	 */
	public Pyramid(ArrayList<Type> images) {
		this.images = new ArrayList<Type>(images.size());
		this.images.addAll(images);
	}
	
	/**
	 * Creates an image pyramid. Use either Pyramid.createGabor() or Pyramid.createGauss() 
	 * functions to create a new instance. Pyramids are meant for
	 * storing one image which has been filtered and resized multiple times.
	 * The layers of the pyramid are then equal to these resized versions of the 
	 * original image. But the Pyramid can also be used for the storage of 
	 * multiple, various images. 
	 * @param images The layers of the pyramid.
	 */
	public Pyramid(Type[] images) {
		this.images = new ArrayList<Type>(images.length);
		this.images.addAll(Arrays.asList(images));
	}
	
	/**
	 * Exports all layers of the pyramid as image files. The filenames will include
	 * the dimension of the image at the layer and the pyramid layer index.
	 * @param parent The parent Processing sketch container.
	 * @param name The export path and filename. Best use relative paths e.g. "export/gaussian.jpg".
	 * The files will be .png if no file ending is included in the input name.
	 */
	public void save(PApplet parent, String name) {
		String[] parts = name.split(".");
		String pathPlusName = "";
		String format;
		
		// separate file format ending from save path 
		if (parts.length > 1) {
			for (int i = 0; i < parts.length - 1; i++) {
				pathPlusName += parts[i];
			}
			format = parts[parts.length - 1];
		} else {
			
			// no set file ending fiund
			pathPlusName = name;
			format = "png";
		}
		
		// PImage.save() only when a PApplet instance is connected to the PImage
		// element. The RVImage.img() method creates PImages without the PApplet
		// instance. That's why the PApplet needs to be added before saving.
		for (int i = 0; i < images.size(); i++) {
			Type image = images.get(i);
			PImage pimage = image.img();
			pimage.parent = parent;
			pimage.save(pathPlusName + "_" + i + "_" + image.width() + "x" + image.height() + "." + format);
			
		}
		
	}
	
	/**
	 * Gets all layers of the pyramid.
	 * @return All images of the pyramid.
	 */
	public ArrayList<Type> getAll() {
		return images;
	}

	/**
	 * Gets the number of layers in this pyramid.
	 * @return The number of layers / images in this pyramid.
	 */
	public int levels() {
		return images.size();
	}

	/**
	 * Gets the image at the layer of the input index.
	 * @param i The index of the layer / image. i must be in range of [0, levels()).
	 * @return The image with the index i.
	 */
	public Type get(int i) {
		return images.get(i);
	}
	
	/**
	 * Gets the width of the image at the input layer.
	 * @param i The index of the layer / image. i must be in range of [0, levels()).
	 * @return The width of the image with the index i.
	 */
	public int width(int i) {
		return images.get(i).width();
	}
	
	/**
	 * Gets the height of the image at the input layer.
	 * @param i The index of the layer / image. i must be in range of [0, levels()).
	 * @return The height of the image with the index i.
	 */
	public int height(int i) {
		return images.get(i).height();
	}
	
	/**
	 * Linear interpolates an input value which is in range of [0, inputMax] to 
	 * the range [0, outputMax].
	 * @param input The input value.
	 * @param inputMax The maximum of the input range.
	 * @param outputMax The maximum of the output range.
	 * @return
	 */
	private float map(float input, float inputMax, float outputMax) {
		return outputMax * input / inputMax;
	}
	
	/**
	 * Linear interpolates an x coordinate which is in range of [0, width(inputIndex)]
	 * to the range of [0, width(outputIndex)]. Can be used to interpolate the 
	 * x coordinates across the various layers of the pyramid.
	 * @param x The input x coordinate.
	 * @param inputIndex The index of the layer which defines the range of x 
	 * using the width of the image at that index.
	 * @param outputIndex The index of the layer which defines the output value
	 * using the width of the image at that index.
	 * @return The interpolated x coordinate.
	 */
	public int mappedX(int x, int inputIndex, int outputIndex) {
		if (inputIndex == outputIndex) {
			return x;
		}
		return (int) map(x, get(inputIndex).width(), get(outputIndex).width());
	}
	
	/**
	 * Linear interpolates an y coordinate which is in range of [0, height(inputIndex)]
	 * to the range of [0, height(outputIndex)]. Can be used to interpolate the 
	 * y coordinates across the various layers of the pyramid.
	 * @param y The input y coordinate.
	 * @param inputIndex The index of the layer which defines the range of x 
	 * using the height of the image at that index.
	 * @param outputIndex The index of the layer which defines the output value
	 * using the height of the image at that index.
	 * @return The interpolated y coordinate.
	 */
	public int mappedY(int y, int inputIndex, int outputIndex) {
		if (inputIndex == outputIndex) {
			return y;
		}
		return (int) map(y, get(inputIndex).height(), get(outputIndex).height());
	}
	
	/**
	 * Performs image by image subtraction of the two pyramids and returns a new
	 * pyramid with the result of the subtraction. Formula: a.get(index) - b.get(index)
	 * The pyramids must have the same number of levels and each level of pyramid
	 * a must have the same dimension as the equal level in pyramid b.
	 * @param a The first pyramid consisting of IntensityImg.
	 * @param b The second pyramid consisting of IntensityImg.
	 * @return The new pyramid containing the subtraction result.
	 */
	public static Pyramid<IntensityImg> sub(Pyramid<IntensityImg> a, Pyramid<IntensityImg> b) {
		ArrayList<IntensityImg> images = new ArrayList<IntensityImg>();
		
		for (int i = 0; i < a.levels(); i++) {
			images.add(IntensityImg.sub(a.get(i), b.get(i)));
		}
		
		return new Pyramid<IntensityImg>(images);
	}
	
	/**
	 * Performs image by image addition of the two pyramids and returns a new
	 * pyramid with the result of the subtraction. Formula: a.get(index) + b.get(index)
	 * The pyramids must have the same number of levels and each level of pyramid
	 * a must have the same dimension as the equal level in pyramid b.
	 * @param a The first pyramid consisting of IntensityImg.
	 * @param b The second pyramid consisting of IntensityImg.
	 * @return The new pyramid containing the addition result.
	 */
	public static Pyramid<IntensityImg> add(Pyramid<IntensityImg> a, Pyramid<IntensityImg> b) {
		ArrayList<IntensityImg> images = new ArrayList<IntensityImg>();
		
		for (int i = 0; i < a.levels(); i++) {
			images.add(IntensityImg.add(a.get(i), b.get(i)));
		}
		
		return new Pyramid<IntensityImg>(images);
	}
	
	/**
	 * Takes a Pyramid containing GaborImg elements and splits it by the angles found
	 * in the GaborImg. For each angle found in the GaborImg elements a separate
	 * Pyramid is created and returned.
	 * @param input A input gabor pyramid.
	 * @return A set of pyramids. Each pyramid contains the responses of a gabor
	 * filter at a certain angle.
	 */
	public static ArrayList<Pyramid<IntensityImg>> splitGaborPyramid(Pyramid<GaborImg> input) {
		double[] angles = input.get(0).angles();

		ArrayList<Pyramid<IntensityImg>> newPyramids = new ArrayList<Pyramid<IntensityImg>>();

		// create an empty list for each angle of the GaborImg 
		ArrayList<ArrayList<IntensityImg>> rawPyramid = new ArrayList<ArrayList<IntensityImg>>();
		for (int i = 0; i < angles.length; i++) {
			ArrayList<IntensityImg> imagesByAngle = new ArrayList<IntensityImg>();
			rawPyramid.add(imagesByAngle);
		}

		// assign the images to a list which represents the pyramid with the
		// response to one single angle.
		for (int i = 0; i < input.levels(); i++) {
			GaborImg level = input.get(i);

			for (int j = 0; j < level.angles().length; j++) {
				IntensityImg img = level.getByAngle(j);
				rawPyramid.get(j).add(img);
			}
		}

		// create the pyramids with the found lists
		for (int i = 0; i < rawPyramid.size(); i++) {
			newPyramids.add(new Pyramid<IntensityImg>(rawPyramid.get(i)));
		}

		return newPyramids;
	}

	/**
	 * Creates a gabor pyramid created from the input image and the responses
	 * of gabor filters with the given size and angles and various scales. The 
	 * images will be resized by factor 2, i.e. each level will have half the 
	 * pixels in relation to the previous layer.
	 * @param input The input image.
	 * @param levels The number of levels of the pyramid.
	 * @param filterSize The size of the gabor filter with filterSize&gt;=3.
	 * @param angles The angles which should be detected in the input image using gabor filters.
	 * @return The gabor pyramid.
	 */
	static public Pyramid<GaborImg> createGabor(LabImg input, int levels, int filterSize, double[] angles) {
		return Pyramid.createGabor(input, levels, filterSize, angles, 1, 0.5);
	}
	
	/**
	 * Creates a gabor pyramid created from the input image and the responses
	 * of gabor filters with the given size and angles and various scales. The 
	 * images will be resized by factor 2, i.e. each level will have half the 
	 * pixels in relation to the previous layer.
	 * @param input The input image.
	 * @param levels The number of levels of the pyramid.
	 * @param filterSize The size of the gabor filter with filterSize&gt;=3.
	 * @param angles The angles which should be detected in the input image using gabor filters.
	 * @param periods One if filter should detected edges. Greater than one for 
	 * textures. periods &gt;= 1.
	 * @param shape The shape that the filter should respond to. For shape=1 the 
	 * filter responds to points (and lines), for shape=0 the filter responds only to lines.
	 * @return The gabor pyramid.
	 */
	static public Pyramid<GaborImg> createGabor(LabImg input, int levels, int filterSize, double[] angles, int periods, double shape) {
	
		ArrayList<GaborImg> pyramid = new ArrayList<GaborImg>(levels);
		LabImg[] images = new LabImg[levels];
		images[0] = input;
		pyramid.add(new GaborImg(images[0], angles, filterSize + periods * 2, periods, shape));
		double scaleFactor = 1 / Math.sqrt(2);

		for (int i = 1; i < levels; i++) {

			double scale = Math.pow(scaleFactor, i);
			int newW = (int) Math.round(pyramid.get(0).width() * scale);
			int newH = (int) Math.round(pyramid.get(0).height() * scale);
			images[i] = images[i - 1].filter(FilterFactory.gauss(filterSize * i), true);
			images[i].resize(newW, newH);			
			
			pyramid.add(new GaborImg(images[i], angles, filterSize + periods * 2));
		}

		return new Pyramid<GaborImg>(pyramid);
	}

	/**
	 * Creates a gaussian pyramid where each level is created by blurring the image
	 * of the previous level and resizing. The images will be resized by factor 2, 
	 * i.e. each level will have half the pixels in relation to the previous layer.
	 * @param input The input image.
	 * @param levels The number of levels of the pyramid.
	 * @param filterSize The size of the gaussian filter with filterSize&gt;=3. The
	 * filterSize will sequentially be enlarged at each layer of the pyramid.
	 * @param <Img> The image type for the input and the gaussian pyramid. Must implement
	 * the RVImage interface.
	 * @return The gaussian pyramid.
	 */
	@SuppressWarnings("unchecked")
	static public <Img extends RVImage> Pyramid<Img> createGauss(Img input,int levels, int filterSize) {

		ArrayList<Img> pyramid = new ArrayList<Img>(levels);

		pyramid.add(input);
		double scaleFactor = 1 / Math.sqrt(2);

		for (int i = 1; i < levels; i++) {

			double scale = Math.pow(scaleFactor, i);
			int newW = (int) Math.round(pyramid.get(0).width() * scale);
			int newH = (int) Math.round(pyramid.get(0).height() * scale);
			Img img = (Img) pyramid.get(i - 1).filter(FilterFactory.gauss(filterSize * i), true);
			img.resize(newW, newH);
			
			pyramid.add(img);
		}

		return new Pyramid<Img>(pyramid);
	}
}
