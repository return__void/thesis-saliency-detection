package net.returnvoid.imagefeatures;


/**
 * This class creates filters for images. Each filter can be applied by images by
 * the function img.filter(FilterFactory.theFilter()); All filters are modelled as
 * two dimensional arrays designed for the convultion with images.
 * @author Diana Lange
 *
 */
public class FilterFactory {

	/**
	 * Half the value of Math.PI.
	 */
	public final static double HALF_PI = Math.PI * 0.5;

	/**
	 * Twice the value of Math.PI.
	 */
	public final static double TWO_PI = Math.PI * 2;

	/**
	 * Squared value of TWO_PI.
	 */
	public final static double TWO_PI_SQRT = Math.sqrt(TWO_PI);


	/**
	 * Returns a filter for estimating the partial derivative in y direction.
	 * @return The sobel filter for y direction.
	 */
	public static double[][] sobelY() {
		return new double[][] {
			new double[] {1, 2, 1},
			new double[] {0, 0, 0},
			new double[] {-1, -2, -1}
		};
	}

	/**
	 * Returns a filter for estimating the partial derivative in x direction.
	 * @return The sobel filter for x direction.
	 */
	public static double[][] sobelX() {
		return new double[][] {
			new double[] {1, 0, -1},
			new double[] {2, 0, -2},
			new double[] {1, 0, -1}
		};
	}

	/**
	 * Returns a filter for image sharpening.
	 * @param strength The strength of sharpening. Should be greater than 1.
	 * @param n The size of the filter. Should be greater than 3.
	 * @return The sharpening filter.
	 */
	public static double[][] sharpening(float strength, int n) {
		if (n < 3) {
			n = 3;
		} else if (n % 2 == 0) {
			n += 1;
		}

		if (strength < 1.1) {
			strength = 1.1f;
		}

		// make sure filter sums up to one. The center pixel gets
		// highlighted by taking the average of its surrounding and subtracting
		// that average.
		double val = (strength - 1) / (n * n);

		double[][] f = new double[n][n];
		f[n / 2][n / 2] = strength + val;
		val = (strength + val - 1) / (n * n);

		for (int i = 0; i < f.length; i++) {                     
			for (int j = 0; j < f[i].length; j++) {                     
				f[i][j] -= val;
			}
		}

		return f;
	}

	/**
	 * Returns a filter for blurring.
	 * @param n The size of the filter. Should be greater than 3.
	 * @return The box blurring filter.
	 */
	public static double[][] box(int n) {
		if (n < 3) {
			n = 3;
		} else if (n % 2 == 0) {
			n += 1;
		}

		// filter sums up to one
		double val = 1d / (n * n);

		double[][] f = new double[n][];
		double[] row = new double[n];

		// create one row
		for (int i = 0; i < row.length; i++) {
			row[i] = val;
		}

		// all rows are the same
		for (int i = 0; i < f.length; i++) {                     
			f[i] = row;
		}

		return f;
	}

	/**
	 * Returns a filter for blurring.
	 * @param n The size of the filter. Should be greater than 3.
	 * @return The gaussian blurring filter.
	 */
	public static double[][] gauss(int n) {
		if (n < 3) {
			n = 3;
		} else if (n % 2 == 0) {
			n += 1;
		}

		double[][] filter = new double[n][];

		float std = (n / 2) / 3f;
		double sum = 0;

		for (int y = 0; y < n / 2.0; y++) {
			double[] row = new double[n];

			double rowSum = 0;

			for (int x = 0; x < n / 2.0; x++) {

				double g = FilterFactory.gaussFunction2D(x, y, std);

				// filter is symmetric, values can be copied
				row[n / 2 - x] = row[n / 2 + x] = g;

				if (x == 0) {
					rowSum += row[n / 2];
				} else {
					rowSum += 2 * row[n / 2 - x];
				}
			}

			// filter is symmetric, values can be copied
			filter[n / 2 - y] = filter[n / 2 + y] = row;

			if (y == 0) {
				sum += rowSum;
			} else {
				sum += 2 * rowSum;
			}
		}

		// make filter sum up to one
		for (int i = 0; i < filter.length; i++) {
			for(int j = 0; j < filter[i].length; j++) {
				filter[i][j] /= sum;
			}
		}


		return filter;
	}

	/**
	 * Returns a filter for detecting edge orientations.
	 * @param n The size of the filter. Should be greater than 3.
	 * @param periods One if filter should detected edges. Greater than one for 
	 * textures. Best use positive integer values.
	 * @param angle The angle that should be detected by this filter.
	 * @return The orientation filter for the given angle.
	 */
	public static double[][] gabor(int n, int periods, double angle) {

		return FilterFactory.gabor(n, angle, periods, 0.5);
	}

	/**
	 * Returns a filter for detecting edge orientations.
	 * @param n The size of the filter. Should be greater than 3.
	 * @param angle The angle that should be detected by this filter.
	 * @return The orientation filter for the given angle.
	 */
	public static  double[][] gabor(int n, double angle) {

		return FilterFactory.gabor(n, angle, 1, 0.5);
	}

	/**
	 * Returns a filter for detecting edge orientations.
	 * @param n The size of the filter. Should be greater than 3.
	 * @param angle The angle that should be detected by this filter.
	 * @param periods One if filter should detected edges. Greater than one for 
	 * textures. periods&gt;=1.
	 * @param shape The shape that the filter should respond to. For shape=1 the 
	 * filter responds to points (and lines), for shape=0 the filter responds only 
	 * to lines.
	 * @return The orientation filter for the given angle.
	 */
	public static double[][] gabor(int n, double angle, int periods, double shape) {
		if (n < 3) {
			n = 3;
		} else if (n % 2 == 0) {
			n += 1;
		}

		double[][] filter = new double[n][];
		double std = (n / 3d) / 3d;
		for (int y = 0; y < n; y++) {

			double[] row = new double[n];

			for (int x = 0; x < n; x++) {
				row[x] = FilterFactory.gaborFunction(x - n / 2, y - n / 2, angle, periods % 2 == 1 ? FilterFactory.HALF_PI : 0, (float) n / periods, std, shape);
			}
			filter[y] = row;
		}

		return filter;
	}


	/**
	 * A function for the creation of gabor filters. The function s equal to the 
	 * real part of the gabor transformation. Because this function does use
	 * a gaussian, the input values for x, y should be centered at zero.
	 * @param x The x value for the function.
	 * @param y The y value for the function.
	 * @param angle The angle the gabor filter should respond to.
	 * @param offset The offset of the filter. Usually this will be HALF_PI when
	 * periods is an odd integer value and zero for even integer values.
	 * @param period The number of periods for the filter. period&gt;=1.
	 * @param std The standard deviation for the gaussian.
	 * @param shape The shape that the filter should respond to. For shape=1 the 
	 * filter responds to points (and lines), for shape=0 the filter responds only 
	 * to lines.
	 * @return The value of the gabor transformation.
	 */
	public static double gaborFunction(double x, double y, double angle, double offset, double period, double std, double shape) {

		double cosAngle = Math.cos(angle);
		double sinAngle =  Math.sin(angle);
		double xx = x * cosAngle + y * sinAngle;
		double yy = -x * sinAngle + y * cosAngle;

		double gauss = Math.exp(-1 * ((xx * xx + shape * shape * yy * yy) / (2 * std * std)));
		double gabor = Math.cos(FilterFactory.TWO_PI * xx / period + offset);

		return gabor * gauss;
	}

	/**
	 * Estimates the gaussian for the input value.
	 * @param x The input value. Should be centered at zero.
	 * @param std The standard deviation for the function.
	 * @return The gaussian value.
	 */
	public static double gaussFunction1D(double x, double std) {
		double f = 1 / (std * FilterFactory.TWO_PI_SQRT);
		double e =  Math.exp((-1 * x * x) / (2 * std * std));

		return f * e;
	}


	/**
	 * Estimates the gaussian for the input value.
	 * @param x The input value. Should be centered at zero.
	 * @param std The standard deviation for the function.
	 * @param scaling A scaling factor for the gauss function.
	 * @return The gaussian value.
	 */
	public static double gaussFunction1D(double x, double std, double scaling) {
		double e =  Math.exp((-1 * x * x) / (2 * std * std));

		return scaling * e;
	}

	/**
	 * Estimates the gaussian for the input values in two dimension space.
	 * @param x The x coordinate of the input value. Should be centered at zero.
	 * @param y The y coordinate of the input value. Should be centered at zero.
	 * @param std The standard deviation for the function.
	 * @param scaling A scaling factor for the gauss function.
	 * @return The gaussian value.
	 */
	public static double gaussFunction2D(double x, double y, double std, double scaling) {

		double e =  Math.exp((-1 * (x * x + y * y)) / (2 * std * std));

		return scaling * e;
	}

	/**
	 * Estimates the gaussian for the input values in two dimension space.
	 * @param x The x coordinate of the input value. Should be centered at zero.
	 * @param y The y coordinate of the input value. Should be centered at zero.
	 * @param std The standard deviation for the function.
	 * @return The gaussian value.
	 */
	public static double gaussFunction2D(double x, double y, double std) {
		double f = 1 / (std * std * FilterFactory.TWO_PI);
		double e =  Math.exp((-1 * (x * x + y * y)) / (2 * std * std));

		return f * e;
	}
}
