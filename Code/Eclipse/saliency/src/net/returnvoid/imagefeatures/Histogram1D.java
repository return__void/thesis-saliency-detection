package net.returnvoid.imagefeatures;


import net.returnvoid.functions.MeanFct;
import processing.core.PGraphics;

/**
 * A class for creating Histograms of floating point values.
 * 
 * @author Diana Lange
 *
 */
public class Histogram1D extends Histogram {

	/**
	 * The bins of the histogram. Each bin contains how many elements have
	 * been added to the histogram that are in range of the bin. 
	 */
	private int[] binned;
	/**
	 * A function for creating a mean histogram from a set of histograms.
	 */
	private static MeanFct<Histogram1D> meanFct = null;
	

	/**
	 * Creates a new Histogram instance with the same range and same number
	 * of bins as the input histogram.
	 * @param histogram The input histogram (will not be altered).
	 */
	public Histogram1D(Histogram1D histogram) {
		this(histogram.min, histogram.max, histogram.bins());
	}

	/**
	 * Creates a new Histogram with the input range and the given number of
	 * bins.
	 * @param min The minimum value of the Histogram.
	 * @param max The maximum value of the Histogram.
	 * @param bins The number of bins for this Histogram.
	 */
	public Histogram1D(float min, float max, int bins) {
		super(min, max);
		this.binned = new int[bins];

		for (int i = 0; i < bins; i++) {
			binned[i] = 0;
		}
	}
	
	/**
	 * Creates a new Histogram with the input range and the given number of
	 * bins.
	 * @param min The minimum value of the Histogram.
	 * @param max The maximum value of the Histogram.
	 * @param bins The number of bins for this Histogram.
	 */
	/*
	public Histogram(float[] minmax, int[] bins) {
		super(min, max, 8);
		this.binned = new ArrayList<Integer>(bins);

		for (int i = 0; i < bins; i++) {
			binned.add(0);
		}
	}*/


	/**
	 * Sets the value of a bin.
	 * @param i The index of the bin. Should be in range of [0, bins()).
	 * @param count The new value of the bin with index i.
	 */
	@Override
	public void set(int i, int count) {
		this.elementsAdded -= this.binned[i];
		this.elementsAdded += count;

		this.binned[i] = count;

		resetTmpValues();
	}
	
	/**
	 * Creates a hard copy of this Histogram and returns a new instance.
	 * @return The copied Histogram.
	 */
	public Histogram1D copy() {
		Histogram1D copy = new Histogram1D(this.min, this.max, this.binned.length);
		System.arraycopy(binned, 0, copy.binned, 0, binned.length);
		copy.maxBin = maxBin;
		
		return copy;
	}

	/**
	 * Gets the number of elements added to the histogram that where in range of
	 * the bin i.
	 * @param i The index of the bin.
	 * @return The number of added elements within the range of the bin i.
	 */
	@Override
	public int get(int i) {
		return binned[i];
	}

	/**
	 * Returns the number if bins of this Histogram.
	 * @return The number of bins.
	 */
	@Override
	public int bins() {
		return binned.length;
	}

	/**
	 * Adds a value to the histogram. This will alter the value of one bin in
	 * the histogram. 
	 * @param val The input value.
	 */
	public void add(float val) {
		elementsAdded++;
		int index = map(val);
		this.binned[index] += 1;
		resetTmpValues();
	}

	/**
	 * Maps any input value to an index of a bin of this Histogram. The input value
	 * may be out of range of this histogram. If so the returned index will be either
	 * zero or the highest available index depending on which boundary the input was
	 * the nearest.
	 * @param val The input value.
	 * @return The index of the bin to which the input value is mapped.
	 */
	public int map(float val) {


		float x = (val - min) / (max - min);

		int bin = (int) (x * bins());

		if (bin < 0) {
			bin = 0;
		} else if (bin >= bins()) {
			bin = bins() - 1;
		}

		return bin;
	}

	/**
	 * A helper function for drawing the content of the histogram.
	 * @param g The drawing container.
	 * @param x The x location of the drawing (bounding box).
	 * @param y The y location of the drawing (bounding box).
	 * @param w The width of the drawing (bounding box).
	 * @param h The height of the drawing (bounding box).
	 */
	public void draw(PGraphics g, float x, float y, float w, float h) {
		float steps = w / bins();

		
		for (int i = 0; i < binned.length; i++) {
			float xx = x + i * steps;
			float hh = normGet(i) * h;
			float yy = y + h - hh;
			float ww = steps;
			//int percentage = (int) (normGet(i) * 100);
			//int decimals = (int) ((normGet(i) * 100 - percentage) * 100);
			//String txt = percentage + "." + decimals + "%";
			//txt += "\n[ " + binned.get(i) + " ]";

			g.rect(xx - 0.1f, yy, ww + 0.2f, hh);
			//g.text(txt, xx + steps / 2, yy - 10);
		}
	}

	/**
	 * Returns a mean function that can create a mean Histogram from a set of
	 * Histograms. The mean function assumes that all elements in the array
	 * have the same number of bins.
	 * @return The mean creator function for histogram arrays.
	 */
	public static MeanFct<Histogram1D> meanFct() {

		if (Histogram1D.meanFct == null) {

			Histogram1D.meanFct = arr -> {
				Histogram1D mean = arr.get(0).copy();

				for (int i = 0; i < arr.size(); i++) {

					Histogram1D h = arr.get(i);
					for (int j = 0; j < h.binned.length; j++) {
						mean.set(j, mean.get(j) + h.get(j));
					}
				}
				return mean;
			};

		}

		return Histogram1D.meanFct;
	}
}