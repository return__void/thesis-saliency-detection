package net.returnvoid.imagefeatures;

/**
 * A class for creating two-dimensional Histograms of floating point values.
 * 
 * @author Diana Lange
 *
 */
public class Histogram2D extends Histogram {
	/**
	 * The minimum of the range of the histogram of dim. 2. Always true: min < max.
	 */
	protected float min2;
	
	/**
	 * The maximumg of the range of the histogram of dim. 2. Always true: min < max.
	 */
	protected float max2;
	
	/**
	 * The bins of the histogram. Each bin contains how many elements have
	 * been added to the histogram that are in range of the bin. 
	 */
	private int[][] binned;
	
	/**
	 * Creates a new Histogram with the input range and the given number of
	 * bins.
	 * @param min1 The minimum of the range of the first dimension.
	 * @param max1 The maximum of the range of the first dimension.
	 * @param min2 The minimum of the range of the second dimension.
	 * @param max2 The maximum of the range of the second dimension.
	 * @param bins1 The number of bins in the first dimension.
	 * @param bins2 The number of bins in the second dimension.
	 */
	public Histogram2D(float min1, float max1, float min2, float max2, int bins1, int bins2) {
		super(min1, max1);
		this.min2 = min2;
		this.max2 = max2;
		
		binned = new int[bins1][];
		
		for (int i = 0; i < binned.length; i++) {
			int[] row = new int[bins2];
			
			for (int j = 0; j < row.length; j++) {
				row[j] = 0;
			}
			
			binned[i] = row;
		}
	}

	/**
	 * Sets the value of a bin.
	 * @param index The index of the bin. Should be in range of [0, bins()). The
	 * index can be estimated via y * binsDim2() + x where y is element of [0, binsDim1()]
	 * and x is element of [0, binsDim2()].
	 * @param count The new value of the bin with index i.
	 */
	@Override
	public void set(int index, int count) {
		int i = index % binned[0].length;
		int j = index / binned[0].length;
		
		this.elementsAdded -= this.binned[j][i];
		this.elementsAdded += count;

		this.binned[j][i] = count;

		resetTmpValues();
	}

	/**
	 * Gets the number of elements added to the histogram that where in range of
	 * the bin with the input index. The index can be estimated via 
	 * y * binsDim2() + x where y is element of [0, binsDim1()]
	 * and x is element of [0, binsDim2()].
	 * @param index The index of the bin.
	 * @return The number of added elements within the range of the bin i.
	 */
	@Override
	public int get(int index) {
		int i = index % binned[0].length;
		int j = index / binned[0].length;
		
		return this.binned[j][i];
	}
	
	/**
	 * Adds a value to the histogram. This will alter the value of one bin in
	 * the histogram. 
	 * @param val1 The input value for the first dimension.
	 * @param val1 The input value for the second dimension.
	 */
	public void add(float val1, float val2) {
		elementsAdded++;
		int i = mapDim1(val1);
		int j = mapDim2(val2);
		this.binned[i][j] += 1;
		resetTmpValues();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.imagefeatures.Histogram#bins()
	 */
	@Override
	public int bins() {
		return binned[0].length * binned.length;
	}
	
	/**
	 * Returns the number if bins of this Histogram's first dimension.
	 * @return The number of bins.
	 */
	public int binsDim1() {
		return binned.length;
	}
	
	/**
	 * Returns the number if bins of this Histogram's second dimension.
	 * @return The number of bins.
	 */
	public int binsDim2() {
		return binned[0].length;
	}
	
	/**
	 * The minimum of the range of the first dimension.
	 * @return The minimum of the range.
	 */
	public float minDim1() {
		return min;
	}
	
	/**
	 * The maximum of the range of the first dimension.
	 * @return The minimum of the range.
	 */
	public float maxDim1() {
		return max;
	}
	
	/**
	 * The minimum of the range of the second dimension.
	 * @return The minimum of the range.
	 */
	public float minDim2() {
		return min2;
	}
	
	/**
	 * The maximum of the range of the second dimension.
	 * @return The minimum of the range.
	 */
	public float maxDim2() {
		return max2;
	}
	
	/**
	 * Maps any input value to an index of a bin of this Histogram. The input value
	 * may be out of range of this histogram. If so the returned index will be either
	 * zero or the highest available index depending on which boundary the input was
	 * the nearest.
	 * @param val The input value in range of the first dimension.
	 * @return The first dimension index of the bin to which the input value is mapped.
	 */
	public int mapDim1(float val) {


		float x = (val - min) / (max - min);

		int bin = (int) (x * binsDim1());

		if (bin < 0) {
			bin = 0;
		} else if (bin >= binsDim1()) {
			bin = binsDim1() - 1;
		}

		return bin;
	}
	
	/**
	 * Maps any input value to an index of a bin of this Histogram. The input value
	 * may be out of range of this histogram. If so the returned index will be either
	 * zero or the highest available index depending on which boundary the input was
	 * the nearest.
	 * @param val The input value in range of the second dimension.
	 * @return The second dimension index of the bin to which the input value is mapped.
	 */
	public int mapDim2(float val) {


		float x = (val - min2) / (max2 - min2);

		int bin = (int) (x * binsDim2());

		if (bin < 0) {
			bin = 0;
		} else if (bin >= binsDim2()) {
			bin = binsDim2() - 1;
		}

		return bin;
	}

}
