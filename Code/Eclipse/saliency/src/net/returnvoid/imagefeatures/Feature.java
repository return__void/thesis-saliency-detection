package net.returnvoid.imagefeatures;

import net.returnvoid.segmentation.Superpixel;
import net.returnvoid.segmentation.Segment;

import net.returnvoid.color.LabColor;
import net.returnvoid.functions.DistanceFct;
import net.returnvoid.functions.FeatureExtractorFct;
import net.returnvoid.image.GaborImg;
import net.returnvoid.image.IntensityImg;
import net.returnvoid.image.LabImg;
import net.returnvoid.image.RGBYColor;
import net.returnvoid.image.RGBYImg;
import net.returnvoid.math.Coord;

/**
 * A feature is some kind of information that is extracted from any kind of raw
 * data. The feature is comparable to other features of the same Type by a distance
 * function that is stored along with the value of the input element. This distance
 * function is called within the distance() function of this class.
 * 
 * @author Diana Lange
 *
 * @param <Type> The Type of the feature, e.g. LabColor, Histogram. An instance
 * of that Type will be stored within instances of the Feature class along with
 * a distances measure that is defined on this Type.
 */
public class Feature<Type> {

	/**
	 * A feature e.g. a color, a histogram. 
	 */
	private Type element;

	/**
	 * A distance measure that is defined on the type of the element. 
	 */
	private DistanceFct<Type> distanceFct;


	/**
	 * Creates a feature. Features can be any elements that can be compared
	 * by a distance function.
	 * @param element A feature e.g. a color, a histogram. 
	 * @param distanceFct A distance measure that is defined on the type of the element. 
	 */
	public Feature(Type element, DistanceFct<Type> distanceFct) {
		this.distanceFct = distanceFct;
		this.element = element;
	}

	/**
	 * Gets the content of the feature, e.g. the color, the histogram.
	 * @return The feature.
	 */
	public Type get() {
		return element;
	}

	/**
	 * Sets the distance function for the feature.
	 * @param fct The new distance function.
	 */
	public void setDistanceFct(DistanceFct<Type> fct) {
		this.distanceFct = fct;
	}

	/**
	 * Gets the current distance function of the feature.
	 * @return The distance function.
	 */
	public DistanceFct<Type> distanceFct() {
		return distanceFct;
	}

	/**
	 * Estimates the distance between two elements of the same type. If the input
	 * has another type, then the distance() function will return -1.
	 * @param other The other element.
	 * @return The distance between this element and the other element -1 if input
	 * has a different type as this element.
	 */
	@SuppressWarnings("unchecked")
	public float distance(Feature<?> other) {
		try {
			return distanceFct.distance(this.get(), (Type) other.get());
		} catch(Exception e) {
			return -1;
		}
	}

	/**
	 * Creates an instance of feature from the known element and distance function.
	 * @param element An element, e.g. a color, a histogram,...
	 * @param distFct A distance function that is defined on the element.
	 * @param <T> The feature type.
	 * @return A new feature instance.
	 */
	public static <T> Feature<T> create(T element, DistanceFct<T> distFct) {
		return new Feature<T>(element, distFct);
	}

	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts the mean RGBYColor from the points in the input image that are 
	 * located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts the mean RGBYColor of a region
	 * in an image.
	 */
	public static FeatureExtractorFct<RGBYImg, Segment, RGBYColor> rgbyColorFeatureFct() {
		FeatureExtractorFct<RGBYImg, Segment, RGBYColor> rgbyColorFeatureFct = (img, segment) -> {

			RGBYColor meanColor = new RGBYColor();

			for (int[] point : segment.points()) {
				RGBYColor color = img.rgby(point[0], point[1]);
				meanColor.add(color);
			}

			meanColor.mult(1f /  segment.points().length);

			return meanColor;
		};

		return rgbyColorFeatureFct;
	}




	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts the mean gradient vector from the points in the input Gradient that are 
	 * located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts the mean vector of a region 
	 * in an Gradient.
	 */
	public static FeatureExtractorFct<Gradient, Segment, Coord> meanGradientFeatureFct() {

		FeatureExtractorFct<Gradient, Segment, Coord> gradientFeatureFct = (gradient, segment) -> {

			Coord meanVec = null;

			for (int[] point : segment.points()) {
				Coord gradientVec = gradient.get(point[0], point[1]);

				if (meanVec == null) {
					meanVec = gradientVec.copy();
				} else {
					meanVec.add(gradientVec);
				}
			}

			if (meanVec != null) {
				meanVec.mult(1f / segment.points().length);
			} else {
				meanVec = new Coord(0, 0);
			}
			return meanVec;
		};


		return gradientFeatureFct;
	}

	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts a histogram of gradient orientations from the points in the input 
	 * Gradient that are located within the boundaries of the input segment. Only
	 * the orientations with strong gradient magnitudes are mapped to their 
	 * orientation. When there is no strong gradient magnitude the edge
	 * is not visible and orientation has no visible meaning. The orientations
	 * with from strong edges are stored in the first 9 bins of the histogram,
	 * all other gradient values are stored in the 10th bin of the histogram.
	 * @return The FeatureExtractorFct that extracts the Histogram of gradient
	 * orientations of a region in an gradient.
	 */
	public static FeatureExtractorFct<Gradient, Segment, Histogram1D> HOGFeatureFct() {

		FeatureExtractorFct<Gradient, Segment, Histogram1D> fct = (gradient, segment) -> {

			float cutoff = gradient.minMag() + (gradient.maxMag() - gradient.minMag())* 0.15f;

			Histogram1D his = new Histogram1D(0, 400, 10);	

			for (int[] point : segment.points()) {
				Coord gradientVec = gradient.get(point[0], point[1]);
				float angle = (float) (180 * gradientVec.angle() / Math.PI);
				float mag = gradientVec.mag();

				if (mag < cutoff) {
					angle = his.max();
				}

				his.add(angle);
			}

			return his;
		};


		return fct;
	}
	
	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts a Histogram of edge magnitudes from the points in the input 
	 * Gradient that are located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts histogram of edge magnitudes.
	 */
	public static FeatureExtractorFct<Gradient, Segment, Histogram1D> edgeMagnitudeFeatureFct() {

		FeatureExtractorFct<Gradient, Segment, Histogram1D> edgeStrengthFeatureFct = (gradient, segment) -> {


			float rangeMin = gradient.minMag();
			float rangeMax = gradient.maxMag();

			Histogram1D his = new Histogram1D(rangeMin, rangeMax, 10);

			for (int[] point : segment.points()) {
				Coord gradientVec = gradient.get(point[0], point[1]);
				his.add(gradientVec.mag());
			}


			return his;
		};

		return edgeStrengthFeatureFct;
	}
	
	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts a two one dimensional Histograms of edge magnitudes and orientations from the points in the input
	 * Gradient that are located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts histograms of edge magnitudes and orientations.
	 */
	public static FeatureExtractorFct<Gradient, Segment, Histogram1D[]> simpleEdgeFeatureFct() {

		FeatureExtractorFct<Gradient, Segment, Histogram1D[]> edgeStrengthFeatureFct = (gradient, segment) -> {


			float rangeMin = gradient.minMag();
			float rangeMax = gradient.maxMag();

			Histogram1D strength = new Histogram1D(rangeMin, rangeMax, 8);
			Histogram1D orientation = new Histogram1D(0, 400, 10);
			float cutoff = gradient.minMag() + (gradient.maxMag() - gradient.minMag())* 0.15f;

			for (int[] point : segment.points()) {
				Coord gradientVec = gradient.get(point[0], point[1]);
				float angle = (float) (180 * gradientVec.angle() / Math.PI);
				float mag = gradientVec.mag();
				
				if (mag < cutoff) {
					angle = orientation.max();
				}
				
				strength.add(mag);
				orientation.add(angle);
			}


			return new Histogram1D[] {orientation, strength};
		};

		return edgeStrengthFeatureFct;
	}
	
	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts a two dimensional Histogram of edge magnitudes and orientations from the points in the input
	 * Gradient that are located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts histogram of edge magnitudes and orientations.
	 */
	public static FeatureExtractorFct<Gradient, Segment, Histogram2D> edgeFeatureFct() {

		FeatureExtractorFct<Gradient, Segment, Histogram2D> edgeFeatureFct = (gradient, segment) -> {

			float rangeMin = gradient.minMag();
			float rangeMax = gradient.maxMag();
			float cutoff = gradient.minMag() + (gradient.maxMag() - gradient.minMag())* 0.15f;

			Histogram2D his = new Histogram2D(0, 400, rangeMin, rangeMax, 10, 5);

			for (int[] point : segment.points()) {
				Coord gradientVec = gradient.get(point[0], point[1]);
				float angle = (float) (180 * gradientVec.angle() / Math.PI);	
				float mag = gradientVec.mag();
				
				if (mag < cutoff) {
					angle = his.maxDim1();
				}
				
				his.add(angle, mag);
			}


			return his;
		};

		return edgeFeatureFct;
	}

	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts the gabor responses from the points in the input image that are 
	 * located within the boundaries of the input segment. Returns a two dimensional
	 * histogram which contains the information of orientation response and response strength.
	 * @return The FeatureExtractorFct that extracts texture information from
	 * an image.
	 */
	public static FeatureExtractorFct<GaborImg, Segment, Histogram2D> textureFeatureFct() {

		FeatureExtractorFct<GaborImg, Segment, Histogram2D> fct = (gabor, segment) -> {

			IntensityImg intensity = gabor.sum();
			float cutoff = intensity.range().min() + (intensity.range().max() - intensity.range().min()) * 0.1f;
			Histogram2D histogram = new Histogram2D(0, gabor.angles().length, intensity.range().min(), intensity.range().max(), gabor.angles().length + 1, 5);

			for (int[] point : segment.points()) {		
				
				float meanResponse = (float) intensity.get(point[0], point[1]);			
				
				if (intensity.get(point[0], point[1]) < cutoff) {
					histogram.add(gabor.angles().length, meanResponse);
					continue;
				}
				
				float index = 0;
				float highestResponse = -1;
				
				for (int i = 0; i < gabor.angles().length; i++) {
				
					float response = (float) gabor.getByAngle(i).get(point[0], point[1]);
					if (i == 0 || response > highestResponse) {
						index = i;
						highestResponse = response;
					}
				}
				
				histogram.add(index, highestResponse);
				
				
			}

			return histogram;
		};


		return fct;
	}

	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts the gabor responses from the points in the input image that are 
	 * located within the boundaries of the input segment. Returns two one dimensional
	 * histograms which contain the information of orientation response and response strength.
	 * @return The FeatureExtractorFct that extracts texture information from
	 * an image.
	 */
	public static FeatureExtractorFct<GaborImg, Segment, Histogram1D[]> simpleTextureFeatureFct() {

		FeatureExtractorFct<GaborImg, Segment, Histogram1D[]> fct = (gabor, segment) -> {
			
			IntensityImg intensity = gabor.sum();
			float cutoff = intensity.range().min() + (intensity.range().max() - intensity.range().min()) * 0.1f;
			Histogram1D responseStrength = new Histogram1D(intensity.range().min(), intensity.range().max(), 5);
			Histogram1D orientation = new Histogram1D(0, gabor.angles().length, gabor.angles().length + 1);

			for (int[] point : segment.points()) {

				responseStrength.add((float) intensity.get(point[0], point[1]));
				
				
				if (intensity.get(point[0], point[1]) < cutoff) {
					orientation.add(gabor.angles().length);
					continue;
				}
				
				float index = 0;
				float highestResponse = -1;
				
				for (int i = 0; i < gabor.angles().length; i++) {
				
					float response = (float) gabor.getByAngle(i).get(point[0], point[1]);
					if (i == 0 || response > highestResponse) {
						index = i;
						highestResponse = response;
					}
				}
				orientation.add(index);
				
			}

			return new Histogram1D[] {responseStrength, orientation};
		};


		return fct;
	}




	/**
	 * Creates a FeatureExtractorFct, that fulfills following task:
	 * Extracts the mean LabColor from the points in the input image that are 
	 * located within the boundaries of the input segment.
	 * @return The FeatureExtractorFct that extracts the mean LabColor of a region
	 * in an image.
	 */
	public static FeatureExtractorFct<LabImg, Superpixel, LabColor> colorFeatureFct() {

		FeatureExtractorFct<LabImg, Superpixel, LabColor> colorFeatureFct = (labImg, superpixel) -> {
			return superpixel.center().lab();
		};

		return colorFeatureFct;
	}
}
