
/**
 * This package includes classes that will help to extract and store image
 * information.
 * 
 * @author Diana Lange
 *
 */
package net.returnvoid.imagefeatures;