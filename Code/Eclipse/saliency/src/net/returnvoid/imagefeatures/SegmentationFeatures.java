package net.returnvoid.imagefeatures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.returnvoid.functions.DistanceFct;
import net.returnvoid.segmentation.Segment;

/**
 * Stores segments and maps a set of features to each segment. Each feature is 
 * named, e.g. "colorFeature". The value of a feature can be obtained by a 
 * segment and the feature name.
 * @author Diana Lange
 *
 */
public class SegmentationFeatures {
	
	/**
	 * A set of segments.
	 */
	private ArrayList<Segment> segments = new ArrayList<Segment>();
	
	/**
	 * A set of feature names that have been added to this instance of SegmentationFeatures.
	 */
	private HashMap<String, String> featureNames = new HashMap<String, String>();
	
	/**
	 * A map from segment to its features. 
	 */
	private HashMap<Segment, HashMap<String, Feature<?>>> features = new HashMap<Segment, HashMap<String, Feature<?>>>();
	
	/**
	 * Creates a new empty SegmentationFeatures instance. 
	 */
	public SegmentationFeatures() {
	}
	
	/**
	 * Adds a feature and stores and maps it to the segment under the given name.
	 * @param segment The segment.
	 * @param featureName The name of the feature.
	 * @param feature The feature of the segment.
	 */
	public void add(Segment segment, String featureName, Feature<?> feature) {
		
		// add new found name to storage
		if (!featureNames.containsKey(featureName)) {
			featureNames.put(featureName, featureName);
		}
		
		// create feature storage if needed
		HashMap<String, Feature<?>> storage;
		
		if (!features.containsKey(segment)) {
			segments.add(segment);
			storage = new HashMap<String, Feature<?>>();
		} else {
			storage = features.get(segment);
		}
		
		// add feature to the feature storage of the segment
		storage.put(featureName, feature);
		features.put(segment, storage);
	}
	
	/**
	 * Updates the distance function for all features of all segments with the 
	 * the input feature name. Make sure that the type of the distance function
	 * is the same type of the feature with the featureName.
	 * @param featureName The name of the feature of which the distance function
	 * should be updated.
	 * @param fct The new distance function for the features with the name.
	 * @param <Type> The type of the distance function. Must match with the type
	 * of the features that are named with "featureName".
	 */
	public <Type> void updateDistanceFct(String featureName, DistanceFct<Type> fct) {
		for (Map.Entry<Segment, HashMap<String, Feature<?>>> entry : features.entrySet()) {
			HashMap<String, Feature<?>> featureMap = entry.getValue();

			try {
				@SuppressWarnings("unchecked")
				Feature<Type> feature = (Feature<Type>) featureMap.get(featureName);
				feature.setDistanceFct(fct);
			} catch (Exception e) {
				// feature and distance function might not be compatible
				// or feature is not included here
				// skip the update if exception is detected.
			}
		}
	}
	
	/**
	 * Removes the feature with the input featureName from all segments.
	 * @param featureName The name of the feature that should be removed.
	 */
	public void remove(String featureName) {
		featureNames.remove(featureName);
		
		for (Map.Entry<Segment, HashMap<String, Feature<?>>> entry : features.entrySet()) {
			HashMap<String, Feature<?>> featureMap = entry.getValue();
			featureMap.remove(featureMap);
		}
	}
	
	/**
	 * Estimates the distance of segment a to segment b concerning the feature
	 * with the given name. Make sure that the input segments and featureName has
	 * been added to this SegmentationFeatures instance previously. Will return -1
	 * if at least one segment has not been found or the feature is missing for
	 * at least one of the segments. 
	 * @param a The first segment (should be element of segments()).
	 * @param b The second segment (should be element of segments()).
	 * @param featureName The name of the feature of which the distance will be
	 * estimated (should be element of featureNames()). 
	 * @return The distance between segment a and b in the feature.
	 */
	public float distance(Segment a, Segment b, String featureName) {
		
		HashMap<String, Feature<?>> mapA = features.get(a);
		HashMap<String, Feature<?>> mapB = features.get(b);
		
		if (a == null | b == null) {
			return -1;
		}
		
		Feature<?> fa = mapA.get(featureName);
		Feature<?> fb = mapB.get(featureName);
		
		if (fa == null || fb == null) {
			
			return -1;
		}
		
		return fa.distance(fb);
	}
	
	/**
	 * Returns all segments that have been added to this storage.
	 * @return The segments that have mapped features in this storage.
	 */
	public ArrayList<Segment> segments() {
		return segments;
	}
	
	/**
	 * A set which contains the names of the features that have been added to
	 * this storage.
	 * @return The names of all available features. 
	 */
	public HashMap<String, String> featureNames() {
		return featureNames;
	}
	
	/**
	 * The number of segments that have been added to this storage.
	 * @return The number of segments that have been added to this storage.
	 */
	public int size() {
		return segments.size();
	}
	
	/**
	 * Gets the segment with index i.
	 * @param i The index of the segment. Should be range of [0, size()).
	 * @return The segment with the index i.
	 */
	public Segment get(int i) {
		return segments.get(i);
	}
	
	/**
	 * Gets a feature of a segment. Will return null if either the segment 
	 * doesn't exist in this storage or no feature has been added with the given 
	 * name or the segment lags the feature with that name.
	 * @param segment The segment (an element of segments()).
	 * @param featureName The name of the feature (an element of featureNames()).
	 * @return The feature of the segment with the given name.
	 * @param <T> The type of the feature with the input "featureName".
	 */
	@SuppressWarnings("unchecked")
	public <T> Feature<T> get(Segment segment, String featureName) {
		
		if (!features.containsKey(segment) || !featureNames.containsKey(featureName)) {
			return null;
		}
		
		HashMap<String, Feature<?>> storage = features.get(segment);
		Feature<T> feature = (Feature<T>) storage.get(featureName);
		
		return feature;
	}
}
