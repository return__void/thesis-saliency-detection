package net.returnvoid.imagefeatures;


import net.returnvoid.image.RVImage;
import net.returnvoid.math.Coord;
import net.returnvoid.math.Range;

/**
 * A class for the estimation and storage of gradient vectors of an input
 * image.
 * 
 * @author Diana Lange
 *
 */
public class Gradient {
	
	/**
	 * The input image.
	 */
	private RVImage input;
	
	/**
	 * A set of gradient vectors of the input image. The gradient vectors are
	 * estimated the first time the value is called by getRaw().
	 */
	private Coord[] rawGradients;
	
	/**
	 * A set of gradient vectors of the input image. This set created from
	 * rawGradients values in a small neighborhood. The neighborhood is set
	 * by the value 'area'. If area=1 then cumulatedGradients=rawGradients.
	 */
	private Coord[] cumulatedGradients;
	
	/**
	 * Sets the neighborhood for the cumulatedGradients.
	 */
	private int area = 5;
	
	/**
	 * The range of the magnitudes of cumulatedGradients.
	 */
	private Range magRange = null;
	
	/**
	 * Counts how many cumulatedGradients vectors have been computed yet. If
	 * all vectors have been estimated the rawGradients array will be cleared.
	 */
	private int computedCount = 0;
	
	/**
	 * Creates a new instance of gradient. Just used for copy() function.
	 */
	private Gradient() {
	}
	
	/**
	 * Builds a new instance of a Gradient. A gradient
	 * creates and stores the gradient vectors of the input image. The gradient
	 * vectors will be computed as the mean vector of the gradients in
	 * a small neighborhood. The neighborhood is defined by the 'area' parameter.
	 * @param input The input image.
	 * @param area The neighborhood for the mean computation with area &gt;= 1.
	 */
	public Gradient(RVImage input, int area) {
		this.area = area;
		this.input = input;
		this.rawGradients = null;
		this.cumulatedGradients = new Coord[input.size()];
	}
	
	/**
	 * Builds a new instance of a Gradient. A gradient
	 * creates and stores the gradient vectors of the input image. The gradient
	 * vectors will be computed as the mean vector of the gradients in
	 * a 5x5 neighborhood.
	 * @param input The input image.
	 */
	public Gradient(RVImage input) {
		this(input, 5);
	}
	
	/**
	 * The width of the gradient (and input).
	 * @return The width.
	 */
	public int width() {
		return input.width();
	}
	
	/**
	 * The height of the gradient (and input).
	 * @return The height.
	 */
	public int height() {
		return input.height();
	}
	
	/**
	 * The number of gradient vectors of this gradient (equal to the number of
	 * pixels in the input image).
	 * @return The number of gradient vectors.
	 */
	public int size() {
		return input.size();
	}
	
	/**
	 * Creates a hard copy of this element and returns a new instance of Gradient.
	 * @return The hard copy of this gradient.
	 */
	public Gradient copy() {
		Gradient g = new Gradient();
		g.input = input.copy();
		g.rawGradients = new Coord[rawGradients.length];
		g.cumulatedGradients = new Coord[cumulatedGradients.length];
		System.arraycopy(rawGradients, 0, g.rawGradients, 0, rawGradients.length);
		System.arraycopy(cumulatedGradients, 0, g.cumulatedGradients, 0, cumulatedGradients.length);
		g.area = area;
		g.computedCount = computedCount;
		g.magRange = magRange;
		
		return g;
	}
	
	/**
	 * Return the input image - the foundation for the gradient estimation.
	 * @return The input image.
	 */
	public RVImage img() {
		return input;
	}
	
	/**
	 * The gradient vectors will be computed as the mean vector of the gradients 
	 * in a small neighborhood. The neighborhood is defined by the 'area' parameter.
	 * @return The neighborhood of the gradient estimation.
	 */
	public int area() {
		return area;
	}
	
	/**
	 * The gradient vectors will be computed as the mean vector of the gradients 
	 * in a small neighborhood. The neighborhood is defined by the 'area' parameter.
	 * @param area The new neighborhood for gradient vector estimation. area &gt;= 1
	 */
	public void setArea(int area) {
		
		if (this.area == area) {
			return;
		}
		
		if (area < 1) {
			area = 1;
		}
		
		if (area % 2 == 0) {
			area++;
		}
		
		this.area = area;
		
		// cumulated gradients must be re-calculated because the neighborhood
		// changed
		magRange = null;
		cumulatedGradients = new Coord[input.size()];
		computedCount = 0;
	}
	
	/**
	 * Returns the minimum of vector magnitude in all gradient vectors. 
	 * @return The minimum of magnitude.
	 */
	public float minMag() {
		computeFull();
		return magRange.min();
	}
	
	/**
	 * Returns the maximum of vector magnitude in all gradient vectors. 
	 * @return The maximum of magnitude.
	 */
	public float maxMag() {
		computeFull();
		return magRange.max();
	}
	
	/**
	 * Estimates the range of magnitudes. Missing gradient vectors will 
	 * be calculated. After the call of this function all elements of cumulatedGradients
	 * will be estimated.
	 */
	private void computeFull() {
		if (magRange == null) {
			magRange = new Range();
			for (int y = 0; y < input.height(); y++) {
				for (int x = 0; x < input.width(); x++) {
					int index = y * input.width() + x;
					// check if vector is estimated and calculate vector
					// if needed
					if (cumulatedGradients[index] == null) {
						computeCumulated(x, y);
					}

					Coord grad = cumulatedGradients[index];
					magRange.record(grad.mag());
				}
			}
		}
	}
	
	/**
	 * Computes the mean gradient vector in the neighborhood defined by the
	 * area parameter at location (x, y). Stores the calculated value in 
	 * the cumulatedGradients array.
	 * @param x The x location of the mean vector.
	 * @param y The y location of the mean vector.
	 */
	private void computeCumulated(int x, int y) {
		
		int index = y * input.width() + x;
		
		// skip if value already exists
		if (cumulatedGradients[index] != null) {
			return;
		}	
		
		
		Coord grad = null;
		
		for (int i = 0; i < area; i++) {
			int yy = y - area / 2 + i;
			for (int j = 0; j < area; j++) {
				int xx = x - area / 2 + j;
				
				Coord local = getRaw(xx, yy);
				
				if (grad == null) {
					grad = local.copy();
				} else {
					grad.add(local);
				}
			}
		}
		
		grad.mult(1f / (area * area));
		
		
		cumulatedGradients[index] = grad;
		computedCount++;
		
		// clear temp values when not needed anymore
		if (computedCount == input.size()) {
			rawGradients = null;
		}
	}

	
	
	/**
	 * Computes the gradient vector at the given position. Stores the calculated
	 * value in the rawGradients array. Creates a new rawGradients array if it
	 * doesn't exist previously.
	 * @param x
	 * @param y
	 */
	private void computeRaw(int x, int y) {
		if (rawGradients == null) {
			rawGradients = new Coord[this.input.size()];
		}
		
		float[] gradVec = input.gradient(x, y);
		rawGradients[y * input.width() + x] = new Coord(gradVec[0], gradVec[1]);
	}
	
	/**
	 * Gets a gradient vector from the rawGradients array. Will compute the
	 * value of the gradient at the input location if needed.
	 * @param x The x coordinate of the gradient vector. Will be limited to the 
	 * range of the input image if needed.
	 * @param y The y coordinate of the gradient vector. Will be limited to the 
	 * range of the input image if needed.
	 * @return The gradient vector at position (x, y).
	 */
	private Coord getRaw(int x, int y) {
		
		if (x < 0) {
			x = 0;
		} else if (x > input.width() - 1) {
			x = input.width() - 1;
		}
		
		if (y < 0) {
			y = 0;
		} else if (y > input.height() - 1) {
			y = input.height() - 1;
		}
		
		int index = y * input.width() + x;
		if (rawGradients == null || rawGradients[index] == null) {
			computeRaw(x, y);
		}
		
		return rawGradients[index];
	}
	
	
	
	/**
	 * Gets the (mean) gradient vector at location (x, y).
	 * @param x The x coordinate (in range of the input image width).
	 * @param y The y coordinate (in range of the input image height).
	 * @return The gradient vector at the given location.
	 */
	public Coord get(int x, int y) {
		if (x < 0) {
			x = 0;
		} else if (x > input.width() - 1) {
			x = input.width() - 1;
		}
		
		if (y < 0) {
			y = 0;
		} else if (y > input.height() - 1) {
			y = input.height() - 1;
		}
		
		int index = y * input.width() + x;
		if (cumulatedGradients[index] == null) {
			computeCumulated(x, y);
		}
	
		
		return cumulatedGradients[index];
	}
	
}
