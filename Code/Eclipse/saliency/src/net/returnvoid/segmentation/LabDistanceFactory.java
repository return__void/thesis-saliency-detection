package net.returnvoid.segmentation;

import net.returnvoid.color.LabColor;
import net.returnvoid.functions.DistanceFct;

/**
 * Collection of distance functions for LabColor elements.
 * 
 * @author Diana Lange
 *
 */
public class LabDistanceFactory {

	/**
	 * Creates and returns a distance function for LabColor elements. 
	 * @return The Euclidean distance function for LabColor.
	 */
	public static DistanceFct<LabColor> euclideanDistanceFct() {


		DistanceFct<LabColor> fct = (a, b) -> {

			if (a.getColor() == b.getColor()) {
				return 0;
			}

			float deltaL = a.getLuminance() - b.getLuminance();
			float deltaA = a.getA() - b.getA();
			float deltaB = a.getB() - b.getB();


			return (float) Math.sqrt(deltaL * deltaL + deltaA * deltaA + deltaB * deltaB);
		};
		return fct;
	}
	
	/**
	 * Creates and returns a distance function for LabColor elements. This function
	 * converts the Lab values to RGB and uses the RGB values to measure the
	 * distance.
	 * @return The Euclidean distance function for LabColor.
	 */
	public static DistanceFct<LabColor> RGBEuclideanDistanceFct() {


		DistanceFct<LabColor> fct = (a, b) -> {

			if (a.getColor() == b.getColor()) {
				return 0;
			}
			
			int c1 = a.getColor();
			int c2 = b.getColor();

			float deltaR = (c1 >> 16 & 255) - (c2 >> 16 & 255);
			float deltaG = (c1 >> 8 & 255) - (c2 >> 8 & 255);
			float deltaB = (c1 & 255) - (c2 & 255);


			return (float) Math.sqrt(deltaR * deltaR + deltaG * deltaG + deltaB * deltaB);
		};
		return fct;
	}
	
	/**
	 * Creates and returns a distance function for LabColor elements. This function
	 * converts is a linear combination of Lab euclidean distance and RGB euclidean
	 * distance.
	 * @return The Euclidean distance function for LabColor.
	 */
	public static DistanceFct<LabColor> RGBPlusLabEuclideanDistanceFct() {


		DistanceFct<LabColor> fct = (a, b) -> {

			DistanceFct<LabColor> lab = LabDistanceFactory.euclideanDistanceFct();
			DistanceFct<LabColor> rgb = LabDistanceFactory.RGBEuclideanDistanceFct();
			
			// normalize distances by upper bounds to make
			// both measures equally important
			return lab.distance(a, b) / 350f + rgb.distance(a, b) / 441.673f;
		};
		return fct;
	}

	/**
	 * Creates and returns a distance function for LabColor elements. 
	 * @return The Manhatten distance function for LabColor.
	 */
	public static DistanceFct<LabColor> manhattenDistanceFct() {

		DistanceFct<LabColor> fct = (a, b) -> {

			if (a.getColor() == b.getColor()) {
				return 0;
			}

			float deltaL = a.getLuminance() - b.getLuminance();
			float deltaA = a.getA() - b.getA();
			float deltaB = a.getB() - b.getB();

			if (deltaL < 0) {
				deltaL *= -1;
			}

			if (deltaA < 0) {
				deltaA *= -1;
			}

			if (deltaB < 0) {
				deltaB *= -1;
			}


			return deltaL + deltaA + deltaB;
		};
		return fct;
	}
}
