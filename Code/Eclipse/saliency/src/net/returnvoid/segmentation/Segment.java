package net.returnvoid.segmentation;

import java.awt.Rectangle;

/**
 * A basic interface for Segments. Segments are defined by the area / region that
 * they cover.
 * @author Diana Lange
 *
 */
public interface Segment {
	
	/** 
	 * This function returns a subset of points() - the outline of the segment. 
	 * Edge points have usually less than 8 neighbors within the segment. If the 
	 * array entry has a third element, than this third element is the number of 
	 * neighbors this point has within the segment, i.e. point[index][2] = numberOfNeighbors. The
	 * other two elements of each edge element are the x and y coordinates.
	 * @return A subset of points() which is the outline of the segment.
	 */
	int[][] edge();
	
	/**
	 * Returns a set of coordinates. This set defines the region of the segment.
	 * Each element in the array represents one coordinate, i.e. point[index][0] = x
	 * and point[index][1] = y. If the array entry has a third element, than this
	 * third element is the number of neighbors this point has within the segment,
	 * i.e. point[index][2] = numberOfNeighbors.
	 * @return A collection of points which are equal to the region of the segment.
	 */
	int[][] points();
	
	/**
	 * Returns a box which covers the whole area of the segment.
	 * @return The bounding box of the segment.
	 */
	Rectangle boundingBox();
	
}
