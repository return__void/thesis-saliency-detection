
/**
 * The classes of this package are designed to perform image segmentation. The main
 * class is SLIC (Simple linear iterative clustering), which produces 
 * over-segmented results based on a clustering process in CieLab color space.
 * 
 * @author Diana Lange
 *
 */
package net.returnvoid.segmentation;