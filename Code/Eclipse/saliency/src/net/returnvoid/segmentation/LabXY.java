package net.returnvoid.segmentation;


import net.returnvoid.color.LabColor;
import net.returnvoid.functions.DistanceFct;
import net.returnvoid.functions.MeanFct;
import net.returnvoid.image.LabImg;
import net.returnvoid.math.Coord;
import net.returnvoid.math.XY;

/**
 * A class for mapping spatial locations to LabColor elements.
 * @author Diana Lange
 *
 */
public class LabXY implements XY {

	/**
	 * The color of the element.
	 */
	private LabColor lab;

	/**
	 * The x coordinate of the element.
	 */
	private float x;
	
	/**
	 * The y coordinate of the element.
	 */
	private float y;

	/**
	 * A distance function for LabXY elements.
	 */
	private static DistanceFct<LabXY> manhattenDistanceFct = null;
	
	/**
	 * A distance function for LabXY elements.
	 */
	private static DistanceFct<LabXY> euclideanDistanceFct = null;
	
	/**
	 * A mean function for LabXY elements.
	 */
	private static MeanFct<LabXY> meanFct = null;

	/**
	 * Creates a new LabXY element with the given color and spatial location.
	 * @param img The LabColor value will be equal to the LabColor of the 
	 * image at the input (x,y) position.
	 * @param x The x coordinate of the element.
	 * @param y The y coordinate of the element.
	 */
	public LabXY(LabImg img, float x, float y) {
		this(img.get((int) x, (int) y), x, y);
	}

	/**
	 * Creates a new LabXY element with the given color and spatial location.
	 * @param lab The color of the element.
	 * @param x The x coordinate of the element.
	 * @param y The y coordinate of the element.
	 */
	public LabXY(LabColor lab, float x, float y) {
		this.lab = lab;
		this.x = x;
		this.y = y;
	}

	/**
	 * Updates the value of this element. All values will get overwritten by
	 * the input.
	 * @param img The new LabColor value will be equal to the LabColor of the 
	 * image at the input (x,y) position.
	 * @param x The new x coordinate.
	 * @param y The new y coordinate.
	 */
	public void set(LabImg img, float x, float y) {
		set(img.get((int) x, (int) y), x, y);
	}

	/**
	 * Updates the value of this element. All values will get overwritten by
	 * the input.
	 * @param lab The new LabColor value.
	 * @param x The new x coordinate.
	 * @param y The new y coordinate.
	 */
	public void set(LabColor lab, float x, float y) {
		this.lab = lab;
		this.x = x;
		this.y = y;
	}

	/**
	 * The LabColor part of this element.
	 * @return The LabColor part of this element.
	 */
	public LabColor lab() {
		return lab;
	}

	/**
	 * The hexadecimal encoded RGB color value of the element.
	 * @return The hexadecimal encoded RGB color.
	 */
	public int hex() {
		return lab.getColor();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#x()
	 */
	public int x() {
		return (int) x;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#y()
	 */
	public int y() {
		return (int) y;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#fx()
	 */
	public float fx() {
		return x;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.math.XY#fy()
	 */
	public float fy() {
		return y;
	}

	/**
	 * The luminance value of LabColor.
	 * @return The luminance value.
	 */
	public float l() {
		return lab.getLuminance();
	}

	/**
	 * The a-color value of LabColor.
	 * @return The a value.
	 */
	public float a() {
		return lab.getA();
	}

	/**
	 * The b-color value of LabColor.
	 * @return The b value.
	 */
	public float b() {
		return lab.getB();
	}

	/**
	 * Makes a hard copy of the element.
	 * @return The new instance with equal values of the element.
	 */
	public LabXY copy() {

		return new LabXY(lab.copy(), x, y);
	}


	/**
	 * Estimates if two LabXY elements are neighbors. Two LabXY elements are 
	 * neighbors if their spatial locations in discrete domain are next to each 
	 * other. The LabXY are therefore thought as pixels and the calculation is
	 * based on a 8-neighborhood grid.
	 * @param other The other LabXY element.
	 * @return True if the other element is spatially located next to the element.
	 */
	public boolean neighbor(LabXY other) {
		return Coord.neighbor((int) x, (int) y,(int) other.x, (int) other.y);
	}

	/**
	 * Calculates the distance between two elements using Manhatten distance function.
	 * @param other The element which the distance should be estimated to.
	 * @return The distance between the elements.
	 */
	public float distance(LabXY other) {
		return distance(other, LabXY.manhattenDistanceFct());
	}

	/**
	 * Calculates the distance between two elements using the given distance function.
	 * @param other The element which the distance should be estimated to.
	 * @param fct A distance function for LabXY elements.
	 * @return The distance between the elements.
	 */
	public float distance(LabXY other, DistanceFct<LabXY> fct) {
		return fct.distance(this, other);
	}

	/**
	 * Creates and returns a distance function for LabXY elements, which is a 
	 * composition of a distance function for LabColor distance and spatial distance. 
	 * @return The Manhatten distance function for LabXY.
	 */
	public static DistanceFct<LabXY>manhattenDistanceFct() {

		if (LabXY.manhattenDistanceFct == null) {
			LabXY.manhattenDistanceFct = (a, b) -> {
				DistanceFct<LabColor> colFct = LabDistanceFactory.manhattenDistanceFct();
				DistanceFct<XY> coordFct = Coord.manhattenDistanceFct();

				return coordFct.distance(a, b) + colFct.distance(a.lab(), b.lab());
			};

		}

		return LabXY.manhattenDistanceFct;
	}

	/**
	 * Creates and returns a distance function for LabXY elements, which is a 
	 * composition of a distance function for LabColor distance and spatial distance. 
	 * @return The Euclidean distance function for LabXY.
	 */
	public static DistanceFct<LabXY>euclideanDistanceFct() {

		if (LabXY.euclideanDistanceFct == null) {
			LabXY.euclideanDistanceFct = (a, b) -> {
				DistanceFct<LabColor> colFct = LabDistanceFactory.euclideanDistanceFct();
				DistanceFct<XY> coordFct = Coord.euclideanDistanceFct();

				return coordFct.distance(a, b) + colFct.distance(a.lab(), b.lab());
			};

		}

		return LabXY.euclideanDistanceFct;
	}

	/**
	 * Creates and returns a mean creator function for LabXY elements.
	 * @return A mean creator function for LabXY elements which can be used to 
	 * calculate a mean value for LabXY elements.
	 */
	public static MeanFct<LabXY> meanFct() {

		if (LabXY.meanFct == null) {
			LabXY.meanFct = arr -> {
				float meanL = 0;
				float meanA = 0;
				float meanB = 0;
				float meanX = 0;
				float meanY = 0;

				for (LabXY e : arr) {

					meanL += e.l();
					meanA += e.a();
					meanB += e.b();

					meanX += e.x();
					meanY += e.y();
				}

				if (arr != null && arr.size() != 0) {
					meanL /= arr.size();
					meanA /= arr.size();
					meanB /= arr.size();

					meanX /= arr.size();
					meanY /= arr.size();
				}
				return new LabXY(new LabColor(meanL, meanA, meanB), meanX, meanY);

			};
		}
		return LabXY.meanFct;
	}



}
