package net.returnvoid.segmentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.returnvoid.color.LabColor;
import net.returnvoid.functions.DistanceFct;
import net.returnvoid.graphics.grid.GridMaker;
import net.returnvoid.graphics.grid.TableGrid;
import net.returnvoid.image.IntensityImg;
import net.returnvoid.image.LabImg;
import net.returnvoid.math.Coord;
import net.returnvoid.math.Range;
import net.returnvoid.math.XY;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

/**
 * Performs SLIC segmentation on images. The SLIC algorithm produces compact and
 * connected segments which adhere the edges within the image. The number of
 * segments is set by the user. The output segments are roughly uniform in size.
 * The implementation is based on: <a href="https://ieeexplore.ieee.org/document/6205760/" target="_blank">https://ieeexplore.ieee.org/document/6205760/</a>
 * @author Diana Lange
 *
 */
public class SLIC {

	/**
	 * The input number of segments.
	 */
	private int K;

	/**
	 * The approx. width / height for each segment.
	 */
	private float S;

	/**
	 * The segments of this segmentation. 
	 */
	private ArrayList<Superpixel> superpixels;

	/**
	 * The input image might be scaled before segmentation. This value gives
	 * the scaling of the input image. The scaling is defined by the value
	 * of maxPixels.  
	 */
	private float zoom;

	/**
	 * A Lab version of the input image. 
	 */
	private LabImg img;

	/**
	 * The maximum of pixels used for any segmentation process. If the input image
	 * has more pixels it will be resized.
	 */
	public static int maxPixels = 300000;

	/**
	 * The segmentation process will stop after this number of iterations. The
	 * process may be stopped earlier if the error threshold is exceeded. 
	 */
	public static int maxIterations = 10;

	/**
	 * The segmentation process will stopped if this threshold is exceeded.
	 */
	public static float stopDeltaError = 0.98f;

	/**
	 * If true, then the current state of the process is printed in the terminal.
	 */
	public static boolean debug = false;

	/**
	 * Defines the number of parallel running threads for easy tasks.
	 */
	public static int threadMultiplierSimple = 20;

	/**
	 * Defines the number of parallel running threads for complex tasks.
	 */
	public static int threadMultiplierComplex = 1;

	/**
	 * Sets if all segments should be connected. If so an additional step will
	 * be done after the clustering process.
	 */
	public static boolean enforceConnectivity = true;

	/**
	 * Contains all coordinates that a currently member of any of the segments.
	 * The key is the x coordinate, the value the y coordinate.
	 */
	private HashMap<Integer, Integer> visitedStorage = new HashMap<Integer, Integer>();

	/**
	 * A set of all LabXY elements, i.e. all pixels of the input image.
	 */
	private ArrayList<LabXY> elements;

	/**
	 * Error in the current iteration.
	 */
	private float error = -1;

	/**
	 * Contains the range of the superpixel sizes. The sizes of superpixels
	 * are equal to the number of elements they contain.
	 */
	private Range superpixelSizeRange = null;

	/**
	 * The distance function for the clustering process.
	 */
	private DistanceFct<LabXY> cluteringDistanceFct = null;

	/**
	 * Creates a new segmentation where the segmentation is already known. 
	 * @param img The input image.
	 * @param superpixels The segmentation.
	 */
	private SLIC(LabImg img, ArrayList<Superpixel> superpixels) {
		this.img = img;
		this.superpixels = superpixels;
		this.K = superpixels.size();
		this.S = (float) Math.sqrt(this.img.size() / this.K);
		this.zoom = 1;
	}

	/**
	 * Creates a new SLIC instance and starts the segmentation of K segments. The
	 * process can not be restarted after it is finished.
	 * @param inputImg The foundation for the segmentation. The image might be
	 * resized complexity reduction. The instance of the input however will not
	 * be altered.
	 * @param K The number of desired segments.
	 */
	public SLIC(PImage inputImg, int K) {
		this(inputImg, K, null);
	}
	
	/**
	 * Creates a new SLIC instance and starts the segmentation of K segments. The
	 * process can not be restarted after it is finished.
	 * @param inputImg The foundation for the segmentation. The image might be
	 * resized complexity reduction. The instance of the input however will not
	 * be altered.
	 * @param K The number of desired segments.
	 */
	public SLIC(IntensityImg inputImg, int K) {
		this(inputImg.img(), K, null);
	}

	/**
	 * Creates a new SLIC instance and starts the segmentation of K segments. The
	 * process can not be restarted after it is finished.
	 * @param inputImg The foundation for the segmentation. The image might be
	 * resized complexity reduction. The instance of the input however will not
	 * be altered.
	 * @param K The number of desired segments.
	 * @param distFct A function that will be used for the segmentation process.
	 */
	public SLIC(PImage inputImg, int K, DistanceFct<LabXY> distFct) {
		this.cluteringDistanceFct = distFct;
		this.img = new LabImg(resizeImage(inputImg));
		this.zoom = (float) inputImg.width / this.img.width();

		// convert all colors of img to labxy which will be used
		// during clustering repetitively
		this.elements = new ArrayList<LabXY>(this.img.size());
		for (int y = 0; y < img.height(); y++) {
			for (int x = 0; x < img.width(); x++) {
				elements.add(img.labxy(x, y));
			}
		}

		// init segments with a center labxy color
		TableGrid grid = GridMaker.createTable(0, 0, img.width(), img.height(), K);
		this.superpixels = new  ArrayList<Superpixel>(grid.size());

		for (int i = 0; i < grid.size(); i++) {

			LabXY center = img.labxy(grid.getX(i).intValue(), grid.getY(i).intValue());
			Superpixel pix = new Superpixel(center);
			pix.initCenter(img, 3);
			superpixels.add(pix);
		}

		// update K since the grid will properly not have K cells.
		this.K = this.superpixels.size();
		this.S = (float) Math.sqrt(this.img.size() / this.K);

		// start segmentation
		this.segmentImage();

		// after segmentation there is no use for these temp data
		// free the space
		this.visitedStorage.clear();
		this.elements.clear();
	}

	/**
	 * Returns the current distance function of the SLIC segmentation. If there is
	 * none set, a function will be created. This default distance function is
	 * a composition of LabColor distance and spatial distance.
	 * @return The distance function for the clustering process.
	 */
	public DistanceFct<LabXY>clusteringDistanceFct() {

		if (cluteringDistanceFct == null) {
			cluteringDistanceFct= (a, b) -> {
				DistanceFct<LabColor> colFct = LabDistanceFactory.euclideanDistanceFct();
				DistanceFct<XY> coordFct = Coord.euclideanDistanceFct();

				return coordFct.distance(a, b) + 10 / S * colFct.distance(a.lab(), b.lab());
			};
		}

		return cluteringDistanceFct;
	}
	
	/**
	 * Merges all segments that are neighbors and that have at have similar colors. 
	 * @return A new SLIC segmentation with the merged segments. 
	 */
	public SLIC mergeSegments() {
		return mergeSegments(clusteringDistanceFct());
	}

	/**
	 * Merges all segments that are neighbors and that have at have similar colors. 
	 * @param fct A distance function for LabXY elements. The distance will be 
	 * estimated via the centers of segments.
	 * @return A new SLIC segmentation with the merged segments. 
	 */
	public SLIC mergeSegments(DistanceFct<LabXY> fct) {
		float treshold = this.meanNeighborSegmentDistance(cluteringDistanceFct);
		return mergeSegments(fct, 0.66f * treshold);
	}

	/**
	 * Merges all segments that are neighbors and that have at least a 'threshold' 
	 * distance to each other using the input distance function. The threshold can
	 * be estimated via meanNeighborSegmentDistance().
	 * @param fct A distance function for LabXY elements. The distance will be 
	 * estimated via the centers of segments.
	 * @param threshold Neighboring segments that have distance less than this 
	 * threshold will be merged. 
	 * @return A new SLIC segmentation with the merged segments. 
	 */
	public SLIC mergeSegments(DistanceFct<LabXY> fct, float threshold) {

		// a set of segments that have been merged with some other segment
		HashMap<Superpixel, Superpixel> visited = new HashMap<Superpixel, Superpixel>();

		// a set of merged segments
		ArrayList<Superpixel> merged = new ArrayList<Superpixel>();

		// the segmentation should be sorted by size starting with the biggest
		// segments. Prefer to merge small segments.
		for (int i = superpixels.size() - 1; i >= 0; i--) {
			Superpixel sp = superpixels.get(i);

			// skip if this segment has already been merged
			if (visited.containsKey(sp)) {
				continue;
			}

			Superpixel pix = sp.copy();

			// look for neighboring segments that have a smaller distance than
			// the threshold
			for (int j = superpixels.size() - 1; j >= 0; j--) {
				Superpixel other = superpixels.get(j);

				// skip if this segment has already been merged
				if (sp == other || visited.containsKey(other)) {
					continue;
				}

				if (SimpleSegment.neighbor(sp, other, true)) {
					float d = fct.distance(sp.center(), other.center());

					if (d < threshold) {
						visited.put(other, other);
						pix.add(other);
					}
				}
			}

			// keep track of visited and merged segments
			merged.add(pix);
			visited.put(sp, sp);
		}

		// sort result by segment size
		Collections.sort(merged, (a, b) -> {
			if (a.size() == b.size()) {
				return 0;
			} else if (a.size() > b.size()) {
				return -1;
			} else {
				return 1;
			}
		});

		SLIC segmentation = new SLIC(this.img, merged);
		segmentation.updateCenters();

		return segmentation;
	}

	/**
	 * Calculates the mean distance that the segments have to their neighboring
	 * segments.
	 * @param fct The distance function that is used to estimate the distance
	 * to the neighbor segments. The distance function will be applied to 
	 * the center elements of the segments.
	 * @return The mean distance of the segments to their neighbors.
	 */
	public float meanNeighborSegmentDistance(DistanceFct<LabXY> fct) {
		float sum = 0;
		int matches = 0;

		for (Superpixel pix : superpixels) {

			for (Superpixel other : superpixels) {
				if (pix == other || !SimpleSegment.neighbor(pix, other, true)) {
					continue;
				}

				sum += fct.distance(pix.center(), other.center());
				matches++;
			}
		}
		return sum / matches;
	}

	/**
	 * Returns the segments of the segmentation.
	 * @return The segments of the segmentation.
	 */
	public ArrayList<Superpixel> superpixels() {
		return superpixels;
	}

	/**
	 * The number of segments.
	 * @return The number of segments.
	 */
	public int K() {
		return K;
	}

	/**
	 * The approx. width / height of each segment.
	 * @return The approx. width / height of each segment.
	 */
	public float gridSize() {
		return S;
	}

	/**
	 * The scale factor that has been applied to the input image.
	 * @return The scaling of the input image.
	 */
	public float zoom() {
		return zoom;
	}

	/**
	 * Returns the image that has been used for the segmentation process. This
	 * might be a resized version of the original input.
	 * @return The image.
	 */
	public LabImg labimg() {
		return img;
	}

	/**
	 * Returns the range of segment sizes. The segment sizes are the number of
	 * elements per segment.
	 * @return The range of segment size of this segmentation.
	 */
	public Range superpixelSize() {

		if (superpixelSizeRange == null) {

			superpixelSizeRange = new Range();

			for (Superpixel px : superpixels) {
				superpixelSizeRange.record(px.size());	
			}

		}

		return superpixelSizeRange;
	}

	/**
	 * Resizes the input image if the number of pixels is bigger than maxPixels.
	 * Returns a copy of the input image or the resized version of it.
	 * @param input The input image. This instance will not be altered.
	 * @return The new instance of the image.
	 */
	private PImage resizeImage(PImage input) {
		PImage copyImg = input.copy();

		if (copyImg.pixels.length > SLIC.maxPixels) {

			float aspectRatio = (float) copyImg.height / copyImg.width;
			float targetW = (float) Math.sqrt((float) SLIC.maxPixels / aspectRatio);

			copyImg.resize((int) Math.floor(targetW), 0);
		}

		return copyImg;
	}
	
	/**
	 * Writes the segment information (color and boundaries) to an image.
	 * @return An image representation of the segmentation.
	 */
	public LabImg writeToImage() {
		LabImg container = new LabImg(labimg().width(), labimg().height());
		
		return SLIC.writeToImage(superpixels, container);
	}

	/**
	 * Writes the segment information (color and boundaries) to the container image.
	 * @param elements A set of segments.
	 * @param container The container image for the visualization of the segments.
	 * @return The altered container.
	 */
	public static LabImg writeToImage(ArrayList<Superpixel> elements, LabImg container) {

		for (Superpixel sp : elements) {

			LabColor col = sp.center().lab(); 

			for (int[] p : sp.points()) {
				container.set(p[0], p[1], col);
			}
		}


		container.updatePixels();

		return container;
	}

	/**
	 * A set of the centers of all segments.
	 * @return A set of the centers of all segments.
	 */
	public ArrayList<LabXY> centers() {
		ArrayList<LabXY> centers = new ArrayList<LabXY>(superpixels.size());
		for (Superpixel sp : superpixels) {
			centers.add(sp.center());
		}

		return centers;
	}

	/**
	 * Starts the segmentation process. The segments should be initialized properly before. 
	 */
	private void segmentImage() {


		float lastError = error;
		for (int i = 0; i < SLIC.maxIterations; i++) {

			if (SLIC.debug) {
				System.out.println("Iteration " + i + " of " + SLIC.maxIterations);
			}

			visitedStorage.clear();

			cluster();
			float deltaError = lastError == -1 ? 0 : error / lastError;

			if (SLIC.debug) {
				System.out.println("delta Error: " + deltaError + ", absolute Error: " + error);
			}
			if (i > 3 && deltaError > SLIC.stopDeltaError) {
				break;
			}

			lastError = error;

		}

		if (SLIC.enforceConnectivity) {
			enforceConnectivity();
		}

		// sort segments by size
		Collections.sort(superpixels, (a, b) -> {
			if (a.size() == b.size()) {
				return 0;
			} else if (a.size() > b.size()) {
				return -1;
			} else {
				return 1;
			}
		});
	}

	/**
	 * Enforce the connectivity of segments by removing disconnected regions
	 * from segments and adding them to other segments.
	 */
	private void enforceConnectivity() {

		// calculate connected regions
		// each superpixel can have multiple connected regions at this point
		// remove detached regions from superpixel and store these regions in 'looseRegions'
		// look for superpixel that have no elements and store them in 'emptySuperpixels'

		LooseRegionManager looseMng = new LooseRegionManager(SLIC.threadMultiplierComplex);
		while(looseMng.getState() != Thread.State.TERMINATED) {		
			// block other processes until the threads are finished
		}

		ArrayList<Superpixel> emptySuperpixels = new ArrayList<Superpixel>();
		ArrayList<ArrayList<LabXY>> looseRegions = new ArrayList<ArrayList<LabXY>>();
		emptySuperpixels.addAll(looseMng.empty());
		looseRegions.addAll(looseMng.regions());

		looseMng.clear();
		looseMng = null;

		// remove superpixels that have no elements

		for (Superpixel empty : emptySuperpixels) {
			superpixels.remove(empty);
		}

		// loose pixels = pixels that are not member of any superpixel yet
		// mark loose pixel as loose regions that will
		// be merged with some other existing superpixel by adding them to "looseRegions"
		for (int y = 0; y < img.height(); y++) {
			for (int x = 0; x < img.width(); x++) {
				Integer key = y * img.width() + x;

				if (!visitedStorage.containsKey(key)) {
					LabXY labxy = img.labxy(x, y);
					ArrayList<LabXY> region = new ArrayList<LabXY>();
					region.add(labxy);
					looseRegions.add(region);
				}
			}
		}

		// find new superpixels for loose regions
		// these new superpixels are connected to the loose region

		// defines the search area for the loose regions
		float minSpatialDistanceFactor = 4;  

		// keep track which regions have already been merged with another superpixel
		ArrayList<ArrayList<LabXY>> notMerged = new ArrayList<ArrayList<LabXY>>();
		notMerged.addAll(looseRegions);

		int count = 0;

		DistanceFct<LabColor> colDistFct = LabDistanceFactory.manhattenDistanceFct();

		if (SLIC.debug) {
			System.out.println("number of loose regions: " + looseRegions.size());
		}

		// start with a small search area
		// if not all loose regions could be attached to another segment then
		// the search area will be enlarged for the next iteration
		while(notMerged.size() > 0 && count < 4) {

			for (ArrayList<LabXY> region : looseRegions) {
				Superpixel newSp = new Superpixel(region);
				Superpixel mergePixel = null;

				float minD = -1;

				// find best matching connected segment
				// if there is more than one match, merge with the segment
				// that has the most similar mean color
				for (Superpixel candidate : superpixels) {

					if (Coord.euclideanDistanceFct().distance(newSp.center(), candidate.center()) < S * minSpatialDistanceFactor && SimpleSegment.neighbor(newSp, candidate, false)) {

						float d = colDistFct.distance(candidate.center().lab(), newSp.center().lab());

						if (mergePixel == null || d < minD) {
							minD = d;
							mergePixel = candidate;
						}

						if (d == 0) {
							break;
						}
					}
				}

				if (mergePixel != null) {
					mergePixel.add(newSp);
					notMerged.remove(region);
				}
			}
			count++; // avoid endless iteration
			minSpatialDistanceFactor++; // make search area bigger for next iteration

			if (count != 3) {
				looseRegions.clear();
				looseRegions.addAll(notMerged);
			}
		}

		// could not find a match for the loose region -> add it as new superpixel
		// this should not happen ever, but better be save than sorry!
		if (notMerged.size() > 0) {
			for (ArrayList<LabXY> region : notMerged) {
				Superpixel newSp = new Superpixel(region);
				superpixels.add(newSp);
			}
		}

		updateCenters();
	}

	/**
	 * Updates the centers of the segments, i.e. creates the mean values of the
	 * current member elements.
	 */
	private void updateCenters() {
		UpdateCenterManager centerMng = new UpdateCenterManager(SLIC.threadMultiplierSimple);
		while(centerMng.getState() != Thread.State.TERMINATED) {
			// Block until update is finished
		}

		error = centerMng.error();
		centerMng = null;
	}

	/**
	 * Do one iteration of segmentation. The segments should be initialized before.
	 * The centers of the segments will be updated after segmentation.
	 */
	private void cluster() {

		ClusterThreadManager m1 =  new ClusterThreadManager(SLIC.threadMultiplierComplex);

		while(m1.getState() != Thread.State.TERMINATED) {
			// block until segmentation is done
		}

		m1.clear();
		m1 = null;

		updateCenters();
	}


	/**
	 * Draws the segments as a convex hull with the mean color of the segment.s
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void drawHull(PGraphics g, float offsetX, float offsetY, float w) {
		for (Superpixel p : superpixels) {
			p.drawHull(g, offsetX, offsetY, w / img.width());
		}
	}

	/**
	 * Visualizes the segments as ellipses with the mean color of the segment 
	 * and the diameter giving the size of the segments. 
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void drawSuperpixelSize(PGraphics g, float offsetX, float offsetY, float w) {
		g.fill(0);
		g.noStroke();
		g.rect(offsetX, offsetY, w, w / img.width() * img.height());
		for (Superpixel p : superpixels) {
			p.drawSize(g, offsetX, offsetY, w / img.width(), S * 0.01f);
		}
	}

	/**
	 * Draws the segments, i.e. all the pixel of the segments with the mean / center color.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void drawPixels(PGraphics g, float offsetX, float offsetY, float w) {

		for (Superpixel p : superpixels) {

			p.drawPixels(g, offsetX, offsetY, w / img.width());
		}
	}

	/**
	 * Visualizes the bounding boxes of the segments using a black outline.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void drawBoundingBoxes(PGraphics g, float offsetX, float offsetY, float w) {

		for (Superpixel p : superpixels) {

			p.drawBoundingBox(g, offsetX, offsetY, w / img.width());
		}
	}

	/**
	 * Draws the segmentation.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void draw(PGraphics g, float offsetX, float offsetY, float w) {
		g.image(SLIC.writeToImage(superpixels, img.copy()).img(), offsetX, offsetY, w, w / img.width() * img.height());
	}

	/**
	 * Draws the segmentation (only outlines of segments).
	 * @param parent The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param w The width of the drawing.
	 */
	public void drawEdge(PApplet parent, float offsetX, float offsetY, float w) {
		float scale = w / img.width();

		PImage output = SimpleSegment.edgeImage(superpixels(), 0xFF000000, img.width(), img.height());
		parent.image(output, offsetX, offsetY, w, scale * img.height());

	}

	/**
	 * Runs the estimation of connected regions for all segments.
	 * @author Diana Lange
	 */
	private class LooseRegionManager extends Thread {

		private ArrayList<ArrayList<LabXY>> unionLooseRegions = new ArrayList<ArrayList<LabXY>>();
		private ArrayList<Superpixel> unionEmpty = new ArrayList<Superpixel>();
		private LooseRegion[] threads;

		private LooseRegionManager(int coreMultiplicator) {
			threads  = new LooseRegion[Runtime.getRuntime().availableProcessors() * coreMultiplicator];

			int range = superpixels.size() / threads.length;
			for (int i = 0; i <  threads.length; i++) {
				int start = i * range;
				int end = start +range + 1; // not included

				if (i == threads.length - 1) {
					end = superpixels.size();
				}

				threads[i] = new LooseRegion(start, end);
			}

			this.start();
		}

		public void clear() {
			for (int i = 0; i < threads.length; i++) {
				threads[i].clear();
				threads[i] = null;
			}

			this.unionEmpty.clear();
			this.unionLooseRegions.clear();
			this.unionEmpty = null;
			this.unionLooseRegions = null;
		}

		public ArrayList<Superpixel> empty() {
			return unionEmpty;
		}

		public ArrayList<ArrayList<LabXY>> regions() {
			return unionLooseRegions;
		}

		@Override 
		public void run() {
			boolean finished = false;
			while (!finished) {
				finished = true;
				for (LooseRegion t : threads) {
					if (t.getState() != Thread.State.TERMINATED) {
						finished = false;
						break;
					}
				}

				if (finished) {
					for (LooseRegion t : threads) {
						unionLooseRegions.addAll(t.regions());
						unionEmpty.addAll(t.empty());
					}
				}
			}
		}

		private class LooseRegion extends Thread {
			private int start;
			private int end; 
			private ArrayList<ArrayList<LabXY>> looseRegions = new ArrayList<ArrayList<LabXY>>();
			private ArrayList<Superpixel> empty = new ArrayList<Superpixel>();;

			private LooseRegion(int start, int end) {
				this.start = start;
				this.end = end;
				this.start();
			}

			public ArrayList<Superpixel>  empty() {
				return empty;
			}

			public ArrayList<ArrayList<LabXY>> regions() {
				return looseRegions;
			}

			public void clear() {
				looseRegions.clear();
				empty.clear();
				looseRegions = null;
				empty = null;
			}

			@Override
			public void run() {
				for (int i = start; i < end; i++) {
					Superpixel sp = superpixels.get(i);
					if (sp.empty()) {
						empty.add(sp);
						continue;
					}

					if (!sp.connected()) {

						ArrayList<ArrayList<LabXY>> loose = sp.getLooseRegions();
						looseRegions.addAll(loose);
						//System.out.println("before: " + sp.size());
						sp.remove(loose);
						//System.out.println("after: " + sp.size());
						//sp.updateCenter();		
					}
				}
			}
		}
	}

	/**
	 * Runs the estimation of the mean values of all segments.
	 * @author Diana Lange
	 */
	private class UpdateCenterManager extends Thread {
		private UpdateCenterThread[] threads;

		private float meanError = 0;

		private UpdateCenterManager(int coreMultiplicator) {

			threads  = new UpdateCenterThread[Runtime.getRuntime().availableProcessors() * coreMultiplicator];

			int range = superpixels.size() / threads.length;
			for (int i = 0; i <  threads.length; i++) {
				int start = i * range;
				int end = start +range + 1; // not included

				if (i == threads.length - 1) {
					end = superpixels.size();
				}

				threads[i] = new UpdateCenterThread(start, end);
			}

			this.start();
		}

		public float error() {
			return meanError;
		}

		@Override 
		public void run() {
			boolean finished = false;
			while (!finished) {
				finished = true;
				for (UpdateCenterThread t : threads) {
					if (t.getState() != Thread.State.TERMINATED) {
						finished = false;
						break;
					}
				}

				if (finished) {
					for (UpdateCenterThread t : threads) {
						meanError += t.error();
						t = null;
					}
				}
			}
		}


		private class UpdateCenterThread extends Thread {
			private int start;
			private int end; 
			private float meanError;

			private UpdateCenterThread(int start, int end) {
				this.start = start;
				this.end = end;
				this.meanError = 0;
				this.start();
			}

			public float error() {
				return meanError;
			}

			@Override
			public void run() {
				for (int i = start; i < end; i++) {
					Superpixel superpixel = superpixels.get(i);
					superpixel.updateCenter();
					meanError += superpixel.error();
				}

				meanError /= (end - start - 1);
			}
		}
	}

	/**
	 * Runs the estimation of a clustering iteration.
	 * @author Diana Lange
	 */
	private class ClusterThreadManager extends Thread {
		private ClusterThread[] threads;

		private ClusterThreadManager(int coreMultiplicator) {

			threads  = new ClusterThread[Runtime.getRuntime().availableProcessors() * coreMultiplicator];

			int range = superpixels.size() / threads.length;
			for (int i = 0; i <  threads.length; i++) {
				int start = i * range;
				int end = start +range + 1; // not included

				if (i == threads.length - 1) {
					end = superpixels.size();
				}

				threads[i] = new ClusterThread(start, end);
			}

			this.start();
		}

		public void clear() {
			for (int i = 0; i < threads.length; i++) {
				threads[i].clear();
				threads[i] = null;
			}
		}

		@Override 
		public void run() {
			boolean finished = false;
			while (!finished) {
				finished = true;
				for (ClusterThread t : threads) {
					if (t.getState() != Thread.State.TERMINATED) {
						finished = false;
						break;
					}
				}

				if (!finished) {
					try {
						this.wait(300);
					} catch(Exception e) {
					}
				}

				if (finished) {
					// connect intermediate values to the final segmentation
					// this means: find best matching segment for each LabXY
					// element (smallest distance to center)
					for (int i = 0; i < threads.length; i++) {
						ClusterThread t = threads[i];
						for (Map.Entry<Integer, float[]> entry : t.localStorage().entrySet()) {

							Integer key = entry.getKey();

							if (visitedStorage.containsKey(key)) {
								continue;
							}

							float[] value = entry.getValue();

							for (int j = i + 1; j < threads.length; j++) {

								if (threads[j].localStorage().containsKey(key) && threads[j].localStorage().get(key)[1] < value[1]) {
									value = threads[j].localStorage().get(key);
								}
							}

							LabXY element = elements.get(key.intValue());
							Superpixel pix = superpixels.get((int) value[0]);
							visitedStorage.put(key, key);

							pix.add(element);
						}

					}

				}
			}
		}


		private class ClusterThread extends Thread {
			private int start;
			private int end; 

			private HashMap<Integer, float[]> localStorage = new HashMap<Integer, float[]>();

			private ClusterThread(int start, int end) {
				this.start = start;
				this.end = end;
				this.start();
			}

			public HashMap<Integer, float[]> localStorage() {
				return localStorage;
			}

			public void clear() {
				this.localStorage.clear();
				this.localStorage = null;
			}

			@Override
			public void run() {

				DistanceFct<LabXY>distFct = clusteringDistanceFct();

				for (int i = start; i < end; i++) {
					Superpixel superpixel = superpixels.get(i);
					superpixel.clear();
					LabXY center = superpixel.center();

					float left = center.fx() - S;
					float top = center.fy() - S;
					float right = center.fx() + S;
					float bottom = center.fy() + S;

					if (left < 0) {
						left = 0;
					} 

					if (top < 0) {
						top = 0;
					}

					if (right > img.width()) {
						right = img.width();
					}

					if (bottom > img.height()) {
						bottom = img.height();
					}

					for (int y = (int) Math.floor(top); y < bottom; y++) {
						for (int x = (int) Math.floor(left); x < right; x++) {
							Integer key = y * img.width() + x;
							LabXY pixel = elements.get(key);

							float d = center.distance(pixel, distFct);

							if (localStorage.containsKey(key)) {
								float[] prevValue = localStorage.get(key);
								//printArray(prevValue[1]);
								if (d < prevValue[1]) {

									prevValue[0] = i;
									prevValue[1] = d;

									//localStorage.put(key, new float[] {i, d});
								}
							} else {
								localStorage.put(key, new float[] {i, d});
							}
						}
					}
				}
			}
		}
	}
}
