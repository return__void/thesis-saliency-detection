package net.returnvoid.segmentation;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;

import net.returnvoid.functions.DistanceFct;
import net.returnvoid.image.LabImg;
import processing.core.PConstants;
import processing.core.PGraphics;

/**
 * A class for SLIC superpixel segmentation. It can hold a set of LabXY elements
 * and a center element which is the mean value of the set. A superpixel segment
 * can also be though as a cluster. The SLIC superpixel segmentation process is
 * actually a variation of k-mean clustering. Instances of this class can be used
 * for such a clustering process.
 * @author Diana Lange
 *
 */
public class Superpixel implements Segment {

	/**
	 * The error of the segment which is the distance of the center in an 
	 * iteration of segmentation to the previous center of the segment.
	 */
	private float error = 100000;

	/**
	 * The mean element of the segment (mean spatial and color).
	 */
	private LabXY center;

	/**
	 * The elements that are members of the segment. The elements define
	 * the color and the boundaries of this segment. 
	 */
	private ArrayList<LabXY> elements = new ArrayList<LabXY>();


	// following up:
	// things that will be null whenever the superpixel changes (e.g. add or removed elements)

	/**
	 *  A set of coordinates which define connected regions. The union of
	 *  all sets are equal to the elements array.
	 */
	private ArrayList<ArrayList<LabXY>> regions = null;
	
	/**
	 * This set defines the region of the segment.
	 * Each element in the array represents one coordinate, i.e. point[index][0] = x
	 * and point[index][1] = y. If the array entry has a third element, than this
	 * third element is the number of neighbors this point has within the segment,
	 * i.e. point[index][2] = numberOfNeighbors.
	 */
	private int[][] points = null;
	
	/** 
	 * A subset of points() - the outline of the segment. Edge points have 
	 * less than 8 neighbors within the segment. If the array entry has a third 
	 * element, than this third element is the number of neighbors this point 
	 * has within the segment, i.e. point[index][2] = numberOfNeighbors. The
	 * other two elements of each edge element are the x and y coordinates. 
	 */
	private int[][] edge = null;
	
	/**
	 * A box which covers the whole area of the segment.
	 */
	private Rectangle boundingBox = null;

	/**
	 * Creates a new segment with no elements but a (cluster) center.
	 * @param labxy The initial cluster center for this segment.
	 */
	public Superpixel(LabXY labxy) {
		this.center = labxy.copy();
	}

	/**
	 * Creates a segment from a set of elements. The input set should be
	 * a connected region.
	 * @param region A set of elements for the new segment. The elements of 
	 * the input should be connected i.e. each element has at least one neighbor 
	 * within the set.
	 */
	public Superpixel(ArrayList<LabXY> region) {
		this.elements.addAll(region);

		this.regions = new ArrayList<ArrayList<LabXY>>();
		this.regions.add(region);

		this.center = region.get(0).copy();
		this.updateCenter();
		//System.out.println(this);
	}

	/**
	 * The center of the segment / cluster - the mean of all member elements.
	 * @return The mean element of the segment.
	 */
	public LabXY center() {
		return center;
	}

	/**
	 * Creates a hard copy of the segment.
	 * @return A new segment with same content.
	 */
	public Superpixel copy() {
		Superpixel pix = new Superpixel(center());
		for (LabXY labxy : elements) {
			pix.elements.add(labxy.copy());
		}
		return pix;
	}

	/**
	 * Resets all instances that needs to be re-calculated after the segment
	 * changed e.g. after an element has been added. This function should be called
	 * after add(), reset(), clear().
	 */
	private void reset() {
		regions = null;
		points = null;
		edge = null;
		boundingBox = null;
	}

	/**
	 * The number of elements, i.e. the number of colors, in the segment.
	 * @return The number of elements in the segment.
	 */
	public int size() {
		return elements.size();
	}

	/**
	 * Returns the error of the segment which is the distance of the center in one 
	 * iteration of segmentation to the center of the segment in the previous iteration.
	 * @return The error of the segment.
	 */
	public float error() {
		return error;
	}

	/**
	 * Removes all elements (colors) from the segment but the center.
	 */
	public void clear() {
		elements.clear();
		reset();
	}

	/**
	 * The elements that are members of the segment. The elements define
	 * the color and the boundaries of this segment. 
	 * @return The elements of the segment.
	 */
	public ArrayList<LabXY> elements() {
		return elements;
	}


	/**
	 * Transforms the Superpixel segment to a SimpleSegment.
	 * @return A segment without the elements but with the boundaries.
	 */
	public SimpleSegment toSimgpleSegment() {
		return new SimpleSegment(this);
	}

	/**
	 * Checks if the segment has no elements.
	 * @return True when the segment has no elements.
	 */
	public boolean empty() {
		return elements.size() == 0;
	}

	/**
	 * Removes the element with the input index from the segment.
	 * @param i The index of the element which should be removed.
	 */
	public void remove(int i) {
		remove(i);
		reset();
	}

	/**
	 * Removes all elements of the input from the segment.
	 * @param regions A set of elements which will be removed if they are member
	 * of the segment.
	 */
	public void remove(ArrayList<ArrayList<LabXY>> regions) {

		for (ArrayList<LabXY> region : regions) {
			for (LabXY e : region) {
				elements.remove(e);
			}
		}

		reset();
	}

	/**
	 * Removes the element from the segment. The element can only be removed if
	 * the input is the same instance as the equal element in the segment.
	 * @param labxy The element that will be removed.
	 */
	public void remove(LabXY labxy) {
		elements.remove(labxy);

		reset();
	}

	/**
	 * Gets an element from the segment.
	 * @param i The index of the element that should be returned.
	 * @return The element with the input index.
	 */
	public LabXY get(int i) {
		return elements.get(i);
	}

	/**
	 * Finds an element within the segment that has the input coordinates and returns
	 * the index of the found element.
	 * @param x The x coordinate of the lookup.
	 * @param y The y coordinate of the lookup.
	 * @return The index of the found element or -1 if the element was not found.
	 */
	public int findIndex(int x, int y) {

		for (int i = 0; i < elements.size(); i++) {
			LabXY e = elements.get(i);
			if (x == e.x() && y == e.y()) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Finds and returns an element of the segment that has the input coordinates.
	 * @param x The x coordinate of the lookup.
	 * @param y The y coordinate of the lookup.
	 * @return The found element or null if no element with the coordinates was found.
	 */
	public LabXY find(int x, int y) {

		for (int i = 0; i < elements.size(); i++) {
			LabXY e = elements.get(i);
			if (x == e.x() && y == e.y()) {
				return e;
			}
		}

		return null;
	}

	/**
	 * Counts how many neighbor the input element has in the segment. Two elements
	 * are neighbors when their pixel equivalent are neighbors.
	 * @param element The input element.
	 * @return The number of neighbors the element has in the segment.
	 */
	private int neigbors(LabXY element) {

		int count = 0;

		for (LabXY other : elements) {
			if (other != element && element.neighbor(other)) {
				count++;
			}

			if (count == 8) {
				return count;
			}
		}

		return count;
	}

	/**
	 * Adds all elements of the input segment to this segment.
	 * @param px A segment.
	 */
	public void add(Superpixel px) {
		elements.addAll(px.elements);
		reset();
	}

	/**
	 * Adds an element to the segment.
	 * @param labxy The new member of the segment.
	 */
	public void add(LabXY labxy) {
		elements.add(labxy);
		reset();
	}

	/**
	 * Moves the center to the smallest detected gradient found in the n x n 
	 * neighborhood around the current center. The gradient is estimated from
	 * the input image.
	 * @param img The input image which defines the gradient.
	 * @param n The search area for the new center of the segment.
	 */
	public void initCenter(LabImg img, int n) {

		int minX = center.x();
		int minY = center.y();

		float lowestGrad = Float.MAX_VALUE;

		for (int i = 0; i < n; i++) {
			int y = center.y() - n / 2 + i;

			for (int j = 0; j < n; j++) {
				int x = center.x() - n / 2 + j;
				// get the gradient vector at the location
				float[] gradient = img.gradient(x, y);
				
				// the gradient is directed i.e. values might be negative
				gradient[0] = gradient[0] < 0 ? gradient[0] * -1 : gradient[0];
				gradient[1] = gradient[1] < 0 ? gradient[1] * -1 : gradient[1];

				// the gradient value is simply approximated as the sum of the unsigned vector entries. 
				if (gradient[0] + gradient[1] < lowestGrad) {
					lowestGrad = gradient[0] + gradient[1];
					minX = x;
					minY = y;
				}
			}
		}

		// move center to the new location
		center.set(img, minX, minY);
	}

	/**
	 * Returns all regions that are not connected to the main region of the segment.
	 * The main region of the segment is the connected region with the most elements.
	 * 
	 * @return A set of regions that are not connected to the main region of the segment. 
	 * Each array entry is a connected region. If there are no disconnected 
	 * regions then null is returned.
	 */
	public ArrayList<ArrayList<LabXY>> getLooseRegions() {

		// regions not estimated yet
		if (regions == null) {
			connected();
		}

		// there are no disconnected regions
		if (regions.size() <= 1) {
			return null;
		}

		// find biggest, connected region
		// that one will be the content of the
		// superpixel after all not connected, small regions
		// are removed from this superpixel

		Collections.sort(regions, (a, b) -> {
			if (a.size() == b.size()) {
				return 0;
			} else if (a.size() > b.size()) {
				return -1;
			} else {
				return 1;
			}
		});

		ArrayList<LabXY> mainRegion = regions.get(0);

		// put all small, not connected regions in an array
		// each array represents a connected region
		ArrayList<ArrayList<LabXY>> loose = new ArrayList<ArrayList<LabXY>>();

		for (ArrayList<LabXY> region : regions) {
			if (region != mainRegion) {
				loose.add(region);
			}
		}

		return loose;
	}

	/**
	 * Estimates the distance between two segments using the provided distance
	 * function.
	 * @param other The other segment.
	 * @param fct A distance function for superpixel segments.
	 * @return The distance between the two segments.
	 */
	public float distance(Superpixel other, DistanceFct<Superpixel> fct) {
		return fct.distance(this, other);
	}

	/**
	 * Estimates the distance between two segments using the centers of the
	 * segments via Manhatten distance for LabXY elements.
	 * @param other The other segment.
	 * @return The distance between the two segments.
	 */
	public float distance(Superpixel other) {
		return distance(other, (a, b) -> LabXY.manhattenDistanceFct().distance(a.center(), b.center()));
	}

	/**
	 * Checks if the input segment is a neighbor to this segment. The segments
	 * are neighbors if they have at least one neighboring edge element. The 
	 * neighborhood is approximated via the intersection of the bounding boxes of 
	 * the segments. For a more accurate but also more complex estimation use
	 * SimpleSegment.neighbor();
	 * @param other The other segment. 
	 * @return True if the segments are neighbors.
	 */
	public boolean neighbor(Superpixel other) {
		return SimpleSegment.neighbor(this, other, true);
	}

	/**
	 * Checks if the segment is connected. A connected segment has only one region.
	 * @return True if segment consists of one connected region
	 */
	public boolean connected() {

		// estimate connected regions
		// if there is more than one region then the segment is not connected
		
		
		// basic implementation idea comes from:
		// https://www.cs.auckland.ac.nz/courses/compsci773s1c/lectures/ImageProcessing-html/topic3.htm
		// 'the fire' algorithm
		
		if (elements.size() > 0 && this.regions == null) {

			// store all elements of the superpixel and mark them as not visited yet
			ArrayList<LabXY> unlabeled = new ArrayList<LabXY>();
			unlabeled.addAll(elements);

			// storage for the regions
			// each sub-arraylist represents a connected region
			ArrayList<ArrayList<LabXY>> regions = new ArrayList<ArrayList<LabXY>>();

			// create first region
			regions.add(new ArrayList<LabXY>());

			// storage for already visited/labeled elements
			// these are seeds where it still needs to be checked
			// if the unlabeled contains neighbors to these seeds
			ArrayList<LabXY> seeds = new ArrayList<LabXY>();

			// select first seed
			// first seeds could be random element
			// to speed this up, the last element is picked (indexes of the array doesn't change)
			regions.get(0).add(unlabeled.get(unlabeled.size() - 1));
			seeds.add(unlabeled.get(unlabeled.size() - 1));
			unlabeled.remove(unlabeled.size() - 1);

			// run until all elements are labeled
			while (unlabeled.size() > 0) {

				// if there are sill seeds then there are still elements that
				// are connected to the current region
				// only for one region at once the connected elements are searched
				// that current current region is always the last element in the 'regions' array
				while (seeds.size() > 0) {

					// put new labeled elements as new seeds for next iteration
					ArrayList<LabXY> newSeeds = new ArrayList<LabXY>();
					for (LabXY seed : seeds) {

						// look for neighbors to the current seeds
						for (LabXY noLabel : unlabeled) {

							// neighbor found: label as visited, add to new seeds for next iteration
							if (seed.neighbor(noLabel)) {
								newSeeds.add(noLabel);
								regions.get(regions.size() - 1).add(noLabel);
							}
						}

						// remove all visited elements from unlabeled
						for (LabXY labeled : newSeeds) {
							unlabeled.remove(labeled);
						}
					}
					seeds = newSeeds;
				}

				// no more seeds available but there are still unlabeled elements:
				// that mean there exists elements that are not connected to the current
				// region, therefore start a new region with one starting seed
				if (unlabeled.size() > 0) {

					regions.add(new ArrayList<LabXY>());
					regions.get(regions.size() - 1).add(unlabeled.get(unlabeled.size() - 1));
					seeds.add(unlabeled.get(unlabeled.size() - 1));
					unlabeled.remove(unlabeled.size() - 1);
				}
			}

			// save calculated regions
			this.regions = regions;
		} 

		/*
		String txt = "";
		for (ArrayList<LabXY> region : regions) {
			txt += region.size() + ", ";
		}

		System.out.println(txt);
		 */

		// return if there is more than one connected region
		return elements.size() == 0 || regions.size() == 1;
	}


	/**
	 * Updates the center of the segment and calculates the current error.
	 * The new center of the segment will be the mean value of the member elements.
	 */
	public  void updateCenter() {

		if (elements.size() == 1) {
			LabXY oldCenter = center.copy();
			center = elements.get(0).copy();
			error = LabXY.manhattenDistanceFct().distance(center, oldCenter);
		} else if (elements.size() > 1) {

			LabXY oldCenter = center.copy();

			center = LabXY.meanFct().mean(elements);

			error = LabXY.manhattenDistanceFct().distance(center, oldCenter);
		} else {
			error = 0;
		}

		// should not happen anymore, but better check...
		if (Float.isNaN(error)) {
			error = 0;
		}
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#points()
	 */
	@Override
	public int[][]points() {

		if (this.points == null) {
			int[][] points = new int[elements.size()][3];

			for (int i = 0; i < elements.size(); i++) {
				points[i][0] = elements.get(i).x();
				points[i][1] = elements.get(i).y();
				points[i][2] = neigbors(elements.get(i));
			}

			this.points = points;
		}

		return points;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#edge()
	 */
	@Override
	public int[][] edge() {
		if (this.edge == null) {

			/*			
			ArrayList<int[]> edgeElement = new ArrayList<int[]>();

			int[][] points = points();

			for (int[] e : points) {

				if (e[2] < 8) {
					edgeElement.add(e);
				}
			}

			int[][] edge = new int[edgeElement.size()][3];

			for (int i = 0; i < edge.length; i++) {
				edge[i] = edgeElement.get(i);
			}

			this.edge = edge;
			 */

			this.edge = SimpleSegment.edge(points());
		}
		return edge;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#boundingBox()
	 */
	@Override
	public Rectangle boundingBox() {
		if (this.boundingBox == null) {
			this.boundingBox = SimpleSegment.bouningBox(edge());
		}

		return boundingBox;
	}

	/**
	 * Draws the segment, i.e. all the pixel of the segment with the mean / center color.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param scale The points of the segment will be scaled by that value.
	 */
	public void drawPixels(PGraphics g, float offsetX, float offsetY, float scale) {
		int[][] points = points();

		g.strokeWeight(0.25f);
		g.stroke(center.hex());
		g.fill(center.hex());

		for (int[] point : points) {
			g.rect(offsetX + point[0] * scale, offsetY + point[1] * scale, scale, scale);
		}
	}

	/**
	 * Draws the segment as a convex hull with just the outline.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param scale The points of the segment will be scaled by that value.
	 */
	public void drawHullOutline(PGraphics g, float offsetX, float offsetY, float scale) {
		float[][] points = SimpleSegment.hull(this);

		g.stroke(0, 100);
		g.noFill();

		g.beginShape();
		for (float[] point : points) {
			g.vertex(offsetX + point[0] * scale, offsetY + point[1] *  scale);
		}
		g.endShape(PConstants.CLOSE);
	}

	/**
	 * Draws the segment as a convex hull with the mean color of the segment.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param scale The points of the segment will be scaled by that value.
	 */
	public void drawHull(PGraphics g, float offsetX, float offsetY, float scale) {
		float[][] points = SimpleSegment.hull(this);
		g.fill(center.hex());
		g.stroke(0, 100);

		g.beginShape();
		for (float[] point : points) {
			g.vertex(offsetX + point[0] * scale, offsetY + point[1] *  scale);
		}
		g.endShape(PConstants.CLOSE);
	}

	
	/**
	 * Visualizes the segment as an ellipse with the mean color of the segment 
	 * and the diameter giving the size of the segment. 
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param scale The points of the segment will be scaled by that value.
	 * @param f A scaling factor for the ellipse.
	 */
	public void drawSize(PGraphics g, float offsetX, float offsetY, float scale, float f) {

		float d = elements.size() * scale * f;

		g.fill(center.hex());
		g.noStroke();
		g.ellipse(offsetX + center.x() * scale, offsetY + center.y() * scale, d , d);
	}

	/**
	 * Visualizes the bounding box of the segment using a black outline.
	 * @param g The container for the drawing.
	 * @param offsetX The points of the segment will be translated by that value.
	 * @param offsetY The points of the segment will be translated by that value.
	 * @param scale The points of the segment will be scaled by that value.
	 */
	public void drawBoundingBox(PGraphics g, float offsetX, float offsetY, float scale) {
		g.noFill();
		g.stroke(0, 100);

		g.rect(offsetX + boundingBox().x * scale, offsetY + boundingBox().y *  scale, boundingBox().width *  scale, boundingBox().height * scale);

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[" + center.x() + ", " + center.y() + "] " + elements.size() + "x";
	}
}
