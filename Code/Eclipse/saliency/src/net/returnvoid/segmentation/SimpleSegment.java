package net.returnvoid.segmentation;

import java.awt.Rectangle;
import java.util.ArrayList;

import external.mesh.Hull;
import external.mesh.MPolygon;
import external.misc.ThinningService;
import net.returnvoid.math.Coord;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * A simple segment implementation. Simple segments are defined by their boundaries
 * i.e. a set of points.
 * @author Diana Lange
 *
 */
public class SimpleSegment implements Segment {

	/**
	 * This set defines the region of the segment.
	 * Each element in the array represents one coordinate, i.e. point[index][0] = x
	 * and point[index][1] = y. If the array entry has a third element, than this
	 * third element is the number of neighbors this point has within the segment,
	 * i.e. point[index][2] = numberOfNeighbors.
	 */
	private int[][] points;

	/** 
	 * A subset of points() - the outline of the segment. Edge points have 
	 * less than 8 neighbors within the segment. If the array entry has a third 
	 * element, than this third element is the number of neighbors this point 
	 * has within the segment, i.e. point[index][2] = numberOfNeighbors. The
	 * other two elements of each edge element are the x and y coordinates.
	 */
	private int[][] edge;
	
	/**
	 * A box which covers the whole area of the segment.
	 */
	private Rectangle boundingBox = null;

	/**
	 * Builds a new SimpleSegment from any kind of other Segment.
	 * @param segment Another segment which might include more information than
	 * a SimpleSegment.
	 */
	public SimpleSegment(Segment segment) {
		this.points = segment.points();
		this.edge = segment.edge();
	}

	/**
	 * Builds a new SimpleSegment from a set of points. Each array entry
	 * of points should have at least two elements where point[index][0] = x and 
	 * point[index][1] = y.
	 * @param points A set of coordinates.
	 */
	public SimpleSegment(int[][] points) {
		this.points = points;
		this.edge = null;
	}

	/**
	 * Builds a new SimpleSegment from a set of points. Each array entry
	 * of points should have at least two elements where point[index][0] = x and 
	 * point[index][1] = y.
	 * @param points A set of coordinates.
	 * @param edge A subset of points (points that are at the edge of the segment).
	 */
	public SimpleSegment(int[][] points, int[][] edge) {
		this.points = points;
		this.edge = edge;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#edge()
	 */
	@Override
	public int[][] edge() {
		if (this.edge == null) {
			this.edge = SimpleSegment.edge(points);
		}
		return this.edge;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#points()
	 */
	@Override
	public int[][] points() {
		return points;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.segmentation.Segment#boundingBox()
	 */
	@Override
	public Rectangle boundingBox() {
		if (this.boundingBox == null && this.edge != null) {
			this.boundingBox = SimpleSegment.bouningBox(edge());
		} else if (this.boundingBox == null) {
			this.boundingBox = SimpleSegment.bouningBox(points());
		} 

		return boundingBox;
	}

	/**
	 * Counts how much neighbors the input point has in the set of points.
	 * @param points A set of points with points[index][0] = x and points[index][1] = y.
	 * @param point A point with point[0] = x and point[1] = y.
	 * @return The number of neighbors that the point has in points (between 0 and 0).
	 */
	public static int neighbors(int[][] points, int[] point) {

		int count = 0;

		for (int[] other : points) {
			if (other != point && Coord.neighbor(other[0], other[1], point[0], point[1])) {
				count++;
			}

			if (count == 8) {
				return count;
			}
		}

		return count;

	}

	/**
	 * Checks if two segments are neighbors. Two segments are neighbors when 
	 * at least two edge elements of the segments are neighbors to each other.
	 * @param a A segment.
	 * @param b Another segment.
	 * @param fast If true, then the neighbor check is done via the edge() sets 
	 * of each segment. If false, the neighbor check is approximated via the
	 * bounding boxes of the segments: Two segments are neighbors if there is
	 * a not empty intersection area between the bounding boxes of the segments.
	 * @return True if the given segments are neighbors.
	 */
	public static boolean neighbor(Segment a, Segment b, boolean fast) {

		// fast: approx. with bounding boxes
		if (fast) {
			return a.boundingBox().intersects(b.boundingBox());
		} else {
			
			// slow: check for each edge element if they are neighbors
			for (int[] e : a.edge()) {
				for (int[] o : b.edge()) {
					if (Coord.neighbor(e[0], e[1], o[0], o[1])) {
						return true;
					}
				}
			}
		}


		return false;
	}

	/**
	 * Finds the extrema of the input points and creates a bounding box that
	 * covers them.
	 * @param points A set of points with points[index][0] = x and points[index][1] = y.
	 * @return A bounding box of the input points.
	 */
	public static Rectangle bouningBox(int[][]points) {

		// find horizontal and vertical extrema

		int minX = -1;
		int minY = -1;
		int maxX = -1;
		int maxY = -1;

		for (int[] point : points) {

			if (minX == -1 || point[0] < minX) {
				minX = point[0];
			}

			if (maxX == -1 || point[0] > maxX) {
				maxX = point[0];
			}

			if (minY == -1 || point[1] < minY) {
				minY = point[1];
			}

			if (maxY == -1 || point[1] > maxY) {
				maxY = point[1];
			}

		}


		return new Rectangle(-1 + minX, - 1 + minY, 2 + maxX - minX, 2 + maxY - minY);
	}

	/**
	 * Creates a subset of the input points which only contains elements which
	 * have less than 8 neighbors in the input set and are therefore at the edge
	 * of the region that is defined by the points. The input points set may be 
	 * altered within the function, i.e. to each point the number of neighbors
	 * of the point in the set is added.
	 * @param points A set of points with points[index][0] = x and points[index][1] = y. 
	 * If the points include more than the x, y coordinates, then the third entry should
	 * be the number of neighbors of the point in the point set.
	 * @return A subset of points. Each point has 3 entries {x, y, numberOfNeighbors}.
	 */
	public static int[][] edge(int[][] points) {

		ArrayList<int[]> edgeElement = new ArrayList<int[]>();
		
		for (int[] e : points) {
			// check if points array includes already the number of neighbors
			// the third element of each point is considered the number of 
			// neighbors.
			
			// no third element: calculate the number of neighbors and add information
			// to the point
			if (e.length == 2) {
				e = new int[] {e[0], e[1], SimpleSegment.neighbors(points, e)};
			} 

			// element has less than 8 neighbors and is therefore and edge 
			if (e[2] < 8) {
				edgeElement.add(e);
			}
		}

		// Arraylist to array
		int[][] edge = new int[edgeElement.size()][3];

		for (int i = 0; i < edge.length; i++) {
			edge[i] = edgeElement.get(i);
		}

		return edge;
	}

	/**
	 * Creates a set of coordinates which is equal to a convex hull shape of the
	 * segment.
	 * @param segment A segment.
	 * @return A set of points that defines the convex hull of the segment. Each 
	 * array entry represents one coordinate where the first element is the x 
	 * coordinate and the second the y coordinate.
	 */
	public static float[][] hull(Segment segment) {

		float[][] hullPoints;

		try {
			Hull hull = new Hull(segment.edge());
			MPolygon region = hull.getRegion();
			hullPoints = region.getCoords();
		} 
		catch (Exception e) {
			hullPoints = new float[][] {new float[]{0, 0}};
		}

		return hullPoints;
	}

	/**
	 * Creates an image where the boundaries of the given segments are highlighted.
	 * Non edge points will be transparent.
	 * @param segments A set of segments.
	 * @param borderColor The color of the boundaries in the returned image.
	 * @param w Zero or the width of the returned image. The points of any given segment
	 * should be within the input width. If the range of the points are not known, than w 
	 * and h should be zero.
	 * @param h Zero or the height of the returned image. The points of any given segment
	 * should be within the input height. If the range of the points are not known, than w 
	 * and h should be zero.
	 * @return An image with the boundaries of the input segments.
	 */
	public static PImage edgeImage(ArrayList<? extends Segment> segments, int borderColor, int w, int h) {

		// range of points is not known -> extrema for w and h are calculated
		if (w < 1 || h < 1) {
			for (Segment s : segments) {
				for (int[] p : s.edge()) {
					if (p[0] > w) {
						w = p[0];
					}

					if (p[1] > h) {
						h = p[1];
					}
				}
			}

			w++;
			h++;
		}		

		// transform edge() points to binary data (edge or non edge)
		int[][] binary = new int[h][w];
		for (Segment seg : segments) {

			// mark all edge pixels with 1
			// non edge pixels will be 0
			for (int[] p : seg.edge()) {

				binary[p[1]][p[0]] = 1;
			}
		}

		// thin edges so every edge will be one pixel wide
		ThinningService thin = new ThinningService();
		thin.doZhangSuenThinning(binary, true);

		// write binary data to pixel image
		PImage output = new PImage(w, h, PConstants.ARGB);

		for (int yy = 0; yy < binary.length; yy++) {
			for (int xx = 0; xx < binary[yy].length; xx++) {
				int c = binary[yy][xx] == 0 ? 0 : borderColor;
				output.pixels[yy * output.width + xx] = c;
			}
		}

		output.updatePixels();

		return output;
	}
}
