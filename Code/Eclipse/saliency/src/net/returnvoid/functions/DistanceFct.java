package net.returnvoid.functions;

/**
 * A functional interface for calculating distances between objects.
 * 
 * @author Diana Lange
 *
 * @param <Type> The type of the objects of which the distance should be calculated.
 */
@FunctionalInterface
public interface DistanceFct<Type> {

	/**
	 * Calculates the distance between the two inputs and returns a floating point
	 * value that describes the relationship between the inputs. The returned
	 * value is usually not bounded, but the higher the distance is, the less
	 * similar are the inputs.
	 * 
	 * @param o First input object.
	 * @param v Second input object.
	 * @return The distance between the two objects or zero if the inputs are
	 * identical.
	 */
	public float distance(Type o, Type v);
}
