package net.returnvoid.functions;

import java.util.Arrays;
import java.util.List;

/**
 * A function interface for the estimation of averages. This will take a set
 * of elements of the same type as input and will return the average value of
 * this set as output with the same type.
 * @author Diana Lange
 *
 * @param <Type> The type of the input set and the output value.
 */
@FunctionalInterface
public interface MeanFct<Type> {
	
	/**
	 * Estimates the average of the input elements.
	 * @param elements A set of values.
	 * @return The average value of the input.
	 */
	public default Type mean(Type[] elements) {
		return mean(Arrays.asList(elements));
	}
	
	/**
	 * Estimates the average of the input elements.
	 * @param elements A set of values.
	 * @return The average value of the input.
	 */
	public Type mean(List<Type> elements);
}
