package net.returnvoid.functions;

/**
 * A functional interface which takes two arguments and transforms them to one
 * output. This is designed for extracting information, e.g. a mean color, of 
 * an image within the boundaries of a segment. But naturally this interface can 
 * also be used for other purposes.
 * 
 * @author Diana Lange
 *
 * @param <Img> The type of the first input argument. If used for image feature 
 * extraction this will be some kind of an image.
 * @param <Segment> The type of the second input argument. If uses for image
 * feature extraction this will be some segment that defines the boundaries of
 * which the feature will be extracted from the image.
 * @param <Feature> The type of the output. 
 */
@FunctionalInterface
public interface FeatureExtractorFct<Img, Segment, Feature> {

	/**
	 * Extracts information of the two input arguments and transforms it to
	 * the output.
	 * @param img The first input argument.
	 * @param segment The second input argument.
	 * @return The extracted information of the two inputs. 
	 */
	public Feature extract(Img img, Segment segment);
}
