package net.returnvoid.functions;

/**
 * Functional interface for parsing values from one data type to another.
 * 
 * @author Diana Lange
 *
 * @param <Input> Input type of the parse function.
 * @param <Output> Output type of the parse function.
 */
@FunctionalInterface
public interface ParserFct<Input, Output> {

	/**
	 * Parses the input to the output.
	 * 
	 * @param input Input value.
	 * @return Parsed version of the input value.
	 */
	Output parse(Input input);
}

