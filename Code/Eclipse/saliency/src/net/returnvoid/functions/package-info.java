
/**
 * This package includes a set of functional interfaces, i.e. interfaces with
 * one method defined on generic types. The main feature of this package is
 * the <b>DistanceFct</b> interface for estimating the distance between two
 * elements of the same type.
 * @author Diana Lange
 *
 */
package net.returnvoid.functions;