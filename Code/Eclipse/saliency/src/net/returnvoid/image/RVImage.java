package net.returnvoid.image;

import processing.core.PImage;

/**
 * A basic interface for all images. The main feature of this interface is that
 * all images can estimate the gradient value at a certain position in the image
 * and that filters can be applied.
 * 
 * @author Diana Lange
 *
 */
public interface RVImage {

	/**
	 * Estimates the value of the gradient at location (x, y).
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The (signed) gradient value at this location width gradient()[0] being
	 * the x value of the gradient and gradient()[1] being the y value.
	 */
	public float[] gradient(int x, int y);
	
	/**
	 * Creates a Processing compatible version of the image. The returned PImage
	 * may not have a parent element, which is needed for the execution of myPImage.save() 
	 * method. So before calling the save method of the returned image better set
	 * the parent element manually, i.e.<br>
	 * PImage myPImage = myRVImage.img(); <br>
	 * myPImage.parent = aPAppletInstance;
	 * @return The PImage version of this image.
	 */
	public PImage img();
	
	/**
	 * The width of the image.
	 * @return The width of the image.
	 */
	public int width();
	
	/**
	 * The height of the image.
	 * @return The height of the image.
	 */
	public int height();
	
	/** 
	 * Applies a filter to the image. 
	 * @param filter The image filter, e.g. an element of FilterFactory.
	 * @return The filtered image.
	 */
	public RVImage filter(double[][] filter);
	
	/**
	 * Applies a filter to the image.
	 * @param filter The image filter, e.g. an element of FilterFactory.
	 * @param copy If true, a new instance with the filtered result is returned.
	 * Otherwise the filter will be applied directly on the this image instance.
	 * @return The new image instance when copy=true or this instance.
	 */
	public RVImage filter(double[][] filter, boolean copy);
	
	/**
	 * Makes a hard copy of the image and returns a new instance of the image with
	 * the same values.
	 * @return The new image instance.
	 */
	public RVImage copy();
	
	/**
	 * Scales the image to the new width and height. When one of the input values
	 * is zero the image will keep its aspect ratio.
	 * @param newWidth The new width of the image or 0 if image should be scaled 
	 * to a fixed height. Make sure that at least one of the values of newWidth, newHeight
	 * is greater than zero.
	 * @param newHeight The new height of the image or 0 if image should be scaled 
	 * to a fixed width. Make sure that at least one of the values of newWidth, newHeight
	 * is greater than zero.
	 */
	public void resize(int newWidth, int newHeight);
	
	/**
	 * Returns the number of pixels in the image.
	 * @return The number of pixels in the image.
	 */
	public int size();
}
