
/**
 * This package includes image representations for various tasks and color spaces.
 * @author Diana Lange
 *
 */
package net.returnvoid.image;