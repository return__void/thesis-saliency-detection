package net.returnvoid.image;

import net.returnvoid.color.LabColor;
import net.returnvoid.color.RGBColor;
import net.returnvoid.functions.DistanceFct;
import net.returnvoid.segmentation.LabDistanceFactory;
import net.returnvoid.segmentation.LabXY;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * A class for images in CieLab color space. Takes image in RGB color space as input
 * and converts these colors to CieLab. 
 *  
 * @author Diana Lange
 *
 */
public class LabImg implements RVImage {

	/**
	 * The input image (may be altered within this class).
	 */
	private PImage img;

	/**
	 * The LabColor values for all pixels in the input image. The array entries
	 * will me null until the computation is forced by calling a getter function
	 * e.g. get(x, y).
	 */
	private LabColor[] labPixels;

	/**
	 * Records whether the an element in the labPixels array has been changed. If
	 * so the pixels array of the input image has to be updated when the img()
	 * function is called.
	 */
	private boolean changed = false;

	/**
	 * The filtering process is parallelized. This value set how many threads
	 * (parallel running processes) will be used.
	 */
	public static int threadMultiplier = 10;

	/**
	 * Creates an new empty image (all pixels will be black).
	 * @param width The width of the new image.
	 * @param height The height of the new image.
	 */
	public LabImg(int width, int height) {
		this(new PImage(width, height, PConstants.RGB));
	}

	/**
	 * Transforms the input IntensityImg to a LabImg.
	 * @param img The input image (will not be altered).
	 */
	public LabImg(IntensityImg img) {
		this(img.img());
	}

	/**
	 * Creates a new LabImg from the input RGB image. The input image may
	 * be altered when this LabImg is filtered or in any other way changed.
	 * @param img The input image.
	 */
	public LabImg(PImage img) {

		this.img = img;
		this.img.loadPixels();
		this.labPixels = new LabColor[img.pixels.length];
	}



	/**
	 * Loads the LabColor at the index and stores it in the labPixels array.
	 * @param index The index of the pixel.
	 */
	private void loadPixel(int index) {
		labPixels[index] = (new RGBColor((img.pixels[index] >> 16) & 255, (img.pixels[index] >> 8) & 255, img.pixels[index] & 255)).toLab();
	}

	/**
	 * Fills the pixel array with LabColor elements. All values that have been
	 * stored in the array previously will be overridden.
	 */
	public void loadPixels() {
		for (int i = 0; i < img.pixels.length; i++) {
			labPixels[i] = (new RGBColor((img.pixels[i] >> 16) & 255, (img.pixels[i] >> 8) & 255, img.pixels[i] & 255)).toLab();
		}
		this.changed = false;
	}



	/**
	 * Updates the pixel arrays, i.e. writes the information from the LabColor
	 * array to the RGB image.
	 */
	public void updatePixels() {
		updatePixels(false);
	}

	/**
	  * Updates the pixel arrays, i.e. writes the information from the LabColor
	 * array to the RGB image.
	 * @param clear If true, all information from the LabColor array will be removed.
	 */
	public void updatePixels(boolean clear) {
		if (changed) {
			for (int i = 0; i < labPixels.length; i++) {
				if (labPixels[i] != null) {
					img.pixels[i] = labPixels[i].getOpColor();
					if (clear) {
						labPixels[i] = null;
					}
				}
			}
			img.updatePixels();
			changed = false;
		}
	}

	/**
	 * Sets a color at the position of the labxy element with the color value of
	 * the input.
	 * @param labxy The element containing the color and location information.
	 */
	public void set(LabXY labxy) {
		set(labxy.x(), labxy.y(), labxy.lab());
	}

	/**
	 * Sets the color at the input location.
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param lab The new LabColor for the (x,y) position.
	 */
	public void set(int x, int y, LabColor lab) {
		int index = y * img.width + x;
		if (index < 0) {
			index = 0;
		} else if (index >= img.pixels.length) {
			index = img.pixels.length - 1;
		}

		labPixels[index] = lab;
		changed = true;
	}



	/**
	 * Gets the color at location(x, y).
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @return The LabColor element.
	 */
	public LabColor get(int x, int y) {
		return get((y < 0 ? 0 : y > img.height - 1 ? img.height - 1 : y) * img.width + (x < 0 ? 0 : x > img.width - 1 ? img.width - 1 : x));
	}

	/**
	 * Gets the color with the input index.
	 * @param index The index of the color. Range: [0, size()).
	 * @return The LabColor element.
	 */
	public LabColor get(int index) {
		if (index < 0) {
			index = 0;
		} else if (index >= img.pixels.length) {
			index = img.pixels.length - 1;
		}

		if (labPixels[index] == null) {
			loadPixel(index);
		}

		return labPixels[index];
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#img()
	 */
	public PImage img() {
		if (changed) {
			updatePixels(false);
		}

		return img;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#size()
	 */
	public int size() {
		return labPixels.length;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#width()
	 */
	public int width() {
		return img.width;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#height()
	 */
	public int height() {
		return img.height;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#copy()
	 */
	public LabImg copy() {
		LabImg copyImg = new LabImg(img.copy());
		for (int i = 0; i < labPixels.length; i++) {

			if (labPixels[i] != null) {
				copyImg.labPixels[i] = labPixels[i].copy();
			}
		}

		copyImg.changed = changed;

		return copyImg;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#resize(int, int)
	 */
	public void resize(int newW, int newH) {

		if (newW == img.width && newH == img.height) {
			return;
		}

		this.updatePixels(true);

		this.img.resize(newW, newH);
		this.labPixels = new LabColor[this.img.width * this.img.height];
	}

	/**
	 * Estimates the value of the gradient at location (x, y).
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The signed gradient value at this location width gradient()[0] being
	 * the x value of the gradient and gradient()[1] being the y value. The sign
	 * of the gradient is estimated from the luminance parameters.
	 */
	public float[] gradient(int x, int y) {
		DistanceFct<LabColor> fct = LabDistanceFactory.euclideanDistanceFct();
		LabColor xplus1 = get(x + 1, y);
		LabColor xminus1 = get(x - 1, y);
		int xDir = xplus1.getLuminance() - xminus1.getLuminance() > 0 ? 1 : -1;

		LabColor yplus1 = get(x, y + 1);
		LabColor yminus1 = get(x, y - 1);
		int yDir = yplus1.getLuminance() - yminus1.getLuminance() > 0 ? 1 : -1;

		return new float[] {xDir * fct.distance(xplus1, xminus1), yDir * fct.distance(yplus1, yminus1)};
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][])
	 */
	public LabImg filter(double[][] filter) {
		return filter(filter, false);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][], boolean)
	 */
	public LabImg filter(double[][] filter, boolean copy) {
		this.updatePixels(!copy);
		/*
		int[] temp = new int[img.pixels.length];

		for(int y = 0; y < this.img.height; y++) {
			for(int x = 0; x < this.img.width; x++) {

				temp[y * img.width + x] = convolute(img.pixels, x, y, filter, true);
			}
		}
		 */

		ConvolutionManager cm = new ConvolutionManager(new int[img.pixels.length], filter);
		while(cm.getState() != Thread.State.TERMINATED) {
			// Block until update is finished
		}

		// filtering is done in RGB color space since CieLab is an irregular 
		// space and keeping it in valid range would be a pain in the ass
		int[] temp = cm.result();
		cm.clear();

		if (!copy) {
			System.arraycopy(temp, 0, this.img.pixels, 0, this.img.pixels.length);
			this.img.updatePixels();
			return this;
		} else {
			LabImg filtered = this.copy();
			System.arraycopy(temp, 0, filtered.img.pixels, 0, filtered.img.pixels.length);
			filtered.img.updatePixels();
			return filtered;
		}
	}

	/**
	 * Applies a filter at the pixel with location (x, y) and returns the RGB
	 * color value of the response.
	  *@param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param filter The image filter, e.g. an element of FilterFactory.
	 * @return The response to the filter.
	 */
	public int filterResponse(int x, int y, double[][] filter) {
		this.updatePixels(false);

		if (x < 0) {
			x = 0;
		} else if (x > img.width - 1) {
			x = img.width - 1;
		}

		if (y < 0) {
			y = 0;
		} else if (y > img.height - 1) {
			y = img.height - 1;
		}

		return convolute(img.pixels, x, y, filter, true);
	}


	/**
	 * Convolutes the input RGB array at the index with the given filter.
	 * @param input The input RGB array. This will not be altered.
	 * @param index The index for the convolution.
	 * @param filter The image filter.
	 * @param limit If true, the filter respond will be limited to the RGB color range.
	 * @return The respond to the filter.
	 */
	private int convolute(int[] input, int index, double[][] filter, boolean limit) {
		return convolute(input, index % img.width, index / img.width, filter, limit);
	}

	/**
	 * Convolutes the input RGB array at the (x,y) location with the given filter.
	 * @param input The input RGB array. This will not be altered.
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param filter The image filter.
	 * @param limit If true, the filter respond will be limited to the RGB color range.
	 * @return The respond to the filter.
	 */
	private int convolute(int[] input, int x, int y, double[][] filter, boolean limit) {
		int w = img.width;
		int h = img.height;
		double r = 0;
		double g = 0;
		double b = 0;

		// do convolution
		// the borders are filled with the border colors from the image
		for (int i = 0; i < filter.length; i++) {
			int yy = y + i - filter.length / 2;

			yy = yy < 0 ? 0 : yy > h - 1 ? h - 1 : yy;

			for (int j = 0; j < filter[i].length; j++) {
				int xx = x + j - filter.length / 2;

				xx = xx < 0 ? 0 : xx > w - 1 ? w - 1 : xx;

				int col = input[yy * w + xx];
				r += (col >> 16 & 255) * filter[i][j];
				g += (col >> 8 & 255) * filter[i][j];
				b += (col & 255) * filter[i][j];
			}
		}

		if (limit) {

			if (r > 255) {
				r = 255;
			} else if (r < 0) {
				r = 0;
			}

			if (g > 255) {
				g = 255;
			} else if (g < 0) {
				g = 0;
			}

			if (b > 255) {
				b = 255;
			} else if (b < 0) {
				b = 0;
			}
		}

		return 255 << 24 | (int) r << 16 | (int) g << 8 | (int) b;
	}


	/**
	 * Creates a LabXY element containing the input (x, y) and the color at that
	 * position. 
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @return The LabXY element at the input location.
	 */
	public LabXY labxy(int x, int y) {
		return new LabXY(get(y * img.width + x), x, y);
	}


	/**
	 * Estimates the value of the gradient at location (x, y) using only the
	 * luminance.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The signed gradient value at this location width gradient()[0] being
	 * the x value of the gradient and gradient()[1] being the y value.
	 */
	public float[] intensityGradient(int x, int y) {
		//Distance<LabColor> fct = LabXY.euclideanLabDistanceFct();
		LabColor xplus1 = get(x + 1, y);
		LabColor xminus1 = get(x - 1, y);

		LabColor yplus1 = get(x, y + 1);
		LabColor yminus1 = get(x, y - 1);

		return new float[] {xplus1.getLuminance() - xminus1.getLuminance(), yplus1.getLuminance() - yminus1.getLuminance()};
	}

	/**
	 * Performs convolution efficient by starting multiple threads and each parallel
	 * running thread processes a portion of the image convoltion.
	 * @author Diana Lange
	 *
	 */
	private class ConvolutionManager extends Thread{
		private int[] responses;
		private ConvolutionPerformerThread[] threads;

		private ConvolutionManager(int[] container, double[][]filter) {
			this.responses = container;

			threads  = new ConvolutionPerformerThread[Runtime.getRuntime().availableProcessors() * LabImg.threadMultiplier];

			// cut image pixels into equal parts and start child threads
			// who will process that part of the image
			int range = container.length / threads.length;
			for (int i = 0; i <  threads.length; i++) {
				int start = i * range;
				int end = start + range + 1; // not included

				if (i == threads.length - 1) {
					end = container.length;
				}

				threads[i] = new ConvolutionPerformerThread(container, filter, start, end);
			}

			this.start();
		}

		public int[] result() {
			return responses;
		}

		public void clear() {
			for (int i = 0; i < threads.length; i++) {
				threads[i].clear();
				threads[i] = null;
			}
			responses = null;
		}

		@Override 
		public void run() {
			boolean finished = false;
			while (!finished) {
				finished = true;
				for (ConvolutionPerformerThread t : threads) {
					if (t.getState() != Thread.State.TERMINATED) {
						finished = false;
						break;
					}
				}

				if (!finished) {
					try {
						this.wait(30);
					} catch(Exception e) {
					}
				}
			}
		}

		private class ConvolutionPerformerThread extends Thread {
			private int start;
			private int end; 
			private int[] responses;
			private double[][] filter;

			private ConvolutionPerformerThread(int[] container, double[][]filter, int start, int end) {
				this.responses = container;
				this.filter = filter;
				this.start = start;
				this.end = end;
				this.start();
			}

			public void clear() {
				responses = null;
				filter = null;
			}

			@Override
			public void run() {

				for (int i = start; i < end; i++) {
					responses[i] = convolute(img.pixels, i, filter, true);
				}
			}	
		}
	}

}
