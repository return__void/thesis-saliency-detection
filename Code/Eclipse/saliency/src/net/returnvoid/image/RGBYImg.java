package net.returnvoid.image;

import net.returnvoid.math.Range;
import processing.core.PImage;

/**
 * A class for storing the color information of images in RGBYColor space which
 * has been used in the model of https://ieeexplore.ieee.org/abstract/document/730558/.
 * RGB colors are separated into balanced R, G, B, Y channels which are normalized
 * by the intensity of the input RGB color values. <br>
 * This image class generates for all channels - R, G, B, Y and intensity - separate
 * instances of IntensityImg. All IntensityImg instances are normalized to the 
 * range [0, 255] to simplify the measurement of distances of RGBYColor elements.
 * 
 * @author Diana Lange
 *
 */
public class RGBYImg implements RVImage {

	/**
	 * An image with the intensity information of the input image.
	 */
	private IntensityImg intensity;
	
	/**
	 * An image with the intensity of the balanced Red values of the input image. 
	 */
	private IntensityImg R;
	
	/**
	 * An image with the intensity of the balanced Green values of the input image. 
	 */
	private IntensityImg G;
	
	/**
	 * An image with the intensity of the balanced Blue values of the input image. 
	 */
	private IntensityImg B;
	
	/**
	 * An image with the intensity of the balanced Yellow values of the input image. 
	 */
	private IntensityImg Y;
	
	/**
	 * Creates an empty image. All properties must be set manually. This should
	 * only be used within the copy() function.
	 */
	private RGBYImg() {
	}
	
	/**
	 * Creates a RGBYImg from the input. The intensity information will be
	 * equal to the gray scale values in the input image. 
	 * @param input The input image.
	 */
	public RGBYImg(PImage input) {
		this(new IntensityImg(input), input);
	}
	
	/**
	 * Creates a RGBYImg from the input. The intensity information will be
	 * equal to the luminance values in the input image. 
	 * @param input The input image.
	 */
	public RGBYImg(LabImg input) {
		this(new IntensityImg(input), input.img());
	}
	
	/**
	 * Creates a RGBY from input and a known intensity map. R, G, B, Y will be
	 * normalized by the intensity values of the intensity map. 
	 * @param intensity The intensity image of the input PImage.
	 * @param input The input image for color extraction.
	 */
	private RGBYImg(IntensityImg intensity, PImage input) {
		// for each color channel and intensity a separate map is created
		this.intensity = intensity;
		this.R = new IntensityImg(input.width, input.height);
		this.G = new IntensityImg(input.width, input.height);
		this.B = new IntensityImg(input.width, input.height);
		this.Y = new IntensityImg(input.width, input.height);
		
		input.loadPixels();
		
		// keep track of the boundaries of the color values
		Range colorRange = new Range();
		
		for (int y = 0; y < input.height; y++) {
			for (int x = 0; x < input.width; x++) {
				
				int index = y * input.width + x;
				
				// get color information
				double inte = intensity.get(index);
				double r = input.pixels[index] >> 16 & 255;
				double g = input.pixels[index] >> 8 & 255;
				double b = input.pixels[index] & 255;
				
				// normalize color values
				if (inte > intensity.range().max() / 10 && inte != 0) {
					r /= inte;
					g /= inte;
					b /= inte;
				} else {
					r = g = b = 0;
				}
				
				// transform to RGBY color space
				double RR = r - (g + b) / 2;
				double GG = g - (r + b) / 2;
				double BB = b - (r + g) / 2;
				double YY = (r + g) / 2 - Math.abs(r - g) / 2 - b;
				
				if (YY < 0) {
					YY = 0;
				}
				
				// store values in their maps
				R.set(index, RR);
				G.set(index, GG);
				B.set(index, BB);
				Y.set(index, YY);
				
				
				// record color value range
				colorRange.record(RR);
				colorRange.record(GG);
				colorRange.record(BB);
				colorRange.record(YY);
			}
		}
		
		// interpolate color maps to range [0, 255] without loosing the relationships across the maps
		this.R.toRange(new Range(map(R.range().min(), colorRange.min(), colorRange.max(), 0, 255), map(R.range().max(), colorRange.min(), colorRange.max(), 0, 255)));
		this.G.toRange(new Range(map(G.range().min(), colorRange.min(), colorRange.max(), 0, 255), map(G.range().max(), colorRange.min(), colorRange.max(), 0, 255)));
		this.B.toRange(new Range(map(B.range().min(), colorRange.min(), colorRange.max(), 0, 255), map(B.range().max(), colorRange.min(), colorRange.max(), 0, 255)));
		this.Y.toRange(new Range(map(Y.range().min(), colorRange.min(), colorRange.max(), 0, 255), map(Y.range().max(), colorRange.min(), colorRange.max(), 0, 255)));
	}
	
	/**
	 * Linear interpolates the input value to a given range.
	 * @param input The input value.
	 * @param inputMin The minimum of the range of the input.
	 * @param inputMax The maximum of the range of the input.
	 * @param outputMin The minimum of the range of the output.
	 * @param outputMax The maximum of the range of the output.
	 * @return
	 */
	private float map(float input, float inputMin, float inputMax, float outputMin, float outputMax) {
		
		if (inputMin == inputMax) {
			return outputMin;
		}
		
		float normalizedInput = (input - inputMin) / inputMax;
		
		return outputMin + normalizedInput * (outputMax - outputMin);
	}
	
	/**
	 * Gets the intensity map of this image. The map is in range of [0, 255].
	 * @return The intensity information of this image.
	 */
	public IntensityImg intensity() {
		return intensity;
	}
	
	/**
	 * Gets the Red map of this image. The map is in range of [0, 255].
	 * @return A map containing the color information of the color channel Red.
	 */
	public IntensityImg R() {
		return R;
	}
	
	/**
	 * Gets the Green map of this image. The map is in range of [0, 255].
	 * @return A map containing the color information of the color channel Green.
	 */
	public IntensityImg G() {
		return G;
	}
	
	/**
	 * Gets the Blue map of this image. The map is in range of [0, 255].
	 * @return A map containing the color information of the color channel Blue.
	 */
	public IntensityImg B() {
		return B;
	}
	
	/**
	 * Gets the Yellow map of this image. The map is in range of [0, 255].
	 * @return A map containing the color information of the color channel Yellow.
	 */
	public IntensityImg Y() {
		return Y;
	}
	

	/**
	 * Gets the RGBYColor at the input location.
	 * @param x The x coordinate of the pixel. Range: [0, widht()].
	 * @param y The y coordinate of the pixel. Range: [0, height()].
	 * @return The RGBYColor at the input location.
	 */
	public RGBYColor rgby(int x, int y) {
		
		int index = y * width() + x;
		return new RGBYColor((float) R.get(index), (float) G.get(index), (float) B.get(index), (float) Y.get(index), (float) intensity.get(index));
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#gradient(int, int)
	 */
	@Override
	public float[] gradient(int x, int y) {
		return intensity.gradient(x, y);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#img()
	 */
	@Override
	public PImage img() {
		IntensityImg sum = new IntensityImg(width(), height());
		
		sum.add(R);
		sum.sub(G);
		sum.add(B);
		sum.add(Y);
		sum.mult(intensity);
		
		sum.toRange(new Range(0, 255));
		
		return sum.img();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#width()
	 */
	@Override
	public int width() {
		return intensity.width();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#height()
	 */
	@Override
	public int height() {
		return intensity.height();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][])
	 */
	@Override
	public RGBYImg filter(double[][] filter) {
		return filter(filter, false);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][], boolean)
	 */
	@Override
	public RGBYImg filter(double[][] filter, boolean copy) {
		IntensityImg intensity = this.intensity.filter(filter, copy);
		IntensityImg R = this.R.filter(filter, copy);
		IntensityImg G = this.G.filter(filter, copy);
		IntensityImg B = this.B.filter(filter, copy);
		IntensityImg Y = this.Y.filter(filter, copy);
		
		if (copy) {
			RGBYImg newImg = new RGBYImg();
			newImg.intensity = intensity;
			newImg.R = R;
			newImg.G = G;
			newImg.B = B;
			newImg.Y = Y;
			
			return newImg;
		} else {
			
			this.intensity = intensity;
			this.R = R;
			this.G = G;
			this.B = B;
			this.Y = Y;
			return this;
		}
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#copy()
	 */
	@Override
	public RVImage copy() {
		RGBYImg copy = new RGBYImg();
		copy.intensity = intensity.copy();
		copy.R = R.copy();
		copy.G = G.copy();
		copy.B = B.copy();
		copy.Y = Y.copy();
		return copy;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#resize(int, int)
	 */
	@Override
	public void resize(int newWidth, int newHeight) {
		intensity.resize(newWidth, newHeight);
		R.resize(newWidth, newHeight);
		G.resize(newWidth, newHeight);
		B.resize(newWidth, newHeight);
		Y.resize(newWidth, newHeight);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#size()
	 */
	@Override
	public int size() {
		return intensity.size();
	}

}
