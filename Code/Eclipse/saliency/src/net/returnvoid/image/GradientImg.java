package net.returnvoid.image;

import net.returnvoid.color.HSBColor;
import net.returnvoid.imagefeatures.Gradient;
import net.returnvoid.math.Coord;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * A class for an image representation of image gradients.
 * @author Diana Lange
 *
 */
public class GradientImg implements RVImage{

	/**
	 * The gradient values of the image.
	 */
	private Gradient gradient;
	
	/**
	 * Creates an empty image where everything must be set manually. Use this
	 * only for copy() function.
	 */
	private GradientImg() {
	}
	
	/**
	 * Creates a new gradient image representation of the input. This representation
	 * consists of the input image and a corresponding instance of Gradient. 
	 * @param input The input image. Might be altered within this instance, e.g. by calling the filter() function.
	 */
	public GradientImg(RVImage input) {
		this.gradient = new Gradient(input);
	}
	
	/**
	 * Creates a new gradient image representation of the input. This representation
	 * consists of the input image and a corresponding instance of Gradient. 
	 * @param input The input image. Might be altered within this instance, e.g. by calling the filter() function.
	 * @param area The gradient vectors will be computed as the mean vector of the gradients 
	 * in a small neighborhood. The neighborhood is defined by the 'area' parameter.
	 */
	public GradientImg(RVImage input, int area) {
		this.gradient = new Gradient(input, area);
	}
	
	
	/**
	 * The Gradient instance of this image which includes the input image as well
	 * as the gradient vectors at all locations of the image. 
	 * @return The gradient.
	 */
	public Gradient gradient() {
		return gradient;
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#gradient(int, int)
	 */
	@Override
	public float[] gradient(int x, int y) {
		Coord g = gradient.get(x, y);
		return new float[] {g.fx(), g.fy()};
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#img()
	 */
	@Override
	public PImage img() {
		
		PImage img = new PImage(width(), height(), PConstants.RGB);
		
		float TWO_PI = (float) (Math.PI * 2);
		float PI = (float) (Math.PI);
		
		for (int y = 0; y < gradient.height(); y++) {
			for (int x = 0; x < gradient.width(); x++) {
				Coord vec = gradient.get(x, y);
				HSBColor hsb = new HSBColor(360 * (vec.angle() + PI) / TWO_PI, 100, 100 * vec.mag() / gradient.maxMag()); 
				img.pixels[y * gradient.width() + x] = hsb.getColor();
			}
		}
		
		img.updatePixels();

		return img;
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#width()
	 */
	@Override
	public int width() {
		return gradient.img().width();
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#height()
	 */
	@Override
	public int height() {
		return gradient.img().height();
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][])
	 */
	@Override
	public GradientImg filter(double[][] filter) {
		return filter(filter, false);
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][], boolean)
	 */
	@Override
	public GradientImg filter(double[][] filter, boolean copy) {
		RVImage img = gradient.img().filter(filter, copy);
		
		if (copy) {
			return new GradientImg(img);
		} else {
			this.gradient = new Gradient(img);
			return this;
		}
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#copy()
	 */
	@Override
	public RVImage copy() {
		GradientImg g = new GradientImg();
		g.gradient = gradient.copy();
		return g;
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#resize(int, int)
	 */
	@Override
	public void resize(int newWidth, int newHeight) {
		RVImage img = gradient.img();
		img.resize(newWidth, newHeight);
		this.gradient = new Gradient(img);
	}
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#size()
	 */
	@Override
	public int size() {
		return gradient.size();
	}
}
