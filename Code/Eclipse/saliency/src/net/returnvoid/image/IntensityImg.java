package net.returnvoid.image;

import net.returnvoid.imagefeatures.FilterFactory;
import net.returnvoid.math.Range;
import processing.core.PConstants;
import processing.core.PImage;

/**
 * An image representation of intensity values from an image. Intensity values
 * are basically floating point numbers with arbitrary ranges that are aligned
 * to a two-dimension grid set by a width and height. IntensityImg can also be
 * used for simple matrix manipulation tasks. The range of the intensity values
 * is recorded at any given time. By doing so, it is possible to transform the 
 * values with arbitrary range to any other range (e.g. a RGB color range when 
 * transforming the IntensityImg back to a PImage).
 * @author Diana Lange
 *
 */
public class IntensityImg implements RVImage {

	/**
	 * The width of the image. 
	 */
	private int width;

	/**
	 * The height of the image.
	 */
	private int height;

	/**
	 * The color (grayscale) values. Can have arbitrary ranges.
	 */
	private double[] pixels;

	/**
	 * The range of the pixel values.
	 */
	private Range range = null;

	/**
	 * The filtering process is parallelized. This value set how many threads
	 * (parallel running processes) will be used.
	 */
	public static int threadMultiplier = 10;

	/**
	 * Creates an intensity image from the input image. The intensity image will
	 * contain the the gray scale values of the image. The intensity image will
	 * have the range of RGB values [0, 255].
	 * @param img The input image (will not be altered).
	 */
	public IntensityImg(PImage img) {
		this.width = img.width;
		this.height = img.height;

		pixels = new double[width * height];
		range = new Range();
		img.loadPixels();
		for (int i = 0; i < img.pixels.length; i++) {
			this.pixels[i] = ((img.pixels[i] >> 16 & 255) + (img.pixels[i] >> 8 & 255) + (img.pixels[i] & 255)) / 3d;
			range.record(this.pixels[i]);
		}
	}

	/**
	 * Creates an intensity image from the input image. The intensity image will
	 * contain the the luminance values of the image. The intensity image will
	 * have the range of RGB values [0, 255].
	 * @param img The input image (will not be altered).
	 */
	public IntensityImg(LabImg img) {
		this.width = img.width();
		this.height = img.height();

		pixels = new double[width * height];
		range = new Range();
		img.loadPixels();
		for (int i = 0; i < img.img().pixels.length; i++) {

			this.pixels[i] = 255 * img.get(i).getLuminance() / 100;
			range.record(this.pixels[i]);
		}
	}

	/**
	 * Creates a new, fully black image. The value at each pixel will be zero.
	 * @param width The width of the new image.
	 * @param height The height of the new image.
	 */
	public IntensityImg(int width, int height) {
		this.width = width;
		this.height = height;

		this.pixels = new double[width * height];
		this.range = new Range();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#size()
	 */
	@Override
	public int size() {
		return pixels.length;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#width()
	 */
	public int width() {
		return width;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#height()
	 */
	public int height() {
		return height;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#copy()
	 */
	public IntensityImg copy() {

		IntensityImg copy = new IntensityImg(width, height);
		System.arraycopy(pixels, 0, copy.pixels, 0, pixels.length);
		copy.range = this.range.copy();

		return copy;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#gradient(int, int)
	 */
	public float[] gradient(int x, int y) {
		double xplus1 = get(x + 1, y);
		double xminus1 = get(x - 1, y);
		double deltaX = xplus1 - xminus1;

		double yplus1 = get(x, y + 1);
		double yminus1 = get(x, y - 1);
		double deltaY = yplus1 - yminus1;

		return new float[] {(float) deltaX, (float) deltaY};
	}

	/**
	 * Creates a Processing compatible version of the image. The returned PImage
	 * may not have a parent element, which is needed for the execution of myPImage.save() 
	 * method. So before calling the save method of the returned image better set
	 * the parent element manually, i.e.<br>
	 * PImage myPImage = myRVImage.img(); <br>
	 * myPImage.parent = aPAppletInstance;<br>
	 * <br>
	 * Make sure that the range of the intensity values are in range of [0, 255]
	 * to get a reasonable image result.
	 * @return The PImage version of this image.
	 */
	public PImage img() {
		PImage img = new PImage(width, height, PConstants.RGB);
		img.pixels = new int[width * height];

		for (int i = 0; i < pixels.length; i++) {

			int val = (int) pixels[i];

			if (val < 0) {
				val = 0;
			} else if (val > 255) {
				val = 255;
			}

			img.pixels[i] = 255 << 24 | val << 16 | val << 8 | val;
		}

		img.updatePixels();


		return img;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#resize(int, int)
	 */
	@Override
	public void resize(int newWidth, int newHeight) {
		
		if (width() != newWidth && height() != newHeight) {
		
			Range defaultRange = range().copy();

			toRange(new Range(0, 255));

			PImage img = img();
			img.resize(newWidth, newHeight);
			img.updatePixels();
			img.loadPixels();
			this.pixels = new double[img.pixels.length];
			this.width = img.width;
			this.height = img.height;

			for (int i = 0; i < img.pixels.length; i++) {
				this.pixels[i] = img.pixels[i] & 255;
			}

			toRange(defaultRange);
		}
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][])
	 */
	public IntensityImg filter(double[][] filter) {
		return filter(filter, false);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][], boolean)
	 */
	public IntensityImg filter(double[][] filter, boolean copy) {
		/*
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {

				temp[y * width + x] = convolute(pixels, x, y, filter);
			}
		}*/

		ConvolutionManager cm = new ConvolutionManager(new double[pixels.length], filter);
		while(cm.getState() != Thread.State.TERMINATED) {
			// Block until update is finished
		}
		double[] temp = cm.result();
		cm.clear();

		if (!copy) {
			range.reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] = temp[i];
				range.record(pixels[i]);
			}
			return this;
		} else {
			IntensityImg filtered = new IntensityImg(width, height);
			filtered.pixels = temp;
			for (double p : filtered.pixels) {
				filtered.range.record(p);
			}
			return filtered;
		}
	}

	/**
	 * Applies a filter at the pixel with location (x, y) and returns the intensity
	 * value of the response.
	 *@param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param filter The image filter, e.g. an element of FilterFactory.
	 * @return The response to the filter.
	 */
	public double filterResponse(int x, int y, double[][] filter) {

		if (x < 0) {
			x = 0;
		} else if (x > width - 1) {
			x = width - 1;
		}

		if (y < 0) {
			y = 0;
		} else if (y > height - 1) {
			y = height - 1;
		}

		return convolute(pixels, x, y, filter);
	}

	/**
	 * Convolutes the input array at the index with the given filter.
	 * @param input The input intensity array. This will not be altered.
	 * @param index The index for the convolution.
	 * @param filter The image filter.
	 * @return The respond to the filter.
	 */
	private double convolute(double[] input, int index, double[][] filter) {
		return convolute(input, index % width, index / width, filter);
	}

	/**
	 * Convolutes the input array at the index with the given filter.
	 * @param input The input intensity array. This will not be altered.
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param filter The image filter.
	 * @return The respond to the filter.
	 */
	private double convolute(double[] input, int x, int y, double[][] filter) {
		double intensity = 0;

		for (int i = 0; i < filter.length; i++) {
			int yy = y + i - filter.length / 2;

			yy = yy < 0 ? 0 : yy > height - 1 ? height - 1 : yy;

			for (int j = 0; j < filter[i].length; j++) {
				int xx = x + j - filter.length / 2;

				xx = xx < 0 ? 0 : xx > width - 1 ? width - 1 : xx;

				intensity += input[yy * width + xx] * filter[i][j];
			}
		}


		return intensity;
	}

	/**
	 * Re-Calculates the range of the intensity values.
	 */
	public void updateRange() {
		range.reset();

		for (double pix : pixels) {
			range.record(pix);
		}
	}

	/**
	 * Gets the range of the intensity values of this image.
	 * @return The range of values of this image.
	 */
	public Range range() {

		return range;
	}

	/**
	 * Limits the values of the image to the input range. The pixels which are
	 * in the input range will not be altered.
	 * @param range The new range of the image.
	 */
	public void limitRange(Range range) {
		if (!(this.range().min() < range.min() && this.range().max() > range.max())) {
			this.range().reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] = range.limit(pixels[i]);
				this.range().record(pixels[i]);
			}
		}
	}

	/**
	 * Interpolates the intensity image values to the input range.
	 * @param range The new range of the image.
	 */
	public void toRange(Range range) {

		if (this.range().min() == this.range().max()) {
			for (int i = 0; i < pixels.length; i++) {

				pixels[i] = range.min();
			}
			this.range = new Range(range.min(), range.min());

		} else {

			float inputMin = this.range().min();
			float inputMax = this.range().max();
			float outputMin = range.min();
			float outputMax = range.max();

			for (int i = 0; i < pixels.length; i++) {

				double val = pixels[i];

				pixels[i] = outputMin + (outputMax - outputMin) * (val - inputMin) / (inputMax - inputMin);
			}

			this.range = range;
		}
	}

	/**
	 * Sets the color at the input index.
	 * @param index The index of the pixel. Range: [0, size()).
	 * @param value The new intensity value for the (x,y) position.
	 */
	public void set(int index, double value) {
		pixels[index] = value;
		range.record(value);
	}

	/**
	 * Sets the color at the input location.
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @param value The new intensity value for the (x,y) position.
	 */
	public void set(int x, int y, double value) {
		if (x < 0) {
			x = 0;
		} else if (x > width - 1) {
			x = width - 1;
		}

		if (y < 0) {
			y = 0;
		} else if (y > height - 1) {
			y = height - 1;
		}

		pixels[y * width + x] = value;
		range.record(value);
	}

	/**
	 * Subtracts the pixel values of image b from the matching pixel values in
	 * image a and returns the result as a new instance of IntensityImg. 
	 * Both input images must have the same number of pixels.
	 * @param a The first input image.
	 * @param b The second input image.
	 * @return The new image instance with the result of the addition.
	 */
	public static IntensityImg sub(IntensityImg a, IntensityImg b) {
		if (a.size() == b.size()) {
			IntensityImg container = new IntensityImg(a.width(), a.height());
			for (int i = 0; i < container.pixels.length; i++) {
				container.pixels[i] = a.pixels[i] - b.pixels[i];
				container.range.record(container.pixels[i]);
			}
			return container;
		} else {
			return null;
		}
	}

	/**
	 * Gets the intensity value at the input index.
	 * @param index The index of the intensity value. Range: [0, size()].
	 * @return The intensity value at the input index.
	 */
	public double get(int index) {
		return pixels[index];
	}

	/**
	 * Gets the intensity value at location(x, y).
	 * @param x The x coordinate. Range: [0, width()).
	 * @param y The y coordinate. Range: [0, height()).
	 * @return The intensity value.
	 */
	public double get(int x, int y) {
		if (x < 0) {
			x = 0;
		} else if (x > width - 1) {
			x = width - 1;
		}

		if (y < 0) {
			y = 0;
		} else if (y > height - 1) {
			y = height - 1;
		}

		return pixels[y * width + x];
	}

	/**
	 * Inverts all values of this image, i.e. pixels with the equal to the minimum
	 * of the range will have the value of the maximum of the range after inversion 
	 * and vica versa. The range will not be altered.
	 */
	public void invert() {
		float max = range().max();
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = max - pixels[i];
		}
	}

	/**
	 * Gets the mean intensity value of all local maxima.
	 * @return The mean value.
	 */
	public double meanOfMax() {
		
		if (range.min() == range.max() || (range.min() == Float.MIN_VALUE && range.max() == Float.MAX_VALUE)) {
			return 0;
		}
		
		double[][] deriativeFilterX = FilterFactory.sobelX();
		double[][] deriativeFilterY = FilterFactory.sobelY();

		IntensityImg xx = this.copy();
		IntensityImg yy = this.copy();
		IntensityImg xy = this.copy();
		xx.filter(deriativeFilterX);
		xx.filter(deriativeFilterX);
		yy.filter(deriativeFilterY);
		yy.filter(deriativeFilterY);
		xy.filter(deriativeFilterX);
		xy.filter(deriativeFilterY);
		xy.mult(xy);
		IntensityImg extrema = new IntensityImg(xx.width(), xx.height());
		extrema.add(xx);
		extrema.mult(yy);
		extrema.sub(xy);

		int matches = 0;
		double mean = 0;
		for (int i = 0; i < extrema.height(); i++) {
			for (int j = 0; j < extrema.width(); j++) {

				if (extrema.get(j, i) > 0 && xx.get(j, i) < 0 && yy.get(j, i) < 0) {
					matches++;
				} 
			}
		}


		return matches > 0 ? mean / matches : 0;
	}

	/**
	 * Estimates and returns the mean intensity value of this image,
	 * @return The mean value.
	 */
	public double mean() {
		double mean = 0;
		for (double p : pixels) {
			mean += p;
		}

		return mean / pixels.length;
	}

	/**
	 * Multiplies each pixel with the input scalar.
	 * @param val The value for the multiplication.
	 */
	public void mult(double val) {
		range.reset();
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] *= val;
			range.record(pixels[i]);
		}
	}
	
	/**
	 * Thresholds this image, i.e. creates a binary image with black (pixel value = 0)
	 * and white pixels (pixel value = 255), from a global threshold value.
	 * @param val The threshold value.
	 * @return A new instance which contains the binaries image.
	 */
	public IntensityImg threshold(double val) {
		IntensityImg copy = this.copy();
		
		copy.range().reset();
		for (int i = 0; i < copy.pixels.length; i++) {
			if (copy.pixels[i] < val) {
				copy.set(i, 0);
			} else {
				copy.set(i, 255);
			}
		}
		
		return copy;
	}

	/**
	 * Estimates the local maxima in the image and stores the estimation in a
	 * new IntensityImg where white pixels are local maxima and all pixels are
	 * black.
	 * @return An image with the local maxima.
	 */
	public IntensityImg maxima() {

		double[][] deriativeFilterX = FilterFactory.sobelX();
		double[][] deriativeFilterY = FilterFactory.sobelY();

		IntensityImg xx = this.copy();
		IntensityImg yy = this.copy();
		IntensityImg xy = this.copy();
		xx.filter(deriativeFilterX);
		xx.filter(deriativeFilterX);
		yy.filter(deriativeFilterY);
		yy.filter(deriativeFilterY);
		xy.filter(deriativeFilterX);
		xy.filter(deriativeFilterY);
		xy.mult(xy);
		IntensityImg extrema = new IntensityImg(xx.width(), xx.height());
		extrema.add(xx);
		extrema.mult(yy);
		extrema.sub(xy);

		for (int i = 0; i < extrema.height(); i++) {
			for (int j = 0; j < extrema.width(); j++) {

				if (extrema.get(j, i) > 0 && xx.get(j, i) < 0 && yy.get(j, i) < 0) {
					extrema.set(j, i, 255);
				} else {
					extrema.set(j, i, 0);
				}
			}
		}

		return extrema;
	}

	/**
	 * Multiplies each pixel value of this image with the matching pixel value 
	 * of the input image. Both images must have the same number of pixels.
	 * @param other The other image.
	 */
	public void mult(IntensityImg other) {
		if (size() == other.size()) {
			range.reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] =  pixels[i] * other.pixels[i];
				range.record(pixels[i]);
			}
		}
	}

	/**
	 * Adds the pixel values of the input image to the matching pixel values of
	 * this image. Both images must have the same number of pixels.
	 * @param other The other image.
	 */
	public void add(IntensityImg other) {
		if (size() == other.size()) {
			range.reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] += other.pixels[i];
				range.record(pixels[i]);
			}
		}
	}

	/**
	 * Subtracts the pixel values of the input image from the matching pixel values of
	 * this image. Both images must have the same number of pixels.
	 * @param other The other image.
	 */
	public void sub(IntensityImg other) {
		if (size() == other.size()) {
			range.reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] -= other.pixels[i];
				range.record(pixels[i]);
			}
		}
	}

	/**
	 * Multiplies each pixel value of this image with the exponential value of 
	 * the matching pixel in the input image. Both images must have the same 
	 * number of pixels.
	 * @param other The other image.
	 */
	public void exp(IntensityImg other) {
		if (size() == other.size()) {
			range.reset();
			for (int i = 0; i < pixels.length; i++) {
				pixels[i] =  pixels[i] * Math.exp(other.pixels[i]);
				range.record(pixels[i]);
			}
		}
	}
	
	/**
	 * Estimates the mean absolute error between the ground truth gt and the attention
	 * mask a. Both inputs are expected to be binary in range of [0, 255].
	 * @param gt The ground truth.
	 * @param a The attention mask.
	 * @return The mean absolute error between the two inputs.
	 */
	public static float MAE(IntensityImg gt, IntensityImg a) {
		if (a.width() != gt.width() || a.height() != gt.height()) {
			a = a.copy();
			a.resize(gt.width(), gt.height());
		}
		
		double sum = 0;
		
		for (int i = 0; i < a.size(); i++) {
			double delta = gt.get(i) - a.get(i);
			
			if (delta < 0) {
				delta *= -1;
			}
			
			sum += delta / 255;
		}
		
		sum /= a.size();
	
		return (float) sum;
	}

	/**
	 * Adds the pixel values of image a to the matching pixel values in 
	 * image b and returns the result as a new instance of IntensityImg. 
	 * Both input images must have the same number of pixels.
	 * @param a The first input image.
	 * @param b The second input image.
	 * @return The new image instance with the result of the addition.
	 */
	public static IntensityImg add(IntensityImg a, IntensityImg b) {
		if (a.size() == b.size()) {
			IntensityImg container = new IntensityImg(a.width(), a.height());
			for (int i = 0; i < container.pixels.length; i++) {
				container.pixels[i] = a.pixels[i] + b.pixels[i];
				container.range.record(container.pixels[i]);
			}
			return container;
		} else {
			return null;
		}
	}

	/**
	 * Performs convolution efficient by starting multiple threads and each parallel
	 * running thread processes a portion of the image convoltion.
	 * @author Diana Lange
	 *
	 */
	private class ConvolutionManager extends Thread{
		private double[] responses;
		private ConvolutionPerformerThread[] threads;

		private ConvolutionManager(double[] container, double[][]filter) {
			this.responses = container;

			threads  = new ConvolutionPerformerThread[Runtime.getRuntime().availableProcessors() * IntensityImg.threadMultiplier];
			// cut image pixels into equal parts and start child threads
			// who will process that part of the image
			int range = container.length / threads.length;
			for (int i = 0; i <  threads.length; i++) {
				int start = i * range;
				int end = start + range + 1; // not included

				if (i == threads.length - 1) {
					end = container.length;
				}

				threads[i] = new ConvolutionPerformerThread(container, filter, start, end);
			}

			this.start();
		}

		public double[] result() {
			return responses;
		}

		public void clear() {
			for (int i = 0; i < threads.length; i++) {
				threads[i].clear();
				threads[i] = null;
			}
			responses = null;
		}

		@Override 
		public void run() {
			boolean finished = false;
			while (!finished) {
				finished = true;
				for (ConvolutionPerformerThread t : threads) {
					if (t.getState() != Thread.State.TERMINATED) {
						finished = false;
						break;
					}
				}


				if (!finished) {
					try {
						this.wait(10);
					} catch(Exception e) {
					}
				}
			}
		}

		private class ConvolutionPerformerThread extends Thread {
			private int start;
			private int end; 
			private double[] responses;
			private double[][] filter;

			private ConvolutionPerformerThread(double[] container, double[][]filter, int start, int end) {
				this.responses = container;
				this.filter = filter;
				this.start = start;
				this.end = end;
				this.start();
			}

			public void clear() {
				responses = null;
				filter = null;
			}

			@Override
			public void run() {

				for (int i = start; i < end; i++) {
					responses[i] = convolute(pixels, i, filter);
				}
			}	
		}
	}
}