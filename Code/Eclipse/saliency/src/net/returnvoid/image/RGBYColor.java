package net.returnvoid.image;

import net.returnvoid.functions.DistanceFct;

/**
 * A class for the color spaces used in the opponent color model of 
 * https://ieeexplore.ieee.org/abstract/document/730558/.
 * RGB colors are separated into balanced R, G, B, Y channels which are normalized
 * by the intensity of the input RGB color values. The idea behind is that the
 * values for the RGBY color channels should be separated from the intensity
 * information. In this class the intensity information is stored in conjunction
 * with the RGBY color information.
 * 
 * @author Diana Lange
 *
 */
public class RGBYColor {
	
	/**
	 * The balanced Red value.
	 */
	private float R = 0;
	
	/**
	 * The balanced Green value.
	 */
	private float G = 0;
	
	/**
	 * The balanced Blue value.
	 */
	private float B = 0;
	
	/**
	 * The balanced Yellow value.
	 */
	private float Y = 0;
	
	/**
	 * The Intensity value.
	 */
	private float I = 0;

	/**
	 * Creates a new RGBYColor instance where all color values are set to zero.
	 */
	public RGBYColor() {
	}

	/**
	 * Creates a new RGBYColor instance with the input color values.
	 * @param R The balanced Red value. R=r-(g+b)/2 where rgb are the values of RGB color space.
	 * @param G The balanced Green value. G=g-(r+b)/2 where rgb are the values of RGB color space.
	 * @param B The balanced Blue value. B=b-(r+g)/2 where rgb are the values of RGB color space.
	 * @param Y The balanced Yellow value. Y=(r+g)/2-|r-g|/2-b where rgb are the values of RGB color space.
	 * @param I The Intensity value. I=(r+g+b)/3 where rgb are the values of RGB color space.
	 */
	public RGBYColor(float R, float G, float B, float Y, float I) {
		this.R = R;
		this.G = G;
		this.B = B;
		this.Y = Y;
		this.I = I;
	}

	

	/**
	 * Gets the intensity of this color.
	 * @return The intensity.
	 */
	public float intensity() {
		return I;
	}

	/**
	 * Gets the balanced Red value.
	 * @return The balanced Red value.
	 */
	public float R() {
		return R;
	}

	/**
	 * Gets the balanced Green value.
	 * @return The balanced Green value.
	 */
	public float G() {
		return G;
	}

	/**
	 * Gets the balanced Blue value.
	 * @return The balanced Blue value.
	 */
	public float B() {
		return B;
	}

	/**
	 * Gets the balanced Yellow value.
	 * @return The balanced Yellow value.
	 */
	public float Y() {
		return Y;
	}

	/**
	 * Adds the input color to this color, i.e. the color channels are added element by element.
	 * @param other The summand.
	 */
	public void add(RGBYColor other) {
		R += other.R;
		G += other.G;
		B += other.B;
		Y += other.Y;
		I += other.I;
	}

	/**
	 * Subtracts the input color from this color, i.e. the color channels are subtracted element by element.
	 * @param other The subtrahend.
	 */
	public void sub(RGBYColor other) {
		R -= other.R;
		G -= other.G;
		B -= other.B;
		Y -= other.Y;
		I -= other.I;
	}

	/**
	 * Multiplies all color channel values by the input scalar.
	 * @param scalar The scalar value.
	 */
	public void mult(float scalar) {
		R *= scalar;
		G *= scalar;
		B *= scalar;
		Y *= scalar;
		I *= scalar;
	}
	
	/**
	 * Creates and returns an Euclidean distance function for RGBYColor elements.
	 * This function is a linear composition of intensityEuclideanDistanceFct() and
	 * rgbyEuclideanDistanceFct().
	 * @return A distance function for RGBYColor elements.
	 */
	public static DistanceFct<RGBYColor> euclideanDistanceFct() {

		DistanceFct<RGBYColor> fct = (a, b) -> {
			return RGBYColor.intensityEuclideanDistanceFct().distance(a,b) + RGBYColor.rgbyEuclideanDistanceFct().distance(a,b);
		};


		return fct;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[" + R + ", " + G + ", " + B + ", " + Y + ", " + I + "]"; 
	}
	
	/**
	 * Creates and returns an Euclidean distance function for RGBYColor elements.
	 * This function only uses the intensity values of RGBYColor.
	 * @return A distance function for RGBYColor elements.
	 */
	public static DistanceFct<RGBYColor> intensityEuclideanDistanceFct() {

		DistanceFct<RGBYColor> fct = (a, b) -> {
			float deltaI = (a.I - b.I);

			return (float) Math.sqrt(deltaI * deltaI);
		};

		return fct;
	}
	
	/**
	 * Creates and returns a distance function for RGBYColor elements.
	 * This function implements the opponent color model, i.e. the distance
	 * is based Red-Green and Blue-Yellow differences.
	 * @return A distance function for RGBYColor elements.
	 */
	public static DistanceFct<RGBYColor> opponentColorDistanceFct() {

		DistanceFct<RGBYColor> fct = (a, b) -> {
			float deltaRG = (a.R - a.G) - (b.G - b.R);
			float deltaBY = (a.B - a.Y) - (b.Y - b.B);
			
			
			if (deltaRG < 0) {
				deltaRG *= -1;
			}
			
			if (deltaBY < 0) {
				deltaBY *= -1;
			}
			
			return deltaRG + deltaBY;
		};

		return fct;
	}

	/**
	 * Creates and returns an Euclidean distance function for RGBYColor elements.
	 * This function omits the intensity information of RGBYColor.
	 * @return A distance function for RGBYColor elements.
	 */
	public static DistanceFct<RGBYColor> rgbyEuclideanDistanceFct() {

		DistanceFct<RGBYColor> fct = (a, b) -> {
			float deltaR = (a.R - b.R);
			float deltaG = (a.G - b.G);
			float deltaB = (a.B - b.B);
			float deltaY = (a.Y - b.Y);

			return (float) Math.sqrt(deltaR * deltaR + deltaG * deltaG + deltaB * deltaB + deltaY * deltaY);
		};

		return fct;
	}
}
