package net.returnvoid.image;

import net.returnvoid.imagefeatures.FilterFactory;
import net.returnvoid.math.Range;
import processing.core.PImage;

/**
 * Stores the responses of an image to a set of gabor filters. The gabor filter
 * responses are stored in instances of IntensityImg (one IntensityImg for one
 * filter). The gabor filters will equal in size and settings but the angle property.
 * As a result, each IntensityImg represents a certain detected edge orientation 
 * in the input image.
 * @author Diana Lange
 *
 */
public class GaborImg implements RVImage {

	/**
	 * The angles for the gabor filters.
	 */
	private double[] angles;
	
	/**
	 * The responses of the gabor filters. 
	 */
	private IntensityImg[] gaborResponses;
	
	/*
	 * A sum of all gabor response images.
	 */
	private IntensityImg sum;
	
	/**
	 * The size of the gabor filters (best greater than 5).
	 */
	private int filterSize;
	
	/**
	 * The input image for the gabor filters (either LabImg or IntensityImg).
	 */
	private RVImage input;
	
	/**
	 * The number of periods shown in the gabor fitlers.
	 */
	private int periods = 1;
	
	/**
	 * The shape that the filter should respond to. For shape=1 the 
	 * filter responds to points (and lines), for shape=0 the filter responds only 
	 * to lines.
	 */
	private double shape = 0.5;
	
	/**
	 * Creates an image representation of gabor filter responses at various angles.
	 * @param input The input image. The responses will be estimated by filtering 
	 * this image. The image might get altered in this class, e.g. when the filter() function is called.
	 * @param angles The angles for the gabor filters. Should be in range of [0, 2 * Math.PI].
	 * @param filterSize The filter size of the gabor filters. Must be greater than 3.
	 * @param periods The periods of the gabor filter.
	 * @param shape The shape of the elements detected by the gabor filters.
	 */
	public GaborImg(LabImg input, double[] angles, int filterSize, int periods, double shape) {
		this(angles, filterSize, periods, shape);
		this.input = input;
		compute(input);
	}
	
	/**
	 * Creates an image representation of gabor filter responses at various angles.
	 * @param input The input image. The responses will be estimated by filtering 
	 * this image. The image might get altered in this class, e.g. when the filter() function is called.
	 * @param angles The angles for the gabor filters. Should be in range of [0, 2 * Math.PI].
	 * @param filterSize The filter size of the gabor filters. Must be greater than 3.
	 */
	public GaborImg(LabImg input, double[] angles, int filterSize) {
		this(angles, filterSize, 1, 0.5);
		this.input = input;
		compute(input);
	}
	
	/**
	 * Creates an image representation of gabor filter responses at various angles.
	 * @param input The input image. The responses will be estimated by filtering 
	 * this image. The image might get altered in this class, e.g. when the filter() function is called.
	 * @param angles The angles for the gabor filters. Should be in range of [0, 2 * Math.PI].
	 * @param filterSize The filter size of the gabor filters. Must be greater than 3.
	 * @param periods The periods of the gabor filter.
	 * @param shape The shape of the elements detected by the gabor filters.
	 */
	public GaborImg(IntensityImg input, double[] angles, int filterSize, int periods, double shape) {
		this(angles, filterSize, periods, shape);
		this.input = input;
		compute(input);
	}
	
	/**
	 * Creates an image representation of gabor filter responses at various angles.
	 * @param input The input image. The responses will be estimated by filtering 
	 * this image. The image might get altered in this class, e.g. when the filter() function is called.
	 * @param angles The angles for the gabor filters. Should be in range of [0, 2 * Math.PI].
	 * @param filterSize The filter size of the gabor filters. Must be greater than 3.
	 */
	public GaborImg(IntensityImg input, double[] angles, int filterSize) {
		this(angles, filterSize, 1, 0.5);
		this.input = input;
		compute(input);
	}
	
	/**
	 * Creates an image representation of gabor filter responses at various angles.
	 * Sets no input and doesn't start the computation. Use this only by other
	 * constructors!
	 * @param angles The angles for the gabor filters. Should be in range of [0, 2 * Math.PI].
	 * @param filterSize The filter size of the gabor filters. Must be greater than 3.
	 * @param periods The periods of the gabor filter.
	 * @param shape The shape of the elements detected by the gabor filters.
	 */
	private GaborImg(double[] angles, int filterSize, int periods, double shape) {
		this.periods = periods;
		this.shape = shape;
		this.filterSize = filterSize;
		this.gaborResponses = new IntensityImg[angles.length];
		this.angles = angles;
	}
	
	/**
	 * Creates an empty GaborImg, everything must be set manually. Use this
	 * only for the copy() function.
	 */
	private GaborImg() {
	}
	
	/**
	 * Computes the IntensityImg representation of the gabor filter responses.
	 */
	private void compute(LabImg input) {
		
		sum = null;
		
		for (int i = 0; i < angles.length; i++) {

			// the sign of the filter should not matter
			// compute for both signs the filter response and keep the bigger
			// value
			double[][] gabor1 = FilterFactory.gabor(filterSize, angles[i], periods, shape);
			double[][] gabor2 = FilterFactory.gabor(filterSize, Math.PI + angles[i],periods, shape);
			gaborResponses[i] = new IntensityImg(input.width(), input.height());
			
			for (int y = 0; y < input.height(); y++) {
				for (int x = 0; x < input.width(); x++) {
					int c1 = input.filterResponse(x, y, gabor1);
					int c2 = input.filterResponse(x, y, gabor2);
					
					// get maximum value of the color channels, this will be the global response
					float intensity1 = Math.max(c1 >> 16 & 255, Math.max(c1 >> 8 & 255, c1 & 255));
					float intensity2 = Math.max(c2 >> 16 & 255, Math.max(c2 >> 8 & 255, c2 & 255));
					gaborResponses[i].set(x,  y, intensity1 > intensity2 ? intensity1 : intensity2);
				}
			}

		}
	}
	
	/**
	 * Computes the IntensityImg representation of the gabor filter responses.
	 */
	private void compute(IntensityImg input) {
		
		sum = null;
		
		for (int i = 0; i < angles.length; i++) {

			// the sign of the filter should not matter
			// compute for both signs the filter response and keep the bigger
			// value
			double[][] gabor1 = FilterFactory.gabor(filterSize, angles[i], periods, shape);
			double[][] gabor2 = FilterFactory.gabor(filterSize, Math.PI + angles[i],periods, shape);
			gaborResponses[i] = new IntensityImg(input.width(), input.height());
			
			for (int y = 0; y < input.height(); y++) {
				for (int x = 0; x < input.width(); x++) {
					double c1 = input.filterResponse(x, y, gabor1);
					double c2 = input.filterResponse(x, y, gabor2);
					
					// get maximum value of the color channels, this will be the global response
					gaborResponses[i].set(x,  y, c1 > c2 ? c1 : c2);
				}
			}

		}
	}
	
	/**
	 * Gets the angles that were detected by this instance. The number of angles
	 * is equal to the number the intensity images that can be obtained by the getByAngle() method.
	 * @return  The detected angles in the input image.
	 */
	public double[] angles() {
		return angles;
	}
	
	/**
	 * Gets an intensity image which represents the response to a gabor filter at
	 * a certain angle, i.e. the returned image shows which edges of the input image
	 * has a certain orientation.
	 * @param i The index of the gabor response image. Range: [0, angles().length].
	 * @return The intensity image containing the gabor response at the angle of 
	 * angles()[i]. The range of the returned image is in range of [0, 255].
	 */
	public IntensityImg getByAngle(int i) {
		return gaborResponses[i];
	}
	
	/**
	 * Creates a sum of all gabor response images.
	 * @return The sum of all gabor responses with range of [0, 255].
	 */
	public IntensityImg sum() {

		if (sum == null) {
			sum = new IntensityImg(width(), height());

			for (int i = 0; i < angles.length; i++) {
				sum.add(this.getByAngle(i));
			}

			sum.toRange(new Range(0, 255));

		}

		return sum;
	}
	
	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#gradient(int, int)
	 */
	@Override
	public float[] gradient(int x, int y) {
		return input.gradient(x, y);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#img()
	 */
	@Override
	public PImage img() {

		IntensityImg sum = sum();

		sum.toRange(new Range(0, 255));

		return sum.img();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#width()
	 */
	@Override
	public int width() {
		return getByAngle(0).width();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#height()
	 */
	@Override
	public int height() {
		return getByAngle(0).height();
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][])
	 */
	@Override
	public GaborImg filter(double[][] filter) {
		return filter(filter, false);
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#filter(double[][], boolean)
	 */
	@Override
	public GaborImg filter(double[][] filter, boolean copy) {
		
		// filtering is done on the original input image and gabor responses
		// are re-calculated
		RVImage filtered = input.filter(filter, copy);
		
		if (copy) {
			if (filtered instanceof LabImg) {
				return new GaborImg((LabImg) filtered, angles, filterSize, periods, shape);
			} else {
				return new GaborImg((IntensityImg) filtered, angles, filterSize, periods, shape);
			}
		} else {
			this.input = filtered;
			if (filtered instanceof LabImg) {
				this.compute((LabImg) input);
			} else {
				this.compute((IntensityImg) input);
			}
			return this;
		}
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#copy()
	 */
	@Override
	public GaborImg copy() {
		GaborImg g = new GaborImg();
		g.periods = periods;
		g.shape = shape;
		g.filterSize = filterSize;
		g.angles = new double[angles.length];
		System.arraycopy(angles, 0, g.angles, 0, angles.length);
		g.gaborResponses = new IntensityImg[gaborResponses.length];
		for (int i = 0; i < gaborResponses.length; i++) {
			g.gaborResponses[i] = gaborResponses[i].copy();
		}
		g.input = input.copy();
		return g;
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#resize(int, int)
	 */
	@Override
	public void resize(int newWidth, int newHeight) {
		input.resize(newWidth, newHeight);
		if (input instanceof LabImg) {
			this.compute((LabImg) input);
		} else {
			this.compute((IntensityImg) input);
		}
	}

	/* (non-Javadoc)
	 * @see net.returnvoid.image.RVImage#size()
	 */
	@Override
	public int size() {
		return input.size();
	}
	
}
