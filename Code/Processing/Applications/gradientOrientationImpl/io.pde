void keyReleased() {

  if (key == '+') {
    resFactor = --resFactor < 1 ? 1 : resFactor;
  } else if (key == '-') {
    resFactor++;
  } else if (key >= '1' && key <= (char) ('0' + pyramid.levels())) {
    level = (int) (key - '0') - 1;
  } else if (key == 'i') {
    displayImg = !displayImg;
  } else if (key == 'g') {
    displayGrads = !displayGrads;
  } else if (key == 'w') {

    area -= 2;

    if (area < 1) {
      area = 1;
    }

    for (int i = 0; i < pyramid.levels(); i++) {
      Gradient g = pyramid.get(i).gradient();
      g.setArea(area);
    }
  } else if (key == 's') {
    area += 2;
    for (int i = 0; i < pyramid.levels(); i++) {
      Gradient g = pyramid.get(i).gradient();
      g.setArea(area);
    }
  } else if (key == 'd') {
    cutoff += 0.05;
  } else if (key == 'a') {
    cutoff -= 0.05;

    if (cutoff < 0.05) {
      cutoff = 0.05;
    }
  } else if (key == 'p') {
    pyramid.save(this, "export/" + StringTools.timestamp());
  } else {
    saveFrame("export/" + StringTools.timestamp() + ".png");
  }
  redraw();
}