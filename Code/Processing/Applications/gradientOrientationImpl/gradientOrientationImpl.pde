import java.util.*;

Pyramid<GradientImg> pyramid;


int resFactor = 6;

boolean displayImg = false;
boolean displayGrads = true;

int level = 2;
float cutoff = 0.15;
int area = 5;

void setup() {
  size(1280, 860);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "_MG_6966 Kopie.jpg");
  raw.resize(width, 0);

  pyramid = Pyramid.createGauss(new GradientImg(new LabImg(raw), area), 9, 3);

  noLoop();
}


void draw() {

  background(247);

  GradientImg img = pyramid.get(level);
  GradientImg base = pyramid.get(0);

  float scale = (float) base.width() / img.width();

  //scale();
  if (displayImg) {
    image(img.img(), 0, 0, base.width(), base.height());
  }

  if (displayGrads) {

    int steps = (int) (resFactor / scale);
    if (steps < 1) {
      steps = 1;
    }
    

    Gradient gradient = pyramid.get(level).gradient();

    for (int y = 0; y < base.height(); y += resFactor) {
      int yy = pyramid.mappedY(y, 0, level);
      for (int x = 0; x < base.width(); x += resFactor) {
        
        int xx = pyramid.mappedX(x, 0, level);
        
        
        Coord grad = gradient.get(xx, yy);
        float a = grad.angle() + PI / 2;
        float m = grad.mag() / gradient.maxMag();
        //print(m + ", ");
 
        if (m < cutoff) {
           continue;
        }
        
        m *= 1.1 * sqrt(2 * resFactor * resFactor);


        line(x - cos(a) * m, y - sin(a) * m, x + cos(a) * m, y + sin(a) * m);
      }
    }
  }
}