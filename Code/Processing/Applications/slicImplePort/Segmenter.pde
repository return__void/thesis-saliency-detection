class Segmenter implements ImageUpdateObserver {

  SLIC slic = null;
  PApplet parent;
  int K;
  PImage segmentationImg = null;
  boolean drawHull = false;
  boolean drawSizes = false;
  boolean drawEdges = false;
  boolean drawBoundingBoxes = false;

  Segmenter(PApplet parent, int K) {
    this.K = K;
    this.parent = parent;
  }

  void setK(int K) {
    this.K = K;
    this.setImage(slic.labimg().img());
  }

  void setImage(PImage img) {

    long heapFreeSizeBefore = Runtime.getRuntime().freeMemory();
    long start = System.currentTimeMillis();

    slic = new SLIC(img, K);

    long end = System.currentTimeMillis();
    println("execution time: " + (end - start) / 1000f + "s");

    segmentationImg = null;

    // Get current size of heap in bytes
    //long heapSize = Runtime.getRuntime().totalMemory(); 

    // Get maximum size of heap in bytes. The heap cannot grow beyond this size.// Any attempt will result in an OutOfMemoryException.
    //long heapMaxSize = Runtime.getRuntime().maxMemory();

    // Get amount of free memory within the heap in bytes. This size will increase // after garbage collection and decrease as new objects are created.
    long heapFreeSizeAfter = Runtime.getRuntime().freeMemory();
    println("free heap size before and after iteration: " + heapFreeSizeBefore + " / " + heapFreeSizeAfter);
  }

  void merge() {


    println("start merging with " + slic.K() + " segments");
   
    //println("mean: " + d);
    slic = slic.mergeSegments();
    segmentationImg = null;
    println("number of segments after merging: " + slic.K());
  }

  void keyPressed(char key) {
    if (key == 'h') {
      drawHull = !drawHull;
    } else if (key == 's') {
      drawSizes = !drawSizes;
    } else if (key == 'e') {
      drawEdges = !drawEdges;
    } else if (key == 'b') {
      drawBoundingBoxes = !drawBoundingBoxes;
    }
  }

  void draw() {
    if (slic == null) {
      return;
    }

    float maxW = width / 2.0;
    float maxH = height;

    float scale = slic.labimg().width() > slic.labimg().height() ? maxW / slic.labimg().width() : maxH / slic.labimg().height();

    if (segmentationImg == null) {
      LabImg labImg = slic.labimg().copy();
      SLIC.writeToImage(slic.superpixels(), labImg);
      segmentationImg = labImg.img();
    }

    image(slic.labimg().img(), width / 2, 0, slic.labimg().width() * scale, slic.labimg().height() * scale); 
    image(segmentationImg, 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);

    if (drawEdges) {
      tint(255, 150);
      slic.drawEdge(parent, 0, 0, slic.labimg().width() * scale);
      noTint();
    }

    if (drawHull) {
      slic.drawHull(parent.g, 0, 0, slic.labimg().width() * scale);
    }

    if (drawBoundingBoxes) {
      slic.drawBoundingBoxes(parent.g, 0, 0, slic.labimg().width() * scale);
    }

    if (drawSizes) {
      slic.drawSuperpixelSize(parent.g, 0, 0, slic.labimg().width() * scale);
    }
  }
}