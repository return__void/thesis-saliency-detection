ImageLoader imgLoader;

Segmenter slic;

void setup() {
  size(1900, 720);
  
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  imgLoader = new ImageLoader(this, directoreyPath);

  SLIC.threadMultiplierSimple = 10;
  SLIC.threadMultiplierComplex = 1;
  SLIC.debug = true;
  SLIC.maxPixels = 300000;
  SLIC.maxIterations = 10;

  slic = new Segmenter(this, 4000);
  imgLoader.addObserver(slic);
  imgLoader.nextRandom();

}

void draw() {  

  background(247);
  
  
  slic.draw();
  
}