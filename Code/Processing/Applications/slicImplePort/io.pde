void keyPressed() {
  if (key == 'p') {
    saveFrame("export/" + StringTools.timestamp() + ".jpg");
  }  else if (key == '1') {
    slic.setK(500);
  } else if (key == '2') {
    slic.setK(1000);
  } else if (key == '3') {
    slic.setK(2000);
  } else if (key == '4') {
    slic.setK(4000);
  } else if (key == '5') {
    slic.setK(6000);
  } else if (key == '6') {
    slic.setK(8000);
  } else if (key == '7') {
    slic.setK(10000);
  } else if (key == '0') {
    slic.setK(200);
  } else if (key == 'm') {
    slic.merge();
  } else {
    slic.keyPressed(key);
  }
}