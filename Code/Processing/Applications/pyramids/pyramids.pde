PImage input;
Pyramid<RGBYImg> pyramidRGBY;
Pyramid<LabImg> pyramidLab;
Pyramid<GaborImg> pyramidGabor;
Pyramid<IntensityImg> pyramidIntensity;

int level = 0;
int mode = 0;

void setup() {
  size(1280, 850);
  //size(1280, 500);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  input = loadImage(directoreyPath + "/" + "P1060266.jpg");
  input.resize(800, 0);

  RGBYImg rgby = new RGBYImg(input);

  pyramidRGBY = Pyramid.createGauss(new RGBYImg(input), 9, 3);
  pyramidLab = Pyramid.createGauss(new LabImg(input.copy()), 9, 3);
  pyramidGabor = Pyramid.createGauss(new GaborImg(new LabImg(input.copy()), new double[] {0, PI / 4, PI / 2, PI / 2 + PI / 4}, 5), 9, 3);
  pyramidIntensity = Pyramid.createGauss(rgby.intensity(), 9, 3);

  noLoop();
}

void draw() {
  background(247);

  if (mode == 0) {
    IntensityImg R = pyramidRGBY.get(level).R().copy();
    IntensityImg G = pyramidRGBY.get(level).G().copy();
    IntensityImg B = pyramidRGBY.get(level).B().copy();
    IntensityImg Y = pyramidRGBY.get(level).Y().copy();


    image(R.img(), 0, 0, width / 2, R.height() * (width / 2f) / R.width());
    image(G.img(), width / 2, 0, width / 2, R.height() * (width / 2f) / R.width());
    image(B.img(), 0, height / 2, width / 2, R.height() * (width / 2f) / R.width());
    image(Y.img(), width / 2, height / 2, width / 2, R.height() * (width / 2f) / R.width());
    
    /*
    PImage img = pyramidRGBY.get(level).img();
    image(img, 0, 0, width, img.height * ((float) width) / img.width);
    */

  } else if (mode == 1) {
    LabImg lab = pyramidLab.get(level);

    image(lab.img(), 0, 0, width, lab.height() * ((float) width) / lab.width());
  } else if (mode == 2) {
    
    IntensityImg g0 = pyramidGabor.get(level).getByAngle(0).copy();
    IntensityImg g1 = pyramidGabor.get(level).getByAngle(1).copy();
    IntensityImg g2 = pyramidGabor.get(level).getByAngle(2).copy();
    IntensityImg g3 = pyramidGabor.get(level).getByAngle(3).copy();
    g0.toRange(new Range(0, 255));
    g1.toRange(new Range(0, 255));
    g2.toRange(new Range(0, 255));
    g3.toRange(new Range(0, 255));

    image(g0.img(), 0, 0, width / 2, g0.height() * (width / 2f) / g0.width());
    image(g1.img(), width / 2, 0, width / 2, g1.height() * (width / 2f) / g1.width());
    image(g2.img(), 0, height / 2, width / 2, g2.height() * (width / 2f) / g2.width());
    image(g3.img(), width / 2, height / 2, width / 2, g3.height() * (width / 2f) / g3.width());

  } else {
    IntensityImg img = pyramidIntensity.get(level);

    image(img.img(), 0, 0, width, img.height() * ((float) width) / img.width());
  }
}

void mousePressed() {

  if (mouseButton == LEFT) {
    level++;

    if (level == pyramidRGBY.levels()) {
      level = 0;
    }
  } else {
    mode++;

    if (mode > 3) {
      mode = 0;
    }
  }
  redraw();
}

void keyPressed() {
  if (key == 'f') {
    saveFrame("export/" + StringTools.timestamp() + ".png");
  }
}