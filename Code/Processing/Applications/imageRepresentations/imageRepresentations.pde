
ImageService service;
ImageLoader imgLoader;
RGBYImg rgbyFromRGB;
RGBYImg rgbyFromLab;
GradientImg gradient;
GaborImg gabor;
GaborImg texture;

int activeOption = 0;
String[] options = {
  "IntensityImg from RGB", 
  "IntensityImg from Lab", 
  "RGBYImg normalized width RGB intensity", 
  "RGBYImg normalized width Lab intensity", 
  "GradientImg", 
  "GaborImg edge detector (by angles)", 
  "GaborImg edge detector", 
  "GaborImg texture detector", 
  "GaborImg texture detector (by angles)"
};




void setup() {
  size(1280, 800);
  //size(1280, 500);

  IntensityImg.threadMultiplier = 100;
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  service = new ImageService();
  imgLoader = new ImageLoader(this, directoreyPath);
  imgLoader.addObserver(service);
  imgLoader.nextRandom();
  noLoop();
}

void draw() {
  background(247);



  if (activeOption == 0) {
    PImage img = rgbyFromRGB.intensity().img();


    float scale = img.width >= img.height ? width / (float) img.width : height / (float) img.height;
    float offset = img.height > img.width ? width - img.width * scale : 0;

    image(img, offset, 0, img.width * scale, img.height * scale);
  } else if (activeOption == 1) {
    PImage img = rgbyFromLab.intensity().img();

    float scale = img.width >= img.height ? width / (float) img.width : height / (float) img.height;
    float offset = img.height > img.width ? width - img.width * scale : 0;

    image(img, offset, 0, img.width * scale, img.height * scale);
  } else if (activeOption == 2) {
    PImage R = rgbyFromRGB.R().img();
    PImage G = rgbyFromRGB.G().img();
    PImage B = rgbyFromRGB.B().img();
    PImage Y = rgbyFromRGB.Y().img();

    float scale = 0.5 * (R.width >= R.height ? width / (float) R.width : height / (float) R.height);
    float offset = R.height > R.width ? width  / 2 - R.width * scale : 0;

    image(R, offset, 0, R.width * scale, R.height * scale);
    image(G, width * 0.5 + offset, 0, G.width * scale, G.height * scale);
    image(B, offset, height * 0.5, B.width * scale, B.height * scale);
    image(Y, width * 0.5 + offset, height * 0.5, Y.width * scale, Y.height * scale);
  } else if (activeOption == 3) {
    PImage R = rgbyFromLab.R().img();
    PImage G = rgbyFromLab.G().img();
    PImage B = rgbyFromLab.B().img();
    PImage Y = rgbyFromLab.Y().img();

    float scale = 0.5 * (R.width >= R.height ? width / (float) R.width : height / (float) R.height);
    float offset = R.height > R.width ? width  / 2 - R.width * scale : 0;

    image(R, offset, 0, R.width * scale, R.height * scale);
    image(G, width * 0.5 + offset, 0, G.width * scale, G.height * scale);
    image(B, offset, height * 0.5, B.width * scale, B.height * scale);
    image(Y, width * 0.5 + offset, height * 0.5, Y.width * scale, Y.height * scale);
  } else if (activeOption == 4) {
    PImage img = gradient.img();

    float scale = img.width >= img.height ? width / (float) img.width : height / (float) img.height;
    float offset = img.height > img.width ? width - img.width * scale : 0;

    image(img, offset, 0, img.width * scale, img.height * scale);
  } else if (activeOption == 5) {
    IntensityImg ig0 = gabor.getByAngle(0);
    IntensityImg ig45 = gabor.getByAngle(1);
    IntensityImg ig90 = gabor.getByAngle(2);
    IntensityImg ig135 = gabor.getByAngle(3);
    ig0.toRange(new Range(0, 255));
    ig45.toRange(new Range(0, 255));
    ig90.toRange(new Range(0, 255));
    ig135.toRange(new Range(0, 255));

    PImage g0 = ig0.img();
    PImage g45 = ig45.img();
    PImage g90 = ig90.img();
    PImage g135 = ig135.img();

    float scale = 0.5 * (g0.width >= g0.height ? width / (float) g0.width : height / (float) g0.height);
    float offset = g0.height > g0.width ? width  / 2 - g0.width * scale : 0;

    image(g0, offset, 0, g0.width * scale, g0.height * scale);
    image(g45, width * 0.5 + offset, 0, g45.width * scale, g45.height * scale);
    image(g90, offset, height * 0.5, g90.width * scale, g90.height * scale);
    image(g135, width * 0.5 + offset, height * 0.5, g135.width * scale, g135.height * scale);
  } else if (activeOption == 6) {
    PImage img = gabor.img();

    float scale = img.width >= img.height ? width / (float) img.width : height / (float) img.height;
    float offset = img.height > img.width ? width - img.width * scale : 0;

    image(img, offset, 0, img.width * scale, img.height * scale);
  } else if (activeOption == 7) {
    PImage img = texture.img();

    float scale = img.width >= img.height ? width / (float) img.width : height / (float) img.height;
    float offset = img.height > img.width ? width - img.width * scale : 0;

    image(img, offset, 0, img.width * scale, img.height * scale);
  } else {
    IntensityImg ig0 = texture.getByAngle(0);
    IntensityImg ig45 = texture.getByAngle(1);
    IntensityImg ig90 = texture.getByAngle(2);
    IntensityImg ig135 = texture.getByAngle(3);
    ig0.toRange(new Range(0, 255));
    ig45.toRange(new Range(0, 255));
    ig90.toRange(new Range(0, 255));
    ig135.toRange(new Range(0, 255));

    PImage g0 = ig0.img();
    PImage g45 = ig45.img();
    PImage g90 = ig90.img();
    PImage g135 = ig135.img();

    float scale = 0.5 * (g0.width >= g0.height ? width / (float) g0.width : height / (float) g0.height);
    float offset = g0.height > g0.width ? width  / 2 - g0.width * scale : 0;

    image(g0, offset, 0, g0.width * scale, g0.height * scale);
    image(g45, width * 0.5 + offset, 0, g45.width * scale, g45.height * scale);
    image(g90, offset, height * 0.5, g90.width * scale, g90.height * scale);
    image(g135, width * 0.5 + offset, height * 0.5, g135.width * scale, g135.height * scale);
  }

  String txt = options[activeOption];
  textSize(16);

  noStroke();
  fill(255);
  rect(0, 0, 32 + textWidth(txt), 32);

  fill(0);
  text(txt, 16, 24);
}

void mousePressed() {

  if (mouseButton == LEFT) {
    activeOption++;
    if (activeOption == options.length) {
      activeOption = 0;
    }
  } else {
    activeOption--;

    if (activeOption == -1) {
      activeOption = options.length - 1;
    }
  }

  redraw();
}

void keyPressed() {
}

class ImageService implements ImageUpdateObserver {
  void setImage(PImage img) {
    println("new image at " + StringTools.timestamp());
    PImage copy = img.copy();

    if (copy.width >= copy.height) {
      copy.resize(width, 0);
    } else {
      copy.resize(0, height);
    }

    LabImg lab = new LabImg(copy.copy());
    rgbyFromRGB = new RGBYImg(copy);
    rgbyFromLab = new RGBYImg(lab);
    gradient = new GradientImg(lab.copy());
    gabor = new GaborImg(lab.copy(), new double[] {0, PI / 4, PI / 2, PI / 4 + PI / 2}, 5);
    texture = new GaborImg(lab.copy(), new double[] {0, PI / 4, PI / 2, PI / 4 + PI / 2}, 17, 5, 0.5);
    redraw();
  }
}