class Saliency implements ImageUpdateObserver {

  PApplet parent;
  SegSaliency segSaliency = null;
  PixSaliency pixSaliency = null;
  int K;
  int segSaliencyMapDrawMode = 0;
  int pixSaliencyMapDrawMode = PixSaliency.maps().length - 1;

  boolean optimize = false;
  boolean binarize = false;
  boolean drawHistogram = false;
  boolean performSegmentationOnPixelMap = false;
  PImage img;

  boolean showSegmentationSaliency = true;
  
  SegSaliency.FeatureName[] features = {
    SegSaliency.FeatureName.COLOR_LAB,
    SegSaliency.FeatureName.EDGE,
    SegSaliency.FeatureName.TEXTURE_ALT
  };

  Saliency(PApplet parent, int K) {
    this.K = K;
    this.parent = parent;
    this.segSaliency = new SegSaliency(K, features);
    this.pixSaliency = new PixSaliency();
  }

  void setImage(PImage img) {

    if (this.img == null || this.img != img) {
      this.img = img;
      segSaliency.setImage(img);
      pixSaliency.setImage(img);
      println("done at " + StringTools.timestamp());
    }
  }

  void keyPressed() {
    if (key == '+') {
      if (showSegmentationSaliency) {
        segSaliencyMapDrawMode = ++segSaliencyMapDrawMode > segSaliency.options().size() ? 0 : segSaliencyMapDrawMode;
      } else {
        pixSaliencyMapDrawMode = ++pixSaliencyMapDrawMode > PixSaliency.maps().length ? 0 : pixSaliencyMapDrawMode;
      }
    } else if (key == '-') {
      if (showSegmentationSaliency) {
        segSaliencyMapDrawMode = --segSaliencyMapDrawMode >= 0 ? segSaliencyMapDrawMode : segSaliency.options().size() ;
      } else {
        pixSaliencyMapDrawMode = --pixSaliencyMapDrawMode >= 0 ? pixSaliencyMapDrawMode : PixSaliency.maps().length;
      }
    } else if (key == 'i') {
      performSegmentationOnPixelMap = !performSegmentationOnPixelMap;
    } else if (key == 'm') {
      segSaliency.merge();
    } else if (key == 'b') {
      binarize = !binarize;
    } else if (key == 'o') {
      optimize = !optimize;
      println("Optimization is " + (optimize ? "on" : "off"));
    } else if (key == 'h') {
      drawHistogram = !drawHistogram;
    } else if (key == 'e') {
      println("saving all saliency maps");


      for (String name : segSaliency.options()) {

        String filename = name.replaceAll(": ", "-");

        PImage img = segSaliency.getMap(name).img();
        img.parent = parent;
        img.save("export/" + StringTools.timestamp() + "_" + filename + ".png");
      }

      SLIC slic = segSaliency.segmentation();  
      String[] pixelMaps = PixSaliency.maps();

      for (int i = 0; i < pixelMaps.length; i++) {
        String filename = "pixel-" + pixelMaps[i];
        PImage img = pixSaliency.getMap(i).img();
        PImage segImg = pixSaliency.getSegMap(i, slic).img();
        img.parent = parent;
        segImg.parent = parent;
        img.save("export/" + StringTools.timestamp() + "_" + filename + ".png");
        segImg.save("export/" + StringTools.timestamp() + "_" + filename + "_segmented.png");
      }

      println("done exporting maps");
    } else if (key == 's') {
      showSegmentationSaliency = !showSegmentationSaliency;
    } else if (key == '1') {
      println("switched to RGB distance");
      segSaliency.setColorDistance(LabDistanceFactory.RGBEuclideanDistanceFct());
      //segSaliency.setRGBYDistance(RGBYColor.opponentColorDistanceFct());
    } else if (key == '2') {
      println("switched to Lab distance");
      //segSaliency.setHistogramDistance(Histogram.chiSquaredDistanceFct());
      segSaliency.setColorDistance(LabDistanceFactory.euclideanDistanceFct());
      //segSaliency.setRGBYDistance(RGBYColor.opponentColorDistanceFct());
    } else if (key == '3') {
      println("switched to RBB+Lab distance");
      //segSaliency.setHistogramDistance(Histogram.chiSquaredDistanceFct());
      segSaliency.setColorDistance(LabDistanceFactory.RGBPlusLabEuclideanDistanceFct());
      //segSaliency.setRGBYDistance(RGBYColor.opponentColorDistanceFct());
    } else if (key == '4') {
      println("switched to Chi squared distance for histograms");
      segSaliency.setHistogramDistance(Histogram.chiSquaredDistanceFct());
    } else if (key == '5') {
      println("switched to Euclidean distance for histograms");
      segSaliency.setHistogramDistance(Histogram.euclideanDistanceFct());
    } else if (key == '6') {
      println("switched to summation mode for multi-histograms");
      segSaliency.setHistogramDistance("sum");
    } else if (key == '7') {
      println("switched to multiplication mode for multi-histograms");
      segSaliency.setHistogramDistance("mult");
    } else if (key == '8') {
      println("switched to minimum mode for multi-histograms");
      segSaliency.setHistogramDistance("min");
    } else if (key == '9') {
      println("switched to maximum mode for multi-histograms");
      segSaliency.setHistogramDistance("max");
    }
  }

  void drawHistogram(IntensityImg map) {
    Histogram1D greyLevels = new Histogram1D(0, 255, 256);
    for (int i = 0; i < map.size(); i++) {
      greyLevels.add((float) map.get(i));
    }

    float w = width / 2;
    float h = map.width() >= map.height() ? map.height() * (width / 2f) / map.width() : height;

    noStroke();
    fill(247);
    rect(width / 2, 0, w, h);

    stroke(15);
    line(width / 2, 0, width / 2, h);
    line(width / 2, h, width, h);

    noStroke();
    fill(15);
    greyLevels.draw(g, width - w, 0, w, h);
  }


  void draw() {

    if (segSaliency == null) {
      return;
    }

    float maxW = width / 2.0;
    float maxH = height;

    SLIC slic = segSaliency.segmentation();

    float scale = slic.labimg().width() > slic.labimg().height() ? maxW / slic.labimg().width() : maxH / slic.labimg().height();


    image(slic.labimg().img(), width / 2, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);
    image(segSaliency.segmentationImg(), 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);


    if (showSegmentationSaliency) {
      if (segSaliencyMapDrawMode < segSaliency.options().size()) {

        String name = segSaliency.option(segSaliencyMapDrawMode);

        IntensityImg map = segSaliency.getMap(name);

        if (optimize) {
          map = SegSaliency.optimize(slic, map, 1);
          //map = SegSaliency.optimize(slic, map);
        }

        if (binarize) {
          map = map.threshold(map.mean() * 2);
        }

        PImage saliencyMap = map.img();
        image(saliencyMap, 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);

        if (drawHistogram) {
          drawHistogram(map);
        }


        String text = "Saliency Map: " + name;
        fill(247);
        noStroke();
        textSize(16);
        rect(0, height - 2 * 16, textWidth(text) + 2 * 16, 2 * 16);

        fill(0);
        text(text, 16, height - 10);
      }
    } else {

      if (pixSaliencyMapDrawMode < PixSaliency.maps().length) {
        String name = PixSaliency.maps()[pixSaliencyMapDrawMode];

        IntensityImg map = performSegmentationOnPixelMap ? pixSaliency.getSegMap(pixSaliencyMapDrawMode, slic) : pixSaliency.getMap(pixSaliencyMapDrawMode);

        if (optimize && performSegmentationOnPixelMap) {
          map = SegSaliency.optimize(slic, map, 1);
        }

        if (binarize) {
          map = map.threshold(map.mean() * 2);
        }

        PImage saliencyMap = map.img();
        image(saliencyMap, 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);

        if (drawHistogram) {
          drawHistogram(map);
        }

        String text = "Saliency Map: " + name;
        fill(247);
        noStroke();
        textSize(16);
        rect(0, height - 2 * 16, textWidth(text) + 2 * 16, 2 * 16);

        fill(0);
        text(text, 16, height - 10);
      }
    }
  }
}