
/**
 * Saliency detection application.
 * Please wait until the process is finished. The process is finished when
 * the terminal prints "Done at %timestamp%".
 * Shows two saliency detection algorithms: a pixel based and a segmentation based variation. 
 * Both produce intermediate maps which can be shown separate as well as a final saliency map.
 *
 * Controls: 
 * S     := switches between the saliency detection implementations (pixel based vs. segmentation based)
 * H     := shows / hides histogram of current saliency map
 * + / - := switches between the intermediate maps / saliency maps
 * B     := binarization on / off
 * M     := merge segments (all saliency maps for segmentation based implementation will be re-computed)
 * I     := perform segmentation on / off on pixel based saliency detector
 * E     := export all saliency maps including the intermediate maps for current image
 * O     := saliency map optimization on / off
 * P     := export screenshot of application
 * 1     := set RGB Euclidean distance function for color saliency detection in segmentation based implementation
 * 2     := set Lab Euclidean distance function for color saliency detection in segmentation based implementation
 * 3     := set RGB + Lab Euclidean distance function for color saliency detection in segmentation based implementation
 * 4     := set Chi Squared distance function for histogram saliency detection in segmentation based implementation
 * 5     := set Euclidean distance function for histogram saliency detection in segmentation based implementation
 *
 * Tab   := switch to next random image
 * Arrow left / right := switch to previous / next image in loading image folder
 *
 * @autor Diana Lange
 */

import java.util.Map;
ImageLoader imgLoader;


Saliency saliency;

void setup() {
  size(1900, 820);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data/own");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data/paper";
  } else {
    directoreyPath = "data-public";
  }

  imgLoader = new ImageLoader(this, directoreyPath);

  SLIC.maxPixels = 200000;
  SegSaliency.debug = true;
  SLIC.debug = true;
  PixSaliency.maxPixels = 400000;


  saliency = new Saliency(this, 2000);
  imgLoader.addObserver(saliency);
  imgLoader.nextRandom();
}

void draw() {  

  background(247);

  saliency.draw();
}