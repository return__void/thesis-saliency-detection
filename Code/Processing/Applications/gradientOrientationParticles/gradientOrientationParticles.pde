import java.util.*;

LabImg img;
IntensityImg mask;

Gradient gradient;

ArrayList<Particle> particles = new ArrayList<Particle>();

void setup() {
  size(1280, 720);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "land.jpg");
  PImage maskRaw = loadImage(directoreyPath + "/" + "20180725_191027_pixel-saliency.png");
  raw.resize(width, 0);
  maskRaw.resize(raw.width, raw.height);

  img = new LabImg(raw);
  mask = new IntensityImg(maskRaw);
  //img.filter(FilterFactory.gauss(5), false);

  gradient = new Gradient(mask, 10);

  for (int i = 0; i < 1000; i++) {
    particles.add(new Particle());
  }

  background(10);
  //image(img.img(), 0, 0);
}


void draw() {



  for (Particle p : particles) {
    p.update(img, gradient);
    p.move();
    p.draw();
  }

}

class Particle {
  Coord lloc;
  Coord loc;
  Coord dir;
  float d = 1;
  float alpha = 0;

  boolean painter;
  boolean reset = true;
  float updateStrength;

  LabColor col;

  Particle() {
    loc = new Coord(random(width), random(height));
    lloc = loc.copy();
    float a = random(TWO_PI);
    float s = random(1, 1.5);
    dir = new Coord(cos(a) * s, sin(a) * s);
    col = new LabColor(100, 0, 0);
    painter = random(100) < 30 ? false : true;
    updateStrength = random(0.1, 1.5) * (random(100) < 50 ? 1 : -1);
  }

  void draw() {

    if (painter) {
      stroke(col.getColor(), alpha);
    } else {
      stroke(col.getColor(), alpha);
    }

    strokeWeight(d);
    line(loc.fx(), loc.fy(), lloc.fx(), lloc.fy());

   // point(loc.fx(), loc.fy());
  }

  void update(LabImg img, Gradient gradient) {
    Coord g = gradient.get(loc.x(), loc.y()).copy();
    g.rotate(PI / 2);
    float minMag = gradient.minMag();
    float maxMag = gradient.maxMag();
    float gMag = g.mag();
    float amt = map(gMag, minMag, maxMag, 0.1, 1);
    
    alpha = (float) ((IntensityImg) gradient.img()).get(loc.x(), loc.y());

/*
    d = lerp(d, map(gMag, minMag, maxMag, 6, -6), 0.6);

    if (d < 0.1) {
      d = 0.1;
    }*/

    float mag = dir.mag();
    //mag = lerp(mag, map(gMag, minMag, maxMag, 2, 0.2), 0.8);
    g.unit();
    g.mult(updateStrength);

    dir.add(g);
    dir.unit();
    dir.mult(mag);

    if (painter) {

      LabColor c = img.get(loc.x(), loc.y());

      float l = lerp(col.getLuminance(), c.getLuminance(), amt);
      float a = lerp(col.getA(), c.getA(), amt);
      float b = lerp(col.getB(), c.getB(), amt);

      col.setLuminance(l);
      col.setA(a);
      col.setB(b);
    } else if (reset) {
      col = img.get(loc.x(), loc.y()).copy();
    }
  }

  void move() {
    lloc = loc.copy();
    loc.add(dir);
    check();
  }

  void check() {
    reset = false;

    if (loc.fx() > width) {
      reset = true;
    } else if (loc.fx() < 0) {
      reset = true;
    }

    if (loc.fy() > height) {
      reset = true;
    } else if (loc.fy() < 0) {
      reset = true;
    }

    if (reset) {
      painter = random(100) < 80 ? false : true;
      loc.setX((int) random(width));
      loc.setY((int) random(height));
      lloc = loc.copy();
      updateStrength = random(0.1, 1.2);
    }
  }
}