class Visualizer implements ImageUpdateObserver {

  ImageLoader fotoLoader;
  ImageLoader maskLoader;
  PApplet parent;
  ArrayList<Circle>circles;

  Visualizer(PApplet parent) {
    circles = new ArrayList<Circle>();
    SLIC.debug = true;
    SLIC.enforceConnectivity = false;
    this.parent = parent;
    fotoLoader = new ImageLoader(parent, "data/fotos", false);
    maskLoader = new ImageLoader(parent, "data/masks", false);
    fotoLoader.addObserver(this);
    maskLoader.disableKeys();
    fotoLoader.nextRandom();
  }

  public void setImage(PImage img) {


    String fileName = fotoLoader.getFileName();
    fileName = fileName.replace(".jpg", ".png");
    maskLoader.setIndex(fileName);

    if (img.width != parent.width) {
      img.resize(parent.width, 0);
    }

    PImage mask = maskLoader.getCurrent();
    if (mask.width != img.width || mask.height != img.height) {
      mask.resize(img.width, img.height);
    }

    SLIC slic = new SLIC(mask, 2000);
    //slic = slic.mergeSegments().mergeSegments();
    circles.clear();

    float scale = (float) mask.width / slic.labimg().width();

    for (int i = 0; i < slic.superpixels().size(); i++) {

      LabXY labxy = slic.superpixels().get(i).center();

      // float gauss = (float) FilterFactory.gaussFunction1D(labxy.l() / 100, 0.25);
      float f = map(labxy.l(), 0, 100, 10, 0.8);//map(gauss, 0, 1.6, 1, 10);
      float d = f * sqrt(slic.superpixels().get(i).size());
      circles.add(new Circle(new Coord(scale * labxy.fx(), scale * labxy.fy()), d * scale, labxy.l()));
    }

    Collections.sort(circles, new Comparator<Circle>() {

      public int compare(Circle a, Circle b) {

        if (a.d == b.d) {
          return 0;
        } else if (a.d - b.d > 0) {
          return -1;
        } else {
          return 1;
        }
      }
    }
    );

    parent.redraw();
  }

  color getValue(Coord p) {
    PImage img = maskLoader.getCurrent();

    int x = p.x();
    int y = p.y();

    if (x < 0) {
      x = 0;
    } else if (x >= img.width) {
      x = img.width - 1;
    }

    if (y < 0) {
      y = 0;
    } else if (y >= img.height) {
      y = img.height - 1;
    }

    return img.pixels[y * img.width + x];
  }

  color getColor(Coord p) {
    PImage img = fotoLoader.getCurrent();

    int x = p.x();
    int y = p.y();

    if (x < 0) {
      x = 0;
    } else if (x >= img.width) {
      x = img.width - 1;
    }

    if (y < 0) {
      y = 0;
    } else if (y >= img.height) {
      y = img.height - 1;
    }

    return img.pixels[y * img.width + x];
  }

  void line(Coord p, Coord o) {

    parent.line(p.fx(), p.fy(), o.fx(), o.fy());
  }

  void draw() {
    /*
    noStroke();
     for (Circle circle : circles) {
     circle.draw(fotoLoader.getCurrent());
     }*/

    ArrayList<Circle> children = new ArrayList<Circle>();
    noStroke();
    for (Circle circle : circles) {
      // children.add(circle);
      if (circle.children.size() > 0) {
        children.addAll(circle.children);
      }
    }

    boolean intersectionFound = true;
    while (intersectionFound) {
      intersectionFound = false;

      ArrayList<Integer> removers = new ArrayList<Integer>();

      for (int i = 0; i < children.size(); i++) {

        for (int j = i + 1; j < children.size(); j++) {
          if (children.get(i).intersect(children.get(j))) {
            removers.add(i);
            break;
          }
        }
      }

      if (removers.size() > 0) {
        intersectionFound = true;
        for (int i = removers.size() - 1; i >= 0; i--) {
          children.remove(removers.get(i).intValue());
        }
      }
    }


    float[][] points = new float[children.size()][];
    for (int i = 0; i < points.length; i++) {
      points[i] = new float[] { children.get(i).p.fx(), children.get(i).p.fy() };
    }

    Voronoi vor = new Voronoi(points);

    MPolygon[] regions = vor.getRegions();

    noStroke();
    for (int i = 0; i < regions.length; i++) {

      Coord p = children.get(i).p;

      float[][] coords = regions[i].getCoords();   
      
      float meanR = 0;
      float meanG = 0;
      float meanB = 0;
      
      for (float[] coord : coords) {
        color c = getColor(new Coord(coord[0], coord[1]));
        
        meanR += c >> 16 & 255;
        meanG += c >> 8 & 255;
        meanB += c & 255;
      }
      
      meanR /= coords.length;
      meanG /= coords.length;
      meanB /= coords.length;

      strokeWeight(0.5);
      stroke(255);
      fill(meanR, meanG, meanB);

      beginShape();
      for (float[] coord : coords) {
        vertex(coord[0], coord[1]);
      }
      endShape(CLOSE);
      
    }
  }
}