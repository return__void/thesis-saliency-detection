class Circle {
  Coord p;
  float d;
  boolean parent;
  float val;
  ArrayList<Circle> children = new ArrayList<Circle>();

  Circle(Coord p, float d) {
    this.p = p;
    this.d = d;
    this.parent = false;
    this.val = 0;
  }

  Circle(Coord p, float d, float val) {
    this.p = p;
    this.d = d;
    this.parent = true;
    this.val = val;
    addChildren(0, 0);
  }

  void addChildren(int insideFails, int intersectionFails) {

    if (insideFails < 10 && intersectionFails < 20) {
      Circle child = create();

      if (inside(child)) {

        if (!childIntersect(child)) {

          children.add(child);
          addChildren(0, 0);
        } else {
          addChildren(0, intersectionFails + 1);
        }
      } else {
        addChildren(insideFails + 1, intersectionFails);
      }
    }
  }


  Circle create() {
    float x = p.fx() + random(-d, d) / 2;
    float y = p.fy() + random(-d, d) / 2;
    Coord coord = new Coord(x, y);
    float maxD = map(val, 0, 100, d, 3);
    float d = random(2, maxD);

    return new Circle(coord, d);
  }

  boolean inside(Circle other) {

    return dist(other) < (d/ 2 - other.d / 2);
  }

  boolean childIntersect(Circle other) {
    for (Circle child : children) {
      boolean i = child.intersect(other);

      if (i) {
        return true;
      }
    }
    return false;
  }

  boolean intersect(Circle other) {

    return dist(other) <= other.d / 2 + d / 2;
  }
  
  float dist(Circle other) {
    return Coord.euclideanDistanceFct().distance(p, other.p);
  }

  void draw(PImage img) {

    int x = p.x();
    int y = p.y();

    if (x < 0) {
      x = 0;
    } else if (x >= img.width) {
      x = img.width - 1;
    }

    if (y < 0) {
      y = 0;
    } else if (y >= img.height) {
      y = img.height - 1;
    }


    fill(img.pixels[y * img.width + x]);
    ellipse(p.fx(), p.fy(), d, d);

    for (Circle child : children) {
      child.draw(img);
    }
  }
}