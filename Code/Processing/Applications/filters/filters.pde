LabImg input;

int mode = 0;
int imgMode = 0;

void setup() {
  size(1900, 800);
  //size(1280, 500);

  IntensityImg.threadMultiplier = 100;
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "P1060266.jpg");
  raw.resize(width / 2, 0);

  input = new LabImg(raw);

  noLoop();
}

void draw() {
  background(247);
  image(input.img(), 0, 0, width / 2, input.height() * (width / 2f) / input.width());

  RVImage filtered = null;

  if (imgMode == 0) {
    filtered = new LabImg(input.img().copy());
  } else if (imgMode == 1) {
    filtered = new IntensityImg(input);
  } else {
    filtered = new IntensityImg(input.img());
  }

  double[][] filter;

  String text = "";

  switch(mode) {
  case 0:
    filter = FilterFactory.gauss(11);
    text = "Gauss filter (size: 11)";
    break;
  case 1:
    filter = FilterFactory.box(11);
    text = "Box filter (size: 11)";
    break;
  case 2:
    filter = FilterFactory.sharpening(3, 3);
    text = "Sharpening filter";
    break;
  case 3:
    filter = FilterFactory.sobelX();
    text = "Sobel filter (x)";
    break;
  case 4:
    filter = FilterFactory.sobelY();
    text = "Sobel filter (y)";
    break;

  case 5:

    filter = FilterFactory.gabor(11, PI / 4);
    text = "Gabor filter (size: 11, 90°)";
    break;
  default:
    filter = FilterFactory.gauss(11);
   
    text = "Gauss filter (size: 11)";
    break;
  }

 filtered.filter(filter);



  //filtered.filter(FilterFactory.sharpening(10, 3));
  //filtered.limitRange(new Range(0, 255, 1));


  image(filtered.img(), width / 2, 0, width / 2, filtered.height() * (width / 2f) / filtered.width());


  noStroke();
  textSize(16);
  textAlign(RIGHT, BOTTOM);
  fill(255);
  rect(width - textWidth(text) - 32, filtered.height() * (width / 2f) / filtered.width() - 32, textWidth(text) + 32, 32);
  fill(10);
  text(text, width - 16, filtered.height() * (width / 2f) / filtered.width() - 6);
}

void mousePressed() {

  if (mouseButton ==  LEFT) {

    mode++;

    if (mode > 5) {
      mode = 0;
    }
  } else {
    imgMode++;

    if (imgMode > 2) {
      imgMode = 0;
    }
  }
  redraw();
}

void keyPressed() {
  if (key == 'f') {
    saveFrame("export/" + StringTools.timestamp() + ".png");
  }
}