
/*
 * Open this with Processing 3.
 *
 * This sketch uses salient regions of an image to create an image filter.
 * 
 * Controls:
 * mouseX = sets which level is drawn
 * mouseY = sets how fast is drawn
 * press mouse = new image
 * m = change visualization mode
 * c = change chroma adaption value
 * s = change shape size range
 * a = change transparency range
 * l = change number of levels
 * b = new color palette
 * i = print current settings
 *
 * This code is copyright (c) Diana Lange 2017
 *
 * The library is published under the Creative Commons license NonCommercial 4.0.
 * Please check https://creativecommons.org/licenses/by-nc/4.0/ for more information.
 * 
 * This program is distributed in the hope that it will be useful, but without any warranty.
 */

// 18.03.2017
// 13.09.2017 complete update, lib implementation

int level = 0;
int levels = 10;
float speed = 1;


int mode = 3;


float minL;
float maxL;
float minC;
float maxC;
float chromaLerp = 0.2;
float minShapeSize;
float maxShapeSize;
float minAlpha;
float maxAlpha;


ImageLoader paletteloader;
ImageLoader fotoLoader;
ImageLoader maskLoader;
ColorPalette palette;
GradientGenerator gradient;

void setup() {
  size(1280, 800);
  background(247);

  gradient = new GradientGenerator();
  fotoLoader = new ImageLoader(this, "data/fotos", false);
  maskLoader = new ImageLoader(this, "data/masks", false);
  paletteloader = new ImageLoader(this, "data/colors");
  
  

  initVisualization();
}


void draw() {
  PImage foto = fotoLoader.getCurrent();
  PImage mask = maskLoader.getCurrent();

  level = (int) constrain(map(mouseX, 0, width, 0, levels + 1), 0, levels);
  speed = constrain(map(mouseY, 0, height, 1, 20), 1, 40);

  if (level < levels) {
    drawLevel(foto, mask, level, speed, mode);
  }
}