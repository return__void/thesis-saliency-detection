class GradientGenerator implements ImageUpdateObserver {
  Gradient gradient;
  
  public void setImage(PImage img) {
    gradient = new Gradient(new LabImg(img));
  }
}