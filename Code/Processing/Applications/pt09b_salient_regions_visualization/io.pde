void mousePressed() {

  if (mouseButton == LEFT) {
    initVisualization();
  } else {
    initColorPalette();
    initChromaAdaption();
    initAlphaRange();
    background(palette.getMostImportantColor().getColor());
  }
}

void keyPressed() {
  if (key == 'm') {
    mode = ++mode > 7 ? 0 : mode;
  } else if (key == 'n') {
    mode = --mode >=  0 ? mode : 7;
  } else if (key == 'p') {
    println("saved at " + StringTools.timestamp());
    saveFrame("export/" + StringTools.timestamp() + ".png");
  } else if (key == 'c') {
    initChromaAdaption();
  } else if (key == 's') {
    initShapeSize();
  } else if (key == 'a') {
    initAlphaRange();
  } else if (key == 'l') {
    initLevels();
  } else if (key == 'b') {
     initColorPalette();
  } else if (key == 'i') {
    String modeTxt = "mode: " + mode + " [";
    
    switch(mode) {
      case 0:
      modeTxt += "Lines";
      break;
      case 1:
      modeTxt += "Circles";
      break;
      case 2:
      modeTxt += "Lines and Circles";
      break;
      case 3:
      modeTxt += "thick brushes";
      break;
      case 4:
      modeTxt += "delicate brushes";
      break;
      default:
      modeTxt += "unknown";
      break;
    }
    
    modeTxt += "] (change with key m)";
    
    String sizeTxt =  "element size: [" + minShapeSize + ", " + maxShapeSize + "] (change with key s)";
    String alphaTxt =  "transparency: [" + minAlpha + ", " + maxAlpha + "] (change with key a)";
    String levelsTxt = "number of levels: " + levels + " (change with key l)";
    String chromaTxt = "chroma adaption: " + chromaLerp + " (change with key c)";
    String paletteTxt = "change color palette with key b";
    
    println("\n" + modeTxt + "\n" + sizeTxt + "\n" + alphaTxt + "\n" + levelsTxt + "\n" + chromaTxt + "\n" + paletteTxt);
  }
}