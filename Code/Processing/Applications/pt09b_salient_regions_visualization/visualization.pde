void drawLevel(PImage foto, PImage mask, int level, float speed, int mode) {

  int bgColor = palette.getMostImportantColor().getColor();

  for (int i = 0; i < speed; i++) {

    // position of color in img
    int index = getIndexOfLevel(mask, level, levels, 0);

    // could not find any positions with given level
    if (index == -1) continue;

    // color at that position
    float bri = brightness(mask.pixels[constrain(index, 0, mask.pixels.length - 1)]);
    color c = foto.pixels[index];

    // get color from palette based on input image color
    int paletteColor = getColor(c, true);
    int outputColor = lerpColor(c, paletteColor, random(0.5, 1));
    int conturColor = getColor(c, false);

    float amt = map(bri, 0, 255, 0, PI / 2);
    float eSize = map(sin(amt), 0, 1, maxShapeSize, minShapeSize);
    float a = map(sin(amt), 0, 1, minAlpha, maxAlpha);
    //a = map(bri, 0, 255, 170, 255);

    // position of shape
    float x = index % foto.width;
    float y = index / foto.width;

    // angle of lines / curves
    float angle = PI / 2 + gradient.gradient.get((int) x, (int) y).angle();//angle = map(brightness(outputColor), 0, 255, 0, TWO_PI);

    int visMode = 0;

    if (mode == 0) {
      visMode = 0;
    } else if (mode == 1) {
      visMode = 6;
    } else if (mode == 2) {
      visMode = 1;
    } else if (mode == 3) {
      visMode = 2;
    } else if (mode == 4) {
      visMode = 3;
    }  else if (mode == 5) {
      visMode = 4;
    } else {
      visMode = 5;
    }



    if (visMode == 0) {
      
      eSize /= 2;
      strokeWeight(eSize * 0.5);
      stroke(outputColor, a);
      line(x - cos(angle) * 0.5 * eSize, y - sin(angle) * 0.5 *  eSize, x + cos(angle) * 0.5 *  eSize, y + sin(angle) * 0.5 * eSize);
    }
    //c2 = getRandomColor(x, y, bri);

    else if (visMode == 1) {
      fill(outputColor, a);

      noStroke();
      ellipse(x, y, eSize, eSize);
    } else if (visMode == 2) {
      // size is too small for this shape
      eSize = map(sin(amt), 0, 1, maxShapeSize + 1.5, minShapeSize * 2);
      
      strokeWeight(1);
      color swcolor = lerpColor(bgColor, outputColor, map(sin(amt), 0, 1, 0, 0.8));
      stroke(swcolor, constrain(a * 1.5, 0, 255));
      fill(outputColor, 50 + a);
      
      //stroke(bgColor, 255);
      //fill(outputColor, 255);
      
      // these control points are shitty, no time for fixing..
      // hide bad control points by smoothing
      PVector[] controlpoints = getlocations(foto, x, y, eSize);
      LineSegments seg = new LineSegments(this, controlpoints);
      seg = seg.getCurve().toLineSegments(0.2);
      
      CurvedLens cl = new CurvedLens(this, seg.getCoordinates(), eSize * map(level, 0, levels, 0.16, 0.25));
      cl.draw();
    } else if (visMode == 3) {
       // size is too small for this shape
      eSize = map(sin(amt), 0, 1, maxShapeSize * 1.5, minShapeSize * 2);
      
      strokeWeight(1);
      stroke(conturColor, constrain(a * 1.2, 0, 240));
      fill(outputColor, a);
      
      // these control points are shitty, no time for fixing..
      // hide bad control points by smoothing
      PVector[] controlpoints = getlocations(foto, x, y, eSize);
      LineSegments seg = new LineSegments(this, controlpoints);
      seg = seg.getCurve().toLineSegments(0.2);
      seg.draw();
    } else if (visMode == 4) {
      
      strokeWeight(map(bri, 0, 255, 0, 1));
      stroke(10);
      noFill();
      
      PVector[] controlpoints = getlocations(foto, x, y, eSize);
      LineSegments seg = new LineSegments(this, controlpoints);
      Curve curve = seg.getCurve().toLineSegments(0.2).toCurve();
      
      curve.draw();
    } else if (visMode == 5) {
       // size is too small for this shape
      eSize = map(sin(amt), 0, 1, maxShapeSize * 1.5, minShapeSize * 2);
      
      strokeWeight(map(bri, 0, 255, 0.1, 2));
      stroke(255 - brightness(outputColor));
      stroke(outputColor);
      fill(brightness(outputColor));
      
      // these control points are shitty, no time for fixing..
      // hide bad control points by smoothing
      PVector[] controlpoints = getlocations(foto, x, y, eSize);
      LineSegments seg = new LineSegments(this, controlpoints);
      seg = seg.getCurve().toLineSegments(0.2);
      
      CurvedLens cl = new CurvedLens(this, seg.getCoordinates(), eSize * map(level, 0, levels, 0.16, 0.25));
      cl.draw();
    } else {
       strokeWeight(map(bri, 0, 255, 0, 1));
      stroke(outputColor);
      noFill();
      
      PVector[] controlpoints = getlocations(foto, x, y, eSize);
      LineSegments seg = new LineSegments(this, controlpoints);
      Curve curve = seg.getCurve().toLineSegments(0.2).toCurve();
      
      curve.draw();
    }
  }
}

void initLevels() {
  levels = (int) random(10, 25);
}

void initChromaAdaption() {
  chromaLerp = random(100) < 50 ? 0 : random(1);
}

void initAlphaRange() {
  minAlpha = random(5, 70);
  maxAlpha = random(120, 230);
}

void initShapeSize() {
  minShapeSize = sqrt(width * height) * 0.01 * random(0.4, 1.25);
  maxShapeSize = sqrt(width * height) * random(0.10, 0.20);
}

void initVisualization() {
  initLevels();
  initChromaAdaption();
  initAlphaRange();
  initShapeSize();

  // new input image
  fotoLoader.nextRandom();
  
  String fileName = fotoLoader.getFileName();
  fileName = fileName.replace(".jpg", ".png");
  
  
  maskLoader.setIndex(fileName);

  // color palette generation
  initColorPalette();

  // map ranges of input image to range of color palette
  updateColorRange(fotoLoader.getCurrent());

  // resize images if needed
  PImage foto = fotoLoader.getCurrent();
  PImage mask = maskLoader.getCurrent();
  
 
  if (foto.width != width) {
    foto.resize(width, 0);
  }

  if (mask.width != foto.width) {
    mask.resize(foto.width, foto.height);
  }
  
   gradient.setImage(foto);


  // draw background (input image)
  //image(fotoLoader.getCurrent(), 0, 0);
  background(palette.getMostImportantColor().getColor());
}