int getIndexOfLevel(PImage mask, int currentLevel, int levelCount, int depth) {
  float steps = 255.0 / (levelCount);
  float minBri = currentLevel * steps;
  float maxBri = (1 + currentLevel) * steps;

  int index = (int) random(mask.pixels.length);
  float bri = brightness(mask.pixels[constrain(index, 0, mask.pixels.length - 1)]);

  if (depth == 15) {
    return -1;
  } else if (bri >= minBri && bri <= maxBri) {
    return index;
  } else {
    return getIndexOfLevel(mask, currentLevel, levelCount, depth + 1);
  }
}


int getColor(int c, boolean dither) {
  LCHColor lch = RGBColor.toRGB(c).toLCH();
  float l = lch.getLuminance();
  float chroma = lch.getChroma();
  l = map(l, minL, maxL, palette.getMinLuminance(), palette.getMaxLuminance());
  lch.setLuminance(l);
  
  if (chromaLerp != 0) {
  chroma = map(chroma, minC, maxC, lerp(minC, palette.getMinChroma(), chromaLerp), lerp(maxC, palette.getMaxChroma(), chromaLerp));
  lch.setChroma(chroma);
  }
  
  if (dither) {
    return palette.getDithered(lch, 0.5).getColor();
  } else {
    return palette.get(lch).getColor();
  }
 
}

PVector[] getlocations(PImage img, float x, float y, float length) {
  int num = 3 + (int) (length * 0.6);
  if (num < 3) {
    num = 3;
  }
  int num1 = (num - 1) / 2;
  int num2 = (num - 1) - num1;
  PVector[] pp = new PVector[num];

  pp[num1] = new PVector(x, y);

  float angle = PI / 2 + gradient.gradient.get((int) x, (int) y).angle();//map(brightness(c), 0, 255, 0, TWO_PI);
  float steps = length / num;

  float xx = x;
  float yy = y;
  float aa = angle;
  PVector angleVec = new PVector(cos(angle), sin(angle));

  for (int i = num1 - 1; i >= 0; i--) {

    xx = xx + cos(angleVec.heading() + PI) * steps;
    yy = yy + sin(angleVec.heading() + PI) * steps;

    if (xx < 0) {
      xx = 0;
    } else if (xx > img.width - 1) {
      xx = img.width -1;
    }

    if (yy < 0) {
      yy = 0;
    } else if (yy > img.height - 1) {
      yy = img.height -1;
    }

    pp[i] = new PVector(xx, yy);

    aa = PI / 2 + gradient.gradient.get((int) xx, (int) yy).angle();

    PVector angleVecCurrent = new PVector(cos(aa), sin(aa));
    angleVec.add( angleVecCurrent);
    angleVec.normalize();
  }
  xx = x;
  yy = y;
  aa = angle;
  angleVec.x = cos(angleVec.heading());
  angleVec.y = sin(angleVec.heading());
  for (int i = num1 + 1; i < num; i++) {
    xx = xx + cos(aa) * steps;
    yy = yy + sin(aa) * steps;

    if (xx < 0) {
      xx = 0;
    } else if (xx > img.width - 1) {
      xx = img.width -1;
    }

    if (yy < 0) {
      yy = 0;
    } else if (yy > img.height - 1) {
      yy = img.height -1;
    }

    pp[i] = new PVector(xx, yy);
    aa = PI / 2 + gradient.gradient.get((int) xx, (int) yy).angle();
    PVector angleVecCurrent = new PVector(cos(aa), sin(aa));
    angleVec.add( angleVecCurrent);
    angleVec.normalize();
  }

  return pp;
}



int getIndex(PImage foto, PImage mask, float prop, int depth) {
  int index = (int) random(foto.pixels.length);
  float bri = brightness(mask.pixels[index]);

  if (bri > 200) {
    return index;
  }

  float value = map (bri, 0, 255, 100 * prop, 1 / prop);
  // println (value);

  float iRandom = random (0, value);
  //println (value + ", " + iRandom);
  if (iRandom <= 1 || depth == 10) {
    return index;
  } else {
    return getIndex(foto, mask, prop, depth + 1);
  }
}

void initColorPalette() {
  paletteloader.nextRandom();
  RColor[] colors = ClusteringHelper.getTrainingColors(paletteloader.getCurrent(), 420, ColorDifferenceMeasure.LCH_EUCLIDEAN);
  palette = ClusteringHelper.kmeans(colors, (int) random(70, 150), ColorDifferenceMeasure.LCH_EUCLIDEAN).toColorPalette(false);
}

void updateColorRange(PImage img) {
  
   RColor[] training = ClusteringHelper.getTrainingColors(img, 420, ColorSpace.LCH);
  float[] ranges = RMath.range(ColorSpace.LCH, training);
  minL = ranges[0];
  maxL = ranges[4];
  minC = ranges[1];
  maxC = ranges[5];

  // ranges are too equal
  if (maxC - minC < 5) {
    minC = random(50);
    maxC = random(minC, 100);
  }
}