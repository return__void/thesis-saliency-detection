import peasy.*;
import peasy.org.apache.commons.math.*;
import peasy.org.apache.commons.math.geometry.*;
import peasy.test.*;

PeasyCam cam;

PImage input;
IntensityImg img;
PImage maxima;

void setup() {
  size(1280, 720, OPENGL);
  input = loadImage("20180725_190302_color-global-contrast.png");
  input.resize(100, 0);

  img = new IntensityImg(input);
  maxima = img.maxima().img();
  maxima.loadPixels();

  cam = new PeasyCam(this, 0, 0, 0, 800);

  sphereDetail(5);
}

void draw() {

  background(247);
  lights();
  fill(150);
  stroke(247);
  float scale = 10;

  for (int y = 0; y < img.height() - 1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < img.width(); x++) {

      float x1 = -1 * scale * img.width() / 2 + x * scale;
      float x2 = x1;

      float y1 = -1 * scale * img.height() / 2 + y * scale;
      float y2 = -1 * scale * img.height() / 2 + (y + 1) * scale;

      float z1 = (float) img.get(x, y);
      float z2 = (float) img.get(x, y + 1);
      vertex(x1, y1, z1);
      vertex(x2, y2, z2);
    }
    endShape(CLOSE);
  }

  for (int y = 0; y < img.height() - 1; y++) {

    for (int x = 0; x < img.width(); x++) {

      float x1 = -1 * scale * img.width() / 2 + x * scale;
      float x2 = x1;

      float y1 = -1 * scale * img.height() / 2 + y * scale;
      float y2 = -1 * scale * img.height() / 2 + (y + 1) * scale;

      float z1 = (float) img.get(x, y);
      int c = maxima.pixels[y * img.width() + x] & 255;

      if (c == 255) {
        noStroke();
        fill(15);
        translate(x1, y1, z1 + 1);
        sphere(scale / 2);
        translate(-x1, -y1, -z1 - 1);
      }
    }
  }

  //image(maxima, 0, 0);
}