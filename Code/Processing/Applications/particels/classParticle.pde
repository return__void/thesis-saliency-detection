class Particle implements ImageUpdateObserver {
  Coord pos;
  Coord lpos;
  Coord velo;

  boolean fixed = false;
  float r;
  float space;
  DistanceFct<XY> disFct = Coord.euclideanDistanceFct();
  float slowDown;
  color fillColor = #FFFFFF;

  Particle(float x, float y) {
    pos = new Coord(x, y);
    lpos = pos.copy();
    float a = random(TWO_PI);
    velo = new Coord(cos(a), sin(a));
    r = 1;
    space = 6;
    slowDown = random(0.6, 0.9);
    imgService.fotoLoader.addObserver(this);
  }

  Particle() {
    this(width / 2 + random(-6, 6), height / 2 + random(-6, 6));
  }
  
  public void setImage(PImage img) {
    fixed = false;
    float a = random(TWO_PI);
    velo = new Coord(cos(a), sin(a));
    velo.mult(random(1, 6));
  }

  Particle update(ArrayList<Particle> particles) {

    if (!fixed) {
      Coord mean = new Coord(0, 0);
      int intersectionCount = 0;

      for (int i = 0; i < particles.size(); i++) {
        Particle p = particles.get(i);
        if (p == this) {
          continue;
        }

        if (intersect(p)) {
          float a = atan2(pos.fy() - p.pos.fy(), pos.fx() - p.pos.fx());
          mean.add(new Coord(cos(a), sin(a)));
          intersectionCount++;
        }

        if (intersectionCount > 5) {
          break;
        }
      }

      if (intersectionCount > 0) {
        float speed = velo.mag();
        mean.mult(1f / intersectionCount);
        velo.add(mean);
        velo.unit();
        velo.mult(speed + intersectionCount * 0.3);
        velo.limit(4);
      }
    }

    return this;
  }

  float angle(Particle other) {
    return pos.angle(other.pos);
  }

  boolean intersect(Particle other) {
    return disFct.distance(pos, other.pos) < space + other.space;
  }

  Particle updateSpace(float space) {
    this.space = lerp(this.space, space, 0.25);
    return this;
  }

  Particle updateColor(int fillColor) {
    this.fillColor = lerpColor(this.fillColor, fillColor, 0.25);
    return this;
  }

  Particle updateSpeed(float speed) {
    velo.mult(speed);
    return this;
  }

  Particle updateVelo(Coord dir) {
    dir = dir.copy();
    dir.unit();
    dir.rotate(PI / 2);
    dir.mult(0.5);
    float speed = velo.mag();
    velo.add(dir);
    velo.unit();
    velo.mult(speed);
    return this;
  }

  Particle updateRadius(float r) {
    this.r = lerp(this.r, r, 0.25);
    return this;
  }

  Particle move() {
    if (!fixed) {
      lpos = pos.copy();
      pos.add(velo);
      velo.mult(slowDown);
      if (disFct.distance(pos, lpos) < 0.1) {
        fixed = true;
      }
    }
    return this;
  }

  boolean outside() {
    boolean outside = false;
    if (pos.x() < 0) {
      //pos.setX(0);
      outside = true;
    } else if (pos.x() >= width - 1) {
      //pos.setX(width - 1);
      outside = true;
    }

    if (pos.y() < 0) {
      //pos.setY(0);
      outside = true;
    } else if (pos.y() >= height - 1) {
      //pos.setY(height - 1);
      outside = true;
    }



    return outside;
  }

  Particle draw() {

    /*
    stroke(0, 30);
     noStroke();
     fill(255, 30);
     ellipse(pos.fx(), pos.fy(), space * 2, space * 2);
     */

    stroke(247);
    strokeWeight(r * 2);
    if (fixed) {
      point(pos.fx(), pos.fy());
    } else {
      line(pos.fx(), pos.fy(), lpos.fx(), lpos.fy());
    }

    //ellipse(pos.fx(), pos.fy(), r * 2, r * 2);

    return this;
  }
}