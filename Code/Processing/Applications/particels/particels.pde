ArrayList<Particle> particles = new ArrayList<Particle>();
ImageService imgService;

float maxSpace = 15;

boolean paint = false;
float[] gaussValues;

void setup() {
  size(1280, 720);
  imgService = new ImageService(this);

  TableGrid grid = GridMaker.createTable(0, 0, width, height, 8000);
  for (int i = 0; i < grid.size(); i++) {
    /*
    float a = random(TWO_PI);
     float r1 = random(height / 2);
     float r2 = random(height / 2);
     
     if (r1 > r2) {
     
     } else {
     particles.add(new Particle(width / 2 + cos(a) * r1, height / 2 + sin(a) * r1));
     }*/

    particles.add(new Particle(grid.getX(i), grid.getY(i)));
  }
  maxSpace = 30;

  gaussValues = new float[256];
  Range range = new Range();
  for (int i = 0; i < gaussValues.length; i++) {
    gaussValues[i] = (float) FilterFactory.gaussFunction1D(i / 255f, 0.25);
    range.record(gaussValues[i]);
  }
  
  for (int i = 0; i < gaussValues.length; i++) {
    gaussValues[i] = map(gaussValues[i], 0, range.max(), 0, 1);
  }
  

  background(255);
}


void draw() {
  noStroke();
  fill(5, 50);
  rect(0, 0, width, height);
  //background(255);

  if (paint) {
    for (int i = 0; i < 10; i++) {
      particles.add(new Particle(mouseX + random(-6, 6), mouseY + random(-6, 6)));
    }
  }

  ArrayList<Particle> removers = new ArrayList<Particle>();
  for (Particle p : particles) {
    int x = p.pos.x();
    int y = p.pos.y();
    int fotoColor = imgService.getFotoColor(x, y);
    float fotoGray = ((fotoColor >> 16 & 255) + (fotoColor >> 8 & 255) + (fotoColor & 255)) / 3f;
    float maskValue = imgService.getMaskColor(x, y);
    float space = gaussValues[(int) maskValue];
    float speed = map(space, 0, 1, 0.7, 1);
    //println()

    p.updateRadius(map(fotoGray, 0, 255, 0.25, 3)).updateSpace(0.5 + space * maxSpace).updateColor(fotoColor);
    p.update(particles).updateSpeed(speed).move().draw(); //.updateVelo(imgService.gradient(x, y)) // .u

    if (p.outside()) {
      removers.add(p);
    }
  }

  for (Particle p : removers) {
    imgService.fotoLoader.removeObserver(p);
    particles.remove(p);
  }
}


void mousePressed() {
  paint = !paint;
}