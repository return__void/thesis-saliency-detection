class ImageService implements ImageUpdateObserver {
  ImageLoader fotoLoader;
  ImageLoader maskLoader;
  Gradient gradient;
  PApplet parent;
  
  ImageService(PApplet parent) {
    this.parent = parent;

    fotoLoader = new ImageLoader(parent, "data/fotos", false);
    maskLoader = new ImageLoader(parent, "data/masks", false);
    fotoLoader.addObserver(this);
    maskLoader.disableKeys();
    fotoLoader.nextRandom();
  }
  
  public void setImage(PImage img) {

    String fileName = fotoLoader.getFileName();
    fileName = fileName.replace(".jpg", ".png");
    maskLoader.setIndex(fileName);

    if (img.width != parent.width) {
      img.resize(parent.width, 0);
    }

    PImage mask = maskLoader.getCurrent();
    if (mask.width != img.width || mask.height != img.height) {
      mask.resize(img.width, img.height);
    }
    
    fotoLoader.getCurrent().loadPixels();
    maskLoader.getCurrent().loadPixels();
    gradient = new Gradient(new IntensityImg(fotoLoader.getCurrent()));
  }
  
  float getMaskColor(int x, int y) {
    PImage img = maskLoader.getCurrent();
    
    if (x < 0) {
      x = 0;
    } else if (x > img.width - 1) {
      x = img.width - 1;
    }
    
     if (y < 0) {
      y = 0;
    } else if (y > img.height - 1) {
      y = img.height - 1;
    }
    
    return img.pixels[y * img.width + x] & 255;
  }
  
  Coord gradient(int x, int y) {
    return gradient.get(x, y);
  }
  
  int getFotoColor(int x, int y) {
    PImage img = fotoLoader.getCurrent();
    
    if (x < 0) {
      x = 0;
    } else if (x > img.width - 1) {
      x = img.width - 1;
    }
    
     if (y < 0) {
      y = 0;
    } else if (y > img.height - 1) {
      y = img.height - 1;
    }
    
    return img.pixels[y * img.width + x];
  }
}