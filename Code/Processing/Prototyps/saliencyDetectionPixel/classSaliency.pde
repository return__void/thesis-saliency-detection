class Saliency implements ImageUpdateObserver {

  LabImg img = null;
  IntensityImg intensityMap = null;
  IntensityImg colorMap = null;
  IntensityImg orientationMap = null;
  IntensityImg saliencyMap = null;
  int levels = 9;
  int mode = 0;

  boolean rgbyMode = true;

  PApplet parent;

  Saliency(PApplet parent, int K) {
    this.parent = parent;
  }

  void toggleColorMode() {
    rgbyMode = !rgbyMode;

    if (rgbyMode) {
      rgbyMode(img);
    } else {
      labMode(img);
    }
  }


  ArrayList<Pyramid<IntensityImg>> splitGabor(Pyramid<GaborImg> input) {
    double[] angles = input.get(0).angles();
    ArrayList<GaborImg> images = input.getAll();

    ArrayList<Pyramid<IntensityImg>> newPyramids = new ArrayList<Pyramid<IntensityImg>>();

    ArrayList<ArrayList<IntensityImg>> rawPyramid = new ArrayList<ArrayList<IntensityImg>>();
    for (int i = 0; i < angles.length; i++) {
      ArrayList<IntensityImg> imagesByAngle = new ArrayList<IntensityImg>();
      rawPyramid.add(imagesByAngle);
    }

    for (int i = 0; i < input.levels(); i++) {
      GaborImg level = input.get(i);

      for (int j = 0; j < level.angles().length; j++) {
        IntensityImg img = level.getByAngle(j);
        rawPyramid.get(j).add(img);
      }
    }

    for (int i = 0; i < rawPyramid.size(); i++) {
      newPyramids.add(new Pyramid<IntensityImg>(rawPyramid.get(i)));
    }

    return newPyramids;
  }

  ArrayList<IntensityImg> featureMaps(Pyramid<IntensityImg> input, boolean sub) {
    ArrayList<IntensityImg> featureMaps = new ArrayList<IntensityImg>();
    int w = input.get(0).width();
    int h = input.get(0).height();


    for (int i = 2; i <= 4; i++) {
      IntensityImg featureMap = new IntensityImg(w, h);

      IntensityImg fine = input.get(i);

      for (int j = 3; j <= 4; j++) {
        IntensityImg coarse = input.get(i + j);

        for (int y = 0; y < h; y++) {
          for (int x = 0; x < w; x++) {

            int x1 = input.mappedX(x, 0, i);
            int y1 = input.mappedY(y, 0, i);

            int x2 = input.mappedX(x, 0, i + j);
            int y2 = input.mappedY(y, 0, i + j);

            double valFine = fine.get(x1, y1);
            double valCoarse = coarse.get(x2, y2);
            double val = 0;

            if (sub) {
              val = valFine - valCoarse;
            } else {
              val = valFine + valCoarse;
            }

            if (val < 0) {
              val *= -1;
            }

            featureMap.set(x, y, val);
          }
        }
      }

      featureMaps.add(featureMap);
    }



    return featureMaps;
  }

  void rgbyMode(LabImg img) {
    long start = System.currentTimeMillis();

    RGBYImg rgby = new RGBYImg(this.img.img());

    IntensityImg inte = rgby.intensity();
    IntensityImg R = rgby.R();
    IntensityImg G = rgby.G();
    IntensityImg B = rgby.B();
    IntensityImg Y = rgby.Y();

    println("start creating gaussian pyramid for intensity");
    Pyramid<IntensityImg> pyramidIntensity = Pyramid.createGauss(inte, 9, 3);


    println("start creating gaussian pyramids for RGBY");
    Pyramid<IntensityImg> pyramidR = Pyramid.createGauss(R, levels, 3);
    Pyramid<IntensityImg> pyramidG = Pyramid.createGauss(G, levels, 3);
    Pyramid<IntensityImg> pyramidB = Pyramid.createGauss(B, levels, 3);
    Pyramid<IntensityImg> pyramidY = Pyramid.createGauss(Y, levels, 3);

    println("start creating gaussian pyramids for RG and BY");
    Pyramid<IntensityImg> pyramidRG = Pyramid.sub(pyramidR, pyramidG);
    Pyramid<IntensityImg> pyramidBY = Pyramid.sub(pyramidB, pyramidY);

    println("start creating gabor pyramids");
    double[] angles = {0, PI / 4, PI / 2, PI / 4 + PI / 2};
    ArrayList<Pyramid<IntensityImg>> gaborRaw = Pyramid.splitGaborPyramid(Pyramid.createGabor(this.img.copy(), levels, 3, angles));
    Pyramid<IntensityImg> g0 = gaborRaw.get(0);
    Pyramid<IntensityImg> g45 = gaborRaw.get(1);
    Pyramid<IntensityImg> g90 = gaborRaw.get(2);
    Pyramid<IntensityImg> g135 = gaborRaw.get(3);


    println("start creating feature maps");

    ArrayList<IntensityImg> featureMapsIntensity = featureMaps(pyramidIntensity, true);
    ArrayList<IntensityImg> featureMapsRG = featureMaps(pyramidRG, false);
    ArrayList<IntensityImg> featureMapsBY = featureMaps(pyramidBY, false);
    ArrayList<IntensityImg> featureMapsG0 = featureMaps(g0, true);
    ArrayList<IntensityImg> featureMapsG45 = featureMaps(g45, true);
    ArrayList<IntensityImg> featureMapsG90 = featureMaps(g90, true);
    ArrayList<IntensityImg> featureMapsG135 = featureMaps(g135, true);

    println("start creating conspicuity maps");

    intensityMap = new IntensityImg(img.width(), img.height());
    colorMap = new IntensityImg(img.width(), img.height());
    orientationMap = new IntensityImg(img.width(), img.height());
    IntensityImg[] tempOrientationMap = {new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height())};

    for (int i = 0; i < featureMapsIntensity.size(); i++) {
      IntensityImg iMap = featureMapsIntensity.get(i);
      IntensityImg rgMap = featureMapsRG.get(i);
      IntensityImg byMap = featureMapsBY.get(i);
      IntensityImg g0Map = featureMapsG0.get(i);
      IntensityImg g45Map = featureMapsG45.get(i);
      IntensityImg g90Map = featureMapsG90.get(i);
      IntensityImg g135Map = featureMapsG135.get(i);

      iMap.toRange(new Range(0, 255));
      rgMap.toRange(new Range(0, 255));
      byMap.toRange(new Range(0, 255));
      g0Map.toRange(new Range(0, 255));
      g45Map.toRange(new Range(0, 255));
      g90Map.toRange(new Range(0, 255));
      g135Map.toRange(new Range(0, 255));

      float iMapMean = (float) iMap.meanOfMax();
      float rgMapMean = (float) rgMap.meanOfMax();
      float byMapMean = (float) byMap.meanOfMax();
      float g0MapMean = (float) g0Map.meanOfMax();
      float g45MapMean = (float) g45Map.meanOfMax();
      float g90MapMean = (float) g90Map.meanOfMax();
      float g135MapMean = (float) g135Map.meanOfMax();

      iMap.mult((255 - iMapMean) * (255 - iMapMean));
      rgMap.mult((255 - rgMapMean) * (255 - rgMapMean));
      byMap.mult((255 - byMapMean) * (255 - byMapMean));
      g0Map.mult((255 - g0MapMean) * (255 - g0MapMean));
      g45Map.mult((255 - g45MapMean) * (255 - g45MapMean));
      g90Map.mult((255 - g90MapMean) * (255 - g90MapMean));
      g135Map.mult((255 - g135MapMean) * (255 - g135MapMean));

      intensityMap.add(iMap);
      colorMap.add(rgMap);
      colorMap.add(byMap);
      tempOrientationMap[0].add(g0Map);
      tempOrientationMap[1].add(g45Map);
      tempOrientationMap[2].add(g90Map);
      tempOrientationMap[3].add(g135Map);
    }

    for (int i = 0; i < tempOrientationMap.length; i++) {
      tempOrientationMap[i].toRange(new Range(0, 255));
      float mean = (float) tempOrientationMap[i].meanOfMax();
      tempOrientationMap[i].mult((255 - mean) * (255 - mean));
      orientationMap.add(tempOrientationMap[i]);
    }

    println("start creating saliency maps");

    saliencyMap  = new IntensityImg(img.width(), img.height());
    saliencyMap.add(intensityMap);
    saliencyMap.add(orientationMap);
    saliencyMap.add(colorMap);

    // to RGB range
    intensityMap.toRange(new Range(0, 255));
    colorMap.toRange(new Range(0, 255));
    orientationMap.toRange(new Range(0, 255));
    saliencyMap.toRange(new Range(0, 255));

    long end = System.currentTimeMillis();

    println("execution time: " + (end - start) / 1000 + "s");
  }

  void labMode(LabImg img) {
    long start = System.currentTimeMillis();

    IntensityImg L = new IntensityImg(img.width(), img.height());
    IntensityImg A = new IntensityImg(img.width(), img.height());
    IntensityImg B = new IntensityImg(img.width(), img.height());

    for (int y = 0; y < img.height(); y++) {
      for (int x = 0; x < img.width(); x++) {
        LabColor labColor = img.get(x, y);
        L.set(x, y, labColor.getLuminance());
        A.set(x, y, labColor.getA());
        B.set(x, y, labColor.getB());
      }
    }

    A.toRange(new Range(0, 255));
    B.toRange(new Range(0, 255));

    println("start creating gaussian pyramid for intensity");
    Pyramid<IntensityImg> pyramidIntensity = Pyramid.createGauss(L, 9, 3);

    println("start creating gaussian pyramids for color");
    Pyramid<IntensityImg> pyramidA = Pyramid.createGauss(A, levels, 3);
    Pyramid<IntensityImg> pyramidB = Pyramid.createGauss(B, levels, 3);

    println("start creating gabor pyramids");
    double[] angles = {0, PI / 4, PI / 2, PI / 4 + PI / 2};
    ArrayList<Pyramid<IntensityImg>> gaborRaw = Pyramid.splitGaborPyramid(Pyramid.createGabor(img.copy(), levels, 3, angles));
    Pyramid<IntensityImg> g0 = gaborRaw.get(0);
    Pyramid<IntensityImg> g45 = gaborRaw.get(1);
    Pyramid<IntensityImg> g90 = gaborRaw.get(2);
    Pyramid<IntensityImg> g135 = gaborRaw.get(3);


    println("start creating feature maps");

    ArrayList<IntensityImg> featureMapsIntensity = featureMaps(pyramidIntensity, true);
    ArrayList<IntensityImg> featureMapsA = featureMaps(pyramidA, false);
    ArrayList<IntensityImg> featureMapsB = featureMaps(pyramidB, false);
    ArrayList<IntensityImg> featureMapsG0 = featureMaps(g0, true);
    ArrayList<IntensityImg> featureMapsG45 = featureMaps(g45, true);
    ArrayList<IntensityImg> featureMapsG90 = featureMaps(g90, true);
    ArrayList<IntensityImg> featureMapsG135 = featureMaps(g135, true);

    println("start creating conspicuity maps");

    intensityMap = new IntensityImg(img.width(), img.height());
    colorMap = new IntensityImg(img.width(), img.height());
    orientationMap = new IntensityImg(img.width(), img.height());
    IntensityImg[] tempOrientationMap = {new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height()), new IntensityImg(img.width(), img.height())};

    for (int i = 0; i < featureMapsIntensity.size(); i++) {
      IntensityImg iMap = featureMapsIntensity.get(i);
      IntensityImg aMap = featureMapsA.get(i);
      IntensityImg bMap = featureMapsB.get(i);
      IntensityImg g0Map = featureMapsG0.get(i);
      IntensityImg g45Map = featureMapsG45.get(i);
      IntensityImg g90Map = featureMapsG90.get(i);
      IntensityImg g135Map = featureMapsG135.get(i);

      iMap.toRange(new Range(iMap.range().min(), 255));
      aMap.toRange(new Range(aMap.range().min(), 255));
      bMap.toRange(new Range(bMap.range().min(), 255));
      g0Map.toRange(new Range(g0Map.range().min(), 255));
      g45Map.toRange(new Range(g45Map.range().min(), 255));
      g90Map.toRange(new Range(g90Map.range().min(), 255));
      g135Map.toRange(new Range(g135Map.range().min(), 255));

      float iMapMean = (float) iMap.meanOfMax();
      float aMapMean = (float) aMap.meanOfMax();
      float bMapMean = (float) bMap.meanOfMax();
      float g0MapMean = (float) g0Map.meanOfMax();
      float g45MapMean = (float) g45Map.meanOfMax();
      float g90MapMean = (float) g90Map.meanOfMax();
      float g135MapMean = (float) g135Map.meanOfMax();

      iMap.mult((255 - iMapMean) * (255 - iMapMean));
      aMap.mult((255 - aMapMean) * (255 - aMapMean));
      bMap.mult((255 - bMapMean) * (255 - bMapMean));
      g0Map.mult((255 - g0MapMean) * (255 - g0MapMean));
      g45Map.mult((255 - g45MapMean) * (255 - g45MapMean));
      g90Map.mult((255 - g90MapMean) * (255 - g90MapMean));
      g135Map.mult((255 - g135MapMean) * (255 - g135MapMean));

      intensityMap.add(iMap);
      colorMap.add(aMap);
      colorMap.add(bMap);
      tempOrientationMap[0].add(g0Map);
      tempOrientationMap[1].add(g45Map);
      tempOrientationMap[2].add(g90Map);
      tempOrientationMap[3].add(g135Map);
    }

    for (int i = 0; i < tempOrientationMap.length; i++) {
      tempOrientationMap[i].toRange(new Range(0, 255));
      float mean = (float) tempOrientationMap[i].meanOfMax();
      tempOrientationMap[i].mult((255 - mean) * (255 - mean));
      orientationMap.add(tempOrientationMap[i]);
    }

    println("start creating saliency maps");

    saliencyMap  = new IntensityImg(img.width(), img.height());
    saliencyMap.add(intensityMap);
    saliencyMap.add(orientationMap);
    saliencyMap.add(colorMap);

    // to RGB range
    intensityMap.toRange(new Range(0, 255));
    colorMap.toRange(new Range(0, 255));
    orientationMap.toRange(new Range(0, 255));
    saliencyMap.toRange(new Range(0, 255));

    long end = System.currentTimeMillis();

    println("execution time: " + (end - start) / 1000 + "s");
  }

  void setImage(PImage img) {

    PImage copy = img.copy();

    if (img.width >= img.height) {
      copy.resize(width / 2, 0);
    } else {
      copy.resize(0, height);
    }

    this.img = new LabImg(copy);
    if (rgbyMode) {
      rgbyMode(this.img);
    } else {
      labMode(this.img);
    }
  }



  void keyPressed() {
    if (key == '+') {
      mode++;

      if (mode > 3) {
        mode = 0;
      }
    } else if (key == 'm') {
      toggleColorMode();
    } else if (key == '0') {
      mode = 0;
    } else if (key == 'r') {
      println("saving all saliency maps");

      PImage i = intensityMap.img();
      PImage o = orientationMap.img();
      PImage c = colorMap.img();
      PImage s = saliencyMap.img();
      i.parent = parent;
      o.parent = parent;
      c.parent = parent;
      s.parent = parent;


      i.save("export/" + StringTools.timestamp() + "_intensity.png");
      o.save("export/" + StringTools.timestamp() + "_orientation.png");
      c.save("export/" + StringTools.timestamp() + "_color.png");
      s.save("export/" + StringTools.timestamp() + "_saliency.png");
    }
  }

  void draw() {

    if (this.img == null) {
      return;
    }



    float maxW = width / 2.0;
    float maxH = height;

    float scale = img.width() > img.height() ? maxW / img.width() : maxH / img.height();

    image(img.img(), width / 2, 0, img.width() * scale, img.height() * scale);

    PImage saliency = null;

    if (mode == 0) {
      saliency = saliencyMap.img();
    } else if (mode == 1) {
      saliency = intensityMap.img();
    } else if (mode == 2) {
      saliency = colorMap.img();
    } else {
      saliency = orientationMap.img();
    }

    image(saliency, 0, 0, saliency.width * scale, saliency.height * scale);
  }
}