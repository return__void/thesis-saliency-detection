//import net.returnvoid.color.ColorDifference;
import java.util.Map;
ImageLoader imgLoader;



Saliency saliency;



void setup() {
  size(1900, 820);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data/own");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data/own";
  } else {
    directoreyPath = "data-public";
  }

  imgLoader = new ImageLoader(this, directoreyPath);


  saliency = new Saliency(this, 3000);
  imgLoader.addObserver(saliency);
  imgLoader.nextRandom();
  //saliency.setImage(imgLoader.getCurrent());
}

void draw() {  

  background(247);

  
  saliency.draw();
}