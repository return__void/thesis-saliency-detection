void keyPressed() {
  if (key == 'p') {
    saveFrame("export/" + StringTools.timestamp() + ".jpg");
  } else if (key == '0') {
    IntensityImg.threadMultiplier = 1;
  } else if (key == '1') {
    IntensityImg.threadMultiplier = 10;
  } else if (key == '2') {
    IntensityImg.threadMultiplier = 50;
  } else if (key == '3') {
    IntensityImg.threadMultiplier = 100;
  } else {
    saliency.keyPressed();
  }
  println("thread multiplier: " +  IntensityImg.threadMultiplier);
}