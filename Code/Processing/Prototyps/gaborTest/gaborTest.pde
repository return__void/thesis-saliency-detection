float angle = PI / 4;

int periods = 1;

float shape = 0.5;

float stdFactor = 1;

void setup() {
  size (200, 100);
}

void draw() {

  background(255);

  float[][] filter = gaborFilter(100, angle, periods, shape);

  for (int y = 0; y < filter.length; y++) {
    for (int x = 0; x < filter[y].length; x++) {
      float val = filter[y][x];
      val = map(val, -0.3, 0.3, 0, 255);
      stroke(val);
      point(x, y);
    }
  }
}

void keyPressed() {
  if (key == 'd') {
    shape += 0.25;
  } else if (key == 'a') {
    shape -= 0.25;
  } else if (key == 'w') {
    periods++;
  } else if (key =='s') {
    periods--;
    if (periods < 1) {
      periods = 1;
    }
  } else if (key == 'r') {
    angle += PI / 4;
  } else if (key == 'y') {
    stdFactor -= 0.1;

    if (stdFactor < 0.1) {
      stdFactor = 0.1;
    }
  } else if (key == 'x') {
    stdFactor += 0.1;
  }
}

float gabor(float x, float y, float angle, float offset, float period, float std, float shape) {

  float cosAngle = (float) Math.cos(angle);
  float sinAngle = (float) Math.sin(angle);
  float xx = x * cosAngle + y * sinAngle;
  float yy = -x * sinAngle + y * cosAngle;

  float gauss = (float) Math.exp(-1 * ((xx * xx + shape * shape * yy * yy) / (2 * std * std)));
  float gabor = (float) Math.cos(2 * Math.PI * xx / period + offset);

  return gabor * gauss;
}


float[][] gaborFilter(int n, float angle, int periods, float shape) {
  if (n < 3) {
    n = 3;
  } else if (n % 2 == 0) {
    n += 1;
  }

  float[][] filter = new float[n][];
  float std = stdFactor * (n / 3f) / 3f;
  for (int y = 0; y < n; y++) {

    float[] row = new float[n];

    for (int x = 0; x < n; x++) {
      row[x] = gabor(x - n / 2, y - n / 2, angle, periods % 2 == 1 ? PI * 0.5 : 0, (float) n / periods, std, shape);
    }
    filter[y] = row;
  }



  return filter;
}

float[][] gaborFilter(int n, float angle, int periods) {

  return gaborFilter(n, angle, periods, 0.5);
}

float[][] gaborFilter(int n, float angle) {

  return gaborFilter(n, angle, 6, 1);
}