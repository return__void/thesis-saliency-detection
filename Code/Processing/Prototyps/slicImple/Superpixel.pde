class Superpixel {
  float error = 100000;
  LabImg img;
  LabXY center;
  ArrayList<LabXY> elements = new ArrayList<LabXY>();

  // things that will be null whenever the superpixel changes (e.g. add or removed elements)
  ArrayList<ArrayList<LabXY>> regions = null;
  float[][] points = null;
  float[][] hull = null;
  float[][] edge = null;

  Superpixel(LabImg img, float x, float y) {
    this.img = img;
    this.center = img.labxy((int) x, (int) y);
    initCenter(3);
  }

  Superpixel(ArrayList<LabXY> region, LabImg img) {
    this.img = img;
    elements.addAll(region);
    center = region.get(0).copy();
    updateCenter();
  }

  LabXY center() {
    return center;
  }

  void reset() {
    regions = null;
    points = null;
    hull = null;
    edge = null;
  }

  int size() {
    return elements.size();
  }

  void clear() {
    elements.clear();
    reset();
  }

  void remove(ArrayList<ArrayList<LabXY>> regions) {

    for (ArrayList<LabXY> region : regions) {
      for (LabXY e : region) {
        remove(e);
      }
    }
  }

  boolean empty() {
    return elements.size() == 0;
  }
  
  void remove(int i) {
    remove(elements.get(i));
  }

  void remove(LabXY labxy) {
    elements.remove(labxy);

    reset();
  }
  
  LabXY get(int i) {
    return elements.get(i);
  }
  
  int findIndex(int x, int y) {
    
    for (int i = 0; i < elements.size(); i++) {
      LabXY e = elements.get(i);
      if (x == e.x() && y == e.y()) {
        return i;
      }
    }
    
    return -1;
  }
  
  LabXY find(int x, int y) {
    
    for (int i = 0; i < elements.size(); i++) {
      LabXY e = elements.get(i);
      if (x == e.x() && y == e.y()) {
        return e;
      }
    }
    
    return null;
  }

  void add(Superpixel px) {
    elements.addAll(px.elements);
    reset();
  }

  void add(LabXY labxy) {
    elements.add(labxy);
    reset();
  }

  void initCenter(int n) {

    int minX = center.x();
    int minY = center.y();

    float lowestGrad = 1000000;

    for (int i = 0; i < n; i++) {
      int y = center.y() - n / 2 + i;

      for (int j = 0; j < n; j++) {
        int x = center.x() - n / 2 + j;
        float gradient = img.gradient(x, y);

        if (gradient < lowestGrad) {
          lowestGrad = gradient;
          minX = x;
          minY = y;
        }
      }
    }

    center.set(img, minX, minY);
  }

  ArrayList<ArrayList<LabXY>> getLooseRegions() {

    if (regions == null) {
      connected();
    }

    if (regions.size() == 1) {
      return null;
    }

    ArrayList<LabXY> mainRegion = regions.get(0);

    // find biggest, connected region
    // that one will be the content of the
    // superpixel after all not connected, small regions
    // are removed from this superpixel
    for (ArrayList<LabXY> region : regions) {
      if (regions.size() > mainRegion.size()) {
        mainRegion = region;
      }
    }

    // put all small, not connected regions in an array
    // each array represents a connected region
    ArrayList<ArrayList<LabXY>> loose = new ArrayList<ArrayList<LabXY>>();

    for (ArrayList<LabXY> region : regions) {
      if (region != mainRegion) {
        loose.add(region);
      }
    }

    return loose;
  }

  float distance(Superpixel other) {
    return center.spatialDistance(other.center);
  }

  boolean connects(Superpixel other) {

    for (LabXY e : elements) {
      for (LabXY o : other.elements) {
        if (e.neighbor(o)) {
          return true;
        }
      }
    }

    return false;
  }

  boolean connected() {

    // https://www.cs.auckland.ac.nz/courses/compsci773s1c/lectures/ImageProcessing-html/topic3.htm

    if (elements.size() > 0) {

      // store all elements of the superpixel and mark them as not visited yet
      ArrayList<LabXY> unlabeled = new ArrayList<LabXY>();
      unlabeled.addAll(elements);

      // storage for the regions
      // each sub-arraylist represents a connected region
      ArrayList<ArrayList<LabXY>> regions = new ArrayList<ArrayList<LabXY>>();

      // create first region
      regions.add(new ArrayList<LabXY>());

      // storage for already visited/labeled elements
      // these are seeds where it still needs to be checked
      // if the unlabeled contains neighbors to these seeds
      ArrayList<LabXY> seeds = new ArrayList<LabXY>();

      // select first seed
      // first seeds could be random element
      // to speed this up, the last element is picked (indexes of the array doesn't change)
      regions.get(0).add(unlabeled.get(unlabeled.size() - 1));
      seeds.add(unlabeled.get(unlabeled.size() - 1));
      unlabeled.remove(unlabeled.size() - 1);

      // run until all elements are labeled
      while (unlabeled.size() > 0) {

        // if there are sill seeds then there are still elements that
        // are connected to the current region
        // only for one region at once the connected elements are searched
        // that current current region is always the last element in the 'regions' array
        while (seeds.size() > 0) {

          // put new labeled elements as new seeds for next iteration
          ArrayList<LabXY> newSeeds = new ArrayList<LabXY>();
          for (LabXY seed : seeds) {

            // look for neightbors to the current seeds
            for (LabXY noLabel : unlabeled) {

              // neighbor found: label as visited, add to new seeds for next iteration
              if (seed.neighbor(noLabel)) {
                newSeeds.add(noLabel);
                regions.get(regions.size() - 1).add(noLabel);
              }
            }

            // remove all visited elements from unlabeled
            for (LabXY labeled : newSeeds) {
              unlabeled.remove(labeled);
            }
          }
          seeds = newSeeds;
        }

        // no more seeds available but there are still unlabeled elements:
        // that mean there exists elements that are not connected to the current
        // region, therefor start a new region with one starting seed
        if (unlabeled.size() > 0) {

          regions.add(new ArrayList<LabXY>());
          regions.get(regions.size() - 1).add(unlabeled.get(unlabeled.size() - 1));
          seeds.add(unlabeled.get(unlabeled.size() - 1));
          unlabeled.remove(unlabeled.size() - 1);
        }
      }

      // save calculated regions
      this.regions = regions;
    } 

    // return if there is more than one connected region
    return elements.size() == 0 || regions.size() == 1;
  }


  void updateCenter() {

    if (elements.size() == 1) {
      LabXY oldCenter = center.copy();
      center = elements.get(0).copy();
      error = center.manhattenDistance(oldCenter);
    } else if (elements.size() > 1) {

      LabXY oldCenter = center.copy();

      float meanL = 0;
      float meanA = 0;
      float meanB = 0;
      float meanX = 0;
      float meanY = 0;

      for (LabXY e : elements) {
        meanL += e.l();
        meanA += e.a();
        meanB += e.b();

        meanX += e.x();
        meanY += e.y();
      }

      meanL /= elements.size();
      meanA /= elements.size();
      meanB /= elements.size();

      meanX /= elements.size();
      meanY /= elements.size();

      center.set(new LabColor(meanL, meanA, meanB), meanX, meanY);

      error = center.manhattenDistance(oldCenter);
    } else {
      error = 0;
    }

    // should not happen anymore, but better check...
    if (Float.isNaN(error)) {
      error = 0;
    }
  }

  float[][]points(float scale) {
    float[][] points = new float[elements.size()][2];

    for (int i = 0; i < elements.size(); i++) {
      points[i][0] = elements.get(i).fx() * scale;
      points[i][1] = elements.get(i).fy() * scale;
    }

    return points;
  }

  float[][]points() {

    if (points == null) {
      points = points(1);
    }

    return points;
  }


  float[][] hull() {
    if (hull == null) {
      hull = hull(1);
    }

    return hull;
  }

  float[][] hull(float scale) {
    try {
      Hull hull = new Hull(points(scale));
      MPolygon region = hull.getRegion();
      return region.getCoords();
    } 
    catch (Exception e) {
      return new float[][] {new float[]{center.fx() * scale, center.fx() * scale}};
    }
  }




  int countNeighbors(LabXY element) {

    int count = 0;

    for (LabXY other : elements) {
      if (other != element && element.neighbor(other)) {
        count++;
      }

      if (count == 8) {
        return count;
      }
    }

    return count;
  }

  float[][] edge() {
    if (edge == null) {

      ArrayList<LabXY> edgeElement = new ArrayList<LabXY>();

      for (LabXY e : elements) {
        int c = countNeighbors(e);

        if (c < 8) {
          edgeElement.add(e);
        }
      }

      float[][] edge = new float[edgeElement.size()][2];

      for (int i = 0; i < edge.length; i++) {
        edge[i][0] = edgeElement.get(i).x();
        edge[i][1] = edgeElement.get(i).y();
      }

      this.edge = edge;
    }
    return edge;
  }

  void drawPixels(float offsetX, float offsetY, float scale) {
    float[][] points = points();

    strokeWeight(0.25);
    stroke(center.hex());
    fill(center.hex());

    for (float[] point : points) {
      rect(offsetX + point[0] * scale, offsetY + point[1] * scale, scale, scale);
    }
  }

  void drawHullOutline(float offsetX, float offsetY, float scale) {
    float[][] points = hull();

    stroke(0, 100);
    noFill();

    beginShape();
    for (float[] point : points) {
      vertex(offsetX + point[0] * scale, offsetY + point[1] *  scale);
    }
    endShape(CLOSE);
  }

  void drawHull(float offsetX, float offsetY, float scale) {
    float[][] points = hull();
    fill(center.hex());
    stroke(0, 100);

    beginShape();
    for (float[] point : points) {
      vertex(offsetX + point[0] * scale, offsetY + point[1] *  scale);
    }
    endShape(CLOSE);
  }

  void drawSize(float offsetX, float offsetY, float scale, float f) {

    float d = elements.size() * scale * f;

    fill(center.hex());
    noStroke();
    ellipse(offsetX + center.x() * scale, offsetY + center.y() * scale, d, d);
  }

  String toString() {
    return "[" + center.x() + ", " + center.y() + "]";
  }
}