static class LabXY {
  LabColor lab;
  float x;
  float y;

  LabXY(LabImg img, float x, float y) {
    this(img.get((int) x, (int) y), x, y);
  }

  LabXY(LabColor lab, float x, float y) {
    this.lab = lab;
    this.x = x;
    this.y = y;
  }
  
  void set(LabImg img, float x, float y) {
    set(img.get((int) x, (int) y), x, y);
  }
  
  void set(LabColor lab, float x, float y) {
    this.lab = lab;
    this.x = x;
    this.y = y;
  }
  
  LabColor lab() {
    return lab;
  }
  
  int hex() {
    return lab.getColor();
    
  }

  int x() {
    return (int) x;
  }

  int y() {
    return (int) y;
  }
  
  float fx() {
    return x;
  }

  float fy() {
    return y;
  }

  float l() {
    return lab.getLuminance();
  }

  float a() {
    return lab.getA();
  }

  float b() {
    return lab.getB();
  }
  
  boolean neighbor(LabXY other) {
    
    if (other.x() == x() - 1 && other.y() == y() - 1) {
      return true;
    } else if (other.x() == x() && other.y() == y() - 1) {
      return true;
    } else if (other.x() == x()+ 1 && other.y() == y() - 1) {
      return true;
    }  else if (other.x() == x() - 1 && other.y() == y()) {
      return true;
    } else if (other.x() == x() + 1 && other.y() == y()) {
      return true;
    } else if (other.x() == x() - 1 && other.y() == y() + 1) {
      return true;
    } else if (other.x() == x() && other.y() == y() + 1) {
      return true;
    } else if (other.x() == x() + 1 && other.y() == y() + 1) {
      return true;
    } else {
      return false;
    }
    
  }

  static float labDistance(LabColor a, LabColor b) {
    float deltaL = a.getLuminance() - b.getLuminance();
    float deltaA = a.getA() - b.getA();
    float deltaB = a.getB() - b.getB();
    
    /*
    if (deltaL < 0) {
      deltaL *= -1;
    }
    
    if (deltaA < 0) {
      deltaA *= -1;
    }
    
    if (deltaB < 0) {
      deltaB *= -1;
    }
    */

    //return (deltaL + deltaA + deltaB) / 3;
    return (float) Math.sqrt(deltaL * deltaL + deltaA * deltaA + deltaB * deltaB);
  }

  float labDistance(LabXY other) {
    /*
    float deltaL = l() - other.l();
     float deltaA = a() - other.a();
     float deltaB = a() - other.b();
     
     return (float) Math.sqrt(deltaL * deltaL + deltaA * deltaA + deltaB * deltaB);
     */

    return LabXY.labDistance(this.lab, other.lab);
  }

  float spatialDistance(LabXY other) {
    float deltaX = fx() - other.fx();
    float deltaY = fy() - other.fy();
    
    /*
     if (deltaX < 0) {
      deltaX *= -1;
    }
    
    if (deltaY < 0) {
      deltaY *= -1;
    }*/

    //return (deltaX + deltaY) / 2;
    return (float) Math.sqrt(deltaX * deltaX + deltaY * deltaY);
  }

  float euclideanDistance(LabXY other, float m, float S) {
    return labDistance(other) + m / S * spatialDistance(other);
  }
  
  LabXY copy() {
    LabColor col = lab.copy();
    
    return new LabXY(col, x, y);
  }
  
  float manhattenDistance(LabXY other) {
    float deltaL = l() - other.l();
    float deltaA = a() - other.a();
    float deltaB = b() - other.b();
    float deltaX = fx() - other.fx();
    float deltaY = fy() - other.fy();
    
    return Math.abs(deltaL) + Math.abs(deltaA) + Math.abs(deltaB) + Math.abs(deltaX) + Math.abs(deltaY);
  }
}