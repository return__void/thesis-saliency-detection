class SLIC {
  int K = 8000;
  float S;
  ArrayList<Superpixel> superpixels;
  float imageScale;
  float zoom;

  LabImg img;

  int maxPixels = 300000;


  HashMap<String, float[]> distanceStorage = new HashMap<String, float[]>();
  float lastError = -1;

  SLIC(PImage inputImg, int K) {

    this.img = new LabImg(resizeImage(inputImg));
    this.zoom = (float) inputImg.width / this.img.width();
    this.superpixels = getSuperpixels(K, this.img);
    this.K = this.superpixels.size();
    this.S = (float) Math.sqrt(this.img.size() / this.K);
  }

  ArrayList<Superpixel> getSuperpixels(int K, LabImg img) {
    TableGrid grid = GridMaker.createTable(0, 0, img.width(), img.height(), K);
    ArrayList<Superpixel> superpixels = new  ArrayList<Superpixel>(grid.size());

    for (int i = 0; i < grid.size(); i++) {
      superpixels.add(new Superpixel(img, grid.getX(i), grid.getY(i)));
    }

    return superpixels;
  }


  PImage resizeImage(PImage input) {
    PImage copyImg = input.copy();

    if (copyImg.pixels.length > maxPixels) {

      float aspectRatio = (float) copyImg.height / copyImg.width;
      float targetW = (float) Math.sqrt((float) maxPixels / aspectRatio);

      copyImg.resize((int) Math.floor(targetW), 0);
    }

    return copyImg;
  }

  void writeToImage() {
    for (Superpixel sp : superpixels) {

      LabColor col = sp.center.lab(); 

      for (LabXY e : sp.elements) {
        img.set(e.x(), e.y(), col);
      }
    }

    img.updatePixels();
  }

  void segmentImage() {

    for (int i = 0; i < 10; i++) {

      println("Iteration: " + i);

      float meanError = cluster();
      float deltaError = lastError == -1 ? 0 : meanError / lastError;

      println("delta Error: " + deltaError);
      if (i > 3 && deltaError > 0.98) {
        break;
      }

      lastError = meanError;
    }
    enforceCompactness();
  }

  void enforceCompactness() {
    ArrayList<Superpixel> emptySuperpixels = new ArrayList<Superpixel>();

    for (Superpixel sp : superpixels) {

      if (sp.empty()) {
        emptySuperpixels.add(sp);
      }

      if (!sp.connected()) {

        ArrayList<ArrayList<LabXY>> loose = sp.getLooseRegions();

        for (ArrayList<LabXY> region : loose) {
          Superpixel newSp = new Superpixel(region, sp.img);

          Superpixel mergePixel = null;

          for (Superpixel candiate : superpixels) {

            if (candiate == sp) {
              continue;
            }

            if (newSp.distance(candiate) < 2 * S && newSp.connects(candiate)) {

              if (mergePixel == null || mergePixel != null && candiate.size() > mergePixel.size()) {
                mergePixel = candiate;
              }
            }
          }

          if (mergePixel != null) {
            mergePixel.add(newSp);
            mergePixel.updateCenter();
          }
        }
        sp.remove(loose);
        sp.updateCenter();
      }
    }

    // remove superpixels that have no elements

    for (Superpixel empty : emptySuperpixels) {
      superpixels.remove(empty);
    }

    // loose pixels, did not include to any segment at all
    for (int y = 0; y < img.height(); y++) {
      for (int x = 0; x < img.width(); x++) {
        String key = Coord.get(x, y);

        if (!distanceStorage.containsKey(key)) {
          Superpixel newSp = new Superpixel(img, x, y);
          Superpixel mergePixel = null;
          for (Superpixel sp : superpixels) {
            if (newSp.distance(sp) < 2 * S && newSp.connects(sp)) {

              if (mergePixel == null || mergePixel != null && sp.size() > mergePixel.size()) {
                mergePixel = sp;
              }
            }
          }

          if (mergePixel != null) {
            mergePixel.add(newSp);
            mergePixel.updateCenter();
          }
        }
      }
    }
  }

  float cluster() {

    distanceStorage.clear();

    for (int i = 0; i < superpixels.size(); i++) {
      Superpixel superpixel = superpixels.get(i);
      superpixel.clear();
      LabXY center = superpixel.center();

      float left = center.fx() - S;
      float top = center.fy() - S;
      float right = center.fx() + S;
      float bottom = center.fy() + S;

      if (left < 0) {
        left = 0;
      } 

      if (top < 0) {
        top = 0;
      }

      if (right > img.width() - 1) {
        right = img.width() - 1;
      }

      if (bottom > img.height() - 1) {
        bottom = img.height() - 1;
      }

      for (int y = (int) Math.floor(top); y < bottom; y++) {
        for (int x = (int) Math.floor(left); x < right; x++) {
          String key = Coord.get(x, y);
          LabXY pixel = img.labxy(x, y);

          float d = center.euclideanDistance(pixel, 10, S);

          if (distanceStorage.containsKey(key)) {
            float[] prevValue = distanceStorage.get(key);

            if (d < prevValue[1]) {
              
              Superpixel prevSP = superpixels.get((int) prevValue[0]);
              
              prevSP.remove(prevSP.find(pixel.x(), pixel.y()));
              
              prevValue[0] = i;
              prevValue[1] = d;
              superpixel.add(pixel);
            }
          } else {
            distanceStorage.put(key, new float[] {i, d});
            superpixel.add(pixel);
          }
        }
      }
    }


    // update superpixel centers and calculate error

    float meanError = 0;

    for (Superpixel sp : superpixels) {
      sp.updateCenter();
      meanError += sp.error;
    }

    return meanError / superpixels.size();
  }

  void drawHull(float x, float y, float w) {
    for (Superpixel p : superpixels) {
      p.drawHull(x, y, w / img.width());
    }
  }

  void drawSuperpixelSize(float x, float y, float w) {
    fill(0);
    noStroke();
    rect(x, y, w, w / img.width() * img.height());
    for (Superpixel p : superpixels) {
      p.drawSize(x, y, w / img.width(), S * 0.01);
    }
  }

  void drawPixels(float x, float y, float w) {

    for (Superpixel p : superpixels) {

      p.drawPixels(x, y, w / img.width());
    }
  }

  void draw(float x, float y, float w) {
    image(img.get(), x, y, w, w / img.width() * img.height());
  }

  void drawEdge(float x, float y, float w) {
    float scale = w / img.width();
    int[][] binary = new int[img.height()][img.width()];

    for (Superpixel p : superpixels) {

      float[][]points = p.edge();

      for (float[] point : points) {

        binary[(int) point[1]][(int) point[0]] = 1;
      }
    }

    ThinningService thin = new ThinningService();
    thin.doZhangSuenThinning(binary, true);

    PImage output = createImage(img.width(), img.height(), ARGB);

    for (int yy = 0; yy < binary.length; yy++) {
      for (int xx = 0; xx < binary[yy].length; xx++) {
        int b = binary[yy][xx] == 0 ? 0 : 255;
        output.pixels[yy * output.width + xx] = b << 24 | b << 16 | b << 8 | b;
      }
    }

    output.updatePixels();
    image(output, x, y, w, w / img.width() * img.height());
    //Edge edge = new Edge(img.get());
    //image(edge.output, x, y, w, w / edge.output.width * edge.output.height);
  }
}