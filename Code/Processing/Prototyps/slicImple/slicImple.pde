import java.util.HashMap;
import java.util.Map;
import java.util.Comparator;
import java.util.Collections;

ImageLoader imgLoader;


boolean drawHull = false;
boolean drawSizes = false;
boolean drawEdge = false;


SLIC slic;
void setup() {
  size(1900, 720);
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  imgLoader = new ImageLoader(this, directoreyPath);


  slic = new SLIC(imgLoader.getCurrent(), 8000);
  slic.segmentImage();
  slic.writeToImage();
}

void draw() {  

  image(imgLoader.getCurrent(), width / 2, 0, width / 2, (width / 2.0) / imgLoader.getCurrent().width * imgLoader.getCurrent().height);
  slic.draw(0, 0, width / 2);
  if (drawHull) {
    slic.drawHull(0, 0, width / 2);
  }
  if (drawSizes) {
    slic.drawSuperpixelSize(0, 0, width / 2);
  }

  if (drawEdge) {
    slic.drawEdge(0, 0, width / 2);
  }
}