class LabImg {
  PImage img;
  LabColor[] labPixels;

  LabImg (PImage img) {

    this.img = img;
    this.img.loadPixels();
    labPixels = new LabColor[img.pixels.length];
  }

  void loadPixel(int index) {
    labPixels[index] =  RGBColor.toRGB(img.pixels[index]).toLab();
  }

  int size() {
    return labPixels.length;
  }

  int width() {
    return img.width;
  }

  int height() {
    return img.height;
  }

  void darken(float p) {
    p = 1 - p / 100;
    for (int i = 0; i < labPixels.length; i++) {
      if (labPixels[i] == null) {
        loadPixel(i);
      }

      labPixels[i].setLuminance(labPixels[i].getLuminance() * p);
    }
  }

  void loadPixels() {
    for (int i = 0; i < img.pixels.length; i++) {
      loadPixel(i);
    }
  }

  void updatePixels() {
    for (int i = 0; i < labPixels.length; i++) {
      if (labPixels[i] != null) {
        img.pixels[i] = labPixels[i].getOpColor();
      }
    }
    img.updatePixels();
  }
  
  void set(LabXY labxy) {
    set(labxy.x(), labxy.y(), labxy.lab());
  }

  void set(int x, int y, LabColor lab) {
    int index = y * img.width + x;
    if (index < 0) {
      index = 0;
    } else if (index >= img.pixels.length) {
      index = img.pixels.length - 1;
    }

    labPixels[index] = lab.copy();
  }

  float gradient(int x, int y) {
    LabColor xplus1 = get(x + 1, y);
    LabColor xminus1 = get(x - 1, y);

    LabColor yplus1 = get(x, y + 1);
    LabColor yminus1 = get(x, y - 1);

    return LabXY.labDistance(xplus1, xminus1) + LabXY.labDistance(yplus1, yminus1);
  }

  LabColor get(int x, int y) {
    return get(y * img.width + x);
  }

  LabXY labxy(int x, int y) {
    return new LabXY(get(x, y), x, y);
  }

  LabColor get(int index) {
    if (index < 0) {
      index = 0;
    } else if (index >= img.pixels.length) {
      index = img.pixels.length - 1;
    }

    if (labPixels[index] == null) {
      loadPixel(index);
    }

    return labPixels[index];
  }

  PImage get() {
    return img;
  }
}