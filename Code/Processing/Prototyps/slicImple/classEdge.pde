class Edge {
  PImage input;
  PImage output;

  Edge(PImage input) {
    this.input = input;
    this.output = createImage(input.width, input.height, ARGB);
    int[][] binary = filterImg();
    ThinningService thin = new ThinningService();
    thin.doZhangSuenThinning(binary, true);
    
    for (int y = 0; y < binary.length; y++) {
      for (int x = 0; x < binary[y].length; x++) {
        int b = binary[y][x] == 0 ? 0 : 255;
        this.output.pixels[y * output.width + x] = b << 24 | b << 16 | b << 8 | b;
      }
    }
    
    output.updatePixels();
  }

  int[][] filterImg() {
    input.loadPixels();
    
    int[][] binary = new int[input.height][input.width];

    float xsum = 0;
    float ysum = 0;

    float[][] sobelX = sobelFilter('x');
    float[][] sobelY = sobelFilter('y');

    for (int x = 0; x < input.width; x++) {
      for (int y = 0; y < input.height; y++) {

        xsum = 0;
        ysum = 0;

        for (int i = 0; i < sobelX.length; i++) {
          for (int j = 0; j < sobelX[i].length; j++) {
            int xx = x + i - sobelX.length / 2;
            int yy = y + j - sobelX[i].length / 2;

            if (xx < 0) {
              xx = 0;
            } else if (xx >= input.width) {
              xx = input.width - 1;
            }

            if (yy < 0) {
              yy = 0;
            } else if (yy >= input.height) {
              yy = input.height - 1;
            }

            int index = yy * input.width + xx;

            float b = brightness(input.pixels[index]);

            xsum += b * sobelX[i][j];
            ysum += b * sobelY[i][j];
          }
        }

        int mag = xsum + ysum == 0 ? 0 : 1;
        
        binary[y][x] = mag;
      }
    }
    
    return binary;
  }

  float[][] sobelFilter(char xory) {
    float[][] filter = new float[3][];

    if (xory == 'x') {
      filter[0] = new float[] {1, 0, -1};
      filter[1] = new float[] {2, 0, -2};
      filter[2] = new float[] {1, 0, -1};
    } else {
      filter[0] = new float[] {1, 2, 1};
      filter[1] = new float[] {0, 0, 0};
      filter[2] = new float[] {-1, -2, -1};
    }

    return filter;
  }
}