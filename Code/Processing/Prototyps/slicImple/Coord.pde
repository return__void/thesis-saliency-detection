static class Coord {
  int x;
  int y;
  
  Coord(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  int x() {
    return x;
  }
  
  int y() {
    return y;
  }
  
  void setX(int x) {
    this.x = x;
  }
  
  void setY(int y) {
    this.y = y;
  }
  
  static String get(int x, int y) {
    return "" + new Coord(x, y);
  }
  
  String toString() {
    return x + ", " + y;
  }
}