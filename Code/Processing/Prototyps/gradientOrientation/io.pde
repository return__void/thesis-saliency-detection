void keyReleased() {

  if (key == '+') {
    resFactor = --resFactor < 1 ? 1 : resFactor;
  } else if (key == '-') {
    resFactor++;
  } else if (key >= '1' && key <= (char) ('0' + pyramid.levels())) {
    level = (int) (key - '0') - 1;
    fullGradients.clear();
  } else if (key == 'i') {
    displayImg = !displayImg;
  } else if (key == 'g') {
    displayGrads = !displayGrads;
  } else if (key == 's') {
    pyramid.save();
  } else if (key == 'd') {
    cutoff += 0.1;
  } else if (key == 'a') {
    cutoff -= 0.1;

    if (cutoff < 0.1) {
      cutoff = 0.1;
    }
  } else {
    saveFrame("export/" + StringTools.timestamp() + ".png");
  }
  redraw();
}