static class Pyramid {
  
  LabImg[] images;
  
  Pyramid(LabImg[] images) {
    this.images = images;
  }
  
  int levels() {
    return images.length;
  }
  
  LabImg get(int i) {
    return images[i];
  }
  
  int map(int i, int x, int y) {
  
    float scale = (float) images[i].img().width / images[0].img().width;
    
    int xx = (int) (x * scale);
    int yy = (int) (y * scale);
    
    //println("scale: " + scale + ", " + x + "-" + xx + ", " + y + "-" + yy);
    
    return yy * images[i].img().width + xx;
  }
  
  void save() {
    String filename = StringTools.timestamp();
    for (int i = 0; i < images.length; i++) {
      images[i].img().save("export/" + filename + "-" + i + ".png");
    }
  }

  static Pyramid createGaussian(PImage input,int levels) {
    
    LabImg[] pyramid = new LabImg[levels];
    
    pyramid[0] = new LabImg(input.copy());
    float scale = 1 / sqrt(2);

    for (int i = 1; i < levels; i++) {
      pyramid[i] = new LabImg(pyramid[i - 1].img().copy());
      
      int newW = Math.round(pyramid[i - 1].img().width * scale);
      pyramid[i].filter(FilterFactory.gauss(3 * i), false);
      pyramid[i].resize(newW, 0);
    }

    return new Pyramid(pyramid);
  }
}