import java.util.*;

Pyramid pyramid;

int resFactor = 6;

HashMap<String, Coord> fullGradients = new HashMap<String, Coord>();
boolean displayImg = false;
boolean displayGrads = true;

int level = 2;
float cutoff = 1;

void setup() {
  size(1280, 860);
  
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "_MG_6966 Kopie.jpg");
  raw.resize(width, 0);

  pyramid = Pyramid.createGaussian(raw, 9);

  noLoop();
}


void draw() {

  background(247);

  LabImg img = pyramid.get(level);
  LabImg base = pyramid.get(0);

  //scale((float) raw.width / img.width());
  if (displayImg) {
    image(img.img(), 0, 0, base.width(), base.height());
  }

  if (displayGrads) {

    //strokeWeight(img.width() / (float) raw.width);

    HashMap<String, Coord> gradients = new HashMap<String, Coord>();
    int neighborhood = 6;

    for (int y = 0; y < base.img().height; y+= resFactor) {
      for (int x = 0; x < base.img().width; x+= resFactor) {

        String lookup = Coord.key(x, y);

        if (!fullGradients.containsKey(lookup)) {

          Coord vec = null;

          for (int yy = y - neighborhood / 2; yy < y + neighborhood / 2; yy++) {

            int yyy = yy < 0 ? 0 : yy > base.img().height - 1 ? base.img().height - 1 : yy;

            for (int xx = x - neighborhood / 2; xx < x + neighborhood / 2; xx++) {

              int xxx = xx < 0 ? 0 : xx > base.img().width - 1 ? base.img().width - 1 : xx;

              String key = Coord.key(xxx, yyy);

              if (!gradients.containsKey(key)) {
                
                int index = pyramid.map(level, xxx, yyy);
                
                float[] grad = img.gradient(index % img.img().width, index / img.img().width);
                Coord gradVec = new Coord(grad[0], grad[1]);
                gradients.put(key, gradVec);
              }


              Coord gradVec = gradients.get(key);

              if (vec == null) {
                vec = gradVec.copy();
              } else {
                vec.add(gradVec);
              }
            }
          }

          vec.mult(1.0 / (neighborhood * neighborhood));

          fullGradients.put(lookup, vec);
        }

        Coord vec = fullGradients.get(lookup);

        float a = vec.angle() + PI / 2;
        float m = vec.mag() * 0.5 * sqrt(resFactor);

        if (m < resFactor * cutoff) {
          continue;
        }

        line(x - cos(a) * m / 2, y - sin(a) * m / 2, x + cos(a) * m / 2, y + sin(a) * m / 2);
      }
    }
  }
}