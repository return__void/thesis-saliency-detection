import java.util.*;


Pyramid<LabImg> pyramid;
boolean saveImg = false;
PImage highResImg = null;
PImage lowResImg = null;

void setup() {
  size(1280, 860);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "3301.jpg");
  raw.resize(width, 0);

  pyramid = Pyramid.createGauss(new LabImg(raw), 9, 3);
  
}

void draw() {
  background(247);
  //fast();
  slow();
}

void keyReleased() {
  saveImg = true;
  //saveFrame("export/" + StringTools.timestamp() + ".png");
}


void slow() {


  if (highResImg == null) {

    LabImg base = pyramid.get(0);


    PImage scaleImg = createImage(base.width(), base.height(), RGB);

    ArrayList<HashMap<String, Coord>> gradients = new ArrayList<HashMap<String, Coord>>();
    int neighborhood = 5;

    float meanMag = 0;
    int magCount = 0;

    for (int i = 1; i < pyramid.levels(); i++) {
      HashMap<String, Coord> map = new HashMap<String, Coord>();
      HashMap<String, Coord> grads = new HashMap<String, Coord>();

      LabImg img = pyramid.get(i);
      for (int y = 0; y < img.img().height; y++) {
        for (int x = 0; x < img.img().width; x++) {
          Coord gradVec = null;

          if (neighborhood < 2) {
            float[] grad = img.gradient(x, y);
            gradVec = new Coord(grad[0], grad[1]);
          } else {

            int count = 0;
            for (int yy = y - neighborhood / 2; yy < y + neighborhood / 2; yy++) {

              int yyy = yy < 0 ? 0 : yy > img.img().height - 1 ? img.img().height - 1 : yy;

              for (int xx = x - neighborhood / 2; xx < x + neighborhood / 2; xx++) {
                int xxx = xx < 0 ? 0 : xx > img.img().width - 1 ? img.img().width - 1 : xx;

                String key = Coord.key(xxx, yyy);

                if (!grads.containsKey(key)) {
                  float[] grad = img.gradient(xxx, yyy);
                  grads.put(key, new Coord(grad[0], grad[1]));
                }

                Coord vec = grads.get(key);

                if (gradVec == null) {
                  gradVec = vec.copy();
                } else {
                  gradVec.add(vec);
                }

                count++;
              }
            }

            gradVec.mult(1f / count);
          }

          //gradVec.mult(1f / count);
          meanMag += gradVec.mag();
          magCount++;

          map.put(Coord.key(x, y), gradVec);
        }
      }

      gradients.add(map);
    }

    meanMag /= magCount;

    for (int y = 0; y < base.img().height; y++) {
      for (int x = 0; x < base.img().width; x++) {
        int xx = pyramid.mappedX(x, 0, 1);
        int yy = pyramid.mappedY(y, 0, 1);

        float maxMag = gradients.get(0).get(Coord.key(xx, yy)).mag();
        int maxIndex = 0;

        for (int i = 2; i < pyramid.levels(); i++) {
          HashMap<String, Coord> grads = gradients.get(i - 1);

          xx = pyramid.mappedX(x, 0, i);
          yy = pyramid.mappedY(y, 0, i);

          if (xx > pyramid.get(i).img().width - 1) {
            xx = pyramid.get(i).img().width - 1;
          }

          if (yy > pyramid.get(i).img().height - 1) {
            yy = pyramid.get(i).img().height - 1;
          }

          Coord grad = grads.get(Coord.key(xx, yy));

          float mag = grad.mag();

          if (mag > maxMag) {
            maxMag = mag;
            maxIndex = i;
          }
        }

        if (maxMag < meanMag * 0.8) {
          maxIndex = pyramid.levels();
        }

        float b = map(maxIndex, 1, pyramid.levels(), 255, 0);

        scaleImg.pixels[y * scaleImg.width + x] = color(b);
      }
    }

    //image(img.img(), 0, 0, base.width(), base.height());


    //LabImg blurred = new LabImg(scaleImg);
    //blurred.filter(FilterFactory.gauss2D(5), false);
    highResImg = scaleImg;
  }
  image(highResImg, 0, 0, width, highResImg.height * (float) width / highResImg.width);

  if (saveImg) {
    highResImg.save("export/" + StringTools.timestamp() + ".jpg");
    saveImg = false;
  }
  //image(blurred.img(), 0, 0, width, scaleImg.height * (float) width / scaleImg.width);
}

void fast() {


  if (lowResImg == null) {
    LabImg base = pyramid.get(pyramid.levels() - 1);


    PImage scaleImg = createImage(base.width(), base.height(), RGB);

    ArrayList<HashMap<String, Coord>> gradients = new ArrayList<HashMap<String, Coord>>();
    int neighborhood = 5;

    float meanMag = 0;
    int magCount = 0;

    for (int i = 0; i < pyramid.levels(); i++) {
      HashMap<String, Coord> map = new HashMap<String, Coord>();
      HashMap<String, Coord> grads = new HashMap<String, Coord>();

      LabImg img = pyramid.get(i);

      float scale = img.width() / (float) base.width();

      for (int y = 0; y < base.img().height; y++) {
        for (int x = 0; x < base.img().width; x++) {
          Coord gradVec = null;

          int mapX = (int) (x * scale);
          int mapY = (int) (y * scale);

          if (neighborhood < 2) {
            float[] grad = img.gradient(mapX, mapY);
            gradVec = new Coord(grad[0], grad[1]);
          } else {

            int count = 0;
            for (int yy = mapY - neighborhood / 2; yy < mapY + neighborhood / 2; yy++) {

              int yyy = yy < 0 ? 0 : yy > img.img().height - 1 ? img.img().height - 1 : yy;

              for (int xx = mapX - neighborhood / 2; xx < mapX + neighborhood / 2; xx++) {
                int xxx = xx < 0 ? 0 : xx > img.img().width - 1 ? img.img().width - 1 : xx;

                String key = Coord.key(xxx, yyy);

                if (!grads.containsKey(key)) {
                  float[] grad = img.gradient(xxx, yyy);
                  grads.put(key, new Coord(grad[0], grad[1]));
                }

                Coord vec = grads.get(key);

                if (gradVec == null) {
                  gradVec = vec.copy();
                } else {
                  gradVec.add(vec);
                }

                count++;
              }
            }

            gradVec.mult(1f / count);
          }

          //gradVec.mult(1f / count);
          meanMag += gradVec.mag();
          magCount++;

          map.put(Coord.key(mapX, mapY), gradVec);
        }
      }

      gradients.add(map);
    }

    meanMag /= magCount;

    for (int y = 0; y < base.img().height; y++) {
      for (int x = 0; x < base.img().width; x++) {
        LabImg img = pyramid.get(0);

        float scale = img.width() / (float) base.width();

        float maxMag = gradients.get(0).get(Coord.key((int) (x * scale), (int) (y * scale))).mag();
        int maxIndex = 0;

        for (int i = 1; i < pyramid.levels(); i++) {
          img = pyramid.get(i);

          scale = img.width() / (float) base.width();

          HashMap<String, Coord> grads = gradients.get(i);

          int xx = (int) (x * scale);
          int yy = (int) (y * scale);

          if (xx > pyramid.get(i).img().width - 1) {
            xx = pyramid.get(i).img().width - 1;
          }

          if (yy > pyramid.get(i).img().height - 1) {
            yy = pyramid.get(i).img().height - 1;
          }

          Coord grad = grads.get(Coord.key(xx, yy));

          float mag = grad.mag();

          if (mag > maxMag) {
            maxMag = mag;
            maxIndex = i;
          }
        }

        if (maxMag < meanMag) {
          maxIndex =  pyramid.levels();
        }

        float b = map(maxIndex, 0, pyramid.levels(), 255, 0);

        scaleImg.pixels[y * scaleImg.width + x] = color(b);
      }
    }
    lowResImg = scaleImg;
  }


  image(lowResImg, 0, 0, width, lowResImg.height * (float) width / lowResImg.width);
  //image(scaleImg, 0, 0);

  if (saveImg) {
    lowResImg.save("export/" + StringTools.timestamp() + ".jpg");
    saveImg = false;
  }
}