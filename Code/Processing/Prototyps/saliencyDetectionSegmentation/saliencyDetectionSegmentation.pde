//import net.returnvoid.color.ColorDifference;
import java.util.Map;
ImageLoader imgLoader;



Saliency saliency;



void setup() {
  size(1900, 820);

  File directory = new File(ProcessingTools.getSketchPath(this) + "/data/own");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data/own";
  } else {
    directoreyPath = "data-public";
  }

  imgLoader = new ImageLoader(this, directoreyPath);

  SLIC.threadMultiplierSimple = 10;
  SLIC.threadMultiplierComplex = 1;
  SLIC.debug = true;
  //SLIC.enforceConnectivity = false;
  SLIC.maxPixels = 200000;
  SLIC.maxIterations = 10;

  saliency = new Saliency(this, 3000);
  imgLoader.addObserver(saliency);
  imgLoader.nextRandom();
  //saliency.setImage(imgLoader.getCurrent());
}

void draw() {  

  background(247);

  if (saliency.mean != null) {
    background(saliency.mean.getColor());
  }

  saliency.draw();

  if (saliency.mean != null) {
    if (mousePressed) {
      noStroke();
      fill(saliency.mean.getColor());
      ellipse(mouseX, mouseY, 20, 20);
    }
  }
}