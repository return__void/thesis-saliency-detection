void writeToMap(Superpixel pix, IntensityImg container, float distance) {
  for (int[] coord : pix.points()) {    
    container.set(coord[0], coord[1], distance);
  }
}

IntensityImg computeSaliency(SLIC slic, SegmentationFeatures sf, String mapName) {
  String[] splittedMapName = mapName.split(":");
  final String featureName = splittedMapName[0].trim();
  final String methodName = splittedMapName[1].trim();

  IntensityImg container = null;
  if (methodName.equals(saliency.methodNames[0])) {
    container = globalContrast(slic, sf, featureName);
  } else if (methodName.equals(saliency.methodNames[1])) {
    container = regionalContrast(slic, sf, featureName);
  } else if (methodName.equals(saliency.methodNames[2])) {
    container = localContrast(slic, sf, featureName);
  } else if (methodName.equals(saliency.methodNames[3])) {
    container = weightedContrast(slic, sf, featureName);
  } else if (methodName.equals(saliency.methodNames[4])) {
    container = localRegionalGlobalContrast(slic, sf, featureName);
  } else if (methodName.equals(saliency.methodNames[5])) {
    container = distribution(slic, sf, featureName);
  } else {
    container = globalConstrastWidthDistribution(slic, sf, featureName);
  }

  return container;
}

IntensityImg upsampling(SLIC slic, IntensityImg img) {

  // find mean saliency score

  float meanSal = 0;

  for (int i = 0; i < slic.superpixels().size(); i++) {
    LabXY center = slic.superpixels().get(i).center();
    float salVal = (float) img.get(center.x(), center.y());
    meanSal += salVal;
  }

  meanSal /= slic.superpixels().size();

  // find HDP (high distinct pixel) = segments where the sal value is above mean sal
  ArrayList<Superpixel> HDP = new ArrayList<Superpixel>();
  for (int i = 0; i < slic.superpixels().size(); i++) {
    LabXY center = slic.superpixels().get(i).center();
    float salVal = (float) img.get(center.x(), center.y());

    if (salVal > meanSal) {
      HDP.add(slic.superpixels().get(i));
    }
  }

  // estimate distance to nearest HDP
  float dropOff = 3;
  ArrayList<Float> mues = new ArrayList<Float>(slic.superpixels().size());
  Range mueRange = new Range();

  for (int i = 0; i < slic.superpixels().size(); i++) {

    int minDistanceIndex = 0;
    float minDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), HDP.get(0).center());

    for (int j = 1; j < HDP.size(); j++) {
      float d = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), HDP.get(j).center());

      if (d < minDistance) {
        minDistance = d;
        minDistanceIndex = j;
      }
    }


    float mue = (float) Math.log(minDistance + dropOff);
    mues.add(mue);
    mueRange.record(mue);
  }

  float maxSpatialDistance = Coord.euclideanDistanceFct().distance(new Coord(0, 0), new Coord(slic.labimg().width(), slic.labimg().height()));
  float maxDim = slic.labimg().width() >= slic.labimg().height() ? slic.labimg().width() : slic.labimg().height();
  float ratio = maxDim / maxSpatialDistance;

  ArrayList<Float> deltas = new ArrayList<Float>(slic.superpixels().size());
  Range deltaRange = new Range();

  for (int i = 0; i < slic.superpixels().size(); i++) {
    float delta = ratio - mues.get(i) / mueRange.max();
    deltas.add(delta);
    deltaRange.record(delta);
  }

  for (int i = 0; i < slic.superpixels().size(); i++) {
    LabXY center = slic.superpixels().get(i).center();
    float salVal = (float) img.get(center.x(), center.y());
    double r = (float) deltas.get(i) / deltaRange.max();

    salVal *= r;
    writeToMap(slic.superpixels().get(i), img, salVal);
  }

  img.toRange(new Range(0, 255));

  return img;
}

IntensityImg globalConstrastWidthDistribution(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg global = saliency.getMap(featureName + ": " + saliency.methodNames[3]).copy();//;weightedContrast(slic, sf, featureName);
  IntensityImg distribution = saliency.getMap(featureName + ": " + saliency.methodNames[5]).copy();//distribution(slic, sf, featureName);
  global.toRange(new Range(0, 1));

  distribution.toRange(new Range(0, 1));
  distribution.mult(6);
  global.mult(distribution);
  global.toRange(new Range(0, 255));

  return global;
}

IntensityImg distribution(SLIC slic, SegmentationFeatures sf, String featureName) {

  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
  Coord middle = new Coord(slic.labimg().width() / 2, slic.labimg().height() / 2);
  Coord corner = new Coord(0, 0);

  float maxSpatialDistance = Coord.euclideanDistanceFct().distance(middle, corner);
  float spatialDistanceCutoff = maxSpatialDistance * 0.25;
  float w = slic.labimg().width();
  float h = slic.labimg().height();

  ArrayList<double[]> weightsRecord = new ArrayList<double[]>(slic.superpixels().size());
  Range[] ranges = new Range[slic.superpixels().size()];
  double[] sums = new double[slic.superpixels().size()];
  Range range = new Range();

  for (int i = 0; i < slic.superpixels().size(); i++) {
    double[] weights = new double[slic.superpixels().size()];
    ranges[i] = new Range();


    for (int j = 0; j < slic.superpixels().size(); j++) {

      float distance = sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName);
      weights[j] = distance;
      range.record(distance);
      ranges[i].record(distance);
    }
    weightsRecord.add(weights);
  }

  double meanDistance = 0;
  for (int i = 0; i < slic.superpixels().size(); i++) {

    LabXY center = slic.superpixels().get(i).center();
    Coord loc = new Coord(center.fx(), center.fy());
    Coord mue = new Coord(0, 0);

    double[] weights = weightsRecord.get(i);
    double sum = 0;
    for (int j = 0; j < slic.superpixels().size(); j++) {

      weights[j] = FilterFactory.gaussFunction1D(weights[j] / (range.max() - range.min()), 0.25);
      sum += weights[j];
    }

    sums[i] = sum;
    for (int j = 0; j < slic.superpixels().size(); j++) {

      weights[j] = weights[j] / sums[i];

      LabXY centerOther = slic.superpixels().get(j).center();
      Coord locOther = new Coord(centerOther.fx(), centerOther.fy());
      locOther.mult(weights[j]);
      mue.add(locOther);
    }


    double distance = 0;

    for (int j = 0; j < slic.superpixels().size(); j++) {

      LabXY centerOther = slic.superpixels().get(j).center();
      Coord locOther = new Coord(centerOther.fx(), centerOther.fy());
      locOther.sub(mue);

      float spatialDistanceMean = locOther.dot(locOther);
      float sizeWeight = 1;//slic.superpixels().get(slic.superpixels().size() - 1).size() + slic.superpixels().get(0).size() - slic.superpixels().get(j).size();

      double d = spatialDistanceMean * spatialDistanceMean * sizeWeight * weights[j];
      distance += d;
    }

    writeToMap(slic.superpixels().get(i), saliencyMap, (float) distance);
  }

  saliencyMap.toRange(new Range(0, 255));
  saliencyMap.invert();
  return saliencyMap;
}

IntensityImg globalContrast(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());


  for (int i = 0; i < slic.superpixels().size(); i++) {

    float distance = 0;
    for (int j = 0; j < slic.superpixels().size(); j++) {

      if (i == j) {
        continue;
      }

      float weight = slic.superpixels().get(j).size();
      distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight;
    }

    writeToMap(slic.superpixels().get(i), saliencyMap, distance);
  }

  saliencyMap.toRange(new Range(0, 255));
  return saliencyMap;
}


IntensityImg weightedContrast(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
  float maxSpatialDistance = sqrt((slic.labimg().width() / 2) * (slic.labimg().width() / 2) + (slic.labimg().height() / 2) * (slic.labimg().height() / 2));

  float std = maxSpatialDistance / 20;

  for (int i = 0; i < slic.superpixels().size(); i++) {


    float distance = 0;

    for (int j = 0; j < slic.superpixels().size(); j++) {

      if (i == j) {
        continue;
      }

      float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());
      float weight = slic.superpixels().get(j).size();

      // float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());
      // float spatialWeight = 1 - (0.5 * spatialDistance / maxSpatialDistance);

      //float spatialWeig = (float) FilterFactory.gaussFunction(slic.superpixels().get(i).center(), slic.superpixels().get(j).center(), std, 1 / (std * std * TWO_PI), Coord.euclideanDistanceFct());

      float spatialWeight = (float) FilterFactory.gaussFunction1D(spatialDistance/maxSpatialDistance, 0.25);

      distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight * spatialWeight;
    }

    writeToMap(slic.superpixels().get(i), saliencyMap, distance);
  }

  saliencyMap.toRange(new Range(0, 255));
  return saliencyMap;
}

IntensityImg localContrast(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());

  for (int i = 0; i < slic.superpixels().size(); i++) {


    float distance = 0;
    int localCount = 0;

    for (int j = 0; j < slic.superpixels().size(); j++) {

      if (i == j || !slic.superpixels().get(i).neighbor(slic.superpixels().get(j))) {
        continue;
      }

      float weight = slic.superpixels().get(j).size();

      distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight;
      localCount++;
    }

    if (localCount > 0) {
      distance /= localCount;
    }


    writeToMap(slic.superpixels().get(i), saliencyMap, distance);
  }

  saliencyMap.toRange(new Range(0, 255));
  return saliencyMap;
}


IntensityImg localRegionalGlobalContrast(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg global = saliency.getMap(featureName + ": " + saliency.methodNames[0]).copy();//;weightedContrast(slic, sf, featureName);
  IntensityImg regional = saliency.getMap(featureName + ": " + saliency.methodNames[1]);//distribution(slic, sf, featureName);
  IntensityImg local = saliency.getMap(featureName + ": " + saliency.methodNames[2]);//;weightedContrast(slic, sf, featureName);
  global.add(regional);
  global.add(local);
  global.toRange(new Range(0, 255));

  return upsampling(slic, global);

  /*
  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
   float maxSpatialDistance = sqrt((slic.labimg().width() / 2) * (slic.labimg().width() / 2) + (slic.labimg().height() / 2) * (slic.labimg().height() / 2));
   float maxLocalDistance = maxSpatialDistance / 4;
   
   for (int i = 0; i < slic.superpixels().size(); i++) {
   
   float local = 0;
   float regional = 0;
   float global = 0;
   
   int regionalCount = 0;
   int localCount = 0;
   
   float regionalWeightSum = 0;
   
   for (int j = 0; j < slic.superpixels().size(); j++) {
   
   if (i == j) {
   continue;
   }
   
   float weight = slic.superpixels().get(j).size();
   float d = sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * weight;
   float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());
   
   global += d;
   
   if (spatialDistance < maxLocalDistance) {
   float spatialWeight = (float) FilterFactory.gaussFunction1D(spatialDistance/maxLocalDistance, 0.25);//sin(map(spatialDistance, 0, maxLocalDistance, PI/2, PI));
   regionalWeightSum += spatialWeight;
   regional += d * spatialWeight;
   regionalCount++;
   }
   
   if (slic.superpixels().get(i).neighbor(slic.superpixels().get(j))) {
   local += d;
   localCount++;
   }
   }
   
   global /= (slic.superpixels().size() - 1);
   
   if (regionalCount != 0) {
   regional = (regional / regionalWeightSum);
   }
   
   if (localCount != 0) {
   local /= localCount;
   }
   
   writeToMap(slic.superpixels().get(i), saliencyMap, global + regional + local);
   }
   
   saliencyMap.toRange(new Range(0, 255));
   return saliencyMap;*/
}

IntensityImg regionalContrast(SLIC slic, SegmentationFeatures sf, String featureName) {
  IntensityImg saliencyMap = new IntensityImg(slic.labimg().width(), slic.labimg().height());
  float maxSpatialDistance = sqrt((slic.labimg().width() / 2) * (slic.labimg().width() / 2) + (slic.labimg().height() / 2) * (slic.labimg().height() / 2));
  float maxLocalDistance = maxSpatialDistance / 4;



  for (int i = 0; i < slic.superpixels().size(); i++) {

    float distance = 0;

    float regionalSum = 0;

    for (int j = 0; j < slic.superpixels().size(); j++) {

      float spatialDistance = Coord.euclideanDistanceFct().distance(slic.superpixels().get(i).center(), slic.superpixels().get(j).center());

      if (i == j || !(spatialDistance < maxLocalDistance)) {
        continue;
      }

      // float spatialWeight = sin(map(spatialDistance, 0, maxLocalDistance, PI/2, PI));
      float spatialWeight = (float) FilterFactory.gaussFunction1D(spatialDistance/maxLocalDistance, 0.25);
      float sizeWeight = slic.superpixels().get(j).size();
      distance += sf.distance(slic.superpixels().get(i), slic.superpixels().get(j), featureName) * spatialWeight * sizeWeight;
      regionalSum += spatialWeight;
    }

    if (regionalSum != 0) {
      distance  /= regionalSum;
    }

    writeToMap(slic.superpixels().get(i), saliencyMap, distance);
  }

  saliencyMap.toRange(new Range(0, 255));
  return saliencyMap;
}