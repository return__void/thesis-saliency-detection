class Saliency implements ImageUpdateObserver {

  SLIC slic = null;

  boolean drawHull = false;
  boolean drawSizes = false;
  boolean drawEdge = false;

  FeatureExtractorFct<Gradient, Segment, Histogram> edgeFeatureFct = Feature.edgeMagnitudeFeatureFct();
  FeatureExtractorFct<LabImg, Superpixel, LabColor> colorFeatureFct = Feature.colorFeatureFct();
  FeatureExtractorFct<Gradient, Segment, Histogram> hogFeatureFct = Feature.HOGFeatureFct();
  FeatureExtractorFct<LabImg, Segment, Histogram[]> textureFeatureFct = Feature.textureFeatureFct(17, 5);
  FeatureExtractorFct<RGBYImg, Segment, RGBYColor> rgbyColorFeatureFct = Feature.rgbyColorFeatureFct();

  DistanceFct<RGBYColor> rgbyDis = RGBYColor.euclideanDistanceFct();
  DistanceFct<Histogram> hisDis = Histogram.chiSquaredDistanceFct();
  DistanceFct<LabColor> colDis = LabDistanceFactory.euclideanDistanceFct();
  //DistanceFct<XY> spatialEuclidean = Coord.euclideanDistanceFct();
  DistanceFct<Histogram[]> texDis = Histogram.arrayDistanceFct("sum", Histogram.euclideanDistanceFct());


  String[] featureNames = {
    "color", 
    "rgby", 
    "edge", 
    "HOG", 
    "texture"
  };

  String[] methodNames = {
    "global-contrast", 
    "regional-contrast", 
    "local-contrast", 
    "gauss-weighted-contrast", 
    "linear-weighted-contrast", 
    "distribution-contrast", 
    "distribution-weighted-contrast"
  };

  ArrayList<String> mapNames = createMapNames(featureNames, methodNames);


  int saliencyMapDrawMode = 0;//mapNames.length;

  PImage segmentation = null;
  HashMap<String, IntensityImg> maps = new HashMap<String, IntensityImg>();

  SegmentationFeatures sf;

  int K = 8000;

  LabColor mean = null;

  PApplet parent;

  Saliency(PApplet parent, int K) {
    this.K = K;
    this.parent = parent;
    createMapNames(featureNames, methodNames);
  }

  void analyse() {
    segmentation = null;
    maps.clear();

    mean = LabXY.meanFct().mean(slic.centers()).lab();

    sf = new SegmentationFeatures();

    //LabImg rgby = new LabImg((new RGBYImg(slic.labimg())).img());

    Gradient gradient = new Gradient(slic.labimg());
    RGBYImg rgbyImg = new RGBYImg(slic.labimg());
    LabImg labImg = slic.labimg();


    for (int i = 0; i < slic.superpixels().size(); i++) {
      Histogram edge = edgeFeatureFct.extract(gradient, slic.superpixels().get(i));
      LabColor col = colorFeatureFct.extract(labImg, slic.superpixels().get(i));
      Histogram hog = hogFeatureFct.extract(gradient, slic.superpixels().get(i));
      Histogram[] tex = textureFeatureFct.extract(labImg, slic.superpixels().get(i));
      RGBYColor rgby = rgbyColorFeatureFct.extract(rgbyImg, slic.superpixels().get(i));


      sf.add(slic.superpixels().get(i), featureNames[2], Feature.create(edge, hisDis));
      sf.add(slic.superpixels().get(i), featureNames[0], Feature.create(col, colDis));
      sf.add(slic.superpixels().get(i), featureNames[3], Feature.create(hog, hisDis));
      sf.add(slic.superpixels().get(i), featureNames[4], Feature.create(tex, texDis));
      sf.add(slic.superpixels().get(i), featureNames[1], Feature.create(rgby, rgbyDis));
    }
  }

  void setImage(PImage img) {
    long start = System.currentTimeMillis();

    slic = new SLIC(img, K);

    long end = System.currentTimeMillis();
    println("execution time: " + (end - start) / 1000 + "s");

    analyse();
  }

  void merge() {
    final float S = (float) Math.sqrt(slic.labimg().size() / (float) K);

    println("start merging with " + slic.K() + " segments");
    DistanceFct<LabXY> fct = new DistanceFct<LabXY>() {
      @Override
        public float distance(LabXY a, LabXY b) {

        DistanceFct<LabColor> colFct = LabDistanceFactory.euclideanDistanceFct();
        DistanceFct<XY> spatialFct = Coord.euclideanDistanceFct();


        return colFct.distance(a.lab(), b.lab()) + 10 / S * spatialFct.distance(a, b);
      }
    };
    float d = slic.meanNeighborSegmentDistance(fct);
    // println("mean: " + d);
    slic = slic.mergeSegments(fct, d * 0.66);

    println("number of segments after merging: " + slic.K());

    analyse();
  }


  public ArrayList<String> createMapNames(String[] featureNames, String[] methodNames) {
    ArrayList<String> mapNames = new ArrayList<String>(featureNames.length * methodNames.length);

    for (String feature : featureNames) {
      for (String method : methodNames) {
        String mapName = feature + ": " + method;
        mapNames.add(mapName);
      }
    }
    return mapNames;
  }

  void keyPressed() {
    if (key == 'h') {
      drawHull = !drawHull;
    } else if (key == 's') {
      drawSizes = !drawSizes;
    } else if (key == 'e') {
      drawEdge = !drawEdge;
    } else if (key == '0') {
      saliencyMapDrawMode = mapNames.size();
    } else if (key == '+') {
      saliencyMapDrawMode = ++saliencyMapDrawMode > mapNames.size() ? 0 : saliencyMapDrawMode;
    } else if (key == '-') {
      saliencyMapDrawMode = --saliencyMapDrawMode >= 0 ? saliencyMapDrawMode : mapNames.size();
    } else if (key == 'm') {
      merge();
    } else if (key == 'r') {
      println("saving all created saliency maps");

      //HashMap<String, PImage> maps = new HashMap<String, PImage>();

      for (Map.Entry<String, IntensityImg> entry : maps.entrySet()) {

        String filename = entry.getKey().replaceAll(": ", "-");

        PImage img = entry.getValue().img();
        img.parent = parent;
        img.save("export/" + StringTools.timestamp() + "_" + filename + ".png");
      }
    }
  }
  
  
  IntensityImg getMap(String name) {
 
    if (!maps.containsKey(name)) {

        println("calculating saliency map for " + name + " method. Please wait");
        IntensityImg container = computeSaliency(slic, sf, name);

        maps.put(name, container);
      }
      
      return maps.get(name);
  }

  void draw() {

    if (slic == null) {
      return;
    }

    float maxW = width / 2.0;
    float maxH = height;

    float scale = slic.labimg().width() > slic.labimg().height() ? maxW / slic.labimg().width() : maxH / slic.labimg().height();

    if (segmentation == null) {
      PImage container = createImage(slic.labimg().width(), slic.labimg().height(), RGB);
      LabImg labImg = new LabImg(container);
      SLIC.writeToImage(slic.superpixels(), labImg);
      segmentation = labImg.img();
    }

    image(slic.labimg().img(), width / 2, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);
    image(segmentation, 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);


    if (saliencyMapDrawMode < mapNames.size()) {
      String name = mapNames.get(saliencyMapDrawMode);
      

      PImage saliencyMap = getMap(name).img();
      image(saliencyMap, 0, 0, slic.labimg().width() * scale, slic.labimg().height() * scale);

      String text = "Saliency Map: " + name;
      fill(247);
      noStroke();
      textSize(16);
      rect(0, height - 2 * 16, textWidth(text) + 2 * 16, 2 * 16);

      fill(0);
      text(text, 16, height - 10);
    }

    if (drawHull) {
      slic.drawHull(parent.g, 0, 0, slic.labimg().width() * scale);
    }
    if (drawSizes) {
      slic.drawSuperpixelSize(parent.g, 0, 0, slic.labimg().width() * scale);
    }
    if (drawEdge) {
      tint(255, 150);
      slic.drawEdge(parent, 0, 0, slic.labimg().width() * scale);
      noTint();
    }
  }
}