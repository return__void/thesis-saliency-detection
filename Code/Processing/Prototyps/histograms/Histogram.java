import net.returnvoid.imagefeatures.*;
import java.util.ArrayList;
import processing.core.PGraphics;
import net.returnvoid.functions.*;

public class Histogram extends Range {
  private ArrayList<Integer> binned;
  private int elementsAdded = 0;
  private static Distance<Histogram> chiSquaredDistance = null;
  private static Distance<Histogram> euclideanDistance = null;
  private static Distance<Histogram> crossBinDistance = null;
  private static Distance<Histogram> meanVarianceDistance = null;

  final public static String TYPE_ORDINAL = new String("ordinal");
  final public static String TYPE_NOMINAL = new String("nominal");
  final public static String TYPE_MODULO = new String("modulo");

  public String type = Histogram.TYPE_ORDINAL;

  public Histogram(float min, float max, int bins) {
    super(min, max, 1);
    this.binned = new ArrayList<Integer>(bins);

    for (int i = 0; i < bins; i++) {
      binned.add(0);
    }
  }

  public static Distance<Histogram> meanVarianceDistance() {
    if (Histogram.meanVarianceDistance == null) {
      Histogram.meanVarianceDistance = new Distance<Histogram>() {
        public float distance(Histogram a, Histogram b) {

          float aMean = a.mean();
          float bMean = b.mean();
          double meanMax = Math.max(aMean, bMean);
          if (meanMax == 0) {
            meanMax = 1;
          }

          float aVar= a.variance();
          float bVar = b.variance();
          double varMax = Math.max(aVar, bVar);
          if (varMax == 0) {
            varMax = 1;
          }

          float meanDiff = aMean - bMean;
          float varDiff = aVar - bVar;

          return (float) Math.sqrt(meanDiff * meanDiff / (meanMax * meanMax) + varDiff * varDiff / (varMax * varMax));
        }
      };
    }

    return Histogram.meanVarianceDistance;
  }

  public static Distance<Histogram> crossBinDistance() {
    if (Histogram.crossBinDistance == null) {
      Histogram.crossBinDistance = new Distance<Histogram>() {
        public float distance(Histogram a, Histogram b) {
          float sum = 0;

          for (int i = 0; i < a.binned.size(); i++) {

            int start = i - a.binned.size() / 4;
            int end = i + a.binned.size() / 4;

            if (a.type != Histogram.TYPE_MODULO && start < 0) {
              start = 0;
            }

            if (a.type != Histogram.TYPE_MODULO && end >= a.binned.size() - 1) {
              end = a.binned.size() - 1;
            }

            for (int k = start; k < end; k++) {

              int j = k % a.binned.size();
              float binDiff = a.normBin(i) - b.normBin(j);
              float weight;
              if (a.type == Histogram.TYPE_MODULO) {
                if (j - 1 <= a.binned.size() / 2) {
                  weight = 1 - (j > i ? (j - i) : (i - j)) / (a.binned.size() / 4 + 1f);
                } else {
                  weight = (j > i ? (j - i) : (i - j)) / (a.binned.size() / 4 + 1f);
                }
              } else {
                weight = 1 - (j > i ? (j - i) : (i - j)) / (a.binned.size() / 4 + 1f);
              }


              //System.out.println(i + "/" + j + "[" + k + "], " + weight);


              float weightedDiff = weight * binDiff * binDiff;
              sum += weightedDiff;
            }
          }


          return sum;
        }
      };
    }
    return Histogram.crossBinDistance;
  }

  public float meanIndex() {
    float mean = 0;

    for (int i = 0; i < binned.size(); i++) {

      float weight = (float) binned.get(i) / elementsAdded;
      mean += (i + 1) * weight;
    }
    mean--;

    return mean;
  }

  public float mean() {

    float steps = (max - min) / binned.size();
    return min + meanIndex() * steps + 0.5f * steps;
  }

  public float variance() {
    //System.out.println();

    float sum = 0;
    float steps = (max - min) / binned.size();
    float meanValue = mean();

    for (int i = 0; i < binned.size(); i++) {
      float center = min + i * steps + 0.5f * steps;
      float weight = (float) binned.get(i) / elementsAdded;

      float diff = (center - meanValue);

      //System.out.println(meanValue + ", " + center + ", " + diff);

      sum += diff * diff * weight;
    }

    //System.out.println("=" + sum);

    return (float) Math.sqrt(sum);
  }

  public static Distance<Histogram> euclideanDistance() {
    if (Histogram.euclideanDistance == null) {
      Histogram.euclideanDistance = new Distance<Histogram>() {
        public float distance(Histogram a, Histogram b) {
          float sum = 0;

          for (int i = 0; i < a.binned.size(); i++) {
            float binDiff = a.normBin(i) - b.normBin(i);
            sum += (binDiff * binDiff);
          }

          return (float) Math.sqrt(sum);
        }
      };
    }
    return Histogram.euclideanDistance;
  }

  public static Distance<Histogram> chiSquaredDistance() {
    if (Histogram.chiSquaredDistance == null) {
      Histogram.chiSquaredDistance = new Distance<Histogram>() {
        public float distance(Histogram a, Histogram b) {
          float sum = 0;

          for (int i = 0; i < a.binned.size(); i++) {
            float binDiff = a.normBin(i) - b.normBin(i);
            float binSum = a.normBin(i) + b.normBin(i);
            sum += binSum == 0 ? 0 : ((binDiff * binDiff) / binSum);
          }

          return 0.5f * sum;
        }
      };
    }

    return Histogram.chiSquaredDistance;
  }

  public static Histogram mean(Histogram[] grams) {
    Histogram his = new Histogram(grams[0].min(), grams[0].max(), grams[0].bins());

    for (int i = 0; i < grams.length; i++) {
      for (int j = 0; j < grams[i].binned.size(); j++) {
        his.binned.set(j, his.binned.get(j) + grams[i].binned.get(j));
        his.elementsAdded++;
      }
    }

    return his;
  }

  public int elements() {
    return elementsAdded;
  }

  public String stats() {

    String txt = "";

    float steps = (max - min) / binned.size();
    for (int i = 0; i < binned.size(); i++) {
      float start = min + i * steps;

      txt += "[" + start + ", " + (start + steps) + ") " + binned.get(i) + "x \n";
    }
    txt += "Mean: " + mean();

    return txt;
  }

  public float normBin(int i) {

    if (i < 0) {
      i = 0;
    } else if (i >= binned.size()) {
      i = binned.size() - 1;
    }

    return binned.get(i) / (float) elementsAdded;
  }

  public float distance(Histogram other, Distance<Histogram> fct) {

    if (!comparable(other)) {
      return -1;
    }

    return fct.distance(this, other);
  }

  public float distance(Histogram other) {

    return distance(other, Histogram.chiSquaredDistance());
  }

  public boolean comparable(Histogram other) {
    return binned.size() == other.binned.size();
  }

  public int bins() {
    return binned.size();
  }



  public void add(float val) {
    elementsAdded++;
    int index = map(val);
    this.binned.set(index, this.binned.get(index) + 1);
  }

  public int map(float val) {


    float x = (val - min) / (max - min);

    int bin = (int) (x * bins());

    if (bin < 0) {
      bin = 0;
    } else if (bin >= bins()) {
      bin = bins() - 1;
    }


    return bin;
  }

  public void draw(PGraphics g, float x, float y, float w, float h) {
    float steps = w / bins();

    for (int i = 0; i < binned.size(); i++) {
      float xx = x + i * steps;
      float hh = normBin(i) * h;
      float yy = y + h - hh;
      float ww = steps;
      int percentage = (int) (normBin(i) * 100);
      int decimals = (int) ((normBin(i) * 100 - percentage) * 100);
      String txt = percentage + "." + decimals + "%";
      txt += "\n[ " + binned.get(i) + " ]";

      g.rect(xx, yy, ww, hh);
      g.text(txt, xx + steps / 2, yy - 10);
    }
  }
}