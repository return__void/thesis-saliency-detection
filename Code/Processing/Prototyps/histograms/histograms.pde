Histogram h;
Histogram p;
Histogram q;
Histogram r;

void setup() {
  size(1280, 720);

  h = new Histogram(0, 200, 15);
  p = new Histogram(0, 200, 15);
  q = new Histogram(0, 200, 15);
  r = new Histogram(0, 200, 15);

  for (int i = 0; i < 1000; i++) {
    float val1 = 100 + randomGaussian() * 45;
    float val2 = 100 + randomGaussian() * 45;
   
    float val = abs(val1-100) < abs(val2-100) ? val1 : val2;

    h.add(val);
  }

  for (int i = 0; i < 2000; i++) {
    float val = random(200);

    p.add(val);
  }
  
  for (int i = 0; i < 1000; i++) {
    float val1 = random(200);
    float val2 = random(200);
    float val = val1 > val2 ? val1 : val2;

    q.add(val);
  }
  
  for (int i = 0; i < 1000; i++) {
    float val1 = random(200);
    float val2 = random(200);
    float val3 = random(200);
    float val = val1 > val2 ? val1 : val2;
    
    val = val > val3 ? val : val3;
    
    //val = random(200);

    r.add(val);
  }
}

void draw() {
  background(247);

  fill(0);
  stroke(255);
  strokeWeight(3);
  
  textAlign(CENTER, BOTTOM);
  h.draw(g, 10, 10, width / 2 - 20, height / 2 - 20);
  p.draw(g, width/2 + 10, 10, width / 2 - 20, height / 2 - 20);
  q.draw(g, 10, height/2 + 10, width / 2 - 20, height / 2 - 20);
  r.draw(g, width/2 + 10, height/2 + 10, width / 2 - 20, height / 2 - 20);
  
  textAlign(LEFT, TOP);
  
  String txtH = "H: \n" + "mean index: " + nfs(h.meanIndex(), 1, 2) + ", mean: " +  h.mean() + "+-" + h.variance() + "\n";
  txtH += "Euclidean distance: d(H, P)=" + nfs(h.distance(p, Histogram.euclideanDistance()), 1, 2) + " d(H, Q)=" + nfs(h.distance(q, Histogram.euclideanDistance()), 1, 2) +  " d(H, R)=" + nfs(h.distance(r, Histogram.euclideanDistance()), 1, 2) + "\n";
  txtH += "Chi-Squared distance: d(H, P)=" + nfs(h.distance(p, Histogram.chiSquaredDistance()), 1, 2) + " d(H, Q)=" + nfs(h.distance(q, Histogram.chiSquaredDistance()), 1, 2) +  " d(H, R)=" + nfs(h.distance(r, Histogram.chiSquaredDistance()), 1, 2) + "\n";
  txtH += "Cross-Bin-distance: d(H, P)=" + nfs(h.distance(p, Histogram.crossBinDistance()), 1, 2) + " d(H, Q)=" + nfs(h.distance(q, Histogram.crossBinDistance()), 1, 2) +  " d(H, R)=" + nfs(h.distance(r, Histogram.crossBinDistance()), 1, 2) + "\n";
  txtH += "Mean-Variance-distance: d(H, P)=" + nfs(h.distance(p, Histogram.meanVarianceDistance()), 1, 2) + " d(H, Q)=" + nfs(h.distance(q, Histogram.meanVarianceDistance()), 1, 2) +  " d(H, R)=" + nfs(h.distance(r, Histogram.meanVarianceDistance()), 1, 2);
  
  String txtP = "P: \n" + "mean index: " + nfs(p.meanIndex(), 1, 2) + ", mean: " +  p.mean() + "+-" + p.variance() + "\n";
  txtP += "Euclidean distance: d(P, H)=" + nfs(p.distance(h, Histogram.euclideanDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.euclideanDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.euclideanDistance()), 1, 2) + "\n";
  txtP += "Chi-Squared distance: d(P, H)=" + nfs(p.distance(h, Histogram.chiSquaredDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.chiSquaredDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.chiSquaredDistance()), 1, 2) + "\n";
  txtP += "Cross-Bin-distance: d(P, H)=" + nfs(p.distance(h, Histogram.crossBinDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.crossBinDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.crossBinDistance()), 1, 2) + "\n";
  txtP += "Mean-Variance-distance: d(P, H)=" + nfs(p.distance(h, Histogram.meanVarianceDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.meanVarianceDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.meanVarianceDistance()), 1, 2);
  
  String txtQ = "Q: \n" + "mean index: " + nfs(q.meanIndex(), 1, 2) + ", mean: " +  q.mean() + "+-" + q.variance() + "\n";
  txtQ += "Euclidean distance: d(Q, H)=" + nfs(q.distance(h, Histogram.euclideanDistance()), 1, 2) + " d(Q, P)=" + nfs(q.distance(p, Histogram.euclideanDistance()), 1, 2) +  " d(Q, R)=" + nfs(q.distance(r, Histogram.euclideanDistance()), 1, 2) + "\n";
  txtQ += "Chi-Squared distance: d(Q, H)=" + nfs(q.distance(h, Histogram.chiSquaredDistance()), 1, 2) + " d(Q, P)=" + nfs(q.distance(p, Histogram.chiSquaredDistance()), 1, 2) +  " d(Q, R)=" + nfs(q.distance(r, Histogram.chiSquaredDistance()), 1, 2) + "\n";
  txtQ += "Cross-Bin-distance: d(Q, H)=" + nfs(q.distance(h, Histogram.crossBinDistance()), 1, 2) + " d(Q, P)=" + nfs(q.distance(p, Histogram.crossBinDistance()), 1, 2) +  " d(Q, R)=" + nfs(q.distance(r, Histogram.crossBinDistance()), 1, 2) + "\n";
  txtQ += "Mean-Variance-distance: d(P, H)=" + nfs(p.distance(h, Histogram.meanVarianceDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.meanVarianceDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.meanVarianceDistance()), 1, 2);
  
  String txtR = "R: \n" + "mean index: " + nfs(r.meanIndex(), 1, 2) + ", mean: " +  r.mean() + "+-" + r.variance() + "\n";
  txtR += "Euclidean distance: d(R, H)=" + nfs(r.distance(h, Histogram.euclideanDistance()), 1, 2) + " d(R, P)=" + nfs(r.distance(p, Histogram.euclideanDistance()), 1, 2) +  " d(R, Q)=" + nfs(r.distance(q, Histogram.euclideanDistance()), 1, 2) + "\n";
  txtR += "Chi-Squared distance: d(R, H)=" + nfs(r.distance(h, Histogram.chiSquaredDistance()), 1, 2) + " d(R, P)=" + nfs(r.distance(p, Histogram.chiSquaredDistance()), 1, 2) +  " d(R, Q)=" + nfs(r.distance(q, Histogram.chiSquaredDistance()), 1, 2) + "\n";
  txtR += "Cross-Bin-distance: d(R, H)=" + nfs(r.distance(h, Histogram.crossBinDistance()), 1, 2) + " d(R, P)=" + nfs(r.distance(p, Histogram.crossBinDistance()), 1, 2) +  " d(R, Q)=" + nfs(r.distance(q, Histogram.crossBinDistance()), 1, 2) + "\n";
  txtR += "Mean-Variance-distance: d(P, H)=" + nfs(p.distance(h, Histogram.meanVarianceDistance()), 1, 2) + " d(P, Q)=" + nfs(p.distance(q, Histogram.meanVarianceDistance()), 1, 2) +  " d(P, R)=" + nfs(p.distance(r, Histogram.meanVarianceDistance()), 1, 2);
 
  
  text(txtH, 10, 10); 
  text(txtP, width/2 + 10, 10); 
  text(txtQ, 10, height/2 + 10); 
  text(txtR, width/2 + 10, height/2 + 10); 
}