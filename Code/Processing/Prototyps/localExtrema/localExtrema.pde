LabImg input;
boolean parial = false;

void setup() {
  size(1920, 720);
  //size(1280, 500);

  IntensityImg.threadMultiplier = 100;
  File directory = new File(ProcessingTools.getSketchPath(this) + "/data");

  String directoreyPath;
  if (directory.exists()) {
    directoreyPath = "data";
  } else {
    directoreyPath = "data-public";
  }

  PImage raw = loadImage(directoreyPath + "/" + "P1060266.jpg");
  raw.resize(width / 2, 0);

  input = new LabImg(raw);

  noLoop();
}

void draw() {
  background(247);
  image(input.img(), 0, 0, width / 2, input.height() * (width / 2f) / input.width());

  if (parial) {
    IntensityImg img = new IntensityImg(input);
    PImage intensityImg = img.img();

    double[][] deriativeFilterX = FilterFactory.sobelX();
    double[][] deriativeFilterY = FilterFactory.sobelY();

    IntensityImg xx = new IntensityImg(intensityImg.copy());
    IntensityImg yy = new IntensityImg(intensityImg.copy());
    IntensityImg xy = new IntensityImg(intensityImg.copy());
    xx.filter(deriativeFilterX);
    xx.filter(deriativeFilterX);
    yy.filter(deriativeFilterY);
    yy.filter(deriativeFilterY);
    xy.filter(deriativeFilterX);
    xy.filter(deriativeFilterY);
    xy.mult(xy);
    IntensityImg extrema = new IntensityImg(xx.width(), xx.height());
    extrema.add(xx);
    extrema.mult(yy);
    extrema.sub(xy);

    for (int i = 0; i < extrema.height(); i++) {
      for (int j = 0; j < extrema.width(); j++) {

        if (extrema.get(j, i) > 0 && xx.get(j, i) < 0) {
          extrema.set(j, i, 255);
        } else {
          extrema.set(j, i, 0);
        }
      }
    }

    image(extrema.img(), width / 2, 0, width / 2, extrema.height() * (width / 2f) / extrema.width());
  } else {

    IntensityImg img = new IntensityImg(input);
    PImage intensityImg = img.img();

    double[][] deriativeFilterX = FilterFactory.sobelX();
    double[][] deriativeFilterY = FilterFactory.sobelY();

    IntensityImg x = new IntensityImg(intensityImg.copy());
    IntensityImg y = new IntensityImg(intensityImg.copy());
    x.filter(deriativeFilterX);
    y.filter(deriativeFilterY);

    IntensityImg firstDerivative = new IntensityImg(x.width(), x.height());
    IntensityImg secondDerivative = new IntensityImg(x.width(), x.height());

    firstDerivative.add(x);
    firstDerivative.mult(y);

    x = firstDerivative.copy();
    y = firstDerivative.copy();
    x.filter(deriativeFilterX);
    y.filter(deriativeFilterY);

    secondDerivative.add(x);
    secondDerivative.mult(y);

    noFill();
    stroke(255);
    for (int i = 0; i < firstDerivative.height(); i++) {
      for (int j = 0; j < firstDerivative.width(); j++) {
        //print(second.get(j, i) + ", ");


        if (firstDerivative.get(j, i) == 0 && secondDerivative.get(j, i) < 0) {
          firstDerivative.set(j, i, 255);
        } else {
          firstDerivative.set(j, i, 0);
        }
      }
    }

    image(firstDerivative.img(), width / 2, 0, width / 2, firstDerivative.height() * (width / 2f) / firstDerivative.width());
  }
}

void mousePressed() {
  parial = !parial;

  redraw();
}

void keyPressed() {
  if (key == 'f') {
    saveFrame("export/" + StringTools.timestamp() + ".png");
  }
}