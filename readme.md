﻿# Make computers see: The evolution of Computer Vision on the example of saliency detection in the last 20 years

Bachelorarbeit im Rahmen des Studiengangs Informationsmanagement und Informationstechnologie der Stiftung Universität Hildesheim. Vorgelegt von Diana Lange. Hildesheim, 18. September 2018.

This readme is a quick start up and overview about the practical part of the thesis. If you want to learn more about the work, you can read the whole thesis at: http://diana-lange.de/storage/thesis/lange_diana_thesis.pdf

## Abstract

The capability for selective attention is a key competence that all animals inherit. It developed in the evolution because a fast reaction to the environment was needed and a complete processing of all sensory input in real-time wasn’t possible. The same limitations apply to computer vision systems where a fast determination of important and unimportant information is required. Thus this work will examine methods of salient regions detection, that is regions in images people intuitively point their focus of attention to. Such regions are thought to contain the most valuable information in the visual input facilitating scene understanding. In this work fundamental techniques and paradigm shifts are examined that developed in the past 20 years in saliency detection. Three major waves are discovered across saliency detection publications: The first wave concerns local contrast derived from low-level features in a bottom-up manner. The second wave investigates global contrast, prior knowledge and image abstraction to capture redundancies. The third wave utilizes learning mechanisms for saliency estimation. From each wave at least one publication is chosen and presented. In addition, two saliency detection approaches are implemented in Java programming language and put in use on one example application. 

## The coding challenge

The goal of the coding challenge is to write an application that produces a saliency map from a given image. The final algorithm will be based on one or more scientific papers from the field of computional visual attention systems.

The application will be able to solve a subset of following tasks:

 1. Image pre-processing: Many existing computional visual attention systems decrease the computational cost by performing an image segmentation prior to the actual saliency detection. In particular the SLIC superpixel segmentation has been shown to be useful since the resultings segments maintain the borders of objects in the images, all segments have roughly the same size and the number of segments can be set. Paper: [SLIC superpixels](https://infoscience.epfl.ch/record/149300/files/SLIC_Superpixels_TR_2.pdf)
 2. Image feature extraction: A set of useful image information will be extracted from the image in regard to the prior segmentation. These features might be: color, edge strength, edge orientation and others. These features will serve as input for the calculation of the saliency score for each segment.
 3. Feature distance measures: The saliency score of a segment is often defined by the 'rarity' of it. The rarity is nothing else than a distance measure that describes how non-similar the segment is compared to other segments in the image. Or in other words: The saliency score equals to a linear or non-linear combination of the distances of the features of a segment to the features of other segments. Therefor for each feature a distance measure and a suitable structure for storing the feature information is needed.
 4. Saliency Score and saliency map: The saliency score can be estimated via miscellaneous ways, for example: (a) Local contrast: The distance of a segment to its surrounding segments (b) Regional contrast: The distance of a segment to all segments in a defined neighborhood (c) Global contrast: The distance of a segment to all other segments. The saliency score of a segment is visualized in a saliency map which is a grayscale image where dark areas represent non-salient regions of the image and light areas highly salient regions.
 5. Binarization: To detect one or more salient objects in the saliency map a post-processing step is performed that transforms the greyscale saliency map to a binary (black or white) mask where the salient objects are represented with white areas.

## Implementation overview

Distances along other elements are modelled as functional interfaces. This allows to pass these functions as parameters to other functions and effects of concrete implementation details can be tested more easily.

![enter image description here](https://thesis.diana-lange.de/images/uml/classGraph-functions.jpg)

The superpixel segementation is based on distances of colors in CieLab space and spatial distances. The LabImg class convert the RGB colors of the input image to instances of LabColor or to instances of LabXY (color + location of the color in the image). 
The SLIC class is responsible for the segmentation process which results in a set of Superpixel segments. Each Superpixel contains a set of LabXY elements which describe the colors and the boundaries of the segment.

![enter image description here](https://thesis.diana-lange.de/images/uml/classGraph-segmentation.jpg)

The features of a segment, e.g. a histogram of edge strengths or the average color, are stored as instances of the generic typed class "Feature". The collection of all extracted features of a segmentation is then stored in an instance of "SegmentationFeatures".

![enter image description here](https://thesis.diana-lange.de/images/uml/classGraph-features.jpg)

Image information can be stored with various classes depending on the task. For example LabImg is there to transform RGB values to CieLab colors (and store these CieLab colors for each pixel). IntensityImg instances are basically the same as matrices for storing floating point numbers and matrix operations like addition, scalar multiplication and so on. But they can be also interpreted visually as grayscale images.

![enter image description here](https://thesis.diana-lange.de/images/uml/classGraph-image.jpg)

## Implementation

This chapter shows what has been already implemented. The implementations are done in Java using following libraries & frameworks:

 - Processing mainly for visualization and prototyping
 - Return Void mainly for colors and color space transformation

### SLIC superpixel Segmentation

![enter image description here](https://thesis.diana-lange.de/images/segmentation/20180623_183033.jpg)

### Segment merging

Neighboring segments are iteratively merged if the colors are similar enough. The number of segments is effectivly reduced while keeping the stucture of the image.

![enter image description here](https://thesis.diana-lange.de/images/segmentation/20180624_163651.png)

![enter image description here](https://thesis.diana-lange.de/images/segmentation/20180624_163750.png)

The result of a segmentation and a couple of merging iterations can be seen here. The left image shows the segementation which consists of approx 800 segments and the right images shows the input image which consists of approx. 0.5MP
![enter image description here](https://thesis.diana-lange.de/images/segmentation/20180624_163844.png)

### Saliency Scores Using Segmentation

Implementation based on paper: https://ieeexplore.ieee.org/abstract/document/6247743/

Following features are extracted:

 - Color: Mean color of a segment in CieLab color space
 - Edge: A histogram of the edge strengths
 - RGBY/Intensity: Mean color of a segment in RGBY/Intensity color space (color space seperates color from intensity)
 - Orientation: A histogram of edge orientations
 - Texture: A histogram of texture (responses to gabor filters at various angles)

Shown next are all intermediate maps, feature maps (combinations of intermediate maps for each feature) and the two variations of the final saliency map.

*Color Contrast*

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-1.jpg)

*RGBY/Intensity Contrast*

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-2.jpg)

*Edge Strength Contrast*

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-3.jpg)

*Edge Orientation Contrast*

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-4.jpg)

*Texture Contrast*

![](https://thesis.diana-lange.de/images/saliency/combo-5.jpg)

*Feature Maps*

![](https://thesis.diana-lange.de/images/saliency/combo-6.jpg)

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-7.jpg)

*Saliency Maps*

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-8.jpg)

*Saliency scores for non uniform segements*

The complexity for saliency estimation could be reduced ny merging segments of the over-segmentation result of SLIC. After a merging process the segements are no longer equal in size. Therefore the distances of segments are weighted by the segments size because a high distance to a small segment is less important than a high distance to a big segment.

![enter image description here](https://thesis.diana-lange.de/images/saliency/20180626_192856.jpg)

### Saliency Scores Using Gaussian Pyramids

Implementation based on paper: https://ieeexplore.ieee.org/abstract/document/730558/

Following features are extracted:

 - Color in RGBY color space 
 - Intensity 
 - Orientation using gabor filters

Feature maps a created via pixelwise distances in image pyramids. The saliency map is created from linear combinations of the feature maps.

![enter image description here](https://thesis.diana-lange.de/images/saliency/combo-9.jpg)

### Filters

Filters are modelled as two dimensional double arrays. Any two dimensional filter can be applied to instances of LabImg, but the actual convolution of the filter is done with RGB values since it is easier to keep the resulting pixel values in a valid range.

![enter image description here](https://thesis.diana-lange.de/images/misc/20180630_154013-Kopie.jpg)

### Pyramid

Some saliency detection algorithms are based on gaussian pyramids where an image is iteratively filtered with a gaussian and sub-sampled.

![enter image description here](https://thesis.diana-lange.de/images/misc/20180630_130137-0-Kopie.jpg)

### Edges + Edge orientation

Based on a gaussian pyramid, the edges (edge strengths) and the edge orientations are detected via gradient calculation. A directed gradient vector is calculated in CieLab color space (L, a and b parameters), but the direction of the vector determined solely on the luminance. The edge strength equals to the magnitude and the edge orientation to the angle of the gradient vector. Please note that edges with small magnitude are not displayed in the examples below.

Edge strengths and orientations might serve as a feature for saliency detection. But the calculation of the gradients for all scales is computational quite costly.

![enter image description here](https://thesis.diana-lange.de/images/misc/zusammenstellung.jpg)

![enter image description here](https://thesis.diana-lange.de/images/misc/zusammenstellung2.jpg)

![enter image description here](https://thesis.diana-lange.de/images/misc/zusammenstellung3.jpg)

### Scale space

Inspired by SIFT, this experiment tries to find the scale of edges. The map visualizes the detected scales, where white means that the edge had the highest response (biggest magnitude value of gradient) for a fine scale (input image filtered with a small gaussian filter) and black means the edge had the highest response for a coarse scale (input image filtered with a big gaussian filter). Therefor the grey level in the map shows how sharp the detected edges are. 
Since this analysis is based on a gaussian pyramid, the resolution of the various scale (levels of the pyramid) differ from high resolution (small gauss filter) to very low (big gauss filter). The top row shows a map which is normalized to the highest resolution in the pyramid; coarse scales with less resolution therefor appear pixelated in the map. This process is quite slow since many gradients have to be calculated. The bottom row shows a map which is normalized to the lowest resultion / most coarse scale in the pyramid. This process is faster but the map lags details.
The sharpness of edges might server as a depth estimator, since sharp edges are detected in areas where focues of the photograph is. This feature will have less importance for cameras with little depth of field, e.g. smartphones cameras or other cameras with small sensor formats. Additionally, not all salient regions might be in focus.

![enter image description here](https://thesis.diana-lange.de/images/misc/3301.jpg)

## Saliency Maps for Image Styling

![enter image description here](https://thesis.diana-lange.de/images/vis/_20180724_000116.jpg)

![enter image description here](https://thesis.diana-lange.de/images/vis/_20180719_185955.jpg)
